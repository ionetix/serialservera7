/*
 * serial.h
 *
 *  Created on: Jun 9, 2017
 *      Author: nusher
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include "xil_io.h"

typedef struct roReg {		// structure used for read-only channels
	struct roReg* next;	// pointer to next channel in device
	u32 address;		// address in CPU memory map where Modbus channel data is stored
	char* pollcmd;		// string to send to device to initiate read
	char startchar;		// first character sent in device response
	char stopchar;		// final character sent in device response
	u32 startpos;		// position in return string where data starts
	u32 maxchars;		// maximum number of characters to accept from device
	u32 datatype;		// type of data received (raw bits, int, float, etc.)
	u32 checktype;		// type of checksum
	u32 csbegin;		// beginning of characters for checksum
	u32 csend;			// end of characters for checksum
	u32 check;			// checksum value from device
} roReg;

typedef struct rwReg {		// structure used for read/write channels
	struct rwReg* next;	// pointer to next channel in device
	u32 address;		// address in CPU memory map where Modbus channel data is stored
	u32 readback;		// address in CPU memory where readback data is stored
	u32 rbshift;		// amount to right-shift readback register
	u32 rbmask;			// mask to apply to readback register
	u32 resendtype;		// algorithm type to determine whether to retransmit
	char* prefix;		// constant string used at beginning of transmission
	char* postfix;		// constant string used at end of transmission
	u32 datatype;		// type of data being sent (raw bits, int, float, etc.)
	u32 intchars;		// number of characters before decimal point (int and float data types)
	u32 fracchars;		// number of characters after decimal point (float data type)
	u32 checktype;		// type of checksum
	u32 csbegin;		// beginning of characters for checksum
	u32 csend;			// end of characters for checksum
	char startchar;		// first character of response
	char stopchar;		// second character of response
	u32 maxchars;		// maximum length of response
} rwReg;

roReg* newRegRO(u32 address, char* pollcmd, char startchar, char stopchar, u32 startpos, u32 maxchars, u32 datatype, u32 checktype, u32 csbegin, u32 csend, u32 check);
rwReg* newRegRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, char* prefix, char* postfix, u32 datatype, u32 intchars, u32 fracchars, u32 checktype, u32 csbegin, u32 csend, char startchar, char endchar, u32 maxchars);

void configPort(u32 chan, u32 devType);
roReg* initChannelsRO(u32 chan, u32 devType);
rwReg* initChannelsRW(u32 chan, u32 devType);
u32 sendString(u32 channel, char* string);
u32 checkROstr(roReg* RO, char* string);
void processROstr(roReg* RO, char* string);
u32 checkRW(rwReg* RW);
void updateRW(u32 channel, rwReg* RW);
u32 checkResponse(rwReg* RW, char* string);

#endif /* SERIAL_H_ */
