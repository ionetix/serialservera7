/*
 * main.h
 *
 *  Created on: Mar 24, 2015
 *      Author: nusher
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "regMap.h"

void load_settings();
#define CLOCKRATE 50000000

#endif /* MAIN_H_ */
