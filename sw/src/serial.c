/*
 * serial.c
 *
 *  Created on: Jun 9, 2017
 *      Author: nusher
 */

#include "regMap.h"
#include "main.h"
#include "serial.h"
#include "xil_io.h"
#include <string.h>
#include <stdlib.h>

// strings used to communicate with known devices
// statically allocated so that they can be referenced multiple times without wasting memory
char empty[] = "";
char lakeshore0[] = "KRDG? 1\r\n";
char lakeshore1[] = "KRDG? 2\r\n";
char lakeshore2[] = "KRDG? 3\r\n";
char lakeshore3[] = "KRDG? 4\r\n";
char lakeshore4[] = "KRDG? 5\r\n";
char lakeshore5[] = "KRDG? 6\r\n";
char lakeshore6[] = "KRDG? 7\r\n";
char lakeshore7[] = "KRDG? 8\r\n";
char inficon0[] = {3, 16, 62, 0};
char alicat0[] = "A\r";
char alicat1[] = "A";
char alicat2[] = "\r";
char turbo0[] = {0x02, 0x80, '1', '0', '7', '0', 0x03, '8', '5'};		// Read active stop mode setting
char turbo1[] = {0x02, 0x80, '1', '6', '7', '0', 0x03, '8', '3'};		// Read stop speed reading setting
char turbo2[] = {0x02, 0x80, '2', '0', '5', '0', 0x03, '8', '4'};		// Read pump status
char turbo3[] = {0x02, 0x80, '2', '0', '6', '0', 0x03, '8', '7'};		// Read error code
char turbo4[] = {0x02, 0x80, '2', '0', '3', '0', 0x03, '8', '2'};		// Read drive frequency
char turbo5[] = {0x02, 0x80, '2', '2', '6', '0', 0x03, '8', '5'};		// Read rotation frequency
char turbo6[] = {0x02, 0x80, '3', '0', '2', '0', 0x03, '8', '2'};		// Read pump life
char turbo7[] = {0x02, 0x80, '2', '0', '1', '0', 0x03, '8', '0'};		// Read pump voltage
char turbo8[] = {0x02, 0x80, '2', '0', '0', '0', 0x03, '8', '1'};		// Read pump current
char turbo9[] = {0x02, 0x80, '2', '0', '2', '0', 0x03, '8', '3'};		// Read pump power
char turbo10[] = {0x02, 0x80, '2', '0', '4', '0', 0x03, '8', '5'};		// Read pump temperature
char turbo11[] = {0x02, 0x80, '0', '0', '0', '1', '1', 0x03, 'B', '3'};	// Start
char turbo12[] = {0x02, 0x80, '0', '0', '0', '1', '0', 0x03, 'B', '2'};	// Stop
char turbo13[] = {0x02, 0x80, '0', '0', '8', '1', '0', 0x03, 'B', 'A'};	// Set control to RS232
char turbo14[] = {0x02, 0x80, '1', '0', '7', '1', '1', 0x03, 'B', '5'};	// Enable active stop mode
char turbo15[] = {0x02, 0x80, '1', '6', '7', '1', '1', 0x03, 'B', '3'};	// Enable stop speed reading

// Macros to identify different types of registers
// data types
#define NOTYPE		0
#define	RAWBYTE		1
#define RAWREG		2
#define UINT16		3
#define SINT16		4
#define FLOAT		5
// checksum types
#define NOCHECK		0
#define SUM1		1
#define CRCA		2
// resend types
#define ONCEAND0	0
#define ALWAYS		1
#define DIFF		2

extern u32 mhr[2048];
extern u32 mir[2048];

roReg* newRegRO(u32 address, char* pollcmd, char startchar, char stopchar, u32 startpos, u32 maxchars, u32 datatype, u32 checktype, u32 csbegin, u32 csend, u32 check)
{
	roReg* newchan = malloc(sizeof(roReg));
	newchan->next = NULL;
	newchan->address = address;
	newchan->pollcmd = pollcmd;
	newchan->startchar = startchar;
	newchan->stopchar = stopchar;
	newchan->startpos = startpos;
	newchan->maxchars = maxchars;
	newchan->datatype = datatype;
	newchan->checktype = checktype;
	newchan->csbegin = csbegin;
	newchan->csend = csend;
	newchan->check = check;
	return newchan;
}
rwReg* newRegRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, char* prefix, char* postfix, u32 datatype, u32 intchars, u32 fracchars, u32 checktype, u32 csbegin, u32 csend, char startchar, char stopchar, u32 maxchars)
{
	rwReg* newchan = malloc(sizeof(rwReg));
	newchan->next = NULL;
	newchan->address = address;
	newchan->readback = readback;
	newchan->rbshift = rbshift;
	newchan->rbmask = rbmask;
	newchan->resendtype = resendtype;
	newchan->prefix = prefix;
	newchan->postfix = postfix;
	newchan->datatype = datatype;
	newchan->intchars = intchars;
	newchan->fracchars = fracchars;
	newchan->checktype = checktype;
	newchan->csbegin = csbegin;
	newchan->csend = csend;
	newchan->startchar = startchar;
	newchan->stopchar = stopchar;
	newchan->maxchars = maxchars;
	return newchan;
}

void configPort(u32 chan, u32 devType)
{
	if (chan > 15)
		return;
	switch (devType) {
	// Lakeshore 218 Temperature Monitor
	// 7 data bits, odd parity, 9600 baud
	case 1:
		Xil_Out32(PORT0CFG + (chan << 4), 0x00070000 | (CLOCKRATE / 9600));
		break;
	// Inficon BPG400 vacuum gauge
	// 8 data bits, no parity, 9600 baud
	case 2:
		Xil_Out32(PORT0CFG + (chan << 4), 0x00000000 | (CLOCKRATE / 9600));
		break;
	// Alicat Mass Flow Controller
	// 8 data bits, no parity, 9600 baud
	case 3:
		Xil_Out32(PORT0CFG + (chan << 4), 0x00000000 | (CLOCKRATE / 9600));
		break;
	// Agilent Twis Torr 304 FS turbo pump controller
	case 4:
		Xil_Out32(PORT0CFG + (chan << 4), 0x00000000 | (CLOCKRATE / 9600));
		break;
	}
	return;
}

roReg* initChannelsRO(u32 chan, u32 devType)
{
	roReg* firstRO = NULL;
	roReg* regPtr = NULL;
	if (chan > 15)
		return firstRO;
	switch (devType) {
	// Lakeshore 218 Temperature Monitor
	// Configured to read the 8 inputs in K
	case 1:
		firstRO = newRegRO((u32) mir + (REGOFFSET * chan) + (0<<2), lakeshore0, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = firstRO;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (1<<2), lakeshore1, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (2<<2), lakeshore2, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (3<<2), lakeshore3, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (4<<2), lakeshore4, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (5<<2), lakeshore5, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (6<<2), lakeshore6, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (7<<2), lakeshore7, '\0', '\r', 1, 20, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = firstRO;
		break;
	// Inficon BPG400 vacuum gauge
	// Configured to parse streaming data, one modbus input register per packet (even though both registers are in each packet)
	case 2:
		firstRO = newRegRO((u32) mir + (REGOFFSET * chan) + (0<<2), NULL, 7, '\0', 2, 9, RAWREG, SUM1, 1, 7, 8);
		regPtr = firstRO;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (1<<2), NULL, 7, '\0', 4, 9, RAWREG, SUM1, 1, 7, 8);
		regPtr = regPtr->next;
		regPtr->next = NULL;
		break;
	// Alicat Mass Flow Controller
	case 3:
		firstRO = newRegRO((u32) mir + (REGOFFSET * chan) + (0<<2), alicat0, 'A', '\r', 2, 48, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = firstRO;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (1<<2), alicat0, 'A', '\r', 10, 48, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (2<<2), alicat0, 'A', '\r', 18, 48, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (3<<2), alicat0, 'A', '\r', 26, 48, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (4<<2), alicat0, 'A', '\r', 34, 48, FLOAT, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (5<<2), alicat0, 'A', '\r', 41, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (6<<2), alicat0, 'A', '\r', 42, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (7<<2), alicat0, 'A', '\r', 43, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (8<<2), alicat0, 'A', '\r', 44, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (9<<2), alicat0, 'A', '\r', 45, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (10<<2), alicat0, 'A', '\r', 46, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (11<<2), alicat0, 'A', '\r', 47, 48, RAWBYTE, NOCHECK, 0, 0, 0);
		regPtr = regPtr->next;
		regPtr->next = NULL;
		break;
	// Agilent Twis Torr 304 FS turbo pump controller
	case 4:
		firstRO = newRegRO((u32) mir + (REGOFFSET * chan) + (0<<2), turbo0, 0x02, '\0', 6, 10, RAWBYTE, CRCA, 1, 7, 8);
		regPtr = firstRO;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (1<<2), turbo1, 0x02, '\0', 6, 10, RAWBYTE, CRCA, 1, 7, 8);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (2<<2), turbo2, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (3<<2), turbo3, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (4<<2), turbo4, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (5<<2), turbo5, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (6<<2), turbo6, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (7<<2), turbo7, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (8<<2), turbo8, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (9<<2), turbo9, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = newRegRO((u32) mir + (REGOFFSET * chan) + (10<<2), turbo10, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
		regPtr = regPtr->next;
		regPtr->next = NULL;
		break;
	}
	return firstRO;
}

rwReg* initChannelsRW(u32 chan, u32 devType)
{
	rwReg* firstRW = NULL;
	rwReg* regPtr = NULL;
	if (chan > 15)
		return firstRW;
	switch (devType) {
	// Inficon BPG400 vacuum gauge
	// Configured to force the front panel display to match the units set by the PLC
	case 2:
		firstRW = newRegRW((u32) mhr + (REGOFFSET * chan) + (0<<2), (u32) mir + (REGOFFSET * chan) + (0<<2), 12, 0x00000003, DIFF, inficon0, NULL, RAWBYTE, 1, 0, SUM1, 1, 3, '\0', '\0', 0);
		regPtr = firstRW;
		regPtr->next = NULL;
		mhr[chan<<7] = 1;
		break;
	// Alicat Mass Flow Controller
	case 3:
		firstRW = newRegRW((u32) mhr + (REGOFFSET * chan) + (0<<2), NULL, 0, 0, ALWAYS, alicat1, alicat2, UINT16, 0, 0, NOCHECK, 0, 0, '\0', '\0', 0);
		regPtr = firstRW;
		regPtr->next = NULL;
		mhr[chan<<7] = 0;
		break;
	// Agilent Twis Torr 304 FS turbo pump controller
	case 4:
		firstRW = newRegRW((u32) mhr + (REGOFFSET * chan) + (0<<2), NULL, 0, 0, ONCEAND0, turbo11, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
		regPtr = firstRW;
		regPtr->next = newRegRW((u32) mhr + (REGOFFSET * chan) + (1<<2), NULL, 0, 0, ONCEAND0, turbo12, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
		regPtr = regPtr->next;
		regPtr->next = newRegRW((u32) mhr + (REGOFFSET * chan) + (2<<2), NULL, 0, 0, ONCEAND0, turbo13, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
		regPtr = regPtr->next;
		regPtr->next = newRegRW((u32) mhr + (REGOFFSET * chan) + (3<<2), NULL, 0, 0, ONCEAND0, turbo14, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
		regPtr = regPtr->next;
		regPtr->next = newRegRW((u32) mhr + (REGOFFSET * chan) + (4<<2), NULL, 0, 0, ONCEAND0, turbo15, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
		regPtr = regPtr->next;
		regPtr->next = NULL;
		mhr[(chan<<7) + 0] = 0;
		mhr[(chan<<7) + 1] = 0;
		mhr[(chan<<7) + 2] = 1;
		mhr[(chan<<7) + 3] = 0;
		mhr[(chan<<7) + 4] = 1;
	}

	return firstRW;
}

u32 sendString(u32 channel, char* string)
{
	int x = 0;
	if ((channel > 15) || (string == NULL))
		return -1;
	while (string[x] != '\0') {
		if (Xil_In32(PORT0STS + (channel << 4)) & 0x8)
			return -1;
		Xil_Out32(PORT0WR + (channel << 4), string[x++]);
	}
	return 0;
}

// error check types
// 0: no error checking
// 1: sum of data bytes ?= checksum
// 2: (xor of data bytes) -> ascii ?= checksum
u32 checkROstr(roReg* RO, char* string)
{
	u32 x;
	char sum = 0;
	char crc;
	switch (RO->checktype) {
	// no error checking
	case NOCHECK:
		return 1;
	// simple sum from csbegin to csend (inclusive)
	case SUM1:
		for (x=RO->csbegin; x<=RO->csend; x++)
			sum += string[x];
		return sum == string[RO->check];
	// CRC using XOR of data bytes, then converted to 2-character ASCII
	case CRCA:
/*

		x=0;
		while (string[x] != '\0') {
			xil_printf("0x%02x ", 0x000000FF & string[x++]);
		}
		xil_printf("\r\n");

*/
		for (x=RO->csbegin; x<=RO->csend; x++)
			sum ^= string[x];
		crc = sum & 0x0F;
		crc = (crc > 9) ? ('7' + crc) : ('0' + crc);
//		xil_printf("CRC(l): 0x%02x\r\n", crc);
		if (crc != string[RO->check+1])
			return 0;
		crc = (sum >> 4) & 0x0F;
		crc = (crc > 9) ? ('7' + crc) : ('0' + crc);
//		xil_printf("CRC(h): 0x%02x\r\n", crc);
		return (crc == string[RO->check]);
	}
	return -1;
}

// data types
// 0: 8-bit raw
// 1: integer (from string)
// 2: floating point (from string)
// 3: 16-bit raw
void processROstr(roReg* RO, char* string)
{
	float f;
	int i;
	u32 u;

	switch (RO->datatype) {
	// 8-bit raw data
	case RAWBYTE:
		u = 0x000000FF & *(string + RO->startpos);
		Xil_Out32(RO->address, u);
		break;
	// 16-bit raw data (MSB-first)
	case RAWREG:
		u = (0x0000FF00 & ((u32) *(string + RO->startpos) << 8)) | (0x000000FF & (u32) *(string + RO->startpos + 1));
		Xil_Out32(RO->address, u);
		break;
	// integer (atoi should work for either type)
	case UINT16:
	case SINT16:
		i = atoi(string + RO->startpos);
		Xil_Out32(RO->address, *((u32*) &i));
		break;
	// floating point
	case FLOAT:
		f = atof(string + RO->startpos);
		Xil_Out32(RO->address, *((u32*) &f));
		break;
	}
}

// check whether channel needs to be written
// check types
// 0: send if readback value differs from setting
// 1: always send
// 2: zero MHR and send once
u32 checkRW(rwReg* RW)
{
	switch (RW->resendtype) {
	case ONCEAND0:
		if (Xil_In32(RW->address)) {
			Xil_Out32(RW->address, 0);
			return 1;
		}
		break;
	case ALWAYS:
		return 1;
	case DIFF:
		return Xil_In32(RW->address) != ((Xil_In32(RW->readback) >> RW->rbshift) & RW->rbmask);
	}
	return 0;
}
void updateRW(u32 channel, rwReg* RW)
{
	char ch;
	u32 x = 0;
	char sum = 0;
	char* nextchar = RW->prefix;
	u32 data = Xil_In32(RW->address);
	u32 flag = 0;
	u32 div;

	// send command prefix
	while (*nextchar) {
		if ((x >= RW->csbegin) && (x <= RW->csend))
			sum += *nextchar;
		Xil_Out32(PORT0WR + (channel << 4), *nextchar);
		nextchar++;
		x++;
	}
	// send the data
	switch (RW->datatype) {
	// 8-bit raw data (do not convert to ascii)
	case RAWBYTE:
		ch = data;
		Xil_Out32(PORT0WR + (channel << 4), ch);
		if ((x >= RW->csbegin) && (x <= RW->csend))
			sum += ch;
		x++;
		break;
	case UINT16:
		// parse the integer into characters - sprintf is not used due to its large size.
		div = 1000000000;
		flag = 0;
		if (data & 0x80000000) {
			data *= -1;
			Xil_Out32(PORT0WR + (channel << 4), '-');
			xil_printf("-");
			if ((x >= RW->csbegin) && (x <= RW->csend))
				sum += '-';
			x++;
		}
		while (div) {
			if (flag || (data >= div) || (div == 1)) {
				flag = 1;
				Xil_Out32(PORT0WR + (channel << 4), (data / div) + '0');
				if ((x >= RW->csbegin) && (x <= RW->csend))
					sum += (data / div) + '0';
				x++;
			}
			data %= div;
			div /= 10;
		}
		break;
	}
	// send the postfix
	nextchar = RW->postfix;
	while (*nextchar) {
		if ((x >= RW->csbegin) && (x <= RW->csend))
			sum += *nextchar;
		Xil_Out32(PORT0WR + (channel << 4), *nextchar);
		nextchar++;
		x++;
	}
	// send the checksum
	switch (RW->checktype) {
	case SUM1:
		Xil_Out32(PORT0WR + (channel << 4), sum);
		break;
	}
}
u32 checkResponse(rwReg* RW, char* string)
{
	// To be completed later
	return 1;
}

