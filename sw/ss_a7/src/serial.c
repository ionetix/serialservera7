/*
 * serial.c
 *
 *  Created on: Jun 9, 2017
 *      Author: nusher
 */

#include "regMap.h"
#include "main.h"
#include "serial.h"
#include "xil_io.h"
#include <string.h>
#include <stdlib.h>

// strings used to communicate with known devices
// statically allocated so that they can be referenced multiple times without wasting memory
char empty[] = "";
char cr[] = "\r";
char lakeshore0[] = "KRDG? 1\r\n";		// read channel 1
char lakeshore1[] = "KRDG? 2\r\n";		// read channel 2
char lakeshore2[] = "KRDG? 3\r\n";		// read channel 3
char lakeshore3[] = "KRDG? 4\r\n";		// read channel 4
char lakeshore4[] = "KRDG? 5\r\n";		// read channel 5
char lakeshore5[] = "KRDG? 6\r\n";		// read channel 6
char lakeshore6[] = "KRDG? 7\r\n";		// read channel 7
char lakeshore7[] = "KRDG? 8\r\n";		// read channel 8
char inficon0[] = {3, 16, 62, 1};		// set data units
char alicat0[] = "A\r";					// read all status
char alicat1[] = "A";					// device address (prefix for writes)
char turbo0[] = {(char) 0x02, (char) 0x80, '1', '0', '7', '0', (char) 0x03, '8', '5'};		// Read active stop mode setting
char turbo1[] = {(char) 0x02, (char) 0x80, '1', '6', '7', '0', (char) 0x03, '8', '3'};		// Read stop speed reading setting
char turbo2[] = {(char) 0x02, (char) 0x80, '2', '0', '5', '0', (char) 0x03, '8', '4'};		// Read pump status
char turbo3[] = {(char) 0x02, (char) 0x80, '2', '0', '6', '0', (char) 0x03, '8', '7'};		// Read error code
char turbo4[] = {(char) 0x02, (char) 0x80, '2', '0', '3', '0', (char) 0x03, '8', '2'};		// Read drive frequency
char turbo5[] = {(char) 0x02, (char) 0x80, '2', '2', '6', '0', (char) 0x03, '8', '5'};		// Read rotation frequency
char turbo6[] = {(char) 0x02, (char) 0x80, '3', '0', '2', '0', (char) 0x03, '8', '2'};		// Read pump life
char turbo7[] = {(char) 0x02, (char) 0x80, '2', '0', '1', '0', (char) 0x03, '8', '0'};		// Read pump voltage
char turbo8[] = {(char) 0x02, (char) 0x80, '2', '0', '0', '0', (char) 0x03, '8', '1'};		// Read pump current
char turbo9[] = {(char) 0x02, (char) 0x80, '2', '0', '2', '0', (char) 0x03, '8', '3'};		// Read pump power
char turbo10[] = {(char) 0x02, (char) 0x80, '2', '0', '4', '0', (char) 0x03, '8', '5'};		// Read pump temperature
char turbo11[] = {(char) 0x02, (char) 0x80, '0', '0', '0', '1', '1', (char) 0x03, 'B', '3'};	// Start
char turbo12[] = {(char) 0x02, (char) 0x80, '0', '0', '0', '1', '0', (char) 0x03, 'B', '2'};	// Stop
char turbo13[] = {(char) 0x02, (char) 0x80, '0', '0', '8', '1', '0', (char) 0x03, 'B', 'A'};	// Set control to RS232
char turbo14[] = {(char) 0x02, (char) 0x80, '1', '0', '7', '1', '1', (char) 0x03, 'B', '5'};	// Enable active stop mode
char turbo15[] = {(char) 0x02, (char) 0x80, '1', '6', '7', '1', '1', (char) 0x03, 'B', '3'};	// Enable stop speed reading
char turbo16[] = {(char) 0x02, (char) 0x80, '0', '0', '8', '0', (char) 0x03, '8', 'B'};		// Read control mode
char magnet0[] = "MV?\r";		// read measured voltage
char magnet1[] = "PV?\r";		// read programmed voltage
char magnet2[] = "MC?\r";		// read measured current
char magnet3[] = "PC?\r";		// read programmed current
char magnet4[] = "OUT?\r";		// read output on/off status
char magnet5[] = "RMT?\r";		// read remote mode
char magnet6[] = "FLD?\r";		// read foldback mode on/off status
char magnet7[] = "ADR 06\r";	// set address for power supply comms
char magnet8[] = "RMT 1\r";		// set power supply to remote without disabling front panel buttons
char magnet9[] = "FLD 0\r";		// disable foldback mode
char magnet10[] = "OUT 1\r";	// turn on output
char magnet11[] = "OUT 0\r";	// turn off output
char magnet12[] = "PV ";			// set output voltage (prefix only)
char magnet13[] = "PC ";			// set output current (prefix only)

// Macros to identify different types of registers
// data types
#define NOTYPE		0
#define	RAWBYTE		1
#define RAWREG		2
#define UINT16		3
#define SINT16		4
#define FLOAT		5
#define RAWWORD		6
// checksum types
#define NOCHECK		0
#define SUM1		1
#define CRCA		2
#define NOREPEAT	3
#define LENGTH		4
#define NOREPEATANDLENGTH	5
// resend types
#define ONCEAND0	0
#define ALWAYS		1
#define DIFF		2
// serial port configuration bit masks
#define BITS8		0x00000000
#define BITS7		0x00040000
#define NOPARITY	0x00000000
#define EXTRASTOP	0x00010000
#define EVENPARITY	0x00020000
#define ODDPARITY	0x00030000

extern u32 mhr[2048];
extern u32 mir[2048];

roReg* newRegRO(u32 address, char* pollcmd, char startchar, char stopchar, u32 startpos, u32 maxchars, u32 datatype, u32 checktype, u32 csbegin, u32 csend, u32 check)
{
	roReg* newchan = (roReg*) malloc(sizeof(roReg));
	newchan->next = NULL;
	newchan->address = address;
	newchan->pollcmd = pollcmd;
	newchan->startchar = startchar;
	newchan->stopchar = stopchar;
	newchan->startpos = startpos;
	newchan->maxchars = maxchars;
	newchan->datatype = datatype;
	newchan->checktype = checktype;
	newchan->csbegin = csbegin;
	newchan->csend = csend;
	newchan->check = check;
	return newchan;
}
rwReg* newRegRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, char* prefix, char* postfix, u32 datatype, u32 intchars, u32 fracchars, u32 checktype, u32 csbegin, u32 csend, char startchar, char stopchar, u32 maxchars)
{
	rwReg* newchan = (rwReg*) malloc(sizeof(rwReg));
	newchan->next = NULL;
	newchan->address = address;
	newchan->readback = readback;
	newchan->rbshift = rbshift;
	newchan->rbmask = rbmask;
	newchan->resendtype = resendtype;
	newchan->prefix = prefix;
	newchan->postfix = postfix;
	newchan->datatype = datatype;
	newchan->intchars = intchars;
	newchan->fracchars = fracchars;
	newchan->checktype = checktype;
	newchan->csbegin = csbegin;
	newchan->csend = csend;
	newchan->startchar = startchar;
	newchan->stopchar = stopchar;
	newchan->maxchars = maxchars;
	return newchan;
}
roModbus* newModbusRO(u32 address, u8 unitID, u8 function, u16 startAddr, u16 count)
{
	roModbus* newblock = (roModbus*) malloc(sizeof(roModbus));
	newblock->next = NULL;
	newblock->address = address;
	newblock->unitID = unitID;
	newblock->function = function;
	newblock->startAddr = startAddr;
	newblock->count = count;
	return newblock;
}
rwModbus* newModbusRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, u8 unitID, u8 function, u16 startAddr, u16 count)
{
	rwModbus* newblock = (rwModbus*) malloc(sizeof(rwModbus));
	newblock->next = NULL;
	newblock->address = address;
	newblock->readback = readback;
	newblock->rbshift = rbshift;
	newblock->rbmask = rbmask;
	newblock->resendtype = resendtype;
	newblock->unitID = unitID;
	newblock->function = function;
	newblock->startAddr = startAddr;
	newblock->count = count;
	return newblock;
}

void configPort(u32 chan, u32 devType)
{
	if (chan > 15)
		return;
	switch (devType) {
		// Lakeshore 218 Temperature Monitor
		// 7 data bits, odd parity, 9600 baud
		case 1:
			Xil_Out32(cfgAddr(chan), BITS7 | ODDPARITY | (CLOCKRATE / 9600));
			break;
		// Inficon BPG400 vacuum gauge
		// 8 data bits, no parity, 9600 baud
		case 2:
			Xil_Out32(cfgAddr(chan), BITS8 | NOPARITY | (CLOCKRATE / 9600));
			break;
		// Alicat Mass Flow Controller
		// 8 data bits, no parity, 9600 baud
		case 3:
			Xil_Out32(cfgAddr(chan), BITS8 | NOPARITY | (CLOCKRATE / 9600));
			break;
		// Agilent Twis Torr 304 FS turbo pump controller
		// 8 data bits, no parity, 9600 baud
		case 4:
			Xil_Out32(cfgAddr(chan), BITS8 | NOPARITY | (CLOCKRATE / 9600));
			break;
		// TDK Lambda Genesys power supply
		// 8 data bits, no parity, 19200 baud
		case 5:
			Xil_Out32(cfgAddr(chan), BITS8 | NOPARITY | (CLOCKRATE / 19200));
			break;
		// Technalogix Amplifier
		// 8 data bits, even parity, 19200 baud
		case 6:
			Xil_Out32(cfgAddr(chan), BITS8 | EVENPARITY | (CLOCKRATE / 19200));
			break;
		// Elite RF Amplifier
		// 8 data bits, extra stop bit, 115200 baud
		case 7:
			Xil_Out32(cfgAddr(chan), BITS8 | EXTRASTOP | (CLOCKRATE / 115200));
			break;
		// RF Controller
		// 8 data bits, even parity, 115200 baud
		case 8:
			Xil_Out32(cfgAddr(chan), BITS8 | EVENPARITY | (CLOCKRATE / 115200));
			break;
		// Alpha BCM
		//
		case 9:
			Xil_Out32(cfgAddr(chan), BITS8 | NOPARITY | (CLOCKRATE / 115200));
			break;
	}
	return;
}

roReg* initChannelsRO(u32 chan, u32 devType)
{
	roReg* firstRO = NULL;
	roReg* regPtr = NULL;
	if (chan > 15)
		return firstRO;
	switch (devType) {
		// Lakeshore 218 Temperature Monitor
		// Configured to read the 8 inputs in K
		case 1:
			firstRO = newRegRO(mirAddr(chan, 0), lakeshore0, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = firstRO;
			regPtr->next = newRegRO(mirAddr(chan, 1), lakeshore1, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 2), lakeshore2, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 3), lakeshore3, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 4), lakeshore4, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 5), lakeshore5, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 6), lakeshore6, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 7), lakeshore7, '\0', '\r', 0, 20, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = firstRO;
			break;
		// Inficon BPG400 vacuum gauge
		// Configured to parse streaming data, one modbus input register per packet (even though both registers are in each packet)
		case 2:
			firstRO = newRegRO(mirAddr(chan, 0), NULL, 7, '\0', 2, 9, RAWREG, SUM1, 1, 7, 8);
			regPtr = firstRO;
			regPtr->next = newRegRO(mirAddr(chan, 1), NULL, 7, '\0', 4, 9, RAWREG, SUM1, 1, 7, 8);
			regPtr = regPtr->next;
			regPtr->next = NULL;
			break;
		// Alicat Mass Flow Controller
		case 3:
			firstRO = newRegRO(mirAddr(chan, 0), alicat0, 'A', '\r', 2, 48, FLOAT, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = firstRO;
			regPtr->next = newRegRO(mirAddr(chan, 1), alicat0, 'A', '\r', 10, 48, FLOAT, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 2), alicat0, 'A', '\r', 18, 48, FLOAT, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 3), alicat0, 'A', '\r', 26, 48, FLOAT, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 4), alicat0, 'A', '\r', 34, 48, FLOAT, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 5), alicat0, 'A', '\r', 40, 48, RAWWORD, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 6), alicat0, 'A', '\r', 44, 48, RAWWORD, NOREPEATANDLENGTH, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = NULL;
			break;
		// Agilent Twis Torr 304 FS turbo pump controller
		case 4:
			firstRO = newRegRO(mirAddr(chan, 0), turbo0, 0x02, '\0', 6, 10, RAWBYTE, CRCA, 1, 7, 8);
			regPtr = firstRO;
			regPtr->next = newRegRO(mirAddr(chan, 1), turbo1, 0x02, '\0', 6, 10, RAWBYTE, CRCA, 1, 7, 8);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 2), turbo2, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 3), turbo3, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 4), turbo4, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 5), turbo5, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 6), turbo6, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 7), turbo7, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 8), turbo8, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 9), turbo9, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 10), turbo10, 0x02, '\0', 6, 15, UINT16, CRCA, 1, 12, 13);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 11), turbo16, 0x02, '\0', 6, 10, RAWBYTE, CRCA, 1, 7, 8);
			regPtr = regPtr->next;
			regPtr->next = NULL;
			break;
		// TDK Lambda Genesys power supply
		case 5:
			firstRO = newRegRO(mirAddr(chan, 0), magnet0, '\0', '\r', 0, 6, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = firstRO;
			regPtr->next = newRegRO(mirAddr(chan, 1), magnet1, '\0', '\r', 0, 6, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 2), magnet2, '\0', '\r', 0, 6, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 3), magnet3, '\0', '\r', 0, 6, FLOAT, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 4), magnet4, '\0', '\r', 0, 4, RAWWORD, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 5), magnet5, '\0', '\r', 0, 4, RAWWORD, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = newRegRO(mirAddr(chan, 6), magnet6, '\0', '\r', 0, 4, RAWWORD, NOCHECK, 0, 0, 0);
			regPtr = regPtr->next;
			regPtr->next = NULL;
			break;
		// Technalogix amplifier
		case 6:
			break;
		case 7:
			break;
		// RF controller
		case 8:
			break;
	}
	return firstRO;
}

rwReg* initChannelsRW(u32 chan, u32 devType)
{
	rwReg* firstRW = NULL;
	rwReg* regPtr = NULL;
	if (chan > 15)
		return firstRW;
	switch (devType) {
		// Inficon BPG400 vacuum gauge
		// Configured to force the front panel display to match the units set by the PLC
		case 2:
			firstRW = newRegRW(mhrAddr(chan, 0), mirAddr(chan, 0), 12, 0x00000003, DIFF, inficon0, NULL, RAWBYTE, 1, 0, SUM1, 1, 3, '\0', '\0', 0);
			regPtr = firstRW;
			mhr[chan<<7] = 1;
			break;
		// Alicat Mass Flow Controller
		case 3:
			firstRW = newRegRW(mhrAddr(chan, 0), 0, 0, 0, ALWAYS, alicat1, cr, UINT16, 0, 0, NOCHECK, 0, 0, '\0', '\0', 0);
			regPtr = firstRW;
			regPtr->next = NULL;
			mhr[(chan<<7) + 0] = 0;
			break;
		// Agilent Twis Torr 304 FS turbo pump controller
		case 4:
			firstRW = newRegRW(mhrAddr(chan, 0), 0, 0, 0, ONCEAND0, turbo11, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
			regPtr = firstRW;
			regPtr->next = newRegRW(mhrAddr(chan, 1), 0, 0, 0, ONCEAND0, turbo12, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 2), 0, 0, 0, ONCEAND0, turbo13, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 3), 0, 0, 0, ONCEAND0, turbo14, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 4), 0, 0, 0, ONCEAND0, turbo15, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, 0x02, '\0', 6);
			regPtr = regPtr->next;
			regPtr->next = NULL;
			mhr[(chan<<7) + 0] = 0;
			mhr[(chan<<7) + 1] = 0;
			mhr[(chan<<7) + 2] = 1;
			mhr[(chan<<7) + 3] = 0;
			mhr[(chan<<7) + 4] = 1;
			break;
		// TDK Lambda Genesys power supply
		case 5:
			firstRW = newRegRW(mhrAddr(chan, 0), 0, 0, 0, ONCEAND0, magnet7, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = firstRW;
			regPtr->next = newRegRW(mhrAddr(chan, 1), 0, 0, 0, ONCEAND0, magnet8, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 2), 0, 0, 0, ONCEAND0, magnet9, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 3), 0, 0, 0, ONCEAND0, magnet10, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 4), 0, 0, 0, ONCEAND0, magnet11, empty, NOTYPE, 0, 0, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 5), 0, 0, 0, ALWAYS, magnet12, cr, FLOAT, 1, 3, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			regPtr->next = newRegRW(mhrAddr(chan, 6), 0, 0, 0, ALWAYS, magnet13, cr, FLOAT, 3, 2, NOCHECK, 0, 0, '\0', '\r', 10);
			regPtr = regPtr->next;
			break;
		// RF controller
		case 8:
			break;
	}

	return firstRW;
}

roModbus* initModbusRO(u32 chan, u32 devType)
{
	roModbus* firstRO = NULL;
	roModbus* regPtr = NULL;
	if (chan > 15)
		return firstRO;
	switch (devType) {
		// Technalogix amplifier
		case 6:
			firstRO = newModbusRO(mirAddr(chan, 0), 8, 4, 0, 74);
			regPtr = firstRO;
			regPtr->next = newModbusRO(mirAddr(chan, 37), 8, 3, 0, 3);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 39), 8, 2, 0, 120);
			regPtr = regPtr->next;
			break;
		// Elite RF amplifier
		case 7:
//			firstRO = newModbusRO(mirAddr(chan, 0), 85, 4, 30000, 8);
/*			firstRO = newModbusRO(mirAddr(chan, 2), 85, 4, 30004, 4);
			regPtr = firstRO;
			regPtr->next = newModbusRO(mirAddr(chan, 4), 85, 4, 30008, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 8), 85, 4, 30016, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 12), 85, 4, 30024, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 16), 85, 2, 10001, 32);
			regPtr = regPtr->next;*/
			firstRO = newModbusRO(mirAddr(chan, 0), 85, 4, 30001, 8);
			regPtr = firstRO;
			regPtr->next = newModbusRO(mirAddr(chan, 4), 85, 4, 30009, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 8), 85, 4, 30017, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 12), 85, 4, 30025, 8);
			regPtr = regPtr->next;
			regPtr->next = newModbusRO(mirAddr(chan, 16), 85, 2, 10001, 32);
			regPtr = regPtr->next;
			break;
		// RF controller
		case 8:
			// read all the inputs
			firstRO = newModbusRO(mirAddr(chan, 0), 1, 2, 0, 11);
			regPtr = firstRO;
			// read all the MIRs
			regPtr->next = newModbusRO(mirAddr(chan, 1), 1, 4, 0, 112);
			regPtr = regPtr->next;
			// readback all the MHRs
			regPtr->next = newModbusRO(mirAddr(chan, 57), 1, 3, 0, 110);
			regPtr = regPtr->next;
			break;
		case 9:
			// read all the inputs (everything is floating point)
			firstRO = newModbusRO(mirAddr(chan, 0), 1, 4, 0, 48);
			regPtr = firstRO;
			break;
	}
	return firstRO;
}

rwModbus* initModbusRW(u32 chan, u32 devType)
{
	rwModbus* firstRW = NULL;
	rwModbus* regPtr = NULL;
	if (chan > 15)
		return firstRW;
	switch (devType) {
		// Technalogix amplifier
		case 6:
			firstRW = newModbusRW(mhrAddr(chan, 0), 0, 0, 0, ONCEAND0, 8, 5, 0, 1);
			regPtr = firstRW;
			// holding registers are not currently implemented in the PLC with no plans to do so
			// therefore safest not to write to them to guarantee they stay at default values
//			regPtr->next = newModbusRW(mhrAddr(chan, 1), 0, 0, 0, ONCEAND0, 8, 16, 0, 3);
//			regPtr = regPtr->next;
			break;
		// Elite RF amplifier
		case 7:
			firstRW = newModbusRW(mhrAddr(chan, 0), 0, 0, 0, ONCEAND0, 85, 6, 40001, 1);
			regPtr = firstRW;
			break;
		// RF controller
		case 8:
			firstRW = newModbusRW(mhrAddr(chan, 0), 0, 0, 0, ONCEAND0, 1, 15, 0, 25);
			regPtr = firstRW;
			regPtr->next = newModbusRW(mhrAddr(chan, 1), 0, 0, 0, ALWAYS, 1, 16, 0, 110);
			regPtr = regPtr->next;
			break;
	}
	return firstRW;
}

u32 sendString(u32 channel, char* string)
{
	int x = 0;
	if ((channel > 15) || (string == NULL))
		return -1;
	while (string[x] != '\0') {
		if (Xil_In32(stsAddr(channel)) & 0x8)
			return -1;
		Xil_Out32(wrAddr(channel), string[x++]);
	}
	return 0;
}

u32 sendModbusRead(u32 channel, roModbus* block) {
	if ((channel > 15) || block == NULL)
		return -1;
	init_crcout();
	sendWithCRC(wrAddr(channel), block->unitID);
	sendWithCRC(wrAddr(channel), block->function);
	sendWithCRC(wrAddr(channel), block->startAddr >> 8);
	sendWithCRC(wrAddr(channel), block->startAddr & 0x00FF);
	sendWithCRC(wrAddr(channel), block->count >> 8);
	sendWithCRC(wrAddr(channel), block->count & 0x00FF);
	u16 crc = get_crcout();
	Xil_Out32(wrAddr(channel), crc & 0x00FF);
	Xil_Out32(wrAddr(channel), crc >> 8);
	return 0;
}

// not fully implemented, add functions as needed
u32 sendModbusWrite(u32 channel, rwModbus* block) {
	if ((channel > 15) || block == NULL)
		return -1;
	u16 x;
	u32 data;
	u8 ch;
	u32 byteCount;
	init_crcout();
	sendWithCRC(wrAddr(channel), block->unitID);
	sendWithCRC(wrAddr(channel), block->function);
	sendWithCRC(wrAddr(channel), block->startAddr >> 8);
	sendWithCRC(wrAddr(channel), block->startAddr & 0x00FF);
	switch (block->function) {
		// write single coil
		case 5:
			data = Xil_In32(block->address) & 0x00000001;
			sendWithCRC(wrAddr(channel), data ? 0xFF : 0x00);
			sendWithCRC(wrAddr(channel), 0x00);
			break;
		// write single holding register
		case 6:
			data = Xil_In32(block->address) & 0x0000FFFF;
			sendWithCRC(wrAddr(channel), data >> 8);
			sendWithCRC(wrAddr(channel), data & 0x00FF);
			break;
		// write multiple coils (don't expect this to ever be used)
		// no error checking is done here to verify that the count is not too big
		case 15:
			byteCount = (block->count >> 3) + (block->count % 8 ? 1 : 0);
			sendWithCRC(wrAddr(channel), block->count >> 8);
			sendWithCRC(wrAddr(channel), block->count & 0x00FF);
			sendWithCRC(wrAddr(channel), byteCount & 0x00FF);
			for (x=0; x<block->count; x+=8) {
				switch (x % 32) {
					case 0:
						data = Xil_In32(block->address + (x >> 3));
						ch = data & 0x000000FF;
						if (x + 8 > block->count)
							ch &= 0x01 << (x + 7 - block->count);
						sendWithCRC(wrAddr(channel), ch);
						break;
					case 8:
						ch = (data >> 8) & 0x000000FF;
						if (x + 8 > block->count)
							ch &= 0x01 << (x + 7 - block->count);
						sendWithCRC(wrAddr(channel), ch);
						break;
					case 16:
						ch = (data >> 16) & 0x000000FF;
						if (x + 8 > block->count)
							ch &= 0x01 << (x + 7 - block->count);
						sendWithCRC(wrAddr(channel), ch);
						break;
					case 24:
						ch = (data >> 24) & 0x000000FF;
						if (x + 8 > block->count)
							ch &= 0x01 << (x + 7 - block->count);
						sendWithCRC(wrAddr(channel), ch);
						break;
				}
			}
			break;
		// write multiple holding registers
		// no error checking is done here to verify that the count is not too big
		case 16:
			sendWithCRC(wrAddr(channel), block->count >> 8);
			sendWithCRC(wrAddr(channel), block->count & 0x00FF);
			sendWithCRC(wrAddr(channel), (block->count << 1) & 0x00FF);
			for (u8 x=0; x<block->count<<1; x++) {
				switch (x % 4) {
					case 0:
						data = Xil_In32(block->address + x);
						sendWithCRC(wrAddr(channel), (data >> 8) & 0x000000FF);
						break;
					case 1:
						sendWithCRC(wrAddr(channel), data & 0x000000FF);
						break;
					case 2:
						sendWithCRC(wrAddr(channel), data >> 24);
						break;
					case 3:
						sendWithCRC(wrAddr(channel), (data >> 16) & 0x000000FF);
						break;
				}
			}
			break;
	}
	u16 crc = get_crcout();
	Xil_Out32(wrAddr(channel), crc);
	Xil_Out32(wrAddr(channel), crc >> 8);
	if (block->resendtype == ONCEAND0)
		Xil_Out32(block->address, 0);
	return 0;
}

// error check types
// 0: no error checking
// 1: sum of data bytes ?= checksum
// 2: (xor of data bytes) -> ascii ?= checksum
// 3: no repeat start char allowed
u32 checkROstr(roReg* RO, char* string)
{
	u32 x;
	char sum = 0;
	char crc;
	switch (RO->checktype) {
		// no error checking
		case NOCHECK:
			return 1;
		// simple sum from csbegin to csend (inclusive)
		case SUM1:
			for (x=RO->csbegin; x<=RO->csend; x++)
				sum += string[x];
			return sum == string[RO->check];
		// CRC using XOR of data bytes, then converted to 2-character ASCII
		case CRCA:
			for (x=RO->csbegin; x<=RO->csend; x++)
				sum ^= string[x];
			crc = sum & 0x0F;
			crc = (crc > 9) ? ('7' + crc) : ('0' + crc);
			if (crc != string[RO->check+1])
				return 0;
			crc = (sum >> 4) & 0x0F;
			crc = (crc > 9) ? ('7' + crc) : ('0' + crc);
			return (crc == string[RO->check]);
		// if device only sends start char once in packet check for repeats
		case NOREPEAT:
			if (string[0] != RO->startchar)
				return 0;
			for (x=1; x<RO->maxchars; x++)
				if (string[x] == RO->startchar)
					return 0;
			return 1;
		// string length including stop character must exactly match maxchars
		case LENGTH:
			x=0;
			while (string[x++] != '\0');
			x--;
			return x == RO->maxchars;
		// combination of both previous checks
		case NOREPEATANDLENGTH:
			x=0;
			while (string[x++] != '\0') {
				if (x && string[x] == RO->startchar)
					return 0;
			}
			x--;
			return x == RO->maxchars;
	}
	return -1;
}

// check that Modbus data packet is valid
// check prefix bytes match command and CRC is correct
u32 checkModbusRO(roModbus* RO, char* string) {
	if (string[0] != RO->unitID)
		return 0;
	if (string[1] != RO->function)
		return 0;
	u8 dataLength = (u8) string[2] + 3;
	u16 receivedCRC = (u8) string[dataLength + 1];
	receivedCRC <<= 8;
	receivedCRC |= (u8) string[dataLength];
	init_crcout();
	for (u8 x=0; x<dataLength; x++)
		update_crcout(string[x]);
	u16 calculatedCRC = get_crcout();
	if (calculatedCRC != receivedCRC)
		return 0;
	return 1;
}

// check that Modbus data packet is valid
// responses to implemented write commands all have 8 total bytes
u32 checkModbusRWResponse(rwModbus* RW, char* string) {
	if (string[0] != RW->unitID)
		return 0;
	if (string[1] != RW->function)
		return 0;
	if ((u8) string[2] != RW->startAddr >> 8)
		return 0;
	if ((u8) string[3] != (RW->startAddr & 0x00FF))
		return 0;
	switch(RW->function) {
		// data readback isn't checked for functions 5 and 6
		case 15:
		case 16:
			if ((u8) string[4] != RW->count >> 8)
				return 0;
			if ((u8) string[5] != (RW->count & 0x00FF))
				return 0;
			break;
	}
	init_crcout();
	for (u32 x=0; x<6; x++)
		update_crcout(string[x]);
	u16 receivedCRC = (u8) string[7];
	receivedCRC <<= 8;
	receivedCRC |= (u8) string[6];
	if (get_crcout() != receivedCRC)
		return 0;
	return 1;
}

// data types
// 0: 8-bit raw
// 1: integer (from string)
// 2: floating point (from string)
// 3: 16-bit raw
void processROstr(roReg* RO, char* string)
{
	float f;
	int i;
	u32 u;

	// get the length of the string
	// if the string is too short, don't save the data to memory (assuming buffer overrun corrupted the string)
	u32 length = 0;
	while (string[length++]);
	length--;

	switch (RO->datatype) {
		// 8-bit raw data
		case RAWBYTE:
			u = 0x000000FF & *(string + RO->startpos);
			Xil_Out32(RO->address, u);
			break;
		// 16-bit raw data (MSB-first)
		case RAWREG:
			u = (0x0000FF00 & ((u32) *(string + RO->startpos) << 8)) | (0x000000FF & (u32) *(string + RO->startpos + 1));
			Xil_Out32(RO->address, u);
			break;
		// 32-bit raw data (MSB-first)
		case RAWWORD:
			u = (0x00FF0000 & ((u32) *(string + RO->startpos) << 16)) | (0xFF000000 & ((u32) *(string + RO->startpos + 1) << 24)) | (0x000000FF & ((u32) *(string + RO->startpos + 2))) | (0x0000FF00 & ((u32) *(string + RO->startpos + 3) << 8));
			Xil_Out32(RO->address, u);
			break;
		// integer (atoi should work for either type)
		case UINT16:
		case SINT16:
			if (length <= RO->startpos + 1)
				return;
			i = atoi(string + RO->startpos);
			Xil_Out32(RO->address, *((u32*) &i));
			break;
		// floating point
		case FLOAT:
			if (length <= RO->startpos + 3)
				return;
			f = atof(string + RO->startpos);
			Xil_Out32(RO->address, *((u32*) &f));
			break;
	}
}

void processModbusRO(roModbus* RO, char* string) {
	u32 temp;
	u32 data = 0;
	u8 index = 3;
	u8 x = 0;
	u8 wordCount = 0;

	// Everything is saved in 32-bit registers, so no need to differentiate between the different read functions
	while (x < (u8) string[2]) {
		temp = (u8) string[index];
		switch (RO->function) {
			case 1:
			case 2:
				switch (x % 4) {
					case 0:
						data = 0;
						break;
					case 1:
						temp <<= 8;
						break;
					case 2:
						temp <<= 16;
						break;
					case 3:
						temp <<= 24;
						break;
				}
				break;
			case 3:
			case 4:
				switch (x % 4) {
					case 0:
						data = 0;
						temp <<= 8;
						break;
					case 1:
						break;
					case 2:
						temp <<= 24;
						break;
					case 3:
						temp <<= 16;
						break;
				}
				break;
		}
		data |= temp;
		x++;
		index++;
		if (!(x % 4) || (x == string[2]))
			Xil_Out32(RO->address + (wordCount++ << 2), data);
	}
}

// check whether channel needs to be written
// check types
// 0: send if readback value differs from setting
// 1: always send
// 2: zero MHR and send once
u32 checkRW(rwReg* RW)
{
	switch (RW->resendtype) {
		case ONCEAND0:
			return Xil_In32(RW->address) ? 1 : 0;
		case ALWAYS:
			return 1;
		case DIFF:
			return Xil_In32(RW->address) != ((Xil_In32(RW->readback) >> RW->rbshift) & RW->rbmask);
	}
	return 0;
}
// check whether channel needs to be written
// check types
// 0: send if readback value differs from setting
// 1: always send
// 2: zero MHR and send once
u32 checkModbusRW(rwModbus* RW)
{
	switch (RW->resendtype) {
		case ONCEAND0:
			return Xil_In32(RW->address) ? 1 : 0;
		case ALWAYS:
			return 1;
		case DIFF:
			return Xil_In32(RW->address) != ((Xil_In32(RW->readback) >> RW->rbshift) & RW->rbmask);
	}
	return 0;
}
void updateRW(u32 channel, rwReg* RW)
{
	char ch;
	u32 x = 0;
	char sum = 0;
	char* nextchar = RW->prefix;
	u32 data = Xil_In32(RW->address);
	u32 flag = 0;
	u32 div;

	// send command prefix
	while (*nextchar) {
		if ((channel >= RW->csbegin) && (x <= RW->csend))
			sum += *nextchar;
		Xil_Out32(wrAddr(channel), *nextchar);
//		if (channel == 5)
//			xil_printf("%c", *nextchar);
		nextchar++;
		x++;
	}
	// send the data
	switch (RW->datatype) {
		// 8-bit raw data (do not convert to ascii)
		case RAWBYTE:
			ch = data;
			Xil_Out32(wrAddr(channel), ch);
			if ((x >= RW->csbegin) && (x <= RW->csend))
				sum += ch;
			x++;
			break;
		case UINT16:
			// parse the integer into characters - sprintf is not used due to its large size.
			div = 1000000000;
			flag = 0;
			if (data & 0x80000000) {
				data *= -1;
				Xil_Out32(wrAddr(channel), '-');
				if ((x >= RW->csbegin) && (x <= RW->csend))
					sum += '-';
				x++;
			}
			while (div) {
				if (flag || (data >= div) || (div == 1)) {
					flag = 1;
					Xil_Out32(wrAddr(channel), (data / div) + '0');
					if ((x >= RW->csbegin) && (x <= RW->csend))
						sum += (data / div) + '0';
					x++;
				}
				data %= div;
				div /= 10;
			}
			break;
		case FLOAT:
			// send initial sign char if necessary
			if (data & 0x80000000) {
				data *= -1;
				Xil_Out32(wrAddr(channel), '-');
				if ((x >= RW->csbegin) && (x <= RW->csend))
					sum += '-';
				x++;
			}
			float f = *((float*) &data);
			u32 intPart = (u32) f;
			f -= intPart;
			div = 1000000000;
			flag = 0;
			while (div) {
				if (flag || (intPart >= div) || (div == 1)) {
					flag = 1;
					Xil_Out32(wrAddr(channel), (intPart / div) + '0');
					if ((x >= RW->csbegin) && (x <= RW->csend))
						sum += (intPart / div) + '0';
					x++;
				}
				intPart %= div;
				div /= 10;
			}
			if (RW->fracchars > 0) {
				Xil_Out32(wrAddr(channel), '.');
				if ((x >= RW->csbegin) && (x <= RW->csend))
					sum += '.';
				x++;
			}
			for (u32 count=0; count < RW->fracchars; count++) {
				f *= 10;
				intPart = (u32) f;
				f -= intPart;
				Xil_Out32(wrAddr(channel), intPart + '0');
				if ((x >= RW->csbegin) && (x <= RW->csend))
					sum += intPart + '0';
				x++;
			}
			break;
	}
	// send the postfix
	nextchar = RW->postfix;
	while (*nextchar) {
		if ((x >= RW->csbegin) && (x <= RW->csend))
			sum += *nextchar;
		Xil_Out32(wrAddr(channel), *nextchar);
		nextchar++;
		x++;
	}
	// send the checksum
	switch (RW->checktype) {
	case SUM1:
		Xil_Out32(wrAddr(channel), sum);
		break;
	}
	// clear if register (if it is configured to clear after sending)
	if (RW->resendtype == ONCEAND0)
		Xil_Out32(RW->address, 0);
}
u32 checkResponse(rwReg* RW, char* string)
{
	// To be completed later
	return 1;
}
// might be smaller than atof() if space is an issue
float stringToFloat(char* string) {
	u32 neg = 0;
	u32 integer = 0;
	u32 fraction = 0;
	u32 div = 1;
	int points = -1;
	float result = 0.0;
	if (*string == '-' || *string == '+')
		neg = *(string++) == '-';
	while ((*string >= '0' && *string <= '9') || (*string == '.' && points == -1)) {
		if (*string == '.' && points == -1) {
			points++;
			string++;
		}
		else if (points >= 0) {
			fraction = 10 * fraction + *(string++) - '0';
			points++;
		}
		else
			integer = 10 * integer + *(string++) - '0';
	}
	if (points > 0) {
		while (points--)
			div *= 10;
		result = fraction;
		result /= div;
	}
	result += integer;
	if (neg)
		result *= -1;
	return result;
}

// gets addresses of the read, write, and status registers for each serial port
u32 cfgAddr(u32 port) {
	if (port > 15)
		return 0;
	return PORT0CFG | (port << 4);
}
u32 stsAddr(u32 port) {
	if (port > 15)
		return 0;
	return PORT0STS | (port << 4);
}
u32 rdAddr(u32 port) {
	if (port > 15)
		return 0;
	return PORT0RD | (port << 4);
}
u32 wrAddr(u32 port) {
	if (port > 15)
		return 0;
	return PORT0WR | (port << 4);
}
u32 mhrAddr(u32 port, u32 reg) {
	if (port > 15 || reg > 127)
		return 0;
	return (u32) mhr + (REGOFFSET * port) + (reg << 2);
}
u32 mirAddr(u32 port, u32 reg) {
	if (port > 15 || reg > 127)
		return 0;
	return (u32) mir + (REGOFFSET * port) + (reg << 2);
}
// sends byte and updates Modbus CRC calculation
void sendWithCRC(u32 addr, u8 data) {
	Xil_Out32(addr, data);
	update_crcout(data);
}
