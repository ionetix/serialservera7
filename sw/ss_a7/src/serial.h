/*
 * serial.h
 *
 *  Created on: Jun 9, 2017
 *      Author: nusher
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include "xil_io.h"

typedef struct roReg {		// structure used for read-only channels
	struct roReg* next;	// pointer to next channel in device
	u32 address;		// address in CPU memory map where Modbus channel data is stored
	char* pollcmd;		// string to send to device to initiate read
	char startchar;		// first character sent in device response
	char stopchar;		// final character sent in device response
	u32 startpos;		// position in return string where data starts
	u32 maxchars;		// maximum number of characters to accept from device
	u32 datatype;		// type of data received (raw bits, int, float, etc.)
	u32 checktype;		// type of checksum
	u32 csbegin;		// beginning of characters for checksum
	u32 csend;			// end of characters for checksum
	u32 check;			// checksum value from device
} roReg;

typedef struct rwReg {		// structure used for read/write channels
	struct rwReg* next;	// pointer to next channel in device
	u32 address;		// address in CPU memory map where Modbus channel data is stored
	u32 readback;		// address in CPU memory where readback data is stored
	u32 rbshift;		// amount to right-shift readback register
	u32 rbmask;			// mask to apply to readback register
	u32 resendtype;		// algorithm type to determine whether to retransmit
	char* prefix;		// constant string used at beginning of transmission
	char* postfix;		// constant string used at end of transmission
	u32 datatype;		// type of data being sent (raw bits, int, float, etc.)
	u32 intchars;		// number of characters before decimal point (int and float data types)
	u32 fracchars;		// number of characters after decimal point (float data type)
	u32 checktype;		// type of checksum
	u32 csbegin;		// beginning of characters for checksum
	u32 csend;			// end of characters for checksum
	char startchar;		// first character of response
	char stopchar;		// second character of response
	u32 maxchars;		// maximum length of response
} rwReg;

typedef struct roModbus {	// structure used for reading a block of Modbus coils, inputs, holding registers, or input registers
	struct roModbus* next;	// points to next block of channels in device
	u32 address;		// address in CPU memory map where Modbus (server) data is stored
	u8 unitID;			// unit ID of Modbus device
	u8 function;		// Modbus function to use to read the channels (1: read coils, 2: read inputs, 3: read holding registers, 4: read input registers)
	u16 startAddr;		// starting address on Modbus device (lowest address is 0)
	u16 count;			// number of coils, inputs, holding registers, or input registers to read
} roModbus;


typedef struct rwModbus {	// structure used for writing to a Modbus device - set up to write max of 32-bits at a time
	struct rwModbus* next;	// points to next writeable channel in device
	u32 address;		// address in CPU memory map where Modbus (server) data is stored
	u32 readback;		// address in CPI memory where readback data is stored
	u32 rbshift;		// amount to right-shift readback register
	u32 rbmask;			// mask to apply to readback register
	u32 resendtype;		// algorithm type to determine whether to retransmit
	u8 unitID;			// unit ID of Modbus device
	u8 function;		// Modbus function to use to write the channel(s) (5: single coil, 6: single register, 15: multiple coils, 16: register pair)
	u16 startAddr;		// starting address on Modbus device (lowest address is 0)
	u16 count;			// number of coils or holding registers to write (max 32 bits)
} rwModbus;
roReg* newRegRO(u32 address, char* pollcmd, char startchar, char stopchar, u32 startpos, u32 maxchars, u32 datatype, u32 checktype, u32 csbegin, u32 csend, u32 check);
rwReg* newRegRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, char* prefix, char* postfix, u32 datatype, u32 intchars, u32 fracchars, u32 checktype, u32 csbegin, u32 csend, char startchar, char endchar, u32 maxchars);
roModbus* newModbusRO(u32 address, u8 unitID, u8 function, u16 startAddr, u16 count);
rwModbus* newModbusRW(u32 address, u32 readback, u32 rbshift, u32 rbmask, u32 resendtype, u8 unitID, u8 function, u16 startAddr, u16 count);

void configDevice(u32 port, u32 devType);
void configPort(u32 chan, u32 devType);
roReg* initChannelsRO(u32 chan, u32 devType);
rwReg* initChannelsRW(u32 chan, u32 devType);
roModbus* initModbusRO(u32 chan, u32 devType);
rwModbus* initModbusRW(u32 chan, u32 devType);
u32 sendString(u32 channel, char* string);
u32 sendModbusRead(u32 channel, roModbus* block);
u32 sendModbusWrite(u32 channel, rwModbus* block);
u32 checkROstr(roReg* RO, char* string);
u32 checkModbusRO(roModbus* RO, char* string);
u32 checkModbusRW(rwModbus* RW);
void processROstr(roReg* RO, char* string);
void processModbusRO(roModbus* RO, char* string);
u32 checkRW(rwReg* RW);
void updateRW(u32 channel, rwReg* RW);
u32 checkResponse(rwReg* RW, char* string);
u32 checkModbusRWResponse(rwModbus* RW, char* string);
float stringToFloat(char* string);
void init_crcout();
void update_crcout(unsigned char val);
u16 get_crcout();
u32 cfgAddr(u32 port);
u32 stsAddr(u32 port);
u32 rdAddr(u32 port);
u32 wrAddr(u32 port);
u32 mhrAddr(u32 port, u32 reg);
u32 mirAddr(u32 port, u32 reg);
void sendWithCRC(u32 addr, u8 data);

#endif /* SERIAL_H_ */
