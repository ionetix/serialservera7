/*
 * Copyright (c) 2009-2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

// System type defines which configuration will be loaded when compiled
// 1: ION12SC (Half-rack control system)
// 2: ALPHA CS-30 (First one installed)
#define SYSTEMTYPE 			2

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "xil_io.h"
#include "main.h"
#include "serial.h"

// Constants
#define CORDICGAIN			1.6467602581
#define ADCMAXRAWCIC		29691.0
#define MODBUSTIMEOUT		100000		// 1 ms
//#define MODBUSTIMEOUT		651
//#define MODBUSSTARTTIME		1519

#define BACKSPACE			0x08
#define SPACE				0x20
#define CARRIAGE_RETURN		0x0D

char str[100];		// only used in main function, but declared here to prevent dynamically allocated variables from overwriting the string
u32 mhr[2048];		// memory space used to store Modbus Holding Registers
u32 mir[2048];		// memory space used to store Modbus Input Registers
// serial port registers
u32 config[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// serial port configuration word
char serialStr[16][256];
u32 serialSize[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
roReg* firstRO[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read-only register pointers
roReg* nextRO[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read-only register pointers
rwReg* firstRW[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write register pointers
rwReg* nextRW[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write register pointers
roModbus* firstModbusRO[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write block pointers
roModbus* nextModbusRO[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write block pointers
rwModbus* firstModbusRW[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write block pointers
rwModbus* nextModbusRW[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// read/write block pointers
u32 state[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	// send/receive logic state for each port

// Modbus register map
const int numCoils = 0, numInputs = 0, numInputRegs = 256, numHoldingRegs = 256;

// Flash register map
//const int numSettings = 16;
//const u32 settings[16] = {			&config[0],		&config[1],		&config[2],		&config[3],		&config[4],		&config[5],		&config[6],		&config[7],
//									&config[8],		&config[9],		&config[10],	&config[11],	&config[12],	&config[13],	&config[14],	&config[15]};

int flashBusy()
{
	return Xil_In32(FLASH_STATUS) & 0x00000001;				// check busy flag
}
void flashWriteEnable()
{
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	// wait for previous command to finish
	while (flashBusy());
	Xil_Out32(FLASH_WRITEFIFO, 0x00010001);
	Xil_Out32(FLASH_WRITEFIFO, 0x06000000);
	Xil_Out32(FLASH_START, 1);
//	xil_printf("flash write bit set\r\n");
}
void flashEraseSector(u32 address)
{
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	address &= 0x00FFFFFC;
	while (flashBusy());
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004);
	Xil_Out32(FLASH_WRITEFIFO, 0x20000000 | address);
	Xil_Out32(FLASH_START, 1);
//	xil_printf("flash sector erased\r\n");
}
void flashEraseBlock(u32 address)
{
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	address &= 0x00FFFFFC;
	while (flashBusy());
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004);
	Xil_Out32(FLASH_WRITEFIFO, 0xD8000000 | address);
	Xil_Out32(FLASH_START, 1);
//	xil_printf("flash sector erased\r\n");
}
// assumes any pages being programmed have already been erased (no error checking done here)
void flashProgramPages(u32 address, u32 regCount, const u32* regArray)
{
	u32 byteCount = 0;
	u32 x = 0;
	u32 limit = regCount;
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	address &= 0x00FFFFC0;

	while (x < regCount)
	{
		if (regCount > 64)			limit = 64;				// max of 64 words per page (256B)
		else						limit = regCount;
		byteCount = (limit << 2) + 4;
		while (flashBusy());
		Xil_Out32(FLASH_WRITEFIFO, (byteCount << 16) | byteCount);
		Xil_Out32(FLASH_WRITEFIFO, 0x02000000 | address);
		for (x=0; x<limit; x++)		Xil_Out32(FLASH_WRITEFIFO, Xil_In32(*regArray++));
		Xil_Out32(FLASH_START, 1);
		if (regCount > 64)
		{
			regCount -= 64;
			x = 0;
		}
	}
}
void flashRead(u32 address, u32 regCount)
{
	u32 byteCount;
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	byteCount = regCount << 2;
	address &= 0x00FFFFFC;
	while(flashBusy());
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004 + byteCount);
	Xil_Out32(FLASH_WRITEFIFO, 0x03000000 | address);
	Xil_Out32(FLASH_START, 1);
}
/*void load_config() {
	const u32* regPtr;
	u32 x;
	u32 flashValue;
	u32 regAddress;

	regPtr = settings;
	flashRead(SETTING_ADDR0, numSettings);
	xil_printf("\r\nLoading settings from flash\r\n");
	for (x=0; x<numSettings; x++)
	{
		while (flashBusy());
		flashValue = Xil_In32(FLASH_READFIFO);
		if ((x == 0) && (flashValue == 0xFFFFFFFF))
		{
			x = numSettings;
			xil_printf("Settings page of flash empty\r\n");
		}
		else
		{
			regAddress = *regPtr;
			if (flashValue == 0xFFFFFFFF)
			{
				regPtr++;
				xil_printf("Invalid value stored in flash, 0x%08X set to default\r\n", regAddress);
			}
			else
			{
				Xil_Out32(*regPtr++, flashValue);
				xil_printf("0x%08X written to address 0x%08X\r\n", flashValue, regAddress);
			}
		}
	}
}*/
void executeCommand(char * Str);
void respond(unsigned char * message);
int debug_rcvd()
{
	return Xil_In32(DEBUG_STS) & 0x00000001;
}
char debug_getchar()
{
	return Xil_In32(DEBUG_RX);
}
void debug_putchar(char c)
{
	Xil_Out32(DEBUG_TX, c);
}
int modbus_rcvd()
{
	return Xil_In32(MODBUS_STS) & 0x00000001;
}
unsigned char modbus_getchar()
{
	return Xil_In32(MODBUS_RX);
}
void modbus_putchar(unsigned char c)
{
	// if TX FIFO is full, wait to send byte
	while (Xil_In32(MODBUS_STS) & TXFULL);
	Xil_Out32(MODBUS_TX, c);
}
void init_crcin()
{
	Xil_Out32(INIT_CRCIN, 1);
}
void update_crcin(unsigned char val)
{
	Xil_Out32(UPDATE_CRCIN, (u32) val);
}
u16 get_crcin()
{
	return Xil_In32(READ_CRCIN);
}
void init_crcout()
{
	Xil_Out32(INIT_CRCOUT, 1);
}
void update_crcout(unsigned char val)
{
	Xil_Out32(UPDATE_CRCOUT, (u32) val);
}
u16 get_crcout()
{
	return Xil_In32(READ_CRCOUT);
}
void reset_modbustimer()
{
	Xil_Out32(CLR_TIMER, 1);
}
u32 read_modbustimer()
{
	return Xil_In32(READ_TIMER);
}
void configport(u32 port, u32 config)
{
	if (port > 15)	return;
	switch (config) {
	case 0:
		Xil_Out32(cfgAddr(port), PORTRESET);
		break;
	default:
		Xil_Out32(cfgAddr(port), config);
	}
}
void configDevice(u32 port, u32 devType) {
	if (port < 1 || port > 16)
		return;
	u32 channel = port - 1;
	configPort(channel, devType);
	firstRO[channel] = initChannelsRO(channel, devType);
	firstRW[channel] = initChannelsRW(channel, devType);
	firstModbusRO[channel] = initModbusRO(channel, devType);
	firstModbusRW[channel] = initModbusRW(channel, devType);
	return;
}
void respondModbus(unsigned char * message);
int main()
{
	xil_printf("Running...");
	// modbus state register
	// 0 = waiting for transmission
	// 1 = waiting for next byte
	int modbus_count = 0;
	int modbus_messagesize = 8;
	unsigned char modbus_packet[8];
	u16 crc = 0;

	char ch;
	u32 debugPtr;

	u32 x;

	u32 portStatus = 0;
	u32 portConfig = 0;
	// 0: disabled
	// 1: ready to send poll command for read-only register
	// 2: waiting for response for read-only register
	// 3: ready to write to register
	// 4: waiting for response from register write
	// 5: ready to read Modbus block from device
	// 6: waiting for response from Modbus read
	// 7: ready to write Modbus value(s) to device
	// 8: waiting for response from Modbus write

	// reset all serial ports
	for (x=0; x<16; x++) {
		config[x] = 0;
		configport(x, config[x]);
	}

	// Set up all devices here, use port number (1-16) and device type
	if (SYSTEMTYPE == 1) {
		configDevice(1, 1);		// LakeShore 218 on port 1
		configDevice(3, 4);		// Turbo pump controller on port 3
		configDevice(5, 5);		// Magnet power supply on port 5
		configDevice(7, 2);		// CV vacuum gauge on port 7
		configDevice(9, 2);		// BC vacuum gauge on port 9
		configDevice(11, 3);	// Mass flow controller on port 11
		configDevice(13, 8);	// RF Controlleron port 13
		configDevice(15, 6);	// Technalogix amplifier on port 15
		configDevice(16, 7);	// EliteRF amplifier on port 16
	}
	else if (SYSTEMTYPE == 2) {
		configDevice(1, 9);		// Alpha BCM module on port 1
	}

	for (x=0; x<16; x++) {
		nextRO[x] = firstRO[x];
		nextRW[x] = firstRW[x];
		state[x] = !(Xil_In32(cfgAddr(x)) & PORTRESET);	// set state to 1 if the port has been enabled
	}

	// read values stored in flash (configuration of each port)
//	for (x=0; x<16; x++)
//		load_config(x);

	// configure all serial ports
//	for (x=0; x<16; x++)
//		configport(x, config[x]);

//	xil_printf("\r\n>");

	while(1) {
		// if previous Modbus packet included "load_config" command, load settings from flash
//		if (Xil_In32(LOADSETTINGS)) {
//			load_config();
//			Xil_Out32(LOADSETTINGS, 0);
//		}
		// if previous Modbus packet included "save_settings" command, save settings to flash
//		if (Xil_In32(SAVESETTINGS)) {
//			flashWriteEnable();
//			flashEraseBlock(SETTING_ADDR0);
//			flashWriteEnable();
//			flashProgramPages(SETTING_ADDR0, numSettings, settings);
//			Xil_Out32(SAVESETTINGS, 0);
//		}

		// process I/O for each channel
		for (x=0; x<16; x++) {
			portStatus = Xil_In32(stsAddr(x));
			if ((portStatus & TXFULL) || (portStatus & RXFULL)) {
				xil_printf("Buffer overflow:  port %02d - state %d - port status 0x%08x\r\n", (x+1), state[x], portStatus);
				portConfig = Xil_In32(cfgAddr(x));
				Xil_Out32(cfgAddr(x), PORTRESET);
				Xil_Out32(cfgAddr(x), portConfig);
				xil_printf("After port reset: port %02d - state %d - port status 0x%08x\r\n\r\n", (x+1), state[x], portStatus);
			}
			switch (state[x]) {
				// case 0: port is disabled
				case 0:
					break;
				// start read of a read-only register (send string if necessary)
				case 1:
					// no RO registers defined, skip ahead to RW registers
					if (!firstRO[x])
						state[x] += 2;
					// only try to process the next register after the transmit interval has elapsed
					else if ((portStatus & TXINTERVAL) && ((portStatus & RXINTERVAL) || !nextRO[x] || !nextRO[x]->pollcmd)) {
						// increment to the next register
						if (!nextRO[x])
							nextRO[x] = firstRO[x];
						else
							nextRO[x] = nextRO[x]->next;
						// end of RO registers reached, move to RW registers
						if (!nextRO[x])
							state[x] += 2;
						// start the RO register read
						else {
							// empty the read buffer before sending the read command (or waiting for device that doesn't require one)
							while (!(Xil_In32(stsAddr(x)) & RXEMPTY))
								Xil_In32(rdAddr(x));
							sendString(x, nextRO[x]->pollcmd);
//							if (x == 4)
//								xil_printf("%s\r\n", nextRO[x]->pollcmd);
							serialSize[x] = 0;
							state[x]++;
						}
					}
					break;
				// channel waiting for a read response from device
				case 2:
					// if timeout reached while waiting for response, light fail LED and proceed to the next string (to avoid getting stuck)
					if ((portStatus & RXTIMEOUT) && (portStatus & TXTIMEOUT)) {
						Xil_Out32(stsAddr(x), FAILMASK);
						state[x]--;
					}
					// if character is received on serial port, process it
					// do this first in case loop time is slow and character arrives in same loop as timeout
					else if (!(portStatus & RXEMPTY)) {
						ch = Xil_In32(rdAddr(x));
//						if (x == 4)
//							xil_printf("%c", ch);
						if (serialSize[x] || (!isspace((int) ch) &&	((nextRO[x]->startchar == '\0') || (nextRO[x]->startchar == ch)))) {
							serialStr[x][serialSize[x]++] = ch;
							if (((nextRO[x]->stopchar != '\0') && (ch == nextRO[x]->stopchar)) || (serialSize[x] == nextRO[x]->maxchars)) {
								serialStr[x][serialSize[x]++] = '\0';
								serialStr[x][serialSize[x]] = '\0';
								serialStr[x][serialSize[x]+1] = '\0';
								serialStr[x][serialSize[x]+2] = '\0';
								if (checkROstr(nextRO[x], serialStr[x])) {
									Xil_Out32(stsAddr(x), OKMASK);		// turn on OK LED if string checks are ok
//									if (x == 4)
//										xil_printf("%s\r\n\r\n", serialStr[x]);
									processROstr(nextRO[x], serialStr[x]);
								}
								else {
									Xil_Out32(stsAddr(x), FAILMASK);		// turn on fail LED if string checks fail
								}
								state[x]--;
							}
						}
					}
					break;
				// processing writeable registers
				case 3:
					// no RW registers defined, skip ahead to Modbus reads
					if (!firstRW[x])
						state[x] += 2;
					// only try to process the next register after the transmit interval has elapsed
					else if ((portStatus & TXINTERVAL) && ((portStatus & RXINTERVAL) || !nextRW[x] || !nextRW[x]->maxchars)) {
						// increment to the next register
						if (!nextRW[x])
							nextRW[x] = firstRW[x];
						else
							nextRW[x] = nextRW[x]->next;
						// end of RW registers reached, move to Modbus reads
						if (!nextRW[x])
							state[x] += 2;
						// start the RW register write
						else if (checkRW(nextRW[x])) {
							updateRW(x, nextRW[x]);
							// only wait for a response if the channel device will send one
							if (nextRW[x]->maxchars) {
								serialSize[x] = 0;
								state[x]++;
							}
						}
					}
					break;
				// waiting for response from device
				case 4:
					// if timeout reached while waiting for response, light fail LED and resend command
					if ((portStatus & RXTIMEOUT) && (portStatus & TXTIMEOUT)) {
						Xil_Out32(stsAddr(x), FAILMASK);
						state[x]--;
					}
					// if character is received on serial port, process it
					// do this first in case loop time is slow and character arrives in same loop as timeout
					else if (!(portStatus & RXEMPTY)) {
						ch = Xil_In32(rdAddr(x));
						if (serialSize[x] || (!isspace((int) ch) &&	((nextRW[x]->startchar == '\0') || (nextRW[x]->startchar == ch)))) {
							serialStr[x][serialSize[x]++] = ch;
							if (((nextRW[x]->stopchar != '\0') && (ch == nextRW[x]->stopchar)) || (serialSize[x] == nextRW[x]->maxchars)) {
								serialStr[x][serialSize[x]++] = '\0';
								serialStr[x][serialSize[x]] = '\0';
								serialStr[x][serialSize[x]+1] = '\0';
								serialStr[x][serialSize[x]+2] = '\0';
								Xil_Out32(stsAddr(x), checkResponse(nextRW[x], serialStr[x]) ? OKMASK : FAILMASK);
								state[x]--;
							}
						}
					}
					break;
				// start read of Modbus block
				case 5:
					// no RO Modbus registers defined, skip ahead to Modbus writes
					if (!firstModbusRO[x])
						state[x] += 2;
					// only send a Modbus read after the transmit interval has elapsed
					else if ((portStatus & TXINTERVAL) && (portStatus & RXINTERVAL)) {
						// increment to the next register
						if (!nextModbusRO[x])
							nextModbusRO[x] = firstModbusRO[x];
						else
							nextModbusRO[x] = nextModbusRO[x]->next;
						// end of RO Modbus registers reached, move to Modbus writes
						if (!nextModbusRO[x])
							state[x] += 2;
						// start the Modbus read
						else {
							sendModbusRead(x, nextModbusRO[x]);
							serialSize[x] = 0;
							state[x]++;
						}
					}
					break;
				// process received Modbus data
				case 6:
					// if timeout reached while waiting for response, light fail LED and proceed to the next string (to avoid getting stuck)
					if ((portStatus & RXTIMEOUT) && (portStatus & TXTIMEOUT)) {
						Xil_Out32(stsAddr(x), FAILMASK);
						if (x == 4)
							xil_printf("Port %d(+1): Timed out waiting for data from reg %d (%d)\r\n\r\n", x, nextModbusRO[x]->startAddr, nextModbusRO[x]->function);
						state[x]--;
					}
					// if character is received on serial port, process it
					else if (!(portStatus & RXEMPTY)) {
						ch = Xil_In32(rdAddr(x));
						if (serialSize[x] || nextModbusRO[x]->unitID == ch) {
							serialStr[x][serialSize[x]++] = ch;
						}
						// data byte count is sent in 3rd byte of response
						// total response is 3 bytes of header + data + 2 bytes CRC = data byte count + 5
						if (serialSize[x] > 3 && (u8) serialSize[x] == ((u8) serialStr[x][2]) + 5) {
							if (checkModbusRO(nextModbusRO[x], serialStr[x])) {
								Xil_Out32(stsAddr(x), OKMASK);			// turn on OK LED if string checks are ok
								processModbusRO(nextModbusRO[x], serialStr[x]);
							}
							else
								Xil_Out32(stsAddr(x), FAILMASK);		// turn on fail LED if string checks fail
							state[x]--;
						}
					}
					break;
				// process writeable Modbus registers
				case 7:
					// no RW Modbus registers defined, skip ahead to RO registers
					if (!firstModbusRW[x])
						state[x] += 2;
					// only send a Modbus write after the transmit interval has elapsed
					if ((portStatus & TXINTERVAL) && (portStatus & RXINTERVAL)) {
						// increment to the next register
						if (!nextModbusRW[x])
							nextModbusRW[x] = firstModbusRW[x];
						else
							nextModbusRW[x] = nextModbusRW[x]->next;
						// end of RW Modbus registers reached, move to RO registers
						if (!nextModbusRW[x])
							state[x] += 2;
						// start the Modbus write
						else if (checkModbusRW(nextModbusRW[x])) {
							sendModbusWrite(x, nextModbusRW[x]);
//							if (x == 15)
//								xil_printf("sending reset command to Elite RF amplifier\r\n\r\n");
							serialSize[x] = 0;
							state[x]++;
						}
					}
					break;
				// waiting for response from device
				case 8:
					// if timeout reached while waiting for response, light fail LED and proceed to the next string (to avoid getting stuck)
					if ((portStatus & RXTIMEOUT) && (portStatus & TXTIMEOUT)) {
						Xil_Out32(stsAddr(x), FAILMASK);
						if (x == 4)
							xil_printf("Port %d(+1): Timed out waiting for response\r\n\r\n", x);
						state[x]--;
					}
					// if character is received on serial port, process it
					else if (!(portStatus & RXEMPTY)) {
						ch = Xil_In32(rdAddr(x));
						if (serialSize[x] || nextModbusRW[x]->unitID == ch) {
							serialStr[x][serialSize[x]++] = ch;
						}
						// data byte count is always 8 (only 5, 6, 15, and 16 are implemented)
						if (serialSize[x] == 8) {
							Xil_Out32(stsAddr(x), checkModbusRWResponse(nextModbusRW[x], serialStr[x]) ? OKMASK : FAILMASK);
							state[x]--;
						}
					}
					break;
				default:
					state[x] = 1;
			}
		}

		// if character is received via debug terminal, process it
		if (debug_rcvd())
		{
			ch = debug_getchar();
			if (ch != '\0') {
				if (debugPtr==0 && isspace((int) ch)) {
					if (ch == '\r') xil_printf(">");
				}
				else if (ch == BACKSPACE) {
					if (debugPtr!=0) debugPtr--;
					xil_printf("%c", SPACE);
					xil_printf("%c", BACKSPACE);
				}
				else if (ch == CARRIAGE_RETURN) { //interpret and execute command;
					executeCommand(&str[0]);
					debugPtr = 0;
				}
				else {
					if (ch == ' ' || ch == '\t') {
						if (debugPtr == 0) {} //do nothing
						else if (str[debugPtr-1] == ' ') {} //do nothing
						else str[debugPtr++] = ' ';
					}
					else str[debugPtr++] = ch;
				}
				str[debugPtr] = '\0	';
			}
		}
		// byte received via Modbus link
		if (modbus_rcvd())
		{
			reset_modbustimer();
			if (modbus_count < modbus_messagesize)
			{
				modbus_packet[modbus_count] = modbus_getchar();
				if ((modbus_count == 6) && ((modbus_packet[1] == 15) || (modbus_packet[1] == 16)))
					modbus_messagesize = modbus_packet[6] + 9;
				if (modbus_count < modbus_messagesize - 2)
					update_crcin(modbus_packet[modbus_count]);
				modbus_count++;
			}
		}
		// If end of message, process it
		if (modbus_count == modbus_messagesize)
		{
			// check crc, if valid respond to message
			crc = (((u16) modbus_packet[modbus_messagesize-1]) << 8) | modbus_packet[modbus_messagesize-2];
			// only commands 1-6 and 15-16 are implemented so far
			// support for Modbus command 6 (write single holding register) has been removed
			if ((modbus_packet[0] > 0) && (modbus_packet[0] < 18) && (get_crcin() == crc) && (((modbus_packet[1] > 0) && (modbus_packet[1] < 7)) || (modbus_packet[1] == 15) || (modbus_packet[1] == 16))) {
				respondModbus(modbus_packet);
				Xil_Out32(PLCSTS, OKMASK);
			}
			// prepare for next message
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
		}
		// If receiving message and delay is too long between bytes, abort message
		else if ((modbus_count != modbus_messagesize) && modbus_count && (read_modbustimer() > MODBUSTIMEOUT))
		{
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
			Xil_Out32(PLCSTS, FAILMASK);
		}
	}
}
// Returns the integer portion of a 16.16 bit fixed point value for printing
u32 fixed2Int(u32 fixed, u32 shift)
{
	return fixed >> shift;
}
// Returns the fractional portion (3 significant figures) of 16.16 bit fixed point value for printing
u32 fixed2Fraction(u32 fixed, u32 shift)
{
	if (shift > 20)
		return (((fixed & (0xFFFFFFFF >> (32 - shift))) >> 10) * 1000) >> (shift - 10);
	else
		return ((fixed & (0xFFFFFFFF >> (32 - shift))) * 1000) >> shift;
}
void printFixed(u32 fixed, u32 shift)
{
	xil_printf("%D.%03D", fixed2Int(fixed, shift), fixed2Fraction(fixed, shift));
}
int getFixed(float val)
{
	u32 fixedInt, fixedFraction;
	if (val >= 65535)	return 0xFFFF0000;
	if (val < 0)		return 0x00000000;
	fixedInt = val;
	fixedFraction = (val - fixedInt) * 65536.0;
	return (fixedInt << 16) + fixedFraction;
}
void respondModbus(unsigned char * message)
{
	unsigned char i;
	unsigned char tempChar;
	u16 tempU16;
	u32 tempU32;
	u32 channel;
	int index;
	int count;
	// slave address and function
	init_crcout();
	update_crcout(message[0]);
	modbus_putchar(message[0]);
	update_crcout(message[1]);
	modbus_putchar(message[1]);
	channel = message[0];
	index = (((int) message[2]) << 8) + message[3];
	count = (((int) message[4]) << 8) + message[5];
	switch (message[1])
	{
	// read coil status
	// all coils are implemented as 1-bit wide in firmware (so this code assumes the 31 upper bits are all 0)
/*	case 1:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempChar = (tempU16 & 0x0007) ? ((tempU16 >> 3) + 1) : (tempU16 >> 3);
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bits
		tempChar = 0;
		for (i=0; i<count; i++)
		{
			tempChar >>= 1;
			if (index < numCoils)
				tempChar |= mc[((channel - 1) << 8) + index] << 7;
			if ((i & 7) == 7)
			{
				update_crcout(tempChar);
				modbus_putchar(tempChar);
				tempChar = 0;
			}
		}
		i &= 7;
		if (i)
		{
			tempChar >>= 8 - i;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;*/
	// read input status
	// all inputs are implemented as 1-bit wide in firmware (so this code assumes the 31 upper bits are all 0)
/*	case 2:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempChar = (tempU16 & 0x0007) ? ((tempU16 >> 3) + 1) : (tempU16 >> 3);
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bits
		tempChar = 0;
		for (i=0; i<count; i++)
		{
			tempChar >>= 1;
			if (index < numCoils)
				tempChar |=mi[((channel - 1) << 8) + index] << 7;
			if ((i & 7) == 7)
			{
				update_crcout(tempChar);
				modbus_putchar(tempChar);
				tempChar = 0;
			}
		}
		i &= 7;
		if (i)
		{
			tempChar >>= 8 - i;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;*/
	// read holding registers
	// 32-bit registers have the most significant bit of their address set to 1:
	// when accessing these registers, that bit must be set back to 0 and the data shifted right by 16 bits
	case 3:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempU16 <<= 1;
		tempChar = (tempU16 > 254) ? 254 : tempU16;
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bytes
		for (i=0; i<count; i++)
		{
			tempU32 = (index < numHoldingRegs) ? mhr[((channel - 1) << 7) + (index >> 1)] : 0;
			tempU16 = (index & 0x00000001) ? tempU32 >> 16 : tempU32;
			tempChar = tempU16 >> 8;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			tempChar = tempU16;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			index++;
		}
		break;
	// read input registers
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted right by 16 bits
	// reads from device ID 0 read port status registers
	case 4:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempU16 <<= 1;
		tempChar = (tempU16 > 254) ? 254 : tempU16;
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bytes
		for (i=0; i<count; i++)
		{
			if (channel > 0 && channel < 17)
				tempU32 = (index < numInputRegs) ? mir[((channel - 1) << 7) + (index >> 1)] : 0;
			else
				tempU32 = (index < 32) ? Xil_In32(stsAddr(index >> 1)) : 0;
			tempU16 = (index & 0x00000001) ? tempU32 >> 16 : tempU32;
			tempChar = tempU16 >> 8;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			tempChar = tempU16;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			index++;
		}
		break;
	// force single coil
	// all coils are implemented as 1-bit wide in firmware, so read-modify-write is not necessary
/*	case 5:
		tempU16 = (((int) message[4]) << 8) + message[5];
		// if command is valid, set the coil
		if (index < numCoils)
		{
			if (tempU16 == 0xFF00)
				mc[((channel - 1) << 8) + index] = 1;
			else if (tempU16 == 0x0000)
				mc[((channel - 1) << 8) + index] = 0;
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;*/
	// preset single register
	// all registers are 32-bit in this device, so write to a single Modbus register writes one half of the register
	case 6:
		tempU32 = (((u32) message[4]) << 8) + message[5];
		if (index < numHoldingRegs)
		{
			if (index & 0x0001)
				mhr[((channel - 1) << 7) + (index >> 1)] = (mhr[((channel - 1) << 7) + (index >> 1)] & 0x0000FFFF) | (tempU32 << 16);
			else
				mhr[((channel - 1) << 7) + (index >> 1)] = (mhr[((channel - 1) << 7) + (index >> 1)] & 0xFFFF0000) | tempU32;
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;
	// force multiple coils
	// all coils are implemented as 1-bit wide in firmware, so read-modify-write is not necessary
/*	case 15:
		for (i=0; i<count; i++)
		{
			if (index < numCoils)
			{
				if ((i & 7) == 0)
					tempChar = message[7 + (i >> 3)];
				mc[((channel - 1) << 8) + index] = tempChar & 0x01;
				tempChar >>= 1;
			}
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;*/
	// preset multiple registers
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted left by 16 bits
	// read-modify-write is always used for all registers
	case 16:
		// if the start index is odd, the first write is to the upper half of a register, so do it before moving on to the 32-bit writes
		i=0;
		if ((index & 0x00000001) && (index < numHoldingRegs))
		{
			tempU32 = (((u32) message[7]) << 24) + (((u32) message[8]) << 16);
			mhr[((channel - 1) << 7) + (index >> 1)] = (mhr[((channel - 1) << 7) + (index >> 1)] & 0x0000FFFF) | tempU32;
			index++;
			i++;
		}
		// build the 32-bit value from 4 chars and write to the register pair
		for (; i+1<count; i+=2)
		{
			if (index < numHoldingRegs)
			{
				mhr[((channel - 1) << 7) + (index >> 1)] = (((u32) message[9 + (i << 1)]) << 24) + (((u32) message[10 + (i << 1)]) << 16) + (((u32) message[7 + (i << 1)]) << 8) + message[8 + (i << 1)];
				index += 2;
			}
		}
		// if the last index is even, the last write is to the lower half of a register
		if (i<count)
		{
			if (index < numHoldingRegs)
			{
				tempU32 = (((u32) message[7 + (i << 1)]) << 8) + message[8 + (i << 1)];
				mhr[((channel - 1) << 7) + (index >> 1)] = (mhr[((channel - 1) << 7) + (index >> 1)] & 0xFFFF0000) | tempU32;
			}
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;
	}
	// add crc to end of message
	tempU16 = get_crcout();
	modbus_putchar(tempU16);
	modbus_putchar(tempU16 >> 8);
}
void executeCommand(char * Str)
{
	u32 i = 0;
	u32 range = 0;
	char * ptr[5];
	u32 arg1;
	u32 arg2;

	xil_printf("%s\r\n", Str);
	ptr[i] = (char *)strtok(Str, " ");
	while ((ptr[i] != NULL) && (i<5)) {
		i = i + 1;
		ptr[i] = (char *) strtok(NULL, " ");
	}

	arg1 = strtoul(ptr[1], NULL, 0);

	if (ptr[2] != NULL) arg2 = strtoul(ptr[2], NULL, 0);

	if (strcmp(ptr[0],"r")==0) {
		if (ptr[1] != NULL) {
			if (ptr[2] == NULL)
				range = 1;
			else
				range = arg2;
			for (i=0; i<range; i++) {
				arg2 = Xil_In32(arg1);
				xil_printf("\r\n0x%08X: 0x%08X %11D", arg1, arg2, arg2);
				arg1 += 4;
			}
		}
	}
	else if (strcmp(ptr[0],"w")==0) {
		if (ptr[1] != NULL && ptr[2] != NULL) {
			Xil_Out32(arg1,arg2);
			arg2 = Xil_In32(arg1);
			xil_printf("\r\n0x%08X: 0x%08X %11D", arg1, arg2, arg2);
		}
	}
	else if (strcmp(ptr[0],"mhrw")==0) {
		if (arg1 < 2048 && ptr[2] != NULL) {
			mhr[arg1>>1] = arg2;
			arg2 = mhr[arg1>>1];
			if (arg1 & 0x00000001)
				xil_printf("\r\nMHR%D: 0x%04X", arg1, (arg2 >> 16) & 0x0000FFFF);
			else
				xil_printf("\r\nMHR%D: 0x%04X", arg1, arg2 & 0x0000FFFF);
		}
	}
	else if (strcmp(ptr[0],"mhr")==0) {
		if (ptr[1] != NULL) {
			if (ptr[2] == NULL)
				range = 1;
			else
				range = arg2;
			for (i=arg1; i<(arg1+range); i++) {
				arg2 = mhr[i>>1];
				if (i & 0x00000001)
					xil_printf("\r\nMHR%D: 0x%04X", i, (arg2 >> 16) & 0x0000FFFF);
				else
					xil_printf("\r\nMHR%D: 0x%04X", i, arg2 & 0x0000FFFF);
			}
		}
	}
	else if (strcmp(ptr[0],"mir")==0) {
		if (ptr[1] != NULL) {
			if (ptr[2] == NULL)
				range = 1;
			else
				range = arg2;
			for (i=arg1; i<(arg1+range); i++) {
				arg2 = mir[i>>1];
				if (i & 0x00000001)
					xil_printf("\r\nMIR%D: 0x%04X", i, (arg2 >> 16) & 0x0000FFFF);
				else
					xil_printf("\r\nMIR%D: 0x%04X", i, arg2 & 0x0000FFFF);
			}
		}
	}
	else if (strcmp(ptr[0], "loadsettings")==0) {
		xil_printf("\r\nSwitching to local mode to load settings");
//		load_config();
		xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
	}
	else if (strcmp(ptr[0], "savesettings")==0) {
		flashWriteEnable();
		flashEraseBlock(SETTING_ADDR0);
		flashWriteEnable();
//		flashProgramPages(SETTING_ADDR0, numSettings, settings);
		xil_printf("\r\nSettings saved to flash");
	}
	else
		xil_printf("Invalid command\r\n");
	xil_printf("\r\n>");
}
