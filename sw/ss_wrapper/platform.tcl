# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct C:\git\SerialServerA7\sw\ss_wrapper\platform.tcl
# 
# OR launch xsct and run below command.
# source C:\git\SerialServerA7\sw\ss_wrapper\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {ss_wrapper}\
-hw {C:\git\SerialServerA7\ss_wrapper.xsa}\
-out {C:/git/SerialServerA7/sw}

platform write
domain create -name {standalone_microblaze_0} -display-name {standalone_microblaze_0} -os {standalone} -proc {microblaze_0} -runtime {cpp} -arch {32-bit} -support-app {empty_application}
platform generate -domains 
platform active {ss_wrapper}
platform generate -quick
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform clean
platform generate
platform generate
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform generate
platform generate -domains standalone_microblaze_0 
platform generate -domains standalone_microblaze_0 
platform active {ss_wrapper}
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform generate
platform generate -domains standalone_microblaze_0 
platform clean
platform generate
platform clean
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform active {ss_wrapper}
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform generate
platform clean
platform generate
bsp reload
bsp config stdin "axi_uartlite_0"
bsp config ttc_select_cntr "2"
bsp config stdout "axi_uartlite_0"
bsp config zynqmp_fsbl_bsp "false"
bsp write
bsp reload
catch {bsp regenerate}
platform clean
platform clean
platform generate
bsp reload
bsp write
bsp setlib -name xilflash -ver 4.9
bsp removelib -name xilflash
bsp setlib -name xilffs -ver 4.7
bsp removelib -name xilffs
bsp reload
platform generate -domains standalone_microblaze_0 
platform clean
platform generate
platform clean
platform generate
platform clean
platform generate
platform clean
platform active {ss_wrapper}
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform clean
platform clean
platform clean
platform clean
platform config -updatehw {C:/git/SerialServerA7/ss_wrapper.xsa}
platform clean
platform generate
platform generate -domains standalone_microblaze_0 
platform generate
platform clean
platform clean
platform generate
platform generate
platform generate
platform generate
platform generate
platform generate
platform generate
