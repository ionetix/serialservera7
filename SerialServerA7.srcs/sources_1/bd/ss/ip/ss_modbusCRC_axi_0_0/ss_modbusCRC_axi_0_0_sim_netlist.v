// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Mon Feb 20 15:09:57 2023
// Host        : ION-LT-DP running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/git/serialservera7/SerialServerA7.srcs/sources_1/bd/ss/ip/ss_modbusCRC_axi_0_0/ss_modbusCRC_axi_0_0_sim_netlist.v
// Design      : ss_modbusCRC_axi_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "ss_modbusCRC_axi_0_0,modbusCRC_axi_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "modbusCRC_axi_v1_0,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module ss_modbusCRC_axi_0_0
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 64, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, NUM\n_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  ss_modbusCRC_axi_0_0_modbusCRC_axi_v1_0 inst
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata[7:0]),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "modbusCRC_axi_v1_0" *) 
module ss_modbusCRC_axi_0_0_modbusCRC_axi_v1_0
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [7:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [7:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  ss_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI modbusCRC_axi_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* ORIG_REF_NAME = "modbusCRC_axi_v1_0_S00_AXI" *) 
module ss_modbusCRC_axi_0_0_modbusCRC_axi_v1_0_S00_AXI
   (S_AXI_AWREADY,
    S_AXI_WREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_AWREADY;
  output S_AXI_WREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [7:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire busyIn;
  wire busyIn_i_1_n_0;
  wire busyOut;
  wire busyOut_i_1_n_0;
  wire [15:0]crcIn;
  wire crcIn114_out;
  wire \crcIn[0]_i_1_n_0 ;
  wire \crcIn[10]_i_1_n_0 ;
  wire \crcIn[11]_i_1_n_0 ;
  wire \crcIn[12]_i_1_n_0 ;
  wire \crcIn[13]_i_1_n_0 ;
  wire \crcIn[14]_i_1_n_0 ;
  wire \crcIn[15]_i_1_n_0 ;
  wire \crcIn[15]_i_2_n_0 ;
  wire \crcIn[15]_i_3_n_0 ;
  wire \crcIn[1]_i_1_n_0 ;
  wire \crcIn[2]_i_1_n_0 ;
  wire \crcIn[3]_i_1_n_0 ;
  wire \crcIn[4]_i_1_n_0 ;
  wire \crcIn[5]_i_1_n_0 ;
  wire \crcIn[6]_i_1_n_0 ;
  wire \crcIn[7]_i_1_n_0 ;
  wire \crcIn[8]_i_1_n_0 ;
  wire \crcIn[9]_i_1_n_0 ;
  wire [15:0]crcOut;
  wire crcOut18_out;
  wire \crcOut[0]_i_1_n_0 ;
  wire \crcOut[10]_i_1_n_0 ;
  wire \crcOut[11]_i_1_n_0 ;
  wire \crcOut[12]_i_1_n_0 ;
  wire \crcOut[13]_i_1_n_0 ;
  wire \crcOut[14]_i_1_n_0 ;
  wire \crcOut[15]_i_1_n_0 ;
  wire \crcOut[15]_i_2_n_0 ;
  wire \crcOut[15]_i_3_n_0 ;
  wire \crcOut[1]_i_1_n_0 ;
  wire \crcOut[2]_i_1_n_0 ;
  wire \crcOut[3]_i_1_n_0 ;
  wire \crcOut[4]_i_1_n_0 ;
  wire \crcOut[5]_i_1_n_0 ;
  wire \crcOut[6]_i_1_n_0 ;
  wire \crcOut[7]_i_1_n_0 ;
  wire \crcOut[8]_i_1_n_0 ;
  wire \crcOut[9]_i_1_n_0 ;
  wire [2:0]ctrIn;
  wire \ctrIn[0]_i_1_n_0 ;
  wire \ctrIn[1]_i_1_n_0 ;
  wire \ctrIn[2]_i_1_n_0 ;
  wire [2:0]ctrOut;
  wire \ctrOut[0]_i_1_n_0 ;
  wire \ctrOut[1]_i_1_n_0 ;
  wire \ctrOut[2]_i_1_n_0 ;
  wire loadSettings;
  wire loadSettings_i_1_n_0;
  wire loadSettings_i_2_n_0;
  wire p_0_in;
  wire [2:0]p_0_in_0;
  wire p_10_in;
  wire p_13_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [7:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire saveSettings;
  wire saveSettings_i_1_n_0;
  wire sel;
  wire [2:0]sel0;
  wire [7:0]slaveAddr;
  wire slaveAddr0;
  wire \slaveAddr[7]_i_2_n_0 ;
  wire \slaveAddr[7]_i_3_n_0 ;
  wire slv_reg_rden__0;
  wire timer0;
  wire \timer[0]_i_10_n_0 ;
  wire \timer[0]_i_11_n_0 ;
  wire \timer[0]_i_4_n_0 ;
  wire \timer[0]_i_5_n_0 ;
  wire \timer[0]_i_6_n_0 ;
  wire \timer[0]_i_7_n_0 ;
  wire \timer[0]_i_8_n_0 ;
  wire \timer[0]_i_9_n_0 ;
  wire [31:0]timer_reg;
  wire \timer_reg[0]_i_3_n_0 ;
  wire \timer_reg[0]_i_3_n_1 ;
  wire \timer_reg[0]_i_3_n_2 ;
  wire \timer_reg[0]_i_3_n_3 ;
  wire \timer_reg[0]_i_3_n_4 ;
  wire \timer_reg[0]_i_3_n_5 ;
  wire \timer_reg[0]_i_3_n_6 ;
  wire \timer_reg[0]_i_3_n_7 ;
  wire \timer_reg[12]_i_1_n_0 ;
  wire \timer_reg[12]_i_1_n_1 ;
  wire \timer_reg[12]_i_1_n_2 ;
  wire \timer_reg[12]_i_1_n_3 ;
  wire \timer_reg[12]_i_1_n_4 ;
  wire \timer_reg[12]_i_1_n_5 ;
  wire \timer_reg[12]_i_1_n_6 ;
  wire \timer_reg[12]_i_1_n_7 ;
  wire \timer_reg[16]_i_1_n_0 ;
  wire \timer_reg[16]_i_1_n_1 ;
  wire \timer_reg[16]_i_1_n_2 ;
  wire \timer_reg[16]_i_1_n_3 ;
  wire \timer_reg[16]_i_1_n_4 ;
  wire \timer_reg[16]_i_1_n_5 ;
  wire \timer_reg[16]_i_1_n_6 ;
  wire \timer_reg[16]_i_1_n_7 ;
  wire \timer_reg[20]_i_1_n_0 ;
  wire \timer_reg[20]_i_1_n_1 ;
  wire \timer_reg[20]_i_1_n_2 ;
  wire \timer_reg[20]_i_1_n_3 ;
  wire \timer_reg[20]_i_1_n_4 ;
  wire \timer_reg[20]_i_1_n_5 ;
  wire \timer_reg[20]_i_1_n_6 ;
  wire \timer_reg[20]_i_1_n_7 ;
  wire \timer_reg[24]_i_1_n_0 ;
  wire \timer_reg[24]_i_1_n_1 ;
  wire \timer_reg[24]_i_1_n_2 ;
  wire \timer_reg[24]_i_1_n_3 ;
  wire \timer_reg[24]_i_1_n_4 ;
  wire \timer_reg[24]_i_1_n_5 ;
  wire \timer_reg[24]_i_1_n_6 ;
  wire \timer_reg[24]_i_1_n_7 ;
  wire \timer_reg[28]_i_1_n_1 ;
  wire \timer_reg[28]_i_1_n_2 ;
  wire \timer_reg[28]_i_1_n_3 ;
  wire \timer_reg[28]_i_1_n_4 ;
  wire \timer_reg[28]_i_1_n_5 ;
  wire \timer_reg[28]_i_1_n_6 ;
  wire \timer_reg[28]_i_1_n_7 ;
  wire \timer_reg[4]_i_1_n_0 ;
  wire \timer_reg[4]_i_1_n_1 ;
  wire \timer_reg[4]_i_1_n_2 ;
  wire \timer_reg[4]_i_1_n_3 ;
  wire \timer_reg[4]_i_1_n_4 ;
  wire \timer_reg[4]_i_1_n_5 ;
  wire \timer_reg[4]_i_1_n_6 ;
  wire \timer_reg[4]_i_1_n_7 ;
  wire \timer_reg[8]_i_1_n_0 ;
  wire \timer_reg[8]_i_1_n_1 ;
  wire \timer_reg[8]_i_1_n_2 ;
  wire \timer_reg[8]_i_1_n_3 ;
  wire \timer_reg[8]_i_1_n_4 ;
  wire \timer_reg[8]_i_1_n_5 ;
  wire \timer_reg[8]_i_1_n_6 ;
  wire \timer_reg[8]_i_1_n_7 ;
  wire [3:3]\NLW_timer_reg[28]_i_1_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .R(p_0_in));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in_0[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in_0[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFBF0080)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .I4(p_0_in_0[2]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in_0[0]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in_0[1]),
        .R(p_0_in));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(p_0_in_0[2]),
        .R(p_0_in));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h55555555C0000000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_AWREADY),
        .I4(S_AXI_WREADY),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'hFFE400E4)) 
    \axi_rdata[0]_i_1 
       (.I0(sel0[1]),
        .I1(crcIn[0]),
        .I2(crcOut[0]),
        .I3(sel0[2]),
        .I4(\axi_rdata[0]_i_2_n_0 ),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hCACAFFF0CACA0F00)) 
    \axi_rdata[0]_i_2 
       (.I0(slaveAddr[0]),
        .I1(saveSettings),
        .I2(sel0[1]),
        .I3(timer_reg[0]),
        .I4(sel0[0]),
        .I5(loadSettings),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[10]_i_1 
       (.I0(crcIn[10]),
        .I1(crcOut[10]),
        .I2(timer_reg[10]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[11]_i_1 
       (.I0(crcIn[11]),
        .I1(crcOut[11]),
        .I2(timer_reg[11]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[12]_i_1 
       (.I0(crcIn[12]),
        .I1(crcOut[12]),
        .I2(timer_reg[12]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[13]_i_1 
       (.I0(crcIn[13]),
        .I1(crcOut[13]),
        .I2(timer_reg[13]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[14]_i_1 
       (.I0(crcIn[14]),
        .I1(crcOut[14]),
        .I2(timer_reg[14]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[15]_i_1 
       (.I0(crcIn[15]),
        .I1(crcOut[15]),
        .I2(timer_reg[15]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[15]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[16]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[16]),
        .O(reg_data_out[16]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[17]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[17]),
        .O(reg_data_out[17]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[18]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[18]),
        .O(reg_data_out[18]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[19]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[19]),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[1]),
        .I4(crcOut[1]),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[1]_i_2 
       (.I0(slaveAddr[1]),
        .I1(timer_reg[1]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[20]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[20]),
        .O(reg_data_out[20]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[21]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[21]),
        .O(reg_data_out[21]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[22]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[22]),
        .O(reg_data_out[22]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[23]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[23]),
        .O(reg_data_out[23]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[24]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[24]),
        .O(reg_data_out[24]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[25]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[25]),
        .O(reg_data_out[25]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[26]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[26]),
        .O(reg_data_out[26]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[27]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[27]),
        .O(reg_data_out[27]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[28]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[28]),
        .O(reg_data_out[28]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[29]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[29]),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[2]),
        .I4(crcOut[2]),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[2]_i_2 
       (.I0(slaveAddr[2]),
        .I1(timer_reg[2]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[30]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[30]),
        .O(reg_data_out[30]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \axi_rdata[31]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[2]),
        .I2(sel0[1]),
        .I3(timer_reg[31]),
        .O(reg_data_out[31]));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[3]),
        .I4(crcOut[3]),
        .O(reg_data_out[3]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[3]_i_2 
       (.I0(slaveAddr[3]),
        .I1(timer_reg[3]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[4]),
        .I4(crcOut[4]),
        .O(reg_data_out[4]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[4]_i_2 
       (.I0(slaveAddr[4]),
        .I1(timer_reg[4]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[5]),
        .I4(crcOut[5]),
        .O(reg_data_out[5]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[5]_i_2 
       (.I0(slaveAddr[5]),
        .I1(timer_reg[5]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[6]),
        .I4(crcOut[6]),
        .O(reg_data_out[6]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[6]_i_2 
       (.I0(slaveAddr[6]),
        .I1(timer_reg[6]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFAEABAA)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(crcIn[7]),
        .I4(crcOut[7]),
        .O(reg_data_out[7]));
  LUT5 #(
    .INIT(32'h0A000C00)) 
    \axi_rdata[7]_i_2 
       (.I0(slaveAddr[7]),
        .I1(timer_reg[7]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[8]_i_1 
       (.I0(crcIn[8]),
        .I1(crcOut[8]),
        .I2(timer_reg[8]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'h0000CCAA00F0CCAA)) 
    \axi_rdata[9]_i_1 
       (.I0(crcIn[9]),
        .I1(crcOut[9]),
        .I2(timer_reg[9]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .I5(sel0[0]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(p_0_in));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(p_0_in));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h08)) 
    axi_wready_i_1
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awvalid),
        .I2(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(p_0_in));
  LUT6 #(
    .INIT(64'h00000000BFAAFFAA)) 
    busyIn_i_1
       (.I0(crcIn114_out),
        .I1(ctrIn[1]),
        .I2(ctrIn[0]),
        .I3(busyIn),
        .I4(ctrIn[2]),
        .I5(\crcIn[15]_i_1_n_0 ),
        .O(busyIn_i_1_n_0));
  LUT5 #(
    .INIT(32'h00100000)) 
    busyIn_i_2
       (.I0(p_0_in_0[2]),
        .I1(busyIn),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .I4(p_13_in),
        .O(crcIn114_out));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    busyIn_i_3
       (.I0(S_AXI_WREADY),
        .I1(s00_axi_wstrb[1]),
        .I2(s00_axi_wstrb[0]),
        .O(p_13_in));
  FDRE #(
    .INIT(1'b0)) 
    busyIn_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(busyIn_i_1_n_0),
        .Q(busyIn),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000BFAAFFAA)) 
    busyOut_i_1
       (.I0(crcOut18_out),
        .I1(ctrOut[1]),
        .I2(ctrOut[0]),
        .I3(busyOut),
        .I4(ctrOut[2]),
        .I5(\crcOut[15]_i_1_n_0 ),
        .O(busyOut_i_1_n_0));
  LUT5 #(
    .INIT(32'h10000000)) 
    busyOut_i_2
       (.I0(p_0_in_0[2]),
        .I1(busyOut),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .I4(p_13_in),
        .O(crcOut18_out));
  FDRE #(
    .INIT(1'b0)) 
    busyOut_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(busyOut_i_1_n_0),
        .Q(busyOut),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h1DE2)) 
    \crcIn[0]_i_1 
       (.I0(crcIn[1]),
        .I1(crcIn114_out),
        .I2(s00_axi_wdata[0]),
        .I3(crcIn[0]),
        .O(\crcIn[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[10]_i_1 
       (.I0(crcIn[10]),
        .I1(crcIn114_out),
        .I2(crcIn[11]),
        .O(\crcIn[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[11]_i_1 
       (.I0(crcIn[11]),
        .I1(crcIn114_out),
        .I2(crcIn[12]),
        .O(\crcIn[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[12]_i_1 
       (.I0(crcIn[12]),
        .I1(crcIn114_out),
        .I2(crcIn[13]),
        .O(\crcIn[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hF606)) 
    \crcIn[13]_i_1 
       (.I0(crcIn[14]),
        .I1(crcIn[0]),
        .I2(crcIn114_out),
        .I3(crcIn[13]),
        .O(\crcIn[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[14]_i_1 
       (.I0(crcIn[14]),
        .I1(crcIn114_out),
        .I2(crcIn[15]),
        .O(\crcIn[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0100)) 
    \crcIn[15]_i_1 
       (.I0(p_0_in_0[0]),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_10_in),
        .O(\crcIn[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \crcIn[15]_i_2 
       (.I0(crcIn114_out),
        .I1(busyIn),
        .O(\crcIn[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[15]_i_3 
       (.I0(crcIn[15]),
        .I1(crcIn114_out),
        .I2(crcIn[0]),
        .O(\crcIn[15]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    \crcIn[15]_i_4 
       (.I0(S_AXI_WREADY),
        .I1(s00_axi_wstrb[0]),
        .I2(s00_axi_wstrb[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(s00_axi_wstrb[3]),
        .O(p_10_in));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[1]_i_1 
       (.I0(crcIn[2]),
        .I1(crcIn114_out),
        .I2(crcIn[1]),
        .I3(s00_axi_wdata[1]),
        .O(\crcIn[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[2]_i_1 
       (.I0(crcIn[3]),
        .I1(crcIn114_out),
        .I2(crcIn[2]),
        .I3(s00_axi_wdata[2]),
        .O(\crcIn[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[3]_i_1 
       (.I0(crcIn[4]),
        .I1(crcIn114_out),
        .I2(crcIn[3]),
        .I3(s00_axi_wdata[3]),
        .O(\crcIn[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[4]_i_1 
       (.I0(crcIn[5]),
        .I1(crcIn114_out),
        .I2(crcIn[4]),
        .I3(s00_axi_wdata[4]),
        .O(\crcIn[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[5]_i_1 
       (.I0(crcIn[6]),
        .I1(crcIn114_out),
        .I2(crcIn[5]),
        .I3(s00_axi_wdata[5]),
        .O(\crcIn[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[6]_i_1 
       (.I0(crcIn[7]),
        .I1(crcIn114_out),
        .I2(crcIn[6]),
        .I3(s00_axi_wdata[6]),
        .O(\crcIn[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcIn[7]_i_1 
       (.I0(crcIn[8]),
        .I1(crcIn114_out),
        .I2(crcIn[7]),
        .I3(s00_axi_wdata[7]),
        .O(\crcIn[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[8]_i_1 
       (.I0(crcIn[8]),
        .I1(crcIn114_out),
        .I2(crcIn[9]),
        .O(\crcIn[8]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \crcIn[9]_i_1 
       (.I0(crcIn[9]),
        .I1(crcIn114_out),
        .I2(crcIn[10]),
        .O(\crcIn[9]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[0]_i_1_n_0 ),
        .Q(crcIn[0]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[10]_i_1_n_0 ),
        .Q(crcIn[10]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[11]_i_1_n_0 ),
        .Q(crcIn[11]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[12]_i_1_n_0 ),
        .Q(crcIn[12]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[13]_i_1_n_0 ),
        .Q(crcIn[13]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[14]_i_1_n_0 ),
        .Q(crcIn[14]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[15]_i_3_n_0 ),
        .Q(crcIn[15]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[1]_i_1_n_0 ),
        .Q(crcIn[1]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[2]_i_1_n_0 ),
        .Q(crcIn[2]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[3]_i_1_n_0 ),
        .Q(crcIn[3]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[4]_i_1_n_0 ),
        .Q(crcIn[4]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[5]_i_1_n_0 ),
        .Q(crcIn[5]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[6]_i_1_n_0 ),
        .Q(crcIn[6]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[7]_i_1_n_0 ),
        .Q(crcIn[7]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[8]_i_1_n_0 ),
        .Q(crcIn[8]),
        .S(\crcIn[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcIn_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\crcIn[15]_i_2_n_0 ),
        .D(\crcIn[9]_i_1_n_0 ),
        .Q(crcIn[9]),
        .S(\crcIn[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h1DE2)) 
    \crcOut[0]_i_1 
       (.I0(crcOut[1]),
        .I1(crcOut18_out),
        .I2(s00_axi_wdata[0]),
        .I3(crcOut[0]),
        .O(\crcOut[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[10]_i_1 
       (.I0(crcOut[10]),
        .I1(crcOut18_out),
        .I2(crcOut[11]),
        .O(\crcOut[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[11]_i_1 
       (.I0(crcOut[11]),
        .I1(crcOut18_out),
        .I2(crcOut[12]),
        .O(\crcOut[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[12]_i_1 
       (.I0(crcOut[12]),
        .I1(crcOut18_out),
        .I2(crcOut[13]),
        .O(\crcOut[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hF606)) 
    \crcOut[13]_i_1 
       (.I0(crcOut[14]),
        .I1(crcOut[0]),
        .I2(crcOut18_out),
        .I3(crcOut[13]),
        .O(\crcOut[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[14]_i_1 
       (.I0(crcOut[14]),
        .I1(crcOut18_out),
        .I2(crcOut[15]),
        .O(\crcOut[14]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \crcOut[15]_i_1 
       (.I0(p_0_in_0[0]),
        .I1(p_0_in_0[1]),
        .I2(p_0_in_0[2]),
        .I3(p_10_in),
        .O(\crcOut[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \crcOut[15]_i_2 
       (.I0(crcOut18_out),
        .I1(busyOut),
        .O(\crcOut[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[15]_i_3 
       (.I0(crcOut[15]),
        .I1(crcOut18_out),
        .I2(crcOut[0]),
        .O(\crcOut[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[1]_i_1 
       (.I0(crcOut[2]),
        .I1(crcOut18_out),
        .I2(crcOut[1]),
        .I3(s00_axi_wdata[1]),
        .O(\crcOut[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[2]_i_1 
       (.I0(crcOut[3]),
        .I1(crcOut18_out),
        .I2(crcOut[2]),
        .I3(s00_axi_wdata[2]),
        .O(\crcOut[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[3]_i_1 
       (.I0(crcOut[4]),
        .I1(crcOut18_out),
        .I2(crcOut[3]),
        .I3(s00_axi_wdata[3]),
        .O(\crcOut[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[4]_i_1 
       (.I0(crcOut[5]),
        .I1(crcOut18_out),
        .I2(crcOut[4]),
        .I3(s00_axi_wdata[4]),
        .O(\crcOut[4]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[5]_i_1 
       (.I0(crcOut[6]),
        .I1(crcOut18_out),
        .I2(crcOut[5]),
        .I3(s00_axi_wdata[5]),
        .O(\crcOut[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[6]_i_1 
       (.I0(crcOut[7]),
        .I1(crcOut18_out),
        .I2(crcOut[6]),
        .I3(s00_axi_wdata[6]),
        .O(\crcOut[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h2EE2)) 
    \crcOut[7]_i_1 
       (.I0(crcOut[8]),
        .I1(crcOut18_out),
        .I2(crcOut[7]),
        .I3(s00_axi_wdata[7]),
        .O(\crcOut[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[8]_i_1 
       (.I0(crcOut[8]),
        .I1(crcOut18_out),
        .I2(crcOut[9]),
        .O(\crcOut[8]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \crcOut[9]_i_1 
       (.I0(crcOut[9]),
        .I1(crcOut18_out),
        .I2(crcOut[10]),
        .O(\crcOut[9]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[0]_i_1_n_0 ),
        .Q(crcOut[0]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[10]_i_1_n_0 ),
        .Q(crcOut[10]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[11]_i_1_n_0 ),
        .Q(crcOut[11]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[12]_i_1_n_0 ),
        .Q(crcOut[12]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[13]_i_1_n_0 ),
        .Q(crcOut[13]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[14]_i_1_n_0 ),
        .Q(crcOut[14]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[15]_i_3_n_0 ),
        .Q(crcOut[15]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[1]_i_1_n_0 ),
        .Q(crcOut[1]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[2]_i_1_n_0 ),
        .Q(crcOut[2]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[3]_i_1_n_0 ),
        .Q(crcOut[3]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[4]_i_1_n_0 ),
        .Q(crcOut[4]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[5]_i_1_n_0 ),
        .Q(crcOut[5]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[6]_i_1_n_0 ),
        .Q(crcOut[6]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[7]_i_1_n_0 ),
        .Q(crcOut[7]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[8]_i_1_n_0 ),
        .Q(crcOut[8]),
        .S(\crcOut[15]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    \crcOut_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\crcOut[15]_i_2_n_0 ),
        .D(\crcOut[9]_i_1_n_0 ),
        .Q(crcOut[9]),
        .S(\crcOut[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0006)) 
    \ctrIn[0]_i_1 
       (.I0(ctrIn[0]),
        .I1(busyIn),
        .I2(crcIn114_out),
        .I3(\crcIn[15]_i_1_n_0 ),
        .O(\ctrIn[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0000006A)) 
    \ctrIn[1]_i_1 
       (.I0(ctrIn[1]),
        .I1(busyIn),
        .I2(ctrIn[0]),
        .I3(crcIn114_out),
        .I4(\crcIn[15]_i_1_n_0 ),
        .O(\ctrIn[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000006AAA)) 
    \ctrIn[2]_i_1 
       (.I0(ctrIn[2]),
        .I1(busyIn),
        .I2(ctrIn[1]),
        .I3(ctrIn[0]),
        .I4(crcIn114_out),
        .I5(\crcIn[15]_i_1_n_0 ),
        .O(\ctrIn[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[0]_i_1_n_0 ),
        .Q(ctrIn[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[1]_i_1_n_0 ),
        .Q(ctrIn[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrIn_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrIn[2]_i_1_n_0 ),
        .Q(ctrIn[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0006)) 
    \ctrOut[0]_i_1 
       (.I0(ctrOut[0]),
        .I1(busyOut),
        .I2(crcOut18_out),
        .I3(\crcOut[15]_i_1_n_0 ),
        .O(\ctrOut[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h0000006A)) 
    \ctrOut[1]_i_1 
       (.I0(ctrOut[1]),
        .I1(busyOut),
        .I2(ctrOut[0]),
        .I3(crcOut18_out),
        .I4(\crcOut[15]_i_1_n_0 ),
        .O(\ctrOut[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000006AAA)) 
    \ctrOut[2]_i_1 
       (.I0(ctrOut[2]),
        .I1(busyOut),
        .I2(ctrOut[1]),
        .I3(ctrOut[0]),
        .I4(crcOut18_out),
        .I5(\crcOut[15]_i_1_n_0 ),
        .O(\ctrOut[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[0]_i_1_n_0 ),
        .Q(ctrOut[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[1]_i_1_n_0 ),
        .Q(ctrOut[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ctrOut_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\ctrOut[2]_i_1_n_0 ),
        .Q(ctrOut[2]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    loadSettings_i_1
       (.I0(s00_axi_wdata[0]),
        .I1(p_0_in_0[0]),
        .I2(p_0_in_0[1]),
        .I3(p_0_in_0[2]),
        .I4(loadSettings_i_2_n_0),
        .I5(loadSettings),
        .O(loadSettings_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    loadSettings_i_2
       (.I0(s00_axi_wstrb[0]),
        .I1(S_AXI_WREADY),
        .O(loadSettings_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    loadSettings_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(loadSettings_i_1_n_0),
        .Q(loadSettings),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    saveSettings_i_1
       (.I0(s00_axi_wdata[0]),
        .I1(p_0_in_0[0]),
        .I2(p_0_in_0[1]),
        .I3(p_0_in_0[2]),
        .I4(loadSettings_i_2_n_0),
        .I5(saveSettings),
        .O(saveSettings_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    saveSettings_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(saveSettings_i_1_n_0),
        .Q(saveSettings),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000080)) 
    \slaveAddr[7]_i_1 
       (.I0(p_0_in_0[2]),
        .I1(S_AXI_WREADY),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .I4(\slaveAddr[7]_i_2_n_0 ),
        .O(slaveAddr0));
  LUT6 #(
    .INIT(64'h8000000380000000)) 
    \slaveAddr[7]_i_2 
       (.I0(s00_axi_wdata[7]),
        .I1(s00_axi_wdata[4]),
        .I2(s00_axi_wdata[3]),
        .I3(s00_axi_wdata[6]),
        .I4(s00_axi_wdata[5]),
        .I5(\slaveAddr[7]_i_3_n_0 ),
        .O(\slaveAddr[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \slaveAddr[7]_i_3 
       (.I0(s00_axi_wdata[2]),
        .I1(s00_axi_wdata[1]),
        .I2(s00_axi_wdata[0]),
        .I3(s00_axi_wdata[7]),
        .O(\slaveAddr[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b1)) 
    \slaveAddr_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[0]),
        .Q(slaveAddr[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[1]),
        .Q(slaveAddr[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[2]),
        .Q(slaveAddr[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[3]),
        .Q(slaveAddr[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[4]),
        .Q(slaveAddr[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[5]),
        .Q(slaveAddr[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[6]),
        .Q(slaveAddr[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \slaveAddr_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slaveAddr0),
        .D(s00_axi_wdata[7]),
        .Q(slaveAddr[7]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h08)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden__0));
  LUT4 #(
    .INIT(16'h0400)) 
    \timer[0]_i_1 
       (.I0(p_0_in_0[0]),
        .I1(p_0_in_0[2]),
        .I2(p_0_in_0[1]),
        .I3(p_10_in),
        .O(timer0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_10 
       (.I0(timer_reg[15]),
        .I1(timer_reg[14]),
        .I2(timer_reg[13]),
        .I3(timer_reg[12]),
        .O(\timer[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_11 
       (.I0(timer_reg[11]),
        .I1(timer_reg[10]),
        .I2(timer_reg[9]),
        .I3(timer_reg[8]),
        .O(\timer[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFFFFFFFFFF)) 
    \timer[0]_i_2 
       (.I0(\timer[0]_i_4_n_0 ),
        .I1(\timer[0]_i_5_n_0 ),
        .I2(timer_reg[25]),
        .I3(timer_reg[24]),
        .I4(timer_reg[23]),
        .I5(timer_reg[22]),
        .O(sel));
  LUT5 #(
    .INIT(32'hBFFFFFFF)) 
    \timer[0]_i_4 
       (.I0(\timer[0]_i_7_n_0 ),
        .I1(timer_reg[26]),
        .I2(timer_reg[27]),
        .I3(timer_reg[28]),
        .I4(timer_reg[29]),
        .O(\timer[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF7)) 
    \timer[0]_i_5 
       (.I0(timer_reg[17]),
        .I1(timer_reg[16]),
        .I2(\timer[0]_i_8_n_0 ),
        .I3(\timer[0]_i_9_n_0 ),
        .I4(\timer[0]_i_10_n_0 ),
        .I5(\timer[0]_i_11_n_0 ),
        .O(\timer[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \timer[0]_i_6 
       (.I0(timer_reg[0]),
        .O(\timer[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \timer[0]_i_7 
       (.I0(timer_reg[18]),
        .I1(timer_reg[19]),
        .I2(timer_reg[20]),
        .I3(timer_reg[21]),
        .I4(timer_reg[31]),
        .I5(timer_reg[30]),
        .O(\timer[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_8 
       (.I0(timer_reg[7]),
        .I1(timer_reg[6]),
        .I2(timer_reg[5]),
        .I3(timer_reg[4]),
        .O(\timer[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \timer[0]_i_9 
       (.I0(timer_reg[3]),
        .I1(timer_reg[2]),
        .I2(timer_reg[1]),
        .I3(timer_reg[0]),
        .O(\timer[0]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[0] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_7 ),
        .Q(timer_reg[0]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\timer_reg[0]_i_3_n_0 ,\timer_reg[0]_i_3_n_1 ,\timer_reg[0]_i_3_n_2 ,\timer_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\timer_reg[0]_i_3_n_4 ,\timer_reg[0]_i_3_n_5 ,\timer_reg[0]_i_3_n_6 ,\timer_reg[0]_i_3_n_7 }),
        .S({timer_reg[3:1],\timer[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[10] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_5 ),
        .Q(timer_reg[10]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[11] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_4 ),
        .Q(timer_reg[11]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[12] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_7 ),
        .Q(timer_reg[12]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[12]_i_1 
       (.CI(\timer_reg[8]_i_1_n_0 ),
        .CO({\timer_reg[12]_i_1_n_0 ,\timer_reg[12]_i_1_n_1 ,\timer_reg[12]_i_1_n_2 ,\timer_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[12]_i_1_n_4 ,\timer_reg[12]_i_1_n_5 ,\timer_reg[12]_i_1_n_6 ,\timer_reg[12]_i_1_n_7 }),
        .S(timer_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[13] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_6 ),
        .Q(timer_reg[13]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[14] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_5 ),
        .Q(timer_reg[14]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[15] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[12]_i_1_n_4 ),
        .Q(timer_reg[15]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[16] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_7 ),
        .Q(timer_reg[16]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[16]_i_1 
       (.CI(\timer_reg[12]_i_1_n_0 ),
        .CO({\timer_reg[16]_i_1_n_0 ,\timer_reg[16]_i_1_n_1 ,\timer_reg[16]_i_1_n_2 ,\timer_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[16]_i_1_n_4 ,\timer_reg[16]_i_1_n_5 ,\timer_reg[16]_i_1_n_6 ,\timer_reg[16]_i_1_n_7 }),
        .S(timer_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[17] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_6 ),
        .Q(timer_reg[17]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[18] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_5 ),
        .Q(timer_reg[18]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[19] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[16]_i_1_n_4 ),
        .Q(timer_reg[19]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[1] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_6 ),
        .Q(timer_reg[1]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[20] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_7 ),
        .Q(timer_reg[20]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[20]_i_1 
       (.CI(\timer_reg[16]_i_1_n_0 ),
        .CO({\timer_reg[20]_i_1_n_0 ,\timer_reg[20]_i_1_n_1 ,\timer_reg[20]_i_1_n_2 ,\timer_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[20]_i_1_n_4 ,\timer_reg[20]_i_1_n_5 ,\timer_reg[20]_i_1_n_6 ,\timer_reg[20]_i_1_n_7 }),
        .S(timer_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[21] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_6 ),
        .Q(timer_reg[21]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[22] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_5 ),
        .Q(timer_reg[22]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[23] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[20]_i_1_n_4 ),
        .Q(timer_reg[23]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[24] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_7 ),
        .Q(timer_reg[24]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[24]_i_1 
       (.CI(\timer_reg[20]_i_1_n_0 ),
        .CO({\timer_reg[24]_i_1_n_0 ,\timer_reg[24]_i_1_n_1 ,\timer_reg[24]_i_1_n_2 ,\timer_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[24]_i_1_n_4 ,\timer_reg[24]_i_1_n_5 ,\timer_reg[24]_i_1_n_6 ,\timer_reg[24]_i_1_n_7 }),
        .S(timer_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[25] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_6 ),
        .Q(timer_reg[25]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[26] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_5 ),
        .Q(timer_reg[26]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[27] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[24]_i_1_n_4 ),
        .Q(timer_reg[27]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[28] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_7 ),
        .Q(timer_reg[28]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[28]_i_1 
       (.CI(\timer_reg[24]_i_1_n_0 ),
        .CO({\NLW_timer_reg[28]_i_1_CO_UNCONNECTED [3],\timer_reg[28]_i_1_n_1 ,\timer_reg[28]_i_1_n_2 ,\timer_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[28]_i_1_n_4 ,\timer_reg[28]_i_1_n_5 ,\timer_reg[28]_i_1_n_6 ,\timer_reg[28]_i_1_n_7 }),
        .S(timer_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[29] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_6 ),
        .Q(timer_reg[29]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[2] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_5 ),
        .Q(timer_reg[2]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[30] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_5 ),
        .Q(timer_reg[30]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[31] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[28]_i_1_n_4 ),
        .Q(timer_reg[31]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[3] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[0]_i_3_n_4 ),
        .Q(timer_reg[3]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[4] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_7 ),
        .Q(timer_reg[4]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[4]_i_1 
       (.CI(\timer_reg[0]_i_3_n_0 ),
        .CO({\timer_reg[4]_i_1_n_0 ,\timer_reg[4]_i_1_n_1 ,\timer_reg[4]_i_1_n_2 ,\timer_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[4]_i_1_n_4 ,\timer_reg[4]_i_1_n_5 ,\timer_reg[4]_i_1_n_6 ,\timer_reg[4]_i_1_n_7 }),
        .S(timer_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[5] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_6 ),
        .Q(timer_reg[5]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[6] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_5 ),
        .Q(timer_reg[6]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[7] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[4]_i_1_n_4 ),
        .Q(timer_reg[7]),
        .R(timer0));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[8] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_7 ),
        .Q(timer_reg[8]),
        .R(timer0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \timer_reg[8]_i_1 
       (.CI(\timer_reg[4]_i_1_n_0 ),
        .CO({\timer_reg[8]_i_1_n_0 ,\timer_reg[8]_i_1_n_1 ,\timer_reg[8]_i_1_n_2 ,\timer_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\timer_reg[8]_i_1_n_4 ,\timer_reg[8]_i_1_n_5 ,\timer_reg[8]_i_1_n_6 ,\timer_reg[8]_i_1_n_7 }),
        .S(timer_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \timer_reg[9] 
       (.C(s00_axi_aclk),
        .CE(sel),
        .D(\timer_reg[8]_i_1_n_6 ),
        .Q(timer_reg[9]),
        .R(timer0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
