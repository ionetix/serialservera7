`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    ledDriver
// Description: 	Serial driver for LEDs
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module ledDriver(
	input clk,
  input flash,
  input [15:0] acty,
  input [15:0] ok,
  input [15:0] fail,
	output reg ledClk = 0,
	output reg ledLatch = 0,
	output reg ledData = 0
);

  reg [48:0] shifter = 49'h0FFFFFFFFFFFF;
  reg [47:0] word = 0;
  reg [47:0] nextWord = 0;
	reg [1:0] state = 0;
	reg [5:0] ctr = 0;
	reg [21:0] blinkCtr = 0;
  reg [47:0] blink = 48'h000000000001;
  reg div = 0;    // used to divide the serial clock from clk/2 to clk/4
	
	always @ (posedge clk)
	begin
    div <= ~div;
    if (div)
    begin
      blinkCtr <= blinkCtr + 1'b1;
      if (~flash)
        blink <= 48'h000000000001;
      else if (&blinkCtr)
        blink <= {blink[46:0], blink[47]};
      nextWord <= flash ? 
        {blink[39:32], blink[47:40], blink[23:16], blink[31:24], blink[7:0], blink[15:8]} :
        {fail[8], fail[9], fail[10], fail[11], fail[12], fail[13], fail[14], fail[15],
        fail[0], fail[1], fail[2], fail[3], fail[4], fail[5], fail[6], fail[7],
        ok[8], ok[9], ok[10], ok[11], ok[12], ok[13], ok[14], ok[15],
        ok[0], ok[1], ok[2], ok[3], ok[4], ok[5], ok[6], ok[7],
        acty[8], acty[9], acty[10], acty[11], acty[12], acty[13], acty[14], acty[15],
        acty[0], acty[1], acty[2], acty[3], acty[4], acty[5], acty[6], acty[7]};
      case (state)
      // wait for LED status change
      0:
      begin
        shifter <= {1'b0, nextWord};
        word <= nextWord;
        ctr <= 0;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 0;
        if (word != nextWord)
          state <= state + 1;
      end
      // shift out new LED status
      1:
      begin
        ledClk <= ~ledClk;
        ledData <= shifter[48];
        ledLatch <= 0;
        if (~ledClk)
        begin
          shifter <= {shifter[47:0], 1'b0};
        end
        else
        begin
          ctr <= ctr + 1;
          if (ctr == 48)
            state <= state + 1;
        end
      end
      // send LED latch pulse
      2:
      begin
        ctr <= 0;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 1;
        state <= state + 1;
      end
      // wait for LED driver
      3:
      begin
        ctr <= ctr + 1;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 0;
        if (&ctr)
          state <= state + 1;
      end
      endcase
    end
	end
endmodule
