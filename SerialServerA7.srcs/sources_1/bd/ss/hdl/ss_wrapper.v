//Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
//Date        : Sat Nov 11 15:33:18 2023
//Host        : ION-LT-DP running 64-bit major release  (build 9200)
//Command     : generate_target ss_wrapper.bd
//Design      : ss_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module ss_wrapper
   (LEDS_0,
    LEDS_CLK_0,
    LEDS_DATA_0,
    LEDS_LATCH_0,
    RX_0,
    TX_0,
    clk_in1_0,
    ext_reset_in_0,
    rx_1,
    spi_rtl_0_io0_io,
    spi_rtl_0_io1_io,
    spi_rtl_0_io2_io,
    spi_rtl_0_io3_io,
    spi_rtl_0_ss_io,
    tx_1,
    uart_rtl_0_rxd,
    uart_rtl_0_txd);
  output [4:0]LEDS_0;
  output LEDS_CLK_0;
  output LEDS_DATA_0;
  output LEDS_LATCH_0;
  input [15:0]RX_0;
  output [15:0]TX_0;
  input clk_in1_0;
  input ext_reset_in_0;
  input rx_1;
  inout spi_rtl_0_io0_io;
  inout spi_rtl_0_io1_io;
  inout spi_rtl_0_io2_io;
  inout spi_rtl_0_io3_io;
  inout [0:0]spi_rtl_0_ss_io;
  output tx_1;
  input uart_rtl_0_rxd;
  output uart_rtl_0_txd;

  wire [4:0]LEDS_0;
  wire LEDS_CLK_0;
  wire LEDS_DATA_0;
  wire LEDS_LATCH_0;
  wire [15:0]RX_0;
  wire [15:0]TX_0;
  wire clk_in1_0;
  wire ext_reset_in_0;
  wire rx_1;
  wire spi_rtl_0_io0_i;
  wire spi_rtl_0_io0_io;
  wire spi_rtl_0_io0_o;
  wire spi_rtl_0_io0_t;
  wire spi_rtl_0_io1_i;
  wire spi_rtl_0_io1_io;
  wire spi_rtl_0_io1_o;
  wire spi_rtl_0_io1_t;
  wire spi_rtl_0_io2_i;
  wire spi_rtl_0_io2_io;
  wire spi_rtl_0_io2_o;
  wire spi_rtl_0_io2_t;
  wire spi_rtl_0_io3_i;
  wire spi_rtl_0_io3_io;
  wire spi_rtl_0_io3_o;
  wire spi_rtl_0_io3_t;
  wire [0:0]spi_rtl_0_ss_i_0;
  wire [0:0]spi_rtl_0_ss_io_0;
  wire [0:0]spi_rtl_0_ss_o_0;
  wire spi_rtl_0_ss_t;
  wire tx_1;
  wire uart_rtl_0_rxd;
  wire uart_rtl_0_txd;

  IOBUF spi_rtl_0_io0_iobuf
       (.I(spi_rtl_0_io0_o),
        .IO(spi_rtl_0_io0_io),
        .O(spi_rtl_0_io0_i),
        .T(spi_rtl_0_io0_t));
  IOBUF spi_rtl_0_io1_iobuf
       (.I(spi_rtl_0_io1_o),
        .IO(spi_rtl_0_io1_io),
        .O(spi_rtl_0_io1_i),
        .T(spi_rtl_0_io1_t));
  IOBUF spi_rtl_0_io2_iobuf
       (.I(spi_rtl_0_io2_o),
        .IO(spi_rtl_0_io2_io),
        .O(spi_rtl_0_io2_i),
        .T(spi_rtl_0_io2_t));
  IOBUF spi_rtl_0_io3_iobuf
       (.I(spi_rtl_0_io3_o),
        .IO(spi_rtl_0_io3_io),
        .O(spi_rtl_0_io3_i),
        .T(spi_rtl_0_io3_t));
  IOBUF spi_rtl_0_ss_iobuf_0
       (.I(spi_rtl_0_ss_o_0),
        .IO(spi_rtl_0_ss_io[0]),
        .O(spi_rtl_0_ss_i_0),
        .T(spi_rtl_0_ss_t));
  ss ss_i
       (.LEDS_0(LEDS_0),
        .LEDS_CLK_0(LEDS_CLK_0),
        .LEDS_DATA_0(LEDS_DATA_0),
        .LEDS_LATCH_0(LEDS_LATCH_0),
        .RX_0(RX_0),
        .TX_0(TX_0),
        .clk_in1_0(clk_in1_0),
        .ext_reset_in_0(ext_reset_in_0),
        .rx_1(rx_1),
        .spi_rtl_0_io0_i(spi_rtl_0_io0_i),
        .spi_rtl_0_io0_o(spi_rtl_0_io0_o),
        .spi_rtl_0_io0_t(spi_rtl_0_io0_t),
        .spi_rtl_0_io1_i(spi_rtl_0_io1_i),
        .spi_rtl_0_io1_o(spi_rtl_0_io1_o),
        .spi_rtl_0_io1_t(spi_rtl_0_io1_t),
        .spi_rtl_0_io2_i(spi_rtl_0_io2_i),
        .spi_rtl_0_io2_o(spi_rtl_0_io2_o),
        .spi_rtl_0_io2_t(spi_rtl_0_io2_t),
        .spi_rtl_0_io3_i(spi_rtl_0_io3_i),
        .spi_rtl_0_io3_o(spi_rtl_0_io3_o),
        .spi_rtl_0_io3_t(spi_rtl_0_io3_t),
        .spi_rtl_0_ss_i(spi_rtl_0_ss_i_0),
        .spi_rtl_0_ss_o(spi_rtl_0_ss_o_0),
        .spi_rtl_0_ss_t(spi_rtl_0_ss_t),
        .tx_1(tx_1),
        .uart_rtl_0_rxd(uart_rtl_0_rxd),
        .uart_rtl_0_txd(uart_rtl_0_txd));
endmodule
