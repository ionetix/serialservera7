`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/26/2022 03:27:08 PM
// Design Name: 
// Module Name: timeout
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module timeout(
    input clk,
    input reset,
    output reg t1 = 0,
    output reg t2 = 0
    );
    
    reg [28:0] timer = 0;
    
    always @ (posedge clk)
    begin
        t1 <= |timer[28:25];
        t2 <= timer[28];
        if (reset)
            timer <= 0;
        else if (~timer[28])
            timer <= timer + 1'b1;
    end
endmodule
