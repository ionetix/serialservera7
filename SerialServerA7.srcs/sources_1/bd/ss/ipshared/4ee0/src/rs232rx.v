`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:41:23 03/06/2017 
// Design Name: 
// Module Name:    rs232rx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: RS232 receive driver
//              Input baud rate is clk frequency / clkDiv
//              Reset clears the FIFO, but does not interrupt a receive in progress
//              Parity options: 0/1-no parity, 2-even, 3-odd
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rs232rx #(
		parameter ILA	= 0
    )
    (
	input clk,
	input [15:0] clkDiv,
  input sevenBits,      // 0: 8 data bits, 1: 7 data bits
	input [1:0] parity,		// 0,1: no parity, 2: even, 3:odd
	input reset,
	input rd,
	input rx,
	output [7:0] data,
	output fifoEmpty,
	output fifoFull
	);
  
	reg [16:0] clkCtr = 0;
	reg wr = 0;
	reg [3:0] state = 0;
	reg [7:0] rxDataBuf = 0;
	reg parityBit = 0;
	
	 generate
        if (ILA) begin: add_ila
            reg [14:0] ilaDiv = 0;
            reg ilaClk = 1'b0;
            always @ (posedge clk)
            begin
                ilaDiv <= ~|ilaDiv ? clkCtr[16:2] : ilaDiv - 1'b1;
                ilaClk <= ~|ilaDiv;
            end
        
            ila_0 debug_ila (
            .clk(ilaClk), // input wire clk
            .probe0(rxDataBuf), // input wire [7:0]  probe0  
            .probe1(state), // input wire [3:0]  probe1 
            .probe2(parity), // input wire [1:0]  probe2 
            .probe3(sevenBits), // input wire [0:0]  probe3 
            .probe4(reset), // input wire [0:0]  probe4 
            .probe5(rx), // input wire [0:0]  probe5 
            .probe6(parityBit), // input wire [0:0]  probe6 
            .probe7(wr) // input wire [0:0]  probe7
        );
      end
   endgenerate
	
	always @ (posedge clk)
	begin
		clkCtr <= ~|state ? {1'b0, clkDiv} : (|clkCtr ? clkCtr - 1'b1 : {clkDiv, 1'b0});
		wr <= (state == 12) & (~parity[1] | (parity[0] == parityBit));

		case (state)
			// idle state, wait for start bit
			0:
			begin
				parityBit <= 0;
				if (~rx)
					state <= state + 1'b1;
			end
			// receive start bit
			1:
			begin
				if (~|clkCtr)
					state <= state + 1'b1;
			end
			// receive data bit 0
			2:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 1
			3:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 2
			4:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 3
			5:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 4
			6:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 5
			7:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 6
			8:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 7
			9:
			begin
                if (sevenBits)
                begin
                  rxDataBuf <= {1'b0, rxDataBuf[7:1]};
                  state <= state + 1'b1;
                end
				else if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive parity bit (if enabled) or stop bit
			10:
			begin
				if (~|clkCtr)
				begin
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// wait for stop bit (in parity mode)
			11:
			begin
				if (~parity[1] | ~|clkCtr)
				begin
					state <= state + 1'b1;
				end
			end
			// write valid data to FIFO (discard if parity error) (state 12)
			default:
			begin
				state <= state + 1'b1;
			end
		endcase
	end
	
  fifo_8bit_dist dataBuffer (.clk(clk), .srst(reset), .din(rxDataBuf), .wr_en(wr),
    .rd_en(rd), .dout(data), .full(fifoFull), .empty(fifoEmpty));
//	fifo_8bit dataBuffer (.clk(clk), .srst(reset), .din(rxDataBuf), .wr_en(wr),
//		.rd_en(rd), .dout(data), .full(fifoFull), .empty(fifoEmpty));
endmodule
