`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:41:23 03/06/2017 
// Design Name: 
// Module Name:    rs232rx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: RS232 receive driver
//              Input baud rate is clk frequency / clkDiv
//              Reset clears the FIFO, but does not interrupt a receive in progress
//              Parity options: 0/1-no parity, 2-even, 3-odd
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rs232rxILA(
	input clk,
	input [15:0] clkDiv,
  input sevenBits,      // 0: 8 data bits, 1: 7 data bits
	input [1:0] parity,		// 0,1: no parity, 2: even, 3:odd
	input reset,
	input rd,
	input rx,
	output [7:0] data,
	output fifoEmpty,
	output fifoFull
	);
  
	reg [16:0] clkCtr = 0;
	reg wr = 0;
	reg [3:0] state = 0;
	reg [7:0] rxDataBuf = 0;
	reg parityBit = 0;
	
	always @ (posedge clk)
	begin
		clkCtr <= ~|state ? {1'b0, clkDiv} : (|clkCtr ? clkCtr - 1'b1 : {clkDiv, 1'b0});
		wr <= (state == 12) & (~parity[1] | (parity[0] == parityBit));

		case (state)
			// idle state, wait for start bit
			0:
			begin
				parityBit <= 0;
				if (~rx)
					state <= state + 1'b1;
			end
			// receive start bit
			1:
			begin
				if (~|clkCtr)
					state <= state + 1'b1;
			end
			// receive data bit 0
			2:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 1
			3:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 2
			4:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 3
			5:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 4
			6:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 5
			7:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 6
			8:
			begin
				if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive data bit 7
			9:
			begin
        if (sevenBits)
        begin
          rxDataBuf <= {1'b0, rxDataBuf[7:1]};
          state <= state + 1'b1;
        end
				else if (~|clkCtr)
				begin
					rxDataBuf <= {rx, rxDataBuf[7:1]};
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// receive parity bit (if enabled) or stop bit
			10:
			begin
				if (~|clkCtr)
				begin
					parityBit <= parityBit ^ rx;
					state <= state + 1'b1;
				end
			end
			// wait for stop bit (in parity mode)
			11:
			begin
				if (~parity[1] | ~|clkCtr)
				begin
					state <= state + 1'b1;
				end
			end
			// write valid data to FIFO (discard if parity error) (state 12)
			default:
			begin
				state <= state + 1'b1;
			end
		endcase
	end
	
  fifo_8bit_dist dataBuffer (.clk(clk), .srst(reset), .din(rxDataBuf), .wr_en(wr),
    .rd_en(rd), .dout(data), .full(fifoFull), .empty(fifoEmpty));
//	fifo_8bit dataBuffer (.clk(clk), .srst(reset), .din(rxDataBuf), .wr_en(wr),
//		.rd_en(rd), .dout(data), .full(fifoFull), .empty(fifoEmpty));
/*  reg [12:0] ilaClkCtr = 14'h0;
  reg ilaClk = 1'b1;
  always @ (posedge clk)
  begin
    ilaClkCtr <= |ilaClkCtr ? ilaClkCtr - 1'b1 : clkDiv[15:3];
    ilaClk <= ~ilaClk;
  end*/
  ila_0 ila_0_inst (.clk(clk), .probe0(rxDataBuf), .probe1(state), .probe2(parity),
    .probe3(sevenBits), .probe4(rx), .probe5(parityBit), .probe6(wr), .probe7(clkCtr));
endmodule
