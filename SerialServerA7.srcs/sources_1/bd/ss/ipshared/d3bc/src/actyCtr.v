`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    acty
// Description: 	Serial driver for LEDs
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module actyCtr(
	input clk,
  input rx,
  output reg acty = 0
);

  reg [22:0] ctr = 0;

	always @ (posedge clk)
	begin
    if (~rx | |ctr)
      ctr <= ctr - 1;
    acty <= |ctr;
	end
endmodule
