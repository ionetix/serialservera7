`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    07:46:27 03/06/2017 
// Design Name: 
// Module Name:    rs232tx 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: RS232 transmission driver.
//					 Output baud rate is clk frequency / clkDiv
//					 Reset clears the FIFO, but does not interrupt a transmission in progress
//					 Parity options: 0-no parity, 1-extra stop bit, 2-even, 3-odd
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rs232tx(
	input clk,
	input [15:0] clkDiv,
  input sevenBits,      // 0: 8 data bits, 1: 7 data bits
	input [1:0] parity,		// 0: no parity, 1: extra stop bit, 2: even, 3: odd
	input reset,
	input [7:0] data,
	input wr,
	output reg tx = 1,
	output fifoEmpty,
	output fifoFull
	);

	reg [15:0] clkCtr = 0;
	reg rd = 0;
	reg [3:0] state = 0;
	reg [7:0] txDataBuf = 0;
	reg parityBit = 0;

	wire [7:0] txData;

	always @ (posedge clk)
	begin
		clkCtr <= |clkCtr & |state ? clkCtr - 1'b1 : clkDiv - 1'b1;
		rd <= ~|state & ~fifoEmpty;
		
		case (state)
			// initialize registers and wait for valid data in FIFO
			0:
			begin
				tx <= 1;
				txDataBuf <= txData;
				parityBit <= 0;
				if (~fifoEmpty)
					state <= state + 1'b1;
			end
			// send start bit
			1:
			begin
				tx <= 0;
				if (~|clkCtr)
					state <= state + 1'b1;
			end
			// send data bit 0
			2:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 1
			3:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 2
			4:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 3
			5:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 4
			6:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 5
			7:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 6
			8:
			begin
				tx <= txDataBuf[0];
				if (~|clkCtr)
				begin
					txDataBuf <= {1'b0, txDataBuf[7:1]};
					parityBit <= parityBit ^ tx;
					state <= state + 1'b1;
				end
			end
			// send data bit 7
			9:
			begin
				tx <= txDataBuf[0];
                if (sevenBits)
                    state <= state + 1'b1;
                else if (~|clkCtr)
                begin
                    parityBit <= parityBit ^ tx;
                    state <= state + 1'b1;
                    end
                end
			// send parity bit (if enabled) or stop bit
			10:
			begin
				case (parity)
					2:			tx <= parityBit;
					3:			tx <= ~parityBit;
					default:	tx <= 1;
				endcase
				if (~|clkCtr)
					state <= state + 1'b1;
			end
			// send stop bit (unless it was sent in the last state)
			11:
			begin
				tx <= 1;
				if (~|parity | ~|clkCtr)
					state <= state + 1'b1;
			end
			default:
			begin
				tx <= 1;
				state <= state + 1'b1;
			end
		endcase
	end
	
  fifo_8bit_dist dataBuffer (.clk(clk), .srst(reset), .din(data), .wr_en(wr),
    .rd_en(rd), .dout(txData), .full(fifoFull), .empty(fifoEmpty));
//	fifo_8bit dataBuffer (.clk(clk), .srst(reset), .din(data), .wr_en(wr),
//		.rd_en(rd), .dout(txData), .full(fifoFull), .empty(fifoEmpty));
endmodule
