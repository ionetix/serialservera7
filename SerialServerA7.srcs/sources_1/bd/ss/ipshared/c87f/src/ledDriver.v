`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    ledDriver
// Description: 	Serial driver for LEDs
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module ledDriver(
	input clk,
  input [16:0] acty,
  input [16:0] ok,
  input [16:0] fail,
	output reg ledClk = 0,
	output reg ledLatch = 0,
	output reg ledData = 0
);

  reg [64:0] shifter = 65'h0FFFFFFFFFFFFFFFF;
  reg [63:0] word = 64'hFFFFFFFFFFFFFFFF;
  reg [63:0] nextWord = 0;
  reg [1:0] state = 0;
  reg [6:0] ctr = 0;
//  reg [22:0] testCtr = 0;
//  reg [50:0] test = 51'h1;
//  reg [63:0] test2 = 0;
  reg [1:0] div = 2'h0;    // used to divide the serial clock from clk/2 to clk/4
	
	always @ (posedge clk)
	begin
    div <= div + 1'b1;
    if (&div)
    begin
  //    testCtr <= testCtr + 1'b1;
 //     if (&testCtr)
//	  begin
//	    test <= {test[0], test[50:1]};
//		
//		test2 <= {test[50:48], 13'h0, test[30], test[35:33], test[38:36], test[41:39], test[44:42], test[47:45], test[16:15], test[20:18], test[23:21], test[26:24], test[29:27], test[32:31], test[2:0], test[5:3], test[8:6], test[11:9], test[14:12], test[17]};
//	  end
//      nextWord <= test2;
		nextWord <= {acty[16], ok[16], fail[16], 13'h0, fail[5], acty[4], ok[4], fail[4], acty[3], ok[3], fail[3], acty[2], ok[2], fail[2], acty[1], ok[1], fail[1], acty[0], ok[0], fail[0], ok[10], fail[10], acty[9], ok[9], fail[9], acty[8], ok[8], fail[8], acty[7], ok[7], fail[7], acty[6], ok[6], fail[6], acty[5], ok[5], acty[15], ok[15], fail[15], acty[14], ok[14], fail[14], acty[13], ok[13], fail[13], acty[12], ok[12], fail[12], acty[11], ok[11], fail[11], acty[10]};
	  
/*      nextWord <= {fail[8], fail[9], fail[10], fail[11], fail[12], fail[13], fail[14], fail[15],
        fail[0], fail[1], fail[2], fail[3], fail[4], fail[5], fail[6], fail[7],
        ok[8], ok[9], ok[10], ok[11], ok[12], ok[13], ok[14], ok[15],
        ok[0], ok[1], ok[2], ok[3], ok[4], ok[5], ok[6], ok[7],
        acty[8], acty[9], acty[10], acty[11], acty[12], acty[13], acty[14], acty[15],
        acty[0], acty[1], acty[2], acty[3], acty[4], acty[5], acty[6], acty[7]};*/
      case (state)
      // wait for LED status change
      0:
      begin
        shifter <= {1'b0, nextWord};
        word <= nextWord;
        ctr <= 0;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 0;
        if (word != nextWord)
          state <= state + 1;
      end
      // shift out new LED status
      1:
      begin
        ledClk <= ~ledClk;
        ledData <= shifter[64];
        ledLatch <= 0;
        if (~ledClk)
        begin
          shifter <= {shifter[63:0], 1'b0};
        end
        else
        begin
          ctr <= ctr + 1;
          if (ctr == 64)
            state <= state + 1;
        end
      end
      // send LED latch pulse
      2:
      begin
        ctr <= 0;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 1;
        state <= state + 1;
      end
      // wait for LED driver
      3:
      begin
        ctr <= ctr + 1;
        ledClk <= 0;
        ledData <= 0;
        ledLatch <= 0;
        if (&ctr)
          state <= state + 1;
      end
      endcase
    end
	end
endmodule
