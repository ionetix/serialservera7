#set_property SEVERITY {Warning} [get_drc_checks UCIO-1]
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property PULLUP true [get_ports {ext_reset_in_0}]
set_property IOSTANDARD LVCMOS33 [get_ports {*}]
set_property PACKAGE_PIN L17 [get_ports {clk_in1_0}]
set_property PACKAGE_PIN A18 [get_ports {ext_reset_in_0}]
set_property PACKAGE_PIN C15 [get_ports {rx_1}]
set_property PACKAGE_PIN K3 [get_ports {tx_1}]
set_property PULLDOWN true [get_ports {rx_1}]
set_property PULLDOWN true [get_ports {tx_1}]
set_property PACKAGE_PIN B17 [get_ports {LEDS_0[0]}]
set_property PACKAGE_PIN C17 [get_ports {LEDS_0[1]}]
set_property PACKAGE_PIN B16 [get_ports {LEDS_0[2]}]
set_property PACKAGE_PIN A17 [get_ports {LEDS_0[3]}]
set_property PACKAGE_PIN C16 [get_ports {LEDS_0[4]}]
set_property PACKAGE_PIN L1 [get_ports {LEDS_CLK_0}]
set_property PACKAGE_PIN L2 [get_ports {LEDS_DATA_0}]
set_property PACKAGE_PIN K2 [get_ports {LEDS_LATCH_0}]
set_property PACKAGE_PIN N3 [get_ports {RX_0[0]}]
set_property PACKAGE_PIN N1 [get_ports {RX_0[1]}]
set_property PACKAGE_PIN N2 [get_ports {RX_0[2]}]
set_property PACKAGE_PIN T3 [get_ports {RX_0[3]}]
set_property PACKAGE_PIN R2 [get_ports {RX_0[4]}]
set_property PACKAGE_PIN U1 [get_ports {RX_0[5]}]
set_property PACKAGE_PIN W2 [get_ports {RX_0[6]}]
set_property PACKAGE_PIN V3 [get_ports {RX_0[7]}]
set_property PACKAGE_PIN W5 [get_ports {RX_0[8]}]
set_property PACKAGE_PIN W5 [get_ports {RX_0[8]}]
set_property PACKAGE_PIN V5 [get_ports {RX_0[9]}]
set_property PACKAGE_PIN W4 [get_ports {RX_0[10]}]
set_property PACKAGE_PIN W6 [get_ports {RX_0[11]}]
set_property PACKAGE_PIN U3 [get_ports {RX_0[12]}]
set_property PACKAGE_PIN U8 [get_ports {RX_0[13]}]
set_property PACKAGE_PIN V8 [get_ports {RX_0[14]}]
set_property PACKAGE_PIN A16 [get_ports {RX_0[15]}]
set_property PACKAGE_PIN P3 [get_ports {TX_0[0]}]
set_property PACKAGE_PIN M2 [get_ports {TX_0[1]}]
set_property PACKAGE_PIN P1 [get_ports {TX_0[2]}]
set_property PACKAGE_PIN R3 [get_ports {TX_0[3]}]
set_property PACKAGE_PIN T1 [get_ports {TX_0[4]}]
set_property PACKAGE_PIN T2 [get_ports {TX_0[5]}]
set_property PACKAGE_PIN V2 [get_ports {TX_0[6]}]
set_property PACKAGE_PIN W3 [get_ports {TX_0[7]}]
set_property PACKAGE_PIN V4 [get_ports {TX_0[8]}]
set_property PACKAGE_PIN U4 [get_ports {TX_0[9]}]
set_property PACKAGE_PIN U5 [get_ports {TX_0[10]}]
set_property PACKAGE_PIN U2 [get_ports {TX_0[11]}]
set_property PACKAGE_PIN U7 [get_ports {TX_0[12]}]
set_property PACKAGE_PIN W7 [get_ports {TX_0[13]}]
set_property PACKAGE_PIN M3 [get_ports {TX_0[14]}]
set_property PACKAGE_PIN L3 [get_ports {TX_0[15]}]
#set_property PACKAGE_PIN M18 [get_ports {EMC_OUT_0_addr[0]}]
#set_property PACKAGE_PIN M19 [get_ports {EMC_OUT_0_addr[1]}]
#set_property PACKAGE_PIN K17 [get_ports {EMC_OUT_0_addr[2]}]
#set_property PACKAGE_PIN N17 [get_ports {EMC_OUT_0_addr[3]}]
#set_property PACKAGE_PIN P17 [get_ports {EMC_OUT_0_addr[4]}]
#set_property PACKAGE_PIN P18 [get_ports {EMC_OUT_0_addr[5]}]
#set_property PACKAGE_PIN R18 [get_ports {EMC_OUT_0_addr[6]}]
#set_property PACKAGE_PIN W19 [get_ports {EMC_OUT_0_addr[7]}]
#set_property PACKAGE_PIN U19 [get_ports {EMC_OUT_0_addr[8]}]
#set_property PACKAGE_PIN V19 [get_ports {EMC_OUT_0_addr[9]}]
#set_property PACKAGE_PIN W18 [get_ports {EMC_OUT_0_addr[10]}]
#set_property PACKAGE_PIN T17 [get_ports {EMC_OUT_0_addr[11]}]
#set_property PACKAGE_PIN T18 [get_ports {EMC_OUT_0_addr[12]}]
#set_property PACKAGE_PIN U17 [get_ports {EMC_OUT_0_addr[13]}]
#set_property PACKAGE_PIN U18 [get_ports {EMC_OUT_0_addr[14]}]
#set_property PACKAGE_PIN V16 [get_ports {EMC_OUT_0_addr[15]}]
#set_property PACKAGE_PIN W16 [get_ports {EMC_OUT_0_addr[16]}]
#set_property PACKAGE_PIN W17 [get_ports {EMC_OUT_0_addr[17]}]
#set_property PACKAGE_PIN V15 [get_ports {EMC_OUT_0_addr[18]}]
#set_property PACKAGE_PIN N19 [get_ports {EMC_OUT_0_ce[0]}]
#set_property PACKAGE_PIN W15 [get_ports {EMC_OUT_0_dq_io_tri_io[0]}]
#set_property PACKAGE_PIN W13 [get_ports {EMC_OUT_0_dq_io_tri_io[1]}]
#set_property PACKAGE_PIN W14 [get_ports {EMC_OUT_0_dq_io_tri_io[2]}]
#set_property PACKAGE_PIN U15 [get_ports {EMC_OUT_0_dq_io_tri_io[3]}]
#set_property PACKAGE_PIN U16 [get_ports {EMC_OUT_0_dq_io_tri_io[4]}]
#set_property PACKAGE_PIN V13 [get_ports {EMC_OUT_0_dq_io_tri_io[5]}]
#set_property PACKAGE_PIN V14 [get_ports {EMC_OUT_0_dq_io_tri_io[6]}]
#set_property PACKAGE_PIN U14 [get_ports {EMC_OUT_0_dq_io_tri_io[7]}]
#set_property PACKAGE_PIN P19 [get_ports {EMC_OUT_0_oen[0]}]
#set_property PACKAGE_PIN R19 [get_ports {EMC_OUT_0_wen}]
set_property PACKAGE_PIN J17 [get_ports {uart_rtl_0_rxd}]
set_property PACKAGE_PIN J18 [get_ports {uart_rtl_0_txd}]
set_property PACKAGE_PIN D18 [get_ports {spi_rtl_0_io0_io}]
set_property PACKAGE_PIN D19 [get_ports {spi_rtl_0_io1_io}]
set_property PACKAGE_PIN G18 [get_ports {spi_rtl_0_io2_io}]
set_property PACKAGE_PIN F18 [get_ports {spi_rtl_0_io3_io}]
set_property PACKAGE_PIN K19 [get_ports {spi_rtl_0_ss_io[0]}]
