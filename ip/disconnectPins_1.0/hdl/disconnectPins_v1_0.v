
`timescale 1 ns / 1 ps

	module disconnectPins_v1_0 (
		// Users to add ports here
        (* X_INTERFACE_INFO = "xilinx.com:interface:gpio:1.0 iobuf TRI_T" *)
        output [7:0] tri_t, // Tristate output enable signal (optional)
        (* X_INTERFACE_INFO = "xilinx.com:interface:gpio:1.0 iobuf TRI_O" *)
        output [7:0] tri_o, // Tristate output signal (optional)
        (* X_INTERFACE_INFO = "xilinx.com:interface:gpio:1.0 iobuf TRI_I" *)
        input [7:0] tri_i, // Tristate input signal (optional)
   
        output [7:0] emc_i,
        input [7:0] emc_o,
        input [7:0] emc_t

 /*     (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN DQ_I" *)
      output [7:0] IN_dq_i, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN DQ_O" *)
      input [7:0] IN_dq_o, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN DQ_T" *)
      input [7:0] IN_dq_t, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN ADDR" *)
      input [31:0] IN_addr, // external memory address signal (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN CE" *)
      input [0:0] IN_ce, // Active high chip enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN CE_N" *)
      input [0:0] IN_ce_n, // Active low chip enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN OEN" *)
      input [0:0] IN_oen, // Outut enable signals (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN WEN" *)
      input [0:0] IN_wen, // Write enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN BEN" *)
      input [0:0] IN_ben, // byte enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN QWEN" *)
      input [0:0] IN_qwen, // Quad word enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN RPN" *)
      input IN_rpn, // Reset or power down signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN ADV_LDN" *)
      input IN_adv_ldn, // Active low address valid signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN LBON" *)
      input IN_lbon, // interleaved burst ordering (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN CLKEN" *)
      input IN_clken, // clock enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN RNW" *)
      input IN_rnw, // Read or write signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN CRE" *)
      input IN_cre, // command sequence configuration of RSRAM (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_IN WAIT" *)
      output [0:0] IN_wait, // wait signal from memory (optional)
			
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT DQ_I" *)
      input [7:0] OUT_dq_i, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT DQ_O" *)
      output [7:0] OUT_dq_o, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT DQ_T" *)
      output [7:0] OUT_dq_t, // Data signal from external memory (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT ADDR" *)
      output [18:0] OUT_addr, // external memory address signal (required)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT CE" *)
      output [0:0] OUT_ce, // Active high chip enable signal (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT OEN" *)
      output [0:0] OUT_oen, // Outut enable signals (optional)
      (* X_INTERFACE_INFO = "xilinx.com:interface:emc:1.0 EMC_OUT WEN" *)
      output [0:0] OUT_wen // Write enable signal (optional)*/
		// User ports ends
	);
	// Add user logic here
	assign tri_t = emc_t;
	assign tri_o = emc_o;
	assign emc_i = tri_i; 
	
	
/*	assign IN_dq_i = OUT_dq_i;
	assign OUT_dq_o = IN_dq_o;
	assign OUT_dq_t = IN_dq_t;
	assign OUT_addr = IN_addr[18:0];
	assign OUT_ce = IN_ce;
	assign OUT_oen = IN_oen;
	assign OUT_wen = IN_wen;
	
	assign IN_wait = 1'b0;*/
	// User logic ends

	endmodule
