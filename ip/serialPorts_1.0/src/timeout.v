`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/26/2022 03:27:08 PM
// Design Name: 
// Module Name: timeout
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module timeout(
    input clk,
    input [15:0] clkDiv,
    input reset,
    output reg t1 = 0,  // minimum TX inter-frame delay
    output reg t2 = 0   // RX timeout
    );
    
    reg [23:0] timer1 = 0;
//    reg [25:0] timer1 = 0;
    reg [27:0] timer2 = 0;
    
    always @ (posedge clk)
    begin
        if (reset)
            timer1 <= 0;
        else if (~timer1[23])
            timer1 <= timer1 + 1'b1;
        // testing with 4x the original interval between transmissions to try to solve mass flow controller problem
        t1 <= timer1 > {clkDiv, 8'h00} && timer1 > 75000;   // wait the greater of 750us or 6.4 * 10 * baud rate (spec is 3.5x minimum)
//        t1 <= timer1 > {clkDiv, 10'h00} && timer1 > 75000;   // wait the greater of 750us or 6.4 * 10 * baud rate (spec is 3.5x minimum)
        
        if (reset)
            timer2 <= 0;
        else if (~timer2[27])
            timer2 <= timer2 + 1'b1;
        t2 <= timer2[27];
    end
endmodule
