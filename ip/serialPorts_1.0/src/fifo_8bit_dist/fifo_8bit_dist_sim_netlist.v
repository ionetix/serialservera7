// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.1 (win64) Build 3526262 Mon Apr 18 15:48:16 MDT 2022
// Date        : Wed Jul 27 13:08:25 2022
// Host        : ION-LT-NU running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/nathan.usher/Documents/git/SerialServerA7/ip/serialPorts_1.0/src/fifo_8bit_dist/fifo_8bit_dist_sim_netlist.v
// Design      : fifo_8bit_dist
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "fifo_8bit_dist,fifo_generator_v13_2_7,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "fifo_generator_v13_2_7,Vivado 2022.1" *) 
(* NotValidForBitStream *)
module fifo_8bit_dist
   (clk,
    srst,
    din,
    wr_en,
    rd_en,
    dout,
    full,
    empty);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 core_clk CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME core_clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, INSERT_VIP 0" *) input clk;
  input srst;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_DATA" *) input [7:0]din;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE WR_EN" *) input wr_en;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_EN" *) input rd_en;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ RD_DATA" *) output [7:0]dout;
  (* x_interface_info = "xilinx.com:interface:fifo_write:1.0 FIFO_WRITE FULL" *) output full;
  (* x_interface_info = "xilinx.com:interface:fifo_read:1.0 FIFO_READ EMPTY" *) output empty;

  wire clk;
  wire [7:0]din;
  wire [7:0]dout;
  wire empty;
  wire full;
  wire rd_en;
  wire srst;
  wire wr_en;
  wire NLW_U0_almost_empty_UNCONNECTED;
  wire NLW_U0_almost_full_UNCONNECTED;
  wire NLW_U0_axi_ar_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_overflow_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_ar_prog_full_UNCONNECTED;
  wire NLW_U0_axi_ar_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_ar_underflow_UNCONNECTED;
  wire NLW_U0_axi_aw_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_overflow_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_aw_prog_full_UNCONNECTED;
  wire NLW_U0_axi_aw_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_aw_underflow_UNCONNECTED;
  wire NLW_U0_axi_b_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_overflow_UNCONNECTED;
  wire NLW_U0_axi_b_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_b_prog_full_UNCONNECTED;
  wire NLW_U0_axi_b_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_b_underflow_UNCONNECTED;
  wire NLW_U0_axi_r_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_overflow_UNCONNECTED;
  wire NLW_U0_axi_r_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_r_prog_full_UNCONNECTED;
  wire NLW_U0_axi_r_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_r_underflow_UNCONNECTED;
  wire NLW_U0_axi_w_dbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_overflow_UNCONNECTED;
  wire NLW_U0_axi_w_prog_empty_UNCONNECTED;
  wire NLW_U0_axi_w_prog_full_UNCONNECTED;
  wire NLW_U0_axi_w_sbiterr_UNCONNECTED;
  wire NLW_U0_axi_w_underflow_UNCONNECTED;
  wire NLW_U0_axis_dbiterr_UNCONNECTED;
  wire NLW_U0_axis_overflow_UNCONNECTED;
  wire NLW_U0_axis_prog_empty_UNCONNECTED;
  wire NLW_U0_axis_prog_full_UNCONNECTED;
  wire NLW_U0_axis_sbiterr_UNCONNECTED;
  wire NLW_U0_axis_underflow_UNCONNECTED;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_m_axi_arvalid_UNCONNECTED;
  wire NLW_U0_m_axi_awvalid_UNCONNECTED;
  wire NLW_U0_m_axi_bready_UNCONNECTED;
  wire NLW_U0_m_axi_rready_UNCONNECTED;
  wire NLW_U0_m_axi_wlast_UNCONNECTED;
  wire NLW_U0_m_axi_wvalid_UNCONNECTED;
  wire NLW_U0_m_axis_tlast_UNCONNECTED;
  wire NLW_U0_m_axis_tvalid_UNCONNECTED;
  wire NLW_U0_overflow_UNCONNECTED;
  wire NLW_U0_prog_empty_UNCONNECTED;
  wire NLW_U0_prog_full_UNCONNECTED;
  wire NLW_U0_rd_rst_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_s_axis_tready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire NLW_U0_underflow_UNCONNECTED;
  wire NLW_U0_valid_UNCONNECTED;
  wire NLW_U0_wr_ack_UNCONNECTED;
  wire NLW_U0_wr_rst_busy_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_ar_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_aw_wr_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_rd_data_count_UNCONNECTED;
  wire [4:0]NLW_U0_axi_b_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_r_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axi_w_wr_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_rd_data_count_UNCONNECTED;
  wire [10:0]NLW_U0_axis_wr_data_count_UNCONNECTED;
  wire [7:0]NLW_U0_data_count_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_araddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_arburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_arlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_arlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_arregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_arsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_aruser_UNCONNECTED;
  wire [31:0]NLW_U0_m_axi_awaddr_UNCONNECTED;
  wire [1:0]NLW_U0_m_axi_awburst_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awcache_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_awlen_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awlock_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awprot_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awqos_UNCONNECTED;
  wire [3:0]NLW_U0_m_axi_awregion_UNCONNECTED;
  wire [2:0]NLW_U0_m_axi_awsize_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_awuser_UNCONNECTED;
  wire [63:0]NLW_U0_m_axi_wdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wid_UNCONNECTED;
  wire [7:0]NLW_U0_m_axi_wstrb_UNCONNECTED;
  wire [0:0]NLW_U0_m_axi_wuser_UNCONNECTED;
  wire [7:0]NLW_U0_m_axis_tdata_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tdest_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tid_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tkeep_UNCONNECTED;
  wire [0:0]NLW_U0_m_axis_tstrb_UNCONNECTED;
  wire [3:0]NLW_U0_m_axis_tuser_UNCONNECTED;
  wire [7:0]NLW_U0_rd_data_count_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_buser_UNCONNECTED;
  wire [63:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;
  wire [0:0]NLW_U0_s_axi_ruser_UNCONNECTED;
  wire [7:0]NLW_U0_wr_data_count_UNCONNECTED;

  (* C_ADD_NGC_CONSTRAINT = "0" *) 
  (* C_APPLICATION_TYPE_AXIS = "0" *) 
  (* C_APPLICATION_TYPE_RACH = "0" *) 
  (* C_APPLICATION_TYPE_RDCH = "0" *) 
  (* C_APPLICATION_TYPE_WACH = "0" *) 
  (* C_APPLICATION_TYPE_WDCH = "0" *) 
  (* C_APPLICATION_TYPE_WRCH = "0" *) 
  (* C_AXIS_TDATA_WIDTH = "8" *) 
  (* C_AXIS_TDEST_WIDTH = "1" *) 
  (* C_AXIS_TID_WIDTH = "1" *) 
  (* C_AXIS_TKEEP_WIDTH = "1" *) 
  (* C_AXIS_TSTRB_WIDTH = "1" *) 
  (* C_AXIS_TUSER_WIDTH = "4" *) 
  (* C_AXIS_TYPE = "0" *) 
  (* C_AXI_ADDR_WIDTH = "32" *) 
  (* C_AXI_ARUSER_WIDTH = "1" *) 
  (* C_AXI_AWUSER_WIDTH = "1" *) 
  (* C_AXI_BUSER_WIDTH = "1" *) 
  (* C_AXI_DATA_WIDTH = "64" *) 
  (* C_AXI_ID_WIDTH = "1" *) 
  (* C_AXI_LEN_WIDTH = "8" *) 
  (* C_AXI_LOCK_WIDTH = "1" *) 
  (* C_AXI_RUSER_WIDTH = "1" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_AXI_WUSER_WIDTH = "1" *) 
  (* C_COMMON_CLOCK = "1" *) 
  (* C_COUNT_TYPE = "0" *) 
  (* C_DATA_COUNT_WIDTH = "8" *) 
  (* C_DEFAULT_VALUE = "BlankString" *) 
  (* C_DIN_WIDTH = "8" *) 
  (* C_DIN_WIDTH_AXIS = "1" *) 
  (* C_DIN_WIDTH_RACH = "32" *) 
  (* C_DIN_WIDTH_RDCH = "64" *) 
  (* C_DIN_WIDTH_WACH = "1" *) 
  (* C_DIN_WIDTH_WDCH = "64" *) 
  (* C_DIN_WIDTH_WRCH = "2" *) 
  (* C_DOUT_RST_VAL = "0" *) 
  (* C_DOUT_WIDTH = "8" *) 
  (* C_ENABLE_RLOCS = "0" *) 
  (* C_ENABLE_RST_SYNC = "1" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_ERROR_INJECTION_TYPE = "0" *) 
  (* C_ERROR_INJECTION_TYPE_AXIS = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_RDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WACH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WDCH = "0" *) 
  (* C_ERROR_INJECTION_TYPE_WRCH = "0" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_FULL_FLAGS_RST_VAL = "0" *) 
  (* C_HAS_ALMOST_EMPTY = "0" *) 
  (* C_HAS_ALMOST_FULL = "0" *) 
  (* C_HAS_AXIS_TDATA = "1" *) 
  (* C_HAS_AXIS_TDEST = "0" *) 
  (* C_HAS_AXIS_TID = "0" *) 
  (* C_HAS_AXIS_TKEEP = "0" *) 
  (* C_HAS_AXIS_TLAST = "0" *) 
  (* C_HAS_AXIS_TREADY = "1" *) 
  (* C_HAS_AXIS_TSTRB = "0" *) 
  (* C_HAS_AXIS_TUSER = "1" *) 
  (* C_HAS_AXI_ARUSER = "0" *) 
  (* C_HAS_AXI_AWUSER = "0" *) 
  (* C_HAS_AXI_BUSER = "0" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_AXI_RD_CHANNEL = "1" *) 
  (* C_HAS_AXI_RUSER = "0" *) 
  (* C_HAS_AXI_WR_CHANNEL = "1" *) 
  (* C_HAS_AXI_WUSER = "0" *) 
  (* C_HAS_BACKUP = "0" *) 
  (* C_HAS_DATA_COUNT = "0" *) 
  (* C_HAS_DATA_COUNTS_AXIS = "0" *) 
  (* C_HAS_DATA_COUNTS_RACH = "0" *) 
  (* C_HAS_DATA_COUNTS_RDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WACH = "0" *) 
  (* C_HAS_DATA_COUNTS_WDCH = "0" *) 
  (* C_HAS_DATA_COUNTS_WRCH = "0" *) 
  (* C_HAS_INT_CLK = "0" *) 
  (* C_HAS_MASTER_CE = "0" *) 
  (* C_HAS_MEMINIT_FILE = "0" *) 
  (* C_HAS_OVERFLOW = "0" *) 
  (* C_HAS_PROG_FLAGS_AXIS = "0" *) 
  (* C_HAS_PROG_FLAGS_RACH = "0" *) 
  (* C_HAS_PROG_FLAGS_RDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WACH = "0" *) 
  (* C_HAS_PROG_FLAGS_WDCH = "0" *) 
  (* C_HAS_PROG_FLAGS_WRCH = "0" *) 
  (* C_HAS_RD_DATA_COUNT = "0" *) 
  (* C_HAS_RD_RST = "0" *) 
  (* C_HAS_RST = "0" *) 
  (* C_HAS_SLAVE_CE = "0" *) 
  (* C_HAS_SRST = "1" *) 
  (* C_HAS_UNDERFLOW = "0" *) 
  (* C_HAS_VALID = "0" *) 
  (* C_HAS_WR_ACK = "0" *) 
  (* C_HAS_WR_DATA_COUNT = "0" *) 
  (* C_HAS_WR_RST = "0" *) 
  (* C_IMPLEMENTATION_TYPE = "0" *) 
  (* C_IMPLEMENTATION_TYPE_AXIS = "1" *) 
  (* C_IMPLEMENTATION_TYPE_RACH = "2" *) 
  (* C_IMPLEMENTATION_TYPE_RDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WACH = "2" *) 
  (* C_IMPLEMENTATION_TYPE_WDCH = "1" *) 
  (* C_IMPLEMENTATION_TYPE_WRCH = "2" *) 
  (* C_INIT_WR_PNTR_VAL = "0" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_MEMORY_TYPE = "2" *) 
  (* C_MIF_FILE_NAME = "BlankString" *) 
  (* C_MSGON_VAL = "1" *) 
  (* C_OPTIMIZATION_MODE = "0" *) 
  (* C_OVERFLOW_LOW = "0" *) 
  (* C_POWER_SAVING_MODE = "0" *) 
  (* C_PRELOAD_LATENCY = "0" *) 
  (* C_PRELOAD_REGS = "1" *) 
  (* C_PRIM_FIFO_TYPE = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_AXIS = "1kx18" *) 
  (* C_PRIM_FIFO_TYPE_RACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_RDCH = "1kx36" *) 
  (* C_PRIM_FIFO_TYPE_WACH = "512x36" *) 
  (* C_PRIM_FIFO_TYPE_WDCH = "1kx36" *) 
  (* C_PRIM_FIFO_TYPE_WRCH = "512x36" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL = "4" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH = "1022" *) 
  (* C_PROG_EMPTY_THRESH_NEGATE_VAL = "5" *) 
  (* C_PROG_EMPTY_TYPE = "0" *) 
  (* C_PROG_EMPTY_TYPE_AXIS = "0" *) 
  (* C_PROG_EMPTY_TYPE_RACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_RDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WACH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WDCH = "0" *) 
  (* C_PROG_EMPTY_TYPE_WRCH = "0" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL = "127" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_AXIS = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_RDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WACH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WDCH = "1023" *) 
  (* C_PROG_FULL_THRESH_ASSERT_VAL_WRCH = "1023" *) 
  (* C_PROG_FULL_THRESH_NEGATE_VAL = "126" *) 
  (* C_PROG_FULL_TYPE = "0" *) 
  (* C_PROG_FULL_TYPE_AXIS = "0" *) 
  (* C_PROG_FULL_TYPE_RACH = "0" *) 
  (* C_PROG_FULL_TYPE_RDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WACH = "0" *) 
  (* C_PROG_FULL_TYPE_WDCH = "0" *) 
  (* C_PROG_FULL_TYPE_WRCH = "0" *) 
  (* C_RACH_TYPE = "0" *) 
  (* C_RDCH_TYPE = "0" *) 
  (* C_RD_DATA_COUNT_WIDTH = "8" *) 
  (* C_RD_DEPTH = "128" *) 
  (* C_RD_FREQ = "1" *) 
  (* C_RD_PNTR_WIDTH = "7" *) 
  (* C_REG_SLICE_MODE_AXIS = "0" *) 
  (* C_REG_SLICE_MODE_RACH = "0" *) 
  (* C_REG_SLICE_MODE_RDCH = "0" *) 
  (* C_REG_SLICE_MODE_WACH = "0" *) 
  (* C_REG_SLICE_MODE_WDCH = "0" *) 
  (* C_REG_SLICE_MODE_WRCH = "0" *) 
  (* C_SELECT_XPM = "0" *) 
  (* C_SYNCHRONIZER_STAGE = "2" *) 
  (* C_UNDERFLOW_LOW = "0" *) 
  (* C_USE_COMMON_OVERFLOW = "0" *) 
  (* C_USE_COMMON_UNDERFLOW = "0" *) 
  (* C_USE_DEFAULT_SETTINGS = "0" *) 
  (* C_USE_DOUT_RST = "1" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_ECC_AXIS = "0" *) 
  (* C_USE_ECC_RACH = "0" *) 
  (* C_USE_ECC_RDCH = "0" *) 
  (* C_USE_ECC_WACH = "0" *) 
  (* C_USE_ECC_WDCH = "0" *) 
  (* C_USE_ECC_WRCH = "0" *) 
  (* C_USE_EMBEDDED_REG = "0" *) 
  (* C_USE_FIFO16_FLAGS = "0" *) 
  (* C_USE_FWFT_DATA_COUNT = "1" *) 
  (* C_USE_PIPELINE_REG = "0" *) 
  (* C_VALID_LOW = "0" *) 
  (* C_WACH_TYPE = "0" *) 
  (* C_WDCH_TYPE = "0" *) 
  (* C_WRCH_TYPE = "0" *) 
  (* C_WR_ACK_LOW = "0" *) 
  (* C_WR_DATA_COUNT_WIDTH = "8" *) 
  (* C_WR_DEPTH = "128" *) 
  (* C_WR_DEPTH_AXIS = "1024" *) 
  (* C_WR_DEPTH_RACH = "16" *) 
  (* C_WR_DEPTH_RDCH = "1024" *) 
  (* C_WR_DEPTH_WACH = "16" *) 
  (* C_WR_DEPTH_WDCH = "1024" *) 
  (* C_WR_DEPTH_WRCH = "16" *) 
  (* C_WR_FREQ = "1" *) 
  (* C_WR_PNTR_WIDTH = "7" *) 
  (* C_WR_PNTR_WIDTH_AXIS = "10" *) 
  (* C_WR_PNTR_WIDTH_RACH = "4" *) 
  (* C_WR_PNTR_WIDTH_RDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WACH = "4" *) 
  (* C_WR_PNTR_WIDTH_WDCH = "10" *) 
  (* C_WR_PNTR_WIDTH_WRCH = "4" *) 
  (* C_WR_RESPONSE_LATENCY = "1" *) 
  (* is_du_within_envelope = "true" *) 
  fifo_8bit_dist_fifo_generator_v13_2_7 U0
       (.almost_empty(NLW_U0_almost_empty_UNCONNECTED),
        .almost_full(NLW_U0_almost_full_UNCONNECTED),
        .axi_ar_data_count(NLW_U0_axi_ar_data_count_UNCONNECTED[4:0]),
        .axi_ar_dbiterr(NLW_U0_axi_ar_dbiterr_UNCONNECTED),
        .axi_ar_injectdbiterr(1'b0),
        .axi_ar_injectsbiterr(1'b0),
        .axi_ar_overflow(NLW_U0_axi_ar_overflow_UNCONNECTED),
        .axi_ar_prog_empty(NLW_U0_axi_ar_prog_empty_UNCONNECTED),
        .axi_ar_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_prog_full(NLW_U0_axi_ar_prog_full_UNCONNECTED),
        .axi_ar_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_ar_rd_data_count(NLW_U0_axi_ar_rd_data_count_UNCONNECTED[4:0]),
        .axi_ar_sbiterr(NLW_U0_axi_ar_sbiterr_UNCONNECTED),
        .axi_ar_underflow(NLW_U0_axi_ar_underflow_UNCONNECTED),
        .axi_ar_wr_data_count(NLW_U0_axi_ar_wr_data_count_UNCONNECTED[4:0]),
        .axi_aw_data_count(NLW_U0_axi_aw_data_count_UNCONNECTED[4:0]),
        .axi_aw_dbiterr(NLW_U0_axi_aw_dbiterr_UNCONNECTED),
        .axi_aw_injectdbiterr(1'b0),
        .axi_aw_injectsbiterr(1'b0),
        .axi_aw_overflow(NLW_U0_axi_aw_overflow_UNCONNECTED),
        .axi_aw_prog_empty(NLW_U0_axi_aw_prog_empty_UNCONNECTED),
        .axi_aw_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_prog_full(NLW_U0_axi_aw_prog_full_UNCONNECTED),
        .axi_aw_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_aw_rd_data_count(NLW_U0_axi_aw_rd_data_count_UNCONNECTED[4:0]),
        .axi_aw_sbiterr(NLW_U0_axi_aw_sbiterr_UNCONNECTED),
        .axi_aw_underflow(NLW_U0_axi_aw_underflow_UNCONNECTED),
        .axi_aw_wr_data_count(NLW_U0_axi_aw_wr_data_count_UNCONNECTED[4:0]),
        .axi_b_data_count(NLW_U0_axi_b_data_count_UNCONNECTED[4:0]),
        .axi_b_dbiterr(NLW_U0_axi_b_dbiterr_UNCONNECTED),
        .axi_b_injectdbiterr(1'b0),
        .axi_b_injectsbiterr(1'b0),
        .axi_b_overflow(NLW_U0_axi_b_overflow_UNCONNECTED),
        .axi_b_prog_empty(NLW_U0_axi_b_prog_empty_UNCONNECTED),
        .axi_b_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_prog_full(NLW_U0_axi_b_prog_full_UNCONNECTED),
        .axi_b_prog_full_thresh({1'b0,1'b0,1'b0,1'b0}),
        .axi_b_rd_data_count(NLW_U0_axi_b_rd_data_count_UNCONNECTED[4:0]),
        .axi_b_sbiterr(NLW_U0_axi_b_sbiterr_UNCONNECTED),
        .axi_b_underflow(NLW_U0_axi_b_underflow_UNCONNECTED),
        .axi_b_wr_data_count(NLW_U0_axi_b_wr_data_count_UNCONNECTED[4:0]),
        .axi_r_data_count(NLW_U0_axi_r_data_count_UNCONNECTED[10:0]),
        .axi_r_dbiterr(NLW_U0_axi_r_dbiterr_UNCONNECTED),
        .axi_r_injectdbiterr(1'b0),
        .axi_r_injectsbiterr(1'b0),
        .axi_r_overflow(NLW_U0_axi_r_overflow_UNCONNECTED),
        .axi_r_prog_empty(NLW_U0_axi_r_prog_empty_UNCONNECTED),
        .axi_r_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_prog_full(NLW_U0_axi_r_prog_full_UNCONNECTED),
        .axi_r_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_r_rd_data_count(NLW_U0_axi_r_rd_data_count_UNCONNECTED[10:0]),
        .axi_r_sbiterr(NLW_U0_axi_r_sbiterr_UNCONNECTED),
        .axi_r_underflow(NLW_U0_axi_r_underflow_UNCONNECTED),
        .axi_r_wr_data_count(NLW_U0_axi_r_wr_data_count_UNCONNECTED[10:0]),
        .axi_w_data_count(NLW_U0_axi_w_data_count_UNCONNECTED[10:0]),
        .axi_w_dbiterr(NLW_U0_axi_w_dbiterr_UNCONNECTED),
        .axi_w_injectdbiterr(1'b0),
        .axi_w_injectsbiterr(1'b0),
        .axi_w_overflow(NLW_U0_axi_w_overflow_UNCONNECTED),
        .axi_w_prog_empty(NLW_U0_axi_w_prog_empty_UNCONNECTED),
        .axi_w_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_prog_full(NLW_U0_axi_w_prog_full_UNCONNECTED),
        .axi_w_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axi_w_rd_data_count(NLW_U0_axi_w_rd_data_count_UNCONNECTED[10:0]),
        .axi_w_sbiterr(NLW_U0_axi_w_sbiterr_UNCONNECTED),
        .axi_w_underflow(NLW_U0_axi_w_underflow_UNCONNECTED),
        .axi_w_wr_data_count(NLW_U0_axi_w_wr_data_count_UNCONNECTED[10:0]),
        .axis_data_count(NLW_U0_axis_data_count_UNCONNECTED[10:0]),
        .axis_dbiterr(NLW_U0_axis_dbiterr_UNCONNECTED),
        .axis_injectdbiterr(1'b0),
        .axis_injectsbiterr(1'b0),
        .axis_overflow(NLW_U0_axis_overflow_UNCONNECTED),
        .axis_prog_empty(NLW_U0_axis_prog_empty_UNCONNECTED),
        .axis_prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_prog_full(NLW_U0_axis_prog_full_UNCONNECTED),
        .axis_prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .axis_rd_data_count(NLW_U0_axis_rd_data_count_UNCONNECTED[10:0]),
        .axis_sbiterr(NLW_U0_axis_sbiterr_UNCONNECTED),
        .axis_underflow(NLW_U0_axis_underflow_UNCONNECTED),
        .axis_wr_data_count(NLW_U0_axis_wr_data_count_UNCONNECTED[10:0]),
        .backup(1'b0),
        .backup_marker(1'b0),
        .clk(clk),
        .data_count(NLW_U0_data_count_UNCONNECTED[7:0]),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .din(din),
        .dout(dout),
        .empty(empty),
        .full(full),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .int_clk(1'b0),
        .m_aclk(1'b0),
        .m_aclk_en(1'b0),
        .m_axi_araddr(NLW_U0_m_axi_araddr_UNCONNECTED[31:0]),
        .m_axi_arburst(NLW_U0_m_axi_arburst_UNCONNECTED[1:0]),
        .m_axi_arcache(NLW_U0_m_axi_arcache_UNCONNECTED[3:0]),
        .m_axi_arid(NLW_U0_m_axi_arid_UNCONNECTED[0]),
        .m_axi_arlen(NLW_U0_m_axi_arlen_UNCONNECTED[7:0]),
        .m_axi_arlock(NLW_U0_m_axi_arlock_UNCONNECTED[0]),
        .m_axi_arprot(NLW_U0_m_axi_arprot_UNCONNECTED[2:0]),
        .m_axi_arqos(NLW_U0_m_axi_arqos_UNCONNECTED[3:0]),
        .m_axi_arready(1'b0),
        .m_axi_arregion(NLW_U0_m_axi_arregion_UNCONNECTED[3:0]),
        .m_axi_arsize(NLW_U0_m_axi_arsize_UNCONNECTED[2:0]),
        .m_axi_aruser(NLW_U0_m_axi_aruser_UNCONNECTED[0]),
        .m_axi_arvalid(NLW_U0_m_axi_arvalid_UNCONNECTED),
        .m_axi_awaddr(NLW_U0_m_axi_awaddr_UNCONNECTED[31:0]),
        .m_axi_awburst(NLW_U0_m_axi_awburst_UNCONNECTED[1:0]),
        .m_axi_awcache(NLW_U0_m_axi_awcache_UNCONNECTED[3:0]),
        .m_axi_awid(NLW_U0_m_axi_awid_UNCONNECTED[0]),
        .m_axi_awlen(NLW_U0_m_axi_awlen_UNCONNECTED[7:0]),
        .m_axi_awlock(NLW_U0_m_axi_awlock_UNCONNECTED[0]),
        .m_axi_awprot(NLW_U0_m_axi_awprot_UNCONNECTED[2:0]),
        .m_axi_awqos(NLW_U0_m_axi_awqos_UNCONNECTED[3:0]),
        .m_axi_awready(1'b0),
        .m_axi_awregion(NLW_U0_m_axi_awregion_UNCONNECTED[3:0]),
        .m_axi_awsize(NLW_U0_m_axi_awsize_UNCONNECTED[2:0]),
        .m_axi_awuser(NLW_U0_m_axi_awuser_UNCONNECTED[0]),
        .m_axi_awvalid(NLW_U0_m_axi_awvalid_UNCONNECTED),
        .m_axi_bid(1'b0),
        .m_axi_bready(NLW_U0_m_axi_bready_UNCONNECTED),
        .m_axi_bresp({1'b0,1'b0}),
        .m_axi_buser(1'b0),
        .m_axi_bvalid(1'b0),
        .m_axi_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .m_axi_rid(1'b0),
        .m_axi_rlast(1'b0),
        .m_axi_rready(NLW_U0_m_axi_rready_UNCONNECTED),
        .m_axi_rresp({1'b0,1'b0}),
        .m_axi_ruser(1'b0),
        .m_axi_rvalid(1'b0),
        .m_axi_wdata(NLW_U0_m_axi_wdata_UNCONNECTED[63:0]),
        .m_axi_wid(NLW_U0_m_axi_wid_UNCONNECTED[0]),
        .m_axi_wlast(NLW_U0_m_axi_wlast_UNCONNECTED),
        .m_axi_wready(1'b0),
        .m_axi_wstrb(NLW_U0_m_axi_wstrb_UNCONNECTED[7:0]),
        .m_axi_wuser(NLW_U0_m_axi_wuser_UNCONNECTED[0]),
        .m_axi_wvalid(NLW_U0_m_axi_wvalid_UNCONNECTED),
        .m_axis_tdata(NLW_U0_m_axis_tdata_UNCONNECTED[7:0]),
        .m_axis_tdest(NLW_U0_m_axis_tdest_UNCONNECTED[0]),
        .m_axis_tid(NLW_U0_m_axis_tid_UNCONNECTED[0]),
        .m_axis_tkeep(NLW_U0_m_axis_tkeep_UNCONNECTED[0]),
        .m_axis_tlast(NLW_U0_m_axis_tlast_UNCONNECTED),
        .m_axis_tready(1'b0),
        .m_axis_tstrb(NLW_U0_m_axis_tstrb_UNCONNECTED[0]),
        .m_axis_tuser(NLW_U0_m_axis_tuser_UNCONNECTED[3:0]),
        .m_axis_tvalid(NLW_U0_m_axis_tvalid_UNCONNECTED),
        .overflow(NLW_U0_overflow_UNCONNECTED),
        .prog_empty(NLW_U0_prog_empty_UNCONNECTED),
        .prog_empty_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_empty_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full(NLW_U0_prog_full_UNCONNECTED),
        .prog_full_thresh({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_assert({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .prog_full_thresh_negate({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .rd_clk(1'b0),
        .rd_data_count(NLW_U0_rd_data_count_UNCONNECTED[7:0]),
        .rd_en(rd_en),
        .rd_rst(1'b0),
        .rd_rst_busy(NLW_U0_rd_rst_busy_UNCONNECTED),
        .rst(1'b0),
        .s_aclk(1'b0),
        .s_aclk_en(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arid(1'b0),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlock(1'b0),
        .s_axi_arprot({1'b0,1'b0,1'b0}),
        .s_axi_arqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_aruser(1'b0),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awcache({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awid(1'b0),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlock(1'b0),
        .s_axi_awprot({1'b0,1'b0,1'b0}),
        .s_axi_awqos({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awregion({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awuser(1'b0),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_buser(NLW_U0_s_axi_buser_UNCONNECTED[0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[63:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_ruser(NLW_U0_s_axi_ruser_UNCONNECTED[0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wid(1'b0),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wuser(1'b0),
        .s_axi_wvalid(1'b0),
        .s_axis_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tdest(1'b0),
        .s_axis_tid(1'b0),
        .s_axis_tkeep(1'b0),
        .s_axis_tlast(1'b0),
        .s_axis_tready(NLW_U0_s_axis_tready_UNCONNECTED),
        .s_axis_tstrb(1'b0),
        .s_axis_tuser({1'b0,1'b0,1'b0,1'b0}),
        .s_axis_tvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .sleep(1'b0),
        .srst(srst),
        .underflow(NLW_U0_underflow_UNCONNECTED),
        .valid(NLW_U0_valid_UNCONNECTED),
        .wr_ack(NLW_U0_wr_ack_UNCONNECTED),
        .wr_clk(1'b0),
        .wr_data_count(NLW_U0_wr_data_count_UNCONNECTED[7:0]),
        .wr_en(wr_en),
        .wr_rst(1'b0),
        .wr_rst_busy(NLW_U0_wr_rst_busy_UNCONNECTED));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2022.1"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
h4/8v0FBgXUomE5kJVs58UlO/ao4SLHpniPXt+fomPPYB6tv3U0iBfOL5737ZNNEhgP1kkKeMvq+
VxOLW94g7JZT6mWc5ZuQ7jgK8Qpa6+1xpVVQBB6gVSEeHij7ZHqPdYaLC9rL/SR7notnBC1OujFi
++mTu5z/HJZtnN4VJQw=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Su6POoQw092/hg4JN8GOCSrLUa435VAUaqUned4C4G61yBHlUmaG63UO+KxY5pgyMrDH6/XH2bPa
fona2wB0Y0sw6W61PXOfiew7cH42baMY0P9UBRjH25EZTf72W3O8r7DNj16ob9pPi7bkuCd3aab3
hdfeY613n+hUbAXTLQqbhjqGmO9kFeC/VmdSITa02RauMnpfVxz1wLu9iUQ0V+mPTp6hvfNXlD0F
7oONLZJg+c6/+uSw1WbEiltO2Lplqvbb0sYbZjtTSEQZSdF4DiUdA0SGK+L75aDYGx3Z/ajCRpBx
Mr39wb5wiDr6SJ/QQ/JmYc+HrTs/fbN9BJ/Grg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
JbOromwhdJgnOFMOfO8mpnyFC1anQPoDL/XeHYQuoY4+0yjNmPGasGLGjanpoUgfOYngBHPrFFFH
rapGBPsHEbT6JXWHeRJexf2moVhmq1sHJ7n+Jx1rVNuyclUCC08Fg3sy6FdUQmptKSpqOw1x0DV8
R9ZlmwLTkoN8IV6D7sg=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XbCcyKbk3pmZ92QhZ1iCj+9jpzUJAn91N3YYwVHN3gwcgTU0NRr0oD7EmkLoZ8hVAhh/9YMUp7DE
059wcAzCBsD2W3CWY+GHUSJS57Xt2yi9tZH7binajEyHpCqaFKKO9WxDTO9XnYLVswRvAii0DOJL
mY+z3Z0uDx55BVWqbbvDkA5gABsZLueFt15rXRJPRnAjzWXhYzjiqC1WQDy5UHl/LBDlsOMuouyd
gM4k7zzEZUOy4o1sI2isD+6T/wd+iOsXvq39rguDUtkw3SR4GJmk+rBu3rBh+EvBHKxaWqQjGGNV
qWyrqd89LjZFGnXZ2jvsgxldJWCellgTK1ZEfA==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dG5h8R2Fe36rfzcvmeDU4OapeKO/Lhe0DkL+4c9AG4It+1yVmtHeEWL8eVWMvHdPTwqJqgkMQbh4
OO9/9XZMyYCWFJTHu4ossKo7zKccfTeBbKfgP+rDEckDTGIWXihj2YJ2N0p6q9Ynpsz9qOLdoXTY
gZXwoOe4MrZBJWZrDOqkD1hQ+cRUV9c8S6FlH+AyBNj5dlaAM0Jyq6a8TvcRmLoZfdi1zFWXeTUW
/XfWQRP+vnqqV8VPdyfaJJzaKnG1u9PnvSFauc3SzydGZfICacU2pPxqAaJWzDYwSns+vd4vCu7u
e01UXo4XXeFCvO/9mye0QnyrDHhuE0b1Svw/jQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_07", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
K8hvyEyHvgdg02DFF2GnEdLUq6j/uKT5fsI+Nkpbw14CRrq5p+STF83Or85VDleAax2TYln4LhGn
6G6INbZ4BdMuA4nVtyx5xaogScfMwbjrTAn0bqxT20M++g4cn4gW2g3oEFMnXaYCsLaJ58t4/T42
ocO8oqJeCowKICP/eM+B+/jSusNp4JILdp522MKky1zANadPwlv8a7QrMrJQrnb/lF8qC10yXqfM
LbKfbAEBaHlel46y7YBqdIimfeAVng194wkXobD6WuMhQOpFkigBOLQzoKQWN1TWeY5/rSQt9pcT
xLm+NEQmtlL61OudMCIqm++dCQSgE4NFJj1fCw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
gSLVZdmdCqRy/3LoTp5M48T1hUUfGQp8cxVz4NQ+P65mrZ0oJJXHSaNbzdvtYH41+27aGh3RBbLb
pzz+TmeVuEVneG5nGe1VY2ogM1D7tBMRUvNgXK2PkSRLnk9tYgnxoYi0cYLBxa3piqBh44cdYXif
bT0Uh2vFogmdeH5hxVNFk8FEhULNtR/T9r9ilPNDQALb08fQM461sjlhS2jgRgH0X8LZqnBOii+F
7+GguDMENTlzU0XSYWEcGFH9V5PdYMehb0WgZeiqTchxRuQFmLjDhI4J5dkci8RmkLCwz4KyjfOi
S8Nkg20qh9otuAisfQTh4Qx2lC7x7BHgmuwy0w==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
kXlkvzJI7Tq1glqNfjqmCb8YU69bhN9hH5OsWvFNj7VseyX6/5l9Mgif4B1r1LeKz06I27dmB9g7
AuHBFZ0bPN86mURBL/HK/dTOGyLYAveWeOIK1kqX56i4H9UNIUObEphcz9wdT0OgXHTPMxiIpJhT
1o5oYJW49mDsAv5yxe4FvPo6rFgZAiEo34vJGDxzz4//zJq0z+GxJNCibpLydZBWaJWRfsDUs9pm
1O6hS3KPIL5Evg1JOFt1uwKb1xEA08ETT+qYwg6zmFfwQbs6O7modRmBtEd1n9mrqsgCAviiLPtN
LUFiLdrywPt7LArLCRz4h5uHJxz/21Pj5m1VZtZq9nFmsbp6Lw/0RF1+nN8o+RIu+/tmu74xkL/8
nNEc9mEFy912OKP6WDP4Ajzg4gl9xhtaYA5eGkNB/43YjgGsmTe+L0dyxHIwa734JNMb5zC5dRtR
V4pCnWZKmnDJDXvMftedQzqQvdFwJg5hLxrHfkPD8LqiOwVck/Nt6QSF

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ADtaDIjUIR6zZBfz+lPRaDMdXcoufPACX4aSe06/DoTgIDvM+UOlm8rH20gKO3r8YdsuLtUh7rhz
ekJB22nBPUdbl3FvlGdQIgiCyJ8XgZYvvuOo9I765yKjFxQsFmQE0Ih86fqCqvYmRnsZkpk1uQ7v
JpqhWGBX6tLgYu/txP+ShnzFfkWGhj29JhYII0zqJMBCjGeM89F+mlH+X/YL5Q/fZYyh9Cr2CJx6
ofJpBZ1SPlXwgafXVi0QAUVuQEBmZYVn9Kze++tMEr6qv62ANq23LevYQfCsYKoY5iyf5U7jJ5Qx
eC9nG5Es4y6lz5giep7veaXdBFBHd7VuD56v4w==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
zFwVPvNmX5sBruiGDSfENTp6EBfydwYKhxWi0YDKQ4j0gu6AMV8yJP6GXeJs/A9Zgb1UFE+sJifk
OngE9N2vVRp43pAVauHQf1hUkSWPDJuZ9yEQZbR7F3mmiBKu/Aehj7KcAjv07FWv46HzxRL9E2xx
gpDOzAyNSNubxORv7bVYUV0C4Fr+tZRA6douG4rxi56npPfzIAZjyU4wPvwabxrJ9L4ZRuZXciLk
lJGTIJZTH2uclPmuo57jlIXGo1ZtQZgRCDfn7W02AQ7MDKblx47m+E+sUKKYHZlvf30GkPcwlucZ
ZcUcGnYaRCZnrhwFl0qxxXn2pO15vG4MJXOHMw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Lq86c/0SMuvdLuij6dbfI/ah4/50WGATVNRwXobLfbnZqWOhhEk3VDQATTxe7ZLrUauwrLuMoKhS
j4kqT2raqDijA51Tz7ee+F/MUKvyxGDJqfBi5JJX9y81LCXav7HpdRiPTy6w5O3tQoQbugh61D0B
oJBwNvL22Oi10e+Bu7H1yQvsbksxPAA8VE8HK+OJzZETk0PfHS2ySL5WXLQf7duD6CWmpWdLMrZQ
ojOqvNL31LsO1gZhssTk4RgyZUrZ3CboBbLWDxq2L/SsF5YiRIUPDTe17rRcrxa1y6LzMD/ve/nR
mptJOGxlUgLpJaPAA7jH3b+EQGlrHzHOsG8fFQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 76800)
`pragma protect data_block
UyG7pFZgkhpFaoE2nByYJbJaquJQNXtJAFSqAWeUiCIO1kEQejU2PeQoqaf3/IWXR0Ur4ZM6xQne
XDmWpnnfSr4c3l1MbR61tZZVyDO4/h8miOD2CjjRpy2LCeTCIkR6UOv01FvaRxZ2h0kLomK/+FpO
nVNt7XiuhSOZDRFl/e04zdqALFLfo7QDKnF+1Yen8hHbQjen7MlfanIzy4qakuBIOmcYT1RUXJQs
jwYwHSRpxtHZg09cgssiP5ZDsYrngIQED3ysTp2x80DN5m7bisLnzn+5HRXmZ35oomSt8QJpAbDN
uT1WlWYQi8vRXU8MuNz4U0GB1Q44zhijUx00Pqz3AQGzp16M3wxzJpBbJcyih895Wk7r6lmYfyZS
a3MyzwqMA8NE8GK1LA4OKVdR2F5kTceByGeMkuSl1ghUaCaKOymKB4wKLnLCTI3XkCVx3gaUj1aG
7D7zZ7wHaftMTnJ905I9PUN7PBo8Ruj2iO10wDIKby5+dzunHWpWDLz4zwViDFEOCHm9LWX2Kox9
EgSSqdXd5XQxgpl2ngissczUbHbYh1JcmxtZYvTEt+DSdhs1HarpdssUlbM/nlbTs1NHp31fq5hM
sPqZEjtUqcTw5Oet04exBnHasDQ5ObvGWyJzQmY0cRdBpWFH44xgGwqPgWmrxpfxF5cXfboxMA2t
uKYCOkR8sCranDJAvZbHcndoVF1vB6wg/jc/EpGWWjjzLGl7X6uzispUoxReElY9OLCHV5I4/coo
GF4jGDUK1CFJMMRgHhkT5pxKg/dw3kDU/J8iwS6VrB+TksZOSFffX2fFB0Z+HIMYNgazAeBvWZNj
mS1hPqpBpULrrHxoPZhIH/8tXwYjPLHHYwWFYMp/p86YFX7rec5dZtUYrXS6dx1xzuV0wXSP9Ztc
GksNovwwxZzMGRywOLhmQREOUkfH2ltEpL6rva8pA4wt5utR+Skl5Hlx8nskFhrFIywhaPn1yd5Z
30jY3a+9EcwDEEnnYpJD4/zAjupqfx/IRLdVoSeaV4Gpmw0aMQLZmRpcWGLs5FkIMT+NTBAkUsjD
eqUDOTZtyPJudwqX8i+W5xRbTX+8pCnoonHdsr9KnT0WDZhOzWZttvFZTauSiPdg5PTXaPSermr0
E7odWSFgPVhlpopz9YX6ZVAT/7IW4to354yRQEHgiSrAH5MM4ZMXbaIuM2lBviblfJjzLR9z6vBE
2RafTg9h+UIg2DNzFkoHUcvqrpadKz3Slj5//yN3+z5p/k6PpXMI3QFgPKFML4y7m/gBWMFK6hSq
ssZTndn2yVNE8AxzGxYcsLriJZ3IXoYPp36A+e1gWCwOcUXb+zQ8BBJqRkYybggVhgBFsNSaOjSq
tiuPmSJoOjCUqJo+uuEcYi9X3SjCDZQ9pAVyF8ZGcLQeK+K5+wuD9xgZcFMakZDt1SA8Ay5W4RU8
6HQWqQ164ouHjTX9NPRyleELaC3hFS9TU2tRJqB9YUG5SLrvJeRLMn6azLFvZYCBAkfbeie9zbN2
IvoY/9Aa0E9ce9og40b5OlkmwX6sNj2bzd8Pty6fhPeAQ0J6uIFZ/WsA3nr/70Oq8SFlmdWpA4Ib
koMYKO/oU9lNsFjdnRWx40PN0j4tu4gg3TWpRX7KCllzzO2bo6ewwIzGtu0LPTezSylxbrtn/B8+
0dI+jDjgHJnWkFqQj0bCiSRsBo0kJaNaAf6ASdmoxIOJHGRqnF0AWTfJSKiedBcadgv+5p19qGMB
LR8/5gVY+NguO7hBVzVbjyLinCypUJxgnwysK+4Iz6ZZvPV0R0SEZoZUeu1+HPT96Xeoviyp5Ap4
+jl7bMDg2Kn7S7RGsJrsshCSO1A+OVEad/4/X5NjFb3fMlWBQ33WPI9u4KVyizQ1xr1XNjcwzRLA
JPHR4oXReD5ZS8JY/sPTb/iwUKb8UnNLUjlDwMgzhccUW9qQjoPuUF0Fgnrg4TYzye80Z0TgefKe
4o504VTjdpkTKg3GbcGM4wrnglCSoNP8q9gwXUBJCR/OhmYf0AcW8sLDm7IRjw4hswY6GRAXWidl
0nEbEdWOpHPg24GNbwTybOyRG1SvnM61NtmlUWKhcM9cQPNNHyQcCdp/40DfU2a1fNeX36IUZkBm
VClRtJXrCDooeU3B/eGkQLt4DwvrtslTfsLuwrYb/4K1sXakULvAY4sHnW6Mbi2HMwTsWmm/15CO
sqONhva3tmfxQ6pFCR79k+lwfcQxudPbajLM6rrvwttjz9TIiTImpRJTCLufLHAZmxxWvy1Q1Neg
nDfGbXl3z49FpB7hOFyu7G9aZabp7QawLIDv7j0/fRjTPVS6oHN4qRVjvgxc3F2woAlkQl8Ht6uR
x0XcJ/m0eOlVr5zTkcZKOvwup//esVMLKNWvEnQL9Y6gdM6JJ6sY9wfuzunVX5QNpnvZKF5oljWz
yk8M5sBDAS8qaBGeYf1Rjbo+mCh2Aj2znJt6HFMbNrgRi/JY6xH2pZa8isZzYnG/+FOR7ayvGa6c
LkBvwOPI4gSmzDVhef8OOz64hHBxPCfgivz9OP6F2/fMqRrBVol4unsR55XGRsDwC5nTUq1cPek7
kqlgkQu6wnRl2ryaxiLYtWs2DSGA9nRraRG5CE4A+u5tPpydox/T1iLuSDVif6zRtooCUyOuOdX5
Wi7KTEzZDPwN5UUt9kLiaL0lQY41AI7lO3+l4oq8eg7UJOZXmd2SG7vcjRN/WUjMLrzVMg7e02A9
Wf81/ExYLmCg6Lkf8pMBcxyZnjL1gwjFSDOvIsfWe6UtSxfkjgPyVa4rjD+UMssRUEgKQhVxneQT
RcCtMxxxo1OCPxN3Mo4/F2VjQPCWhKggPdfylHtx6mAJe3Au9S5kr6GAZWhzjGxNHaY2y5qFYxds
bA3efPtC1UjDCJf3JKs/aV8hxuxAamPFaRL6Th0Rf68H/gf3DzeGGMUW+5lDHkFcGQqCcm8W3zKB
p56wI8GA9gqu4QC3ceVxbQ7DipMFFchydiOxT5qlUvcvgJdwZpCG+hVep2b2J9RaclCHASsua0/D
ZZX75yi0+yS8OyrbxivkKG/dL4RbK6ORGmofH3bxbSMAeXIkPitbVp69BhPRfFXjgAKh4+HAaR7V
UKihMZL3ajBLvO8iMkFx4bXvkluYjVv82pWwn+ZFDdHAmrlaIhjnmgqW6wcrU4cCWCdHM9cyQsBz
sZI8tU/unGsn36Fb3KuoJAwuLWOx7y7m7H06DUtAAnX2+UDPhqrGY99RV/EL++MWEE0Wkmb0yGav
TVO7xS3yqlE8LC8ZaaxgshvRc1YZ/NidE/rdBM+IYD4uMsnTitemNsRt61Tzg+TwIMwy/RIAFyvG
xMeHmY9IFUUQoVrntSlqNez0QvDrevxB8cIjxKU1WTRPrwn1GuwpwYcX1RXKAF9pyczL3zKJz0iK
yz/eNUyBnw5GW2Qf3YpnM5fywYIKbifeB20+x119c52MwCk8k8nH/+byB3XgHPZmy8cnikjo5Lrs
PNVxvN/K/fCuLUzcvXD+c7D9n/o0AektnVOfl4+zqUS/RydgXNgI0xu4wmcLmhxzbyupFKusYglG
729I/mNERblQ/VJVUFNHFJRcjg50ivrVAv/Ke7oexJHUuDu+TGFQ3+AIcO9mQ3+8+2HNn0fr7Myh
khNA1NqGZcZU8msOpr8Gw8WV4yEr1KJ/WU0ti8aGTeDlEfK4+C79tMAbXieRiebSBj7g1vjun16d
D5Fuq6cMSyJqXG5j8kVQSHnaUnG53gbSHJugCnL+92r2F3CtARWo83BdYwVnrS0Ifwx+NgdC0Cmc
Dd5cxgdkzIwVUbFS+Bg9U32C1Boh3yEmvLJVk8oa+NvbKESRDtuHbLrSV7kTmlbN7ZVZ4defTlQ7
5peBjymCBSBFvRgCXtKZxvn5zRuzaVw+EWgEXxF8KKXdCIpCK8ZqKxCju3YDRdQCBimEYRmT9SEO
XRB5gHFbFBLU3zAmIATvAjFxdQX6lUncpgd9bMpI3A5R6nvQWrQCUof0mPqdq4OKnHuwFnwPy0f4
FyxxwL7rCzLH4Gp9G6sZe2+GABRIuJIFu5hmsNBmxH2nqCkr9WvqKAvNfE4kITsEVzc4Px6y1Ka2
sQD31xTlkXQk5Q5rSn0S/xdE5LhCNJY4Z9CQTlFjTwu2rGx4Dg2BHqLwsm2FlrEOpfdFFO5ZAym+
t7nWyGswWHqmhl4btZMtms7aIt128kXRr82ytH+ht0muInzASaxpTCTu6UMN5P6ZAl2l901jhZUj
AQP0Tog/R6r3bIhY0HU5h9JECsfzU7DnK1rZlQLqv6+blMbcVGXnY2MTFMAEJTx+W+8NjXrVo9Fw
zSrUqn1KBRArDUs6qrIRZ1H7d99PwMuef9NvsSAbyCHl+uR56NgSV88fzdIRwVJDfnLy//hPIAgf
YMTDawcF2J3djUeWv51nUEz5uS68kLCHdU2N5QUgQY8NdD2HvIOua1u9xJMDwsbG1nSLPgN0vsK3
EG6CnZ9o09VS1tpdTRr366tzVe7swkaKJEUhwKIDiK7DIvBBZhb5mHZcwHBRGUrK9ZG8cG8doNPP
enlH7LzZ4Mga8ioPV5psECfqXEFdbggSMOhcZayZDM82BceC3mkMstOraoUYbgIJE0Jg/fmTkXP9
w/X2Rn8GY1vudYJ0MH3qKRZx1n/N5bG6hyJnNooN+6/o+xR9AFr8j17fzxAs4pzZB4rZhunUkY4V
CAMlNJP7Cu0TvJegQbg/1NNWO5nk2n9noydXqt/ilBJF73ML/O5Re5KFexhq/Cf1JOmmIjcTBRV0
nPFCkHDyxVwtgWlIm4DhEdtFmjjZpWdHcuPkpAgzBizS0+BSqg0Eih9/K72oo5fM78DQLbKgJWlS
+6zP8tsNsWepvp6dnCWev4mXjaffK8EvJ7jP2n6mjaoIepuQMCvccHg4Z2FQxnKjHmmXMGSTh+80
vU8qoWWgAUNJCzxmc75CCM31S+x7v6Gqleiy67uMjqKSiaZFHX8Lia5pHoRomA389VR00iOKTYnl
QGIQ2+/HpakcH3Fh9EJvA+ww+PRs9yanTMAa0TFxL67kWHINxldj8lEEFHs44opVtJXJfJTFGch9
eQxsZvX2ZkTTD42b8H1yNj/nsYkjxkDmc4KKXJn2kd5i2ccTnFYnIb9jKEY5tFgl3NofLTdJpjuS
tUxSkj5LVJ/wLChUUeiDd37y2aMHS5KAWRRUB400e5IQmpCat+nUv2CPhKxVhiAV9Le5Qp+ETtIH
aI/cJhQi4yR/Yn2kKn6DX2d/LNYBcUj/Q0Wh5E22n+DEyeCgBNnQLF4r5MXc0FJ77T9uLYeYgWLp
eIlVvEnb+OCino6U728EoOoV/OutZUa8YIsMhYWLVzGSQb1i92eg7oWR46VtqnDsejUI+AtIM09o
GKeID44evVTr7UuReuUN4H7eYrGhrFFOjmqVG054qz2UJbVSlErc9vmG4TKCNcR69uPvVtRFAG2Q
Lu9i7E1CqUuS4hxK1h0yjD72UidCOX9faa9rDZ6JYDl8+TLGvAjmoRcHhV99zS9SBR1QKj57OWGr
Isg5FZZvLUCPyq9AV+qqNuMsJ3kCzHHZYbzET3Dukjz2owoGmKMip1NKSiQTJFNGKSvTI+AkgxG3
TPy0bg3TLS+d6vzkuI8eTy721el15SG4spYMjXCvyVer9PWC+2/alX9EOneAxt61VEWPGOO1TDxj
5Pgw1Si04JbVTwT8P8h20cUmV00g/Vq5iR8lQjKBED5zCds8Kk3EeryhKJEOY94Chu3M+MC6IFnh
+nM+xKkmr5ra26TBuuswAEF5Ei2LdBkeEsqDVOG5UwSZWP+nC1l7bBssdNDFe9YiWsKw2FCztWfP
Y8pCa98FK9rAH2P2XlhTlX9LLbojHUVs+vPrn1BMFKDLisiSVWtP8iBsnCyMKZuc0R3csC8tPyyf
2PIlcuBPNZ/qsuDCGqoT3IWTHLikoyay+u4OgL3Q9swiWHEfjRSbFyFgsDnXrvH0qiliTqh5oT4i
xf4nWSA1rLDxX/DGmFyDeyjvXS48myq992Ps8wBh1M6kxNHMz51n7YeN577XG/Jy2wrv5jB1J3Gg
pU+PpACJwU/wjrwxJn+a33dGJfcubWFSTj6aRUHZPm7ygQtiCilL/lJ8i7UKm3qAG4Exx2HOCqwu
yU6lVO3HjxcinbU0Ixrh2v2BDvXjDPhaJudKg7f8ISsHYhbGdC+HWL+TgTMS1ODDWumUCCE+WxdZ
GZawC8kYJIU2Xv92HKF4hsLSoV3S5LHCdf4IVq4xfG/EW7SNCByYlLfsVvQEKV7xTgN1RUBfESLC
d1QSx29H1sANnGJ2OME2BY2178chOtnqsdIoRZSRNbDmHS4GbDEIJoKgAacDudSwi/btp0H5nBRX
fC53zRtQbydlxXVQnYsQkTWD+TYYJcidPu9mpgFvWngHksMAnYMYv8DQrxHhTsII0XfyFjzeQ0rz
vjZ02ye+FzfaHPsvtjSsektajxTivVpUzM1sHRh7WDAW+X12scZ+7ZCXhnQtBIZEWRi2gMFcO1hX
4Fr6yuUGWW9yYn86NcEP9DHKKG8UJwciVtw+S0MRoZpa5AYzNZ1pCZo8CzYixE4wYsDgfASKvRz8
xo+pspk4CDkHBVXbB8koR1vW/iem1YVJDZqeNGxC8En58LHXijojFETmiL3S9evUG3G4pEAyPVjL
u83z54hTfFGelEMZDqRxRxkA4kzIazlT5HpVgO9pxXHpNjYw0wggACFFleJn7UV58J4655YmXfmN
JhdKnARKWW3J1bboKnFb5wHbYnVXkGCMjtW3IQ4/mePTq6ZG0U3TTkLOWJgEIR2dPHMcfYRFEqSH
SKVyW6LHOv56wM0GDbfXyOVENsOyFlWK3uH2iyl6CFbsR6zGFqNGAjmtbDwbjHSXwb5oPjFUaqL7
Iak8BYOecTUPxJtpEblU17c2Tkt+zQXK1S9UPrReVVr9hKS3/T0wi6/6f7Cc6xWP3EMq65Ljq/NH
IwH1F1Vcln1kQDFqN1YplFSLSxMUT2dSfwZf/8+5t3qJlMqBVEf+5nMV9iOs/Sv6qywdcX/PICiR
KtoUTMYglOctf9gW+1yJzTi2NKb4VvULsd6sC5KCWuT2ZMO59zoKl9M+L28rBEOmw7H7NRS8r2kH
olt1quiqyrhcAUniJNSytP0bcelO0cwZMh9hisfaDRaFCyb9Ub88H0xf0ZkAccgMA3zxkJsOBGuR
aB+qh75ZeMopbn/zsw01azTDzv+tswM3LycHYIVp7+aAjHSxxXTqgwUbSXtBuoWZFRfcAi0mJcie
+0pd7WeNxyoBd5QsfbyH48nVj3T2ij/64ffsI/gZ+LuW8mSvvU96UWR7letkISC5dxRFV60dNgyp
BpOYofxvzk1h5gkphi029Ry55N6Rv51QlKZjBX6tZaRSvzJ5+uXrQ5zhI2cCtchJ45b77pmrA/d9
JGwmujVBaXMCcMAaLDTRZwf0REY31vwAY2pCPURPh7gKU6sbbvQ0E4gEYwCR7gyHw980iBKYuUXw
hpI+7Ar2UWO8AlAr6SEzYJieHH3WAJbFRANUIKE/SpnNLcYZZVLVcHLAEAnNjnMhuNOZx1FlkUVd
sjhY/UGBQnwzvV2inSeZeynxbPnZkyS8R5TpLMw1PhaoigDHpgwIAP84ma0SgFoBMcIsFpqCicCn
Q9nVBaOvrcSNbo8OdNxKRkhf5TIG/zFEEKza+suArlfkLmWZeTfucwszdZwrVsmuW4v5czpTmaYm
wKsbgPjFkc88LbANhrZXBObWLuPTKRdfNcK1wCxLBgKV/HZrQQ0z56hPSUOjynnApw8N4oKpgx/R
PWKz3ggNZ+1FfvSwCZZ2nCxPzrSfShq+QYd1CrgykkHvWtr2xx6p+bwqR7uhmaM6u5SaNRYYgWOE
wU7qHhej2MsmTeD+sM8dSDnOTy+6J/+AF0YOdCNK0LAH8D7oXqHYsLbEF15wdaP2op/4hOkvxHua
EAbJkCv8t2rg7ljOdEtOsugvJCh+MtApEUkW8qUz9bIWJFvOzX11d4PSH+QGHcKvJ0y2EoZzWb/v
Pgn2datNtsdy2eWpht6yEIHti0LWUdt7gD8iujFOs6/epFCzHYdHrrX4Z4bkVTfTPaK2fKAk+bpB
0Z9jYBhPlBrx20YEkO1HKKTKR2ez/jul3uqxaU1RtdUjXjVTSTYgItu8kG1S8kRJLoRvudnBliYh
+qXZwxKbRI6mDG3IxMkITfSKLJyHQQh8bI1BE/5GrJpV9N2PVtEIisPuiRUrvNJWociZTbiV2oih
Sk7XUzADdeB8Wk5BSOT6zHNAMme5P61ijEsQC6A5IH+mIPWDdyWr/l1cmrmBFVNiDtARTqQRtc+f
GdU9wwJCyQeVFJwrw9GadTZfMz2P7ohBd4bW/BnHlUvAevoYWRhNVPAtOyJn8S890bPctID1IpPO
O9RP2mx+BvMCg0bZwjfFfxIygPtNGWsMEnALR4ioKhq6WS7TEHMWolj9w/4qp1LiQSTOfZYpELyE
v7Zw4Ikk05q5XLYoblPQUoWs5YEbnuJvTfPEi7ATU/71a/DkGneLaznlRyYY602kWpjqgf75Cr7O
wDaxQ4Oo08AXMK2t0HYpxzbMi6mv7DnYSc3pP29ZH7LupUBCu2+pSTzl5Zr6u1I8JB/fe/OHtV5U
UNVYdmrvXmAY4+sYGWwtz6zZagjyWME/6ezlMXQ8r+Hx4g7/ztoZ7zobQJLbXvdvq5tfkAtWyQy4
SwQDnDRZpiO9pcookw6IlcZQENOIqB919Fj5dgcCMKAvuxSMN7jOfX/UXC/9/QtZg1sXsEbWwcBW
UohmWvYxuKFceMQAqshfF/kbGzSwJQrQs8Y2wzXLOCMkmbwld9nzA2md0Rx8if4MJ5TkPeNl5Grv
9lTBDAriOfBwWa+CsVZjxoLQN6cQVL7exSHohp5kIDQzhm2Hg3zYTzkuydGpOoNr9z5Jt9Hd8IBO
ZMElUh7eYSDJ4dDN1s0eaPhJcxrv2fTgYlY3yZ+0/8yo1cQV5zk+r7C9dsxRQtc2SDl2nc5mPmz4
M8Iz7LWUqNQr6SGk0W2tOJTRoD31EJmXNCWWI/Ucmrz1DmUqNIpyXgIIiZfRbhW4cIRM1mzRXHwp
e3ndI+HUkg312dx7PGo+0bt4q6ZqK979cHW21lJ41gE1DcgfLxpo9Fp7xbQ7MdsuygY91WTHq6nm
cvh52HWtPnScxXyG/dJMnkRHfD52X6K9l4lqvMTQ/c5wp8kW4cge5SqQ7ruE51hWwz5LxV9NEtHp
/HX59jl1LlK8fMbQS6B6kWG9SQWFEx5z70dawGeEUbvRJrMncNYcKT+tJRnQawn67oMEW/HEanER
go4vEOFxIQfoUrIa56Q6CNl+m1q8+n0w+OmfZsWfeBBrTXYJ0IOtbcgr3fXRoPgDxNJO0eqEyVPi
Lhf3ozvZ5B9nD6EVCXYuU2WptS5Pj0DfvjPgDr9jQcdZe2rblXe8A3VhaFtuqcVKBmLhexrmnIxZ
4WBtOF37y8HT5sQRscLhjuyg+pSvHNYvWs7T0dI9nJfbWvaKSbSqWLdBg9/75VTLKJ3Q4aQuwPbU
oT29278JTG3rEvMJqg+qxbbBsT9Xn5afVFGB+hZYyXGxC5pGgSuS0d60aXg4O2kiyODIdJjUxjsl
LwGuRcAAI18hErG1sGIexQ6A3Holvl9TuW2/Bbr5AVlkzB03o5l8UPyHTQ/DmB3Re7S/1M7v85m4
tQwSK/O7Cr1+syX2AMtlSyG7HhHtjRASS95VZ5qVTBMe8BGx5wKHeB1GiRHlvP/OQGY3w7uCcbhK
4XQcWWBXw0IQS4Uy2YkcKbRdElw62XcsygVCE5RPu1puWcFrNnZRb1xck+DECPcknyJz34hfqY8F
op55bF21l7ikZEy+F5xnxQyl/OxtfY8SCw/K/h3hx02g11d8xiWPL4eSzwnZPoJ6Nr3YSm0V65d2
OeHToo3rAciVdtmKFX/2ootF0pDtNLn2mTWYiEvEeBvBkIn9kOJjwP/ep8v7qm6dXrMFDeli+LTD
1jnTm1/gwtuh2oVKVHUCn1uORnUsoFCo+cNMTW2CsCylCBbipkd9xbOxcL7sEn7s9wp8j0Bm+7ow
M1jSIsfYUhC7EVJQ1xAoi89xthAm8fsdk0ULQCmDMoSOsdNKhwFA/2E6K8kZDFSaVwSxnW07tPI3
K2zVDURvHj4+UWUajR/2VN9RNrHeotTYKEAq2aIOQdfJBeBk4wRcfmRqujG4vFgIMDPyJuWM+bl+
WRIX4/P54wuLAFRsNnr4tVm4tcaCpK9/upzU8hUl8YjOjT6efdMhNoG4UvK/jPvDSF7zfW+i1Gjv
ehNr9x0I1cCXQunSmNG/rgExZy8NzEq1bJAeJpUY9THAmS0t4GyJr1jtM9vk9BM2QAwLMx8ayWdJ
IJG9iSS/NvKL4MokUYsJx/KWJQ9m3yf6uqxYDnJAx1iqDZ5dSy8FCApML6y6Y9Q+b+/xk3DaqzUL
f5K1sSE6pHkKebfjq05SWDgSNO8oIIPjV26GQIkwlpLpnoArk2OL1AsfjUKtUvaKblPGPTg4hv4O
DvBdp9v88PXhu44UTgpBiKP/LZ5j4EOFMPaM0ZpKQVlW6bs50BfwMa3GgMP24AfD5Q7iIN4TbhnQ
hiHAU92J2v1GU9a7v/hM6HLiPqlMnTwPS7LmMWIdRdyr6fU9Xq6S0YjuDyDJ24IRLtBx9Sb5A8AJ
bk++NDH+xyN/vEv4sxJXpnPw8BHK7uIFhDYNTQj+lwRkVOPev3RM4ZkP6uMfJogxy9D1uyctYBtm
WqK64orXJ4kIzrCaNmiqMCnuXZ6+PeAB6GstwRyU+LvxqDa5iKOxgHPYdX2MmL6pI8YKhcIfx5Hu
+kkjDLaEmSIITj82sJaTADhPvgQz0o58wU2U4tpN+1+1SzBYdwDDm/vFXnHcdaE84ramRkC3CHkS
ZJZxGy4O7eVL77wrSKPP5BI5joH25An++JxpXPU6ffVZjfaAR0nybLOC0O33O2DuMi9v78noC/eY
wOL28K0fFS3P1CwUtFUU2yIZnLavGP8oQ6D4j2lC3JhjICY+5wapDYoTc2U9VTtYEHGOpo0yVJqE
2+n2msBnOVkZw839H1qFZRA8Tcapdu3PIExuPH/9JqTQ6h9HrpK0fqOr27ifraaATTdvFSr10VSW
D99C4DB04oNYMIadOSfB94fotVwsCJrvxRR/vtNN8+eTJiZMbQh0XR07Ak0gJN1At0Kb1UiLmpgQ
bCywb+10mS4ZdA89tXo38hEX2Duo//G5YDNM7HkTGW/udOMKUQr9bl/QNFQ2OCzmtr4xh4SbQqme
4J/zYX9pyJcLhEQuXDqpFpyhr7DT24lRFYgE/kcswWCaZHOnYM0bB8JN/6z2xdYuR7ICc2ClhTMp
YUd/bsD3DD8reS8/uC9kLdy7U59QPQUIkE4J6nQ6UrVKlYJKjSNwWrgTqzJSpatBdDPnIJT8o0hY
qbSHNhb9vl8LDi/4R0yJbm1KGkg9YrzCx5UMVC0mzz1WE4kf5HBHShY+6uCVs8ssYvlixRwIKRmR
6VbF5vW6tXo77TmWNRvY+W9MC/HqFDlnjMRlhjuLxYtjB99r4Bi80bq6Q7RZaXXS/JxY5yR0pJp0
V/wMAoE3c5DM9g8Wyv//GczqqvvazNu9XKiFrBRhG7oEk6qiz0Qa3fhTsyTnWFDoPLv4TSzMlPfa
BQBV/FFukiCMvvr+34wXqApI1PqapvlsyKy3sZCTKZqs7kYwLHtDRDewLUG4j3cFR7OsT3d9dd0V
gXAPkwhjt7s7OHglid+khqPO4MWuH5J8eh8izR0f0J7w8HO/FwvtUh9vMcRnXjGDxHOrAH6EAw/4
HOMI2Ao+/h+1VIBR2GUiHZ9U+68MMVurCCSYVxab+MaIxUtpvdYmpfDg0hMRRCeCDA3d3GaNZ0pY
AYEKJ9UJB5tLTPpVitxiP2WYboKoafAJLvB9xrnawyv+BQ83KiPCr39qOoZ3pGV0OCtdjH/QV1nD
jIf0WD025CPLGeRCHPmRR05jUWmlGYojXbUJyaHo5pm5CmT+P1IY7Y6m2SoHdXxEYpBcOA8lSoox
wje0MTfYLjBQ2mAIOZZoiUOKocKogrM07XBnYwLksYq4Gqd41Vz0O+0xGrscgVdwXfyb/h07C4f4
ZO4t+7IUfQ7PxRB7gEMcoghs2TuVXD/1flnwLEjyLnepOlgRNGvKPdKGyDDyoG8J6zEiMkL0Bov+
YSCY/ZCbI5MH4KOKhIgxGMGv8xlsHTo9QMPZL6G5XDX+wVrBBVUtix8SYclIPan3TRhNjJhMnF3+
A2WpiUx0rznnDtDgVjgfFygWGtel7pv/lQq9Q8nnPhNVZHaOeQcobc+gcSYLeU0Avg+rdTShLXOY
/r/0sgFEPJJkkfOjvcRUI1dZNeBEm1loP+6DZTWp3f0/v4sqWeatX+qxlEFA6t6VCaik8FLOclZU
jpWPsrOAONKEXpcAmdhdWHlnAeqtkhNUN1Q6TPlb+k+3FRPSgLbloUHtADxreKGoXQQb5TQ9nlCE
zg6ytKXUPo4GPpgJLZRcDgHgloF1Ou4kPWWBDV6oaX5zj42maDEM3+Sd64YtdUktH5Dn5Wlt82Yt
TjayWpxggJhkHnl57mXDqefAERHjDOicrkMrFxRCNPCgYkCb8Xcx6/0zOtzJp0qOdgvXh3whEQnv
mn5My9V04rJgWbzEoYQatib4N18ybK+kP/MMjP7dKNq2nxk3C8ph9Rnx2fysYQ278/24Vu4LGvh6
zEkSPblFGD9D/Vsujkv4H3o5bQDyIm5IaJ37DM4RXnCOP932KeQ/xIMpF1t6qbtUGNiXp98NW3P5
75/2Ti/TK6XwqE1QLvvyMB0eiPIWWtVKrA1KEv76nFPpq8xZDrW4wrEL6HMpD/cYdgjbwQEuBmnA
NEzpTzdnU4CIoyEvXTwBbsGjX7AHod47MTvDrSQDxNjJvV4wXTR+AQzwyP1Etiet+CPCNUSkqpKq
OP4LZmV2FFvlxnQuiPkG1XgZYCCOis4xtFzwzszqtZliyjYlFV0GgNZtKgWnkew+2SLtgKsRKXEL
8Lr5LC7sDgAuhF52AH4CyHQX5GeRwwgZHeWuLfyCxVN9kTfx3/dyBO4n/y1oFkwQw1JRXm2z8Boq
kKtMKJ555LeKHYPnvRhAtHcoy7GO3YmxazW+9KIdUTjSQ46wT73b7w5h7BvaEw9hDCTrnLdnmXhN
Q4nRhk4oaD+aEglrb4sNmiHcw+jLDuFDuwL9eDgKREAcuu9Tw7QfEYQbn4eWVx/u6N02yrus/1L4
dHLq2Piqyr/mIf+eGBRCKcXOJS+b1MShZMdVJZiXrwtxz+wh2Dy+eUFUgsJPvilqbZT/YxGLvQwq
gdiXnoAvjUWSEW0HNSynBwyMQOBdnIT6ZV8P6CZiQh2DvN542Jlf0UzfcZ62qLLxAkqNHXcUUd5t
40qHYRGehntmTn4F7eAf2OZzapUxkxWfmHWc4m6VOU9Pf9Xsg31ykY4ytufAX9Sg36x77UB5X20h
KYVU3dR4yMERGfEmmi2HTrPBAIwjz4mHQqELYuwgDi7mybSWVyO00fEJFYTt5DN0EqBs5v1bXiCG
Kb3v8PZVLt053rBtJS3nttoczr9zKAQfitdmg+oFD6m+oOn9gidpcrqyB3EPgUXGXL+7Ql39IVbZ
0IJmGXOFcFy0R23Xj1BmY/4ZPy8SylQpCs9KbZsRlxOcg7Ko07VKSAAmpLWd6cmlUviCOxr3jMsX
QTrVfFcLlhSXyDa5N9gFb3WxERzD9+Ej5CMIg/mmzVdwrufBTdrwzvETfqoHsWhV2bU47RvG1/md
nvJs21Xn6h3cBWiDoRSbOwEl7gOTJ85TduIP7mYgKx81Rj1Q0WdUQBVgF2dsCYDH3CzOhIulSkDo
b32raKzZOvvpiY+ZYp4FxnYgc8LbTu8NTOTImomVDFDDhlk/Vf7eMe0dVN4R8DhMLZ6P+SW6mfAh
TUNFcTNl869YUfDbShzNpN6kdjPkxA3HgVGPg4+kbxIUwsH6RWTRErdpdWXygZdQpzlfoz8cLBi+
SwUGgDW1QfU/U8l7uUcLHK0bNcYTp7IP9oOhSq11jz0T71p6NWWG03KhIJPnS+ekAkVZ4dj+vcDO
M8yxWrLXAOMZ7AG67d5rBgK6GZ0u6CT7uBNL2ZpnTqY532GWiT0vauFbzIDUNz4FZVzYyiZ14T/X
26/fuXmnhR9FXJi9O6r+GIDZGGMDUBZmsoj3bU5rgZXklZ9sWkij5Cp8Bn+DwHb9I6s8FwUsOCza
dfkLY62ea5JHx/UeVNXXWKoOttLJK/mitgVXJfa4xuG9YoYleGYF/+8Rop/qELXt8yXrbKXOcCWN
Glilq7d3Snl6ZaAUGho3rS/2tvAIKZ+K5itDcNWIsbKE5W/d46CJxhbmW5zvU8b8GGfD72tc/d9n
+yKvNbvF0PPrjXDhqCCpRVIyfXi5PF/foyVFxzo972PDy8ImwoOrXNKc7u8LQGTngtlNcTtXMUDo
NlKbetgL+C6ZyUB5vdnQckvJbZcJynnPPY1qt3HtMnCFwjhCg1vzIKWW3itJQ6l3tPS8BJ1itNX5
yMc4Umduo7N24QZyU66Z7LPCMzyDHdO7JGddmnehmghu03z8S7mMVa4D5GiJ1b4CaHdIASAyI5JI
0PohoOgKU39pUDNEfA6NUWNFHTya/yu/HlNsL7LJFDj35dCbbwkzQaZFy6Zuq9gQJcftmEvhYKYb
4kYcyeLFMjqsbngpXjo4YOEu0/rNHCONszoHmwN4xgyyOja1gNeiaMQxqlE64XN0VY7h96bgaGDn
nWHJYtzWRgJ1Da7kcffoYoJCzJBx3xXHY83qLGuGXaA8/qf+mSwLLJdzEbMy8WuVvx36dW04EWum
/mE2oRuEcgel6qpYCFKoIqVz8Z/rpXsInadJHMpap5JPsuOhqDj4M0nnX3xjLErLivvurU0Ghdcw
GkIv/96V4UxEgwoxbitFvZDvTeycJSQRl8g5ZgkdpQYai6IJKahUeXuJYc+cQKi8Ps0Sf/FT7koB
QEEuS/2/LzdiUqM/A8CyWEtkamlgCEwVWw026HhxUb849b29sMrI4L62PBR2PuVHAF0k7z1C7Nzc
HfVH7yPdC14rTyZvobhyjSNauY4+opPLfc5H7wv1MfJbAt4aQPEUqNkqkbPa3uKsktluFajiNDuf
AyuzKjw1vWwImg/1gNVijQNWaA4Csp4p3xFU1kcJCp9PgXm6ZgMHKWW9tpOZUK/ZFbO1nyKroXpo
SpTED5dyUinTrYZVOss7ELhgvGzbGTZFbu2/c+Ux1oJwpuThsJdOzvYgOLazdvzJ+LmBoCHtiOkI
n5enTbu/H5DIgw6VLscpgdd7ojKEjHVjKDPS4RXYgFKne6PdNX8BAwXlOjn7WWLuckUKPQqZwbP1
4sxHn0J+MS1U0a0f1sJVGVhcIhBRPvLR9VPC7RjWMFFCNxWya6rMEi/evfpM01nk6Amw5xjTdIpX
4VsqAgu2CANcrknk5K1UhG5Fd+I91MiOVTuvqooGBk9qmTieF1fchw51kEblLbgMbdO2qsFbTI56
YHqdJc/yJJWbAVogx3n2Y86Wq5Uw1yIaR5L/eLO0tSYnxsY4ADyocxtKQ+RO63LXg0q88s+GtJ37
af6BbalbHCf9Ww5iBPCWAmQlH9bOVUwv61Q3WrW5CvNU5WbdYecvhJUXFfViGYL2JkrCjqXsyS4I
tEKlaR1bCvb1zsM2A2EwJ3GZk0yu9PXmW7bUz6OT0QbsU3dMu9goT8Unfo6FF7LNfRZMCsrvt+OR
YOh3eJRxC1UaAB+L2mkWR83g5XVXxuqlU5H416zuvYQ1svR5RPwsEw8Jj1baEkUF9nrY8gOprL8v
pEVo+/IIbWdfEFBe/1WFLwF6eCsiaIqLCgrR5j9Dc7GkAc2HI/2nRHYw3YZQrLsfwDMtR8LAljv6
4T+xMJXa4CN9RiQ7kWoocKO6iJIcKpFXLAVKGif2LdSixsFmpCck5QngJr1Ta67Z2cqp9pbnBZUv
TpH8lmvPeHqcfLRv4E96PNk0loFhf1WfLepanWAphql7ZYAYZLbdViykFP9oQFOACK8/MJk9Xf+W
DPlNbkcmww7JsnP9PyvxlmRRC5yM206WaygV00Mj8lSUHIhKgEQiUee1secfIv4rBN3nKVB2fnQ/
NNhkoiuuuHPE8nsySZELEnun2971+Li25ei0vLGfb6iwaWyYZqCPKM5qb0MjZKqSkTT8DbfSGYqk
7phWV3P8D3DcB+VqbVpsYBSpvrvii2Z2gAoY48n58uhFO4LkhgpmxkG1Lan5SrXySLP6M9kEBVR6
Y4Mng4cZIWAgSGa4RqlvJh2bEBrJnw6+UUWk5aXf6za6/RQSIz6yhn5VKBFXa/nmeKvF8emAPK3Q
NahcGxoPVLNPHs9WvWUFIyzUfzWnFgvqKy+jzH5d33QKIPtutqer+b1kf13l+ywrb6Mxt7lp0YqX
b5v4wa+Ax9A4d0DN6Eke1tUsmlQMtZbzhx2jcsliomEoPo5h0qi69eumpf8PLBk5ko40uzbQU73j
gzN1t9Z1I3jNWsEr3nuUlHJquPsHxQTPabwJHr957yYdppcrqoCMX3rSFZvrCjd0bPnN0u4bT8Xh
UO/gdyv3gRZWeerYiWNBVrwuCIc2kd0aWQkiRBUjIAsUKY+Rd9bODg0gweKFKG3GcVSaMkJ135x6
vGl9mtKbWOxx4H11T6kbD6c34DylmY5toQRmTrwbLeC459bCfgc//tpDiC5kX0BQEhMhIIOylj1z
xUXvm1f9ww6mRP5VAm4f9k7EBxIpPunokM2WHW9e+ZcofMVxX+gnti/yQo3vm6kC4TJ++RGZDVjE
kO3zlcvCxC970sImyD4l6Yh7ekqlkqRZSaE/8oywtTMviC/FhvZX3yEG2RH2dWmKV2QON9KWzvLu
28GFIB9n2+FLDg9AUt7PI5kxfIi6Bk8Hpbewi1c840c8tB9ZtuQyBxtZWv68krSVWjp09vCrAUO/
4zBY6tLPhnJfwsn34qjFKo5NmShtTLwpM2nlxNDvUJ7du+4JtTvTKkENZEshlAv06Pymd5XI1i7E
Ntm2E7LycCa7HtZEuJjg63PF6akhkudepqh2J+WuQ4cqpeNNOwQBY/CfU6fMLu+unaYVq40vdiuJ
yrPB+Rf8TrBkANKV+F8OK6lN3MU493ABTgsuwmd65jQZujTgfkv7OdCydCDzXLOIL5nvL1A5ze5r
YZjNC1emr2s+EAuTfMiwpsIkviPQADBSTcOUNIGTOzB+0D+l0Ddi0hp3gnuZ3nG9pdu8nd+iAU62
AaW5K/wXSQ4R2pUpKuwp0wg9buEtxynzZByOJjigSo5JVPejIfETVb/i0uXi/Cpq4R0E/0IV0afO
hxkTYo7kJfaYKYzfUnTZHoYwKCfDT/NZcwKVz+NnvSWXRZOFdNLsvdUL49t4GFGVkzXb5jwHZc6s
qYJo2emUG+2jYfrfhbZIz/4szD+h0RArzz1unexdJSbbGAocGGFnQShZm0zCkUz6wPeN+uD/K067
ZrvWIeDKoQV7baTzFXZGSTHokIlE1ZHNQBlurJYdxxclp6Mr4wFi+Yj7mq1YL84sYLWXRVqf8baz
MnL3IBBNkEwYXufWYybVypjzriquqkLRkZ6amSykE2AKRgYC7Ajyb4ISIUML2aufenzjmUtDQ6+e
Tfnb18gXcLw3s4OBLWEWZk4JiJIvZNhwKPe+c9gEriqhssazWonEBoDTAjdYcBi7xrikT+/3xXJH
LEG07VYsKW9D+XpUz7ob5nEnCFf9VoNrqfWPUTSES5OnMc/iqk+uerd+g99dTRRkwtuOEC6m7u0e
JmUDuGfDaCgZVfALIASpwaDHx7bl+cDUuTfRYXivt6ryiWL1aJ4Vft0fsQjGCnVyOr8gUDZRNy55
qrhaeNgFOVlzvKRZEmm6Y42M9JL0bTvyJBGVQZTYBQOH6k54g79VoYhst2G0a8HZubwi4tuA3p/s
IC0iTfCQsVheocCxZmD77D6SUXhTC1qtU6byuzKbrP5fWwqtRk33Orw/85xZ0emCKwhEH4TOMdfm
geup5PVW0IwXTfONH+LUDjXB/OwHJejb3/lhWl2CQfN6NgI/FxTGCZyiHvNSyxYpIVjjhcbCmc8e
1wXGV8cyuRWx6sAoVMGxFd4k2svh0Pm/Z2R0XEKZ3071rXdSMLB1OVCbnXsT4J8qEH0BvuvAs1oR
LwE6CWeTdjoWO/gveQXHC+uTXAN9tbRQG0Jo+fntZJJu6pKWhBBPyTznrHNd7MTUyoKDLgVNF71Y
4vMH1IPcPdF/Ydl2F98LoiVuDrb4N9FcdeK1/PTX/WXsu+xs7OiHuLWZzXB68cyAUnK2owj6VOyA
gWtZXG6ZcXTVZ3Fq5ZKDpiBcBVNzooe4K9xP1ZAEn5o8+ziqoFpdW1Lzd58LasxwU7dCZhSXZfcx
pZ+mtXR9fGdSmVmK7wke3igzgcROBWYUpx+ZpVEWfKEG8NQOBtCEv4e+yg7Hk0ryQb6gpIyDFPlY
PuxvNa/fN9vnfwYVJPnhyBnivc+Lt4MdgZVQtiY1V7MgVLvlT5/omZR9TohyJXzjuOJ00zLfNuiC
HK0V/Onnnh7g5bKnoKFmW1XSyEPSi0cmoYsHVAnKKRDAWhZnBKnPYZNgP6UIPjGRS+1GhWG/NnAx
y+Qf2Z+FWatybxXP44pRlrH8qr7gBdUBAxywlo3FYFsO1W1QlY/fVoQIJF1Q8uFO/YaFoKHxCUL9
bDpTCRe01xsA3tRtuiAqAxpBaUlrXc29pCOLgmG3YZ1mDmN4cbfu6Pc/vzM1kmcNDI4Aua6yp/mE
QUhyl+8iTiueR51PvDJz0aPA9pnv4/ePODy59ZueJbFrKK5IALyO7aBfS8G9zwId/c2l8up55sk9
yDRhdsbZ0Gn583AlC35ELycuUPLBUF/Y4mBNNM3G7HJh5jW/1QtAY860ra5FDcKxRvPaAg6H5JkT
W/8AU/UPB6+LgUPCpVMqTVYpCs9XI216Omi/7Olom+mstGJBKOHaJw/stIT2ENzAEVZWcN8CO3DC
9Ysp2BUjn/qx/DrBYyF56nVOwyon72GiTSIrkZYIPqyG3A1OYH3EKYLbaFpPx8pHqtoqLuQMKLrn
KP0GQRKEJLoRX53DRDk7dhKalISvAsJSbgdA8v28sF2ql4Sn4Wdlo1Dtp5wR7WxhCAIc2zO6A73c
1rPeRkrLB7QHst/Dh82iwxJY2cKJ4WSrLnAe79QvByOk6wfhjNe/EngZ6DjQ4JPnEnhI+7ymdZyY
KQTTgqeXLgEsccDhpLAcari6WhEbq98Fu9X5drF1ZXfDc+fylkibzzBjPg4AhJHlibinw/dV7M/P
+PdZVkTzIQMmLcClDzMnJza4oxF9oebY4JrY+452jM0HiR3DsKCGJTfw05BVuppH3USgkycYe9Z/
cRohAKpcXbnXLy1cYCUuHGV1E0wWDyVXGpm0T0i+qa7TcSbxMYtMXY27FzIHLwZ5ifuiWmPMlIyN
PYRA7D27uPLQ1yt56ecgxpOEdlZkN/BecqskR/xlv2vetDVi8M4ylrZKcOnRkiwZzW3hAfQ0//7c
Gn6mugu+EKFbeTuaSOP2gU9t3ReiG4SxIO9n+BjDoonJENIFJmFjgPSvU7xho1hR+dphVz6/Bhmn
qUK6A/R/Y8/J7tHqLy19FjNub8Uz9nGY3ISaXd0AxcZp7xfX6DZyyAKyp4yfbLq89YGU/yvewz0d
mJ5/ngu0kp09RU7quQU3F27GXFTVvcbj66FYV9R7ocslemRNZ9Yy4UY9kiP8prGqY1wELqvD/XnF
qQ7Ni21DEXfYsr8/sY27ysAzuR7ZAe0A+w4Z6Y++yAEhh5v71O/xZJWJkvkfxL9eYNOTvm1gUw6n
WPPQY6oLJOrB5V8163qMYrMN1jTcNHw0UVAPwNQQbl3JcRewsb+3i7qGZzYVwz5KRwf96B8kiVU3
+IOQ6GUSTMtzexAQJC53H3XdJBtrQ5lqWLJBDi55cJv5NTyPx3aW7taki6sJ7ElgL9RSUauGMbVA
oK4QvKZjmUxso9ks/FyDIMX8JoW9dqnpZ6oeymgFdfXet7NJGmo3+MtDjfwtC1HR25TVrWp8h8c7
y9CR2OR3V+VO8GcXf3NrxzCIySwmo67z8A7q3RIaNjWzmyMiLHXuOGQvL4pWAJa4kApdm0WhryC2
hBS3HXTDysvLcM8teF6YpnDLthI8+ZQ1rfhyhPO5GwGT9eN9vQeZaLy4gV4gy3Gkvz/ZQiGR2yEG
O2g/HhA3FWQiC10fLJ6GxZutFNtLn8qJsk97XCTfEBjRwDRTpyzQcf/bi26B2LIMkoOoEjeyaJxt
Eq6D9DRSjyD3bVGD9AXAhGG6qd02jlc04nxwn4u+eTnimnYvIxiWp+Ea/i0q6tBSLlAvqFZilZlE
VMVKzrog2odChhMH+W6RTMFQLY0V3XGKZzAXR2mA6yUDBnzqZYIgF16QQLShT57z/5/XNvyUx8v/
Unjn++Fb8A42URPhh033TbPC5AxYKwL0AKwCPualY4HAUJFpDiyRkxosFRn2qhIPZTkFSWETPjvd
LwspNT/ZNqAtfUbPwB56passbkgwAe6Pcxsl5tPVlnxsQWnpXrm26MqTNOERouLipig0boBV6cUH
ZnUOS2bxXQQc27SnawC1inwjfPw05g9Xokdsg21SQmUxjtODxVMaqD80jzb3/c30tpgk+hNJL8po
FmKY0sWaHSj0o8VbAv7hHGqpO32AoAhdJVMYGbYaX4G5xKYNqbZnpEMdTgvFUKKSS0Pp+Ixn8Rw1
J7qtm75KbCBf8oP2xXCqdHnUtniaVbUX2KeEe+kPbMo2OgyjJOFkehOeB/YTLw3mqu8HSS/UEMko
0BTAkWWzHSkUg8+pncEL4qmkUhre7fcWzLIHVCQZMEygt0fm0c/Y2e528zIKlYTE3spG+wyD7wDW
G5ksZiyQxjrDH6nINCdBrYk9rpkMMgPPjGqQvfUSWCYTIhL11FVUa9nTfeLsLqabm1g8M8kH64nf
9q1l/vK6br149e+urFy2BMvVUmr+vjThkSaXcrdwIrfOsPmUXaAZNBpb5qjHznRZKd9SMaPgWECf
shG3IaKx2Y9J6L7WANv2wprgEnFyNK5hGnWDGPw5HzFU3LAwruV4cX8x8/d6rAVuYSRjrTIXVFp2
NvcsP4j+QzBNmlVgjF3shjenaLuoeeiQI1oUMHJ5w0j4n13vQBxbCCZQWT2uRCpDxiMF2BA/nuq4
/SFym7yTJioc6n3vWBsZ5cZXmdF3TgrrYxK1vxzsTB3ICCdsLlySAobuh05cnkexXUdo2M1U5KgH
2/sPf2H+rYVZzQ4a1TgOtr48pH3L73Qc8jwxU1PadTxjwi17YPJAClPjvSTx5Bt1xsMsULZJuC91
zKFHPFujbPZtMS4pZL8hr6Yvg1hqDbCEwNhy8oBma1IWG+d2Unka3Nxy7QNNL7KPORg46weIMJZc
nUDc+QCeRusGzGlsxCVJuVMp1RrpyBshD76dstVg8dP92/G7slMLzTpxXM45VXTHvi/sZVhVSslb
iL1RM6lz7BLf+HwmQiB26VgFjociA8Wi3betHMDPlPysFkmSMZRHM3SRBmt9UOY5rXY2/pWpwCq+
QkYEm4MJ0fFyktqpF/kms1UrVaCdxN49PBJsz7z/yn86gxtxSJZ9iomBp1wBEQCUSzWpRJeySdd2
8wa+QdbWOcTg2xD3BWw1k5QePJoMUnkfPpa2FkwI34uhzudTWgYMRsYorJnuZSc2pMZTcy0zWkYT
WdDXsdAAqaaXvAKQTgfIjvsSQoVRGniRTLd9+v/lAyqi+leATK4d3o70QfOeiIy0BU2fT+Wv446+
T6107z3cYZCpZ+RvO6PLdfB52ilUrMjrOSXc3SAqFpz2+FkxuRhnkN26F5UjhUhuqbAuDKSF6jOE
/5Bn0WWFwMbhh/ejHPRiVoNx/NDS3aE79P0A9AHm4Ml0Uke7JuKfF6Z31QCHC2tqNKGYk+q8uyh6
pXRFMA9zf0Rix6PAAelLqPyto9fn/1tY8eZOPoEq79yFMM83vKOq37vhCiaRpbfUQ5g/St/adKDF
l+8+y06JAplISnmolF/xTFr5od+rigTOlv9FXWVCgd32smxnBqRyJxZTSS/Vp6QewbED2W4krZdk
oolMH5fybGpnAokSjAwVEsHDUwRY3DaoX4KhH225uPz9oamAF5/a+79F10IKBEH70BjP8/2qlKaq
3H1g5RPbQav6y13Slu5mmaQq09UMwq6O4e5mSzncJylqjDmEMbfu1A3ghqmW10iF0OV7qu6RIhwV
Pp623yoy/+6eIyD0u/DmbC62qWE+ckg6JNgueBPNslyIZ8kvp7pMo7Y8BAnuXb+3/RvcqT8i1xPp
wZsuXLhc19FY2VUYYi0qcnJ+RsQB4yiM4SVuIzZhBtpVVko/GCj0QqULjF8D0WqRdIDHBbNtt1AS
Y14u8+wbv9NwKI4hB+SSfMLBcQUXUZlfxjj5Cswsfw8B5KcqTR4QjmFOj5vxmryLvBh0xPHf4f/2
v3W7JMiPT2wNd0CaOZBVqdUxcywDhZ+JBUHDLFnv3tKzxbCbFWVOly7lx6/tj66tfy2/ixnGsKkB
PNKHvRCPF2u5NvEOpSMhDmqKzNA8y2H7Cb+bmLV4x6DNgEx7zxbyZhjwNhJ87SJVUeuDzlqQ+BNf
YPMAUrCEzuzDml7IecNDax2bq7rWMmO73SAuprT3Xk4LdlfNKDrPJVnUzIKasmHSZZwI4PjmWaRM
oMN92PAae+IVgLsRtRvrUBiHv1WwNY0R6kTMP/6r5d1re41uSnh13N+vsViqDwZAJ0600/tAeOT/
FkLnGzDT8t85TGgM8tM9fIahG+MUG00ox2bQHisLhdVdKyUBO/zPT0SNgAOQ9FHbdm1RmNDhV/N5
uJCDhvqxpIswF9gyHE5mAO8UYM8L1KzxjJ82JkwVIZr/d6jw+fCDQjehjMppJY3g0iAHen3/5BYR
8ziXH8WzI90iV/Qp65rh5WlL+TsTJEFnp6e8+rtveamljCft241PNQMxiJH6m+RlTw7MG6LbTqPb
+7d8nE1apy29T4jxBVHO89PV8j/Nv0qn4zm1y2Qb5ojT0kY/LC6lmNYOoCzx/0f3bwVtlVHz72Yu
jv4urfgBRaZ/13EwZQSuTWGVDaSRhyiAlJZbW9QB5lnTbQbaHZ5OnPIJYmIoOB6tN6g6AcrrduTA
ZDsMJ2b1Pm5Z2x4xSbuOXdnJ0A6h31qc8Jol/iQeNZ7BVRd/oDXNaCd8W1py+bRmwzkIDPyjfvSl
Hcw0AiRPmB/lm/FXkA+X6ALCZyq5z+5hux/nqJ4B9xfS2EEd3XjR9tTKE1ka1GeCoyaeT2HNCnBx
B744h+Z7QmkFCGEoUGGhiMu0N3+AcREJVyE3p9UqDNRv9Ocgpv3wZvmbd32rjuHn5Ccs84ymwLEs
5qSpeLPo+GBCpfAVW5OOI9XveXI71zQpKzOk0mi0Bbg5f+iy1RCe1WGnEZFGk3jbFRq9q5LqUNBP
gyheT/RZ2QV04WizZs0vAu+ET54MNo2V1cNrPDsOgLFoBRUsQAuNBVn3ygavPkxxWJp5LMhOuJfd
g6IlQPidZiprZTm1KQXtuRWzlOjYDIZLoNLVxRgAhAEomWNNr4deYKIvQfbWULiTyZENg6j9B/RV
x42JI6joi0hV8v7p+1/TR6bjth7ao5/Hr0SK4+lVjcXf8QebCsng4bkpdIysThumdmGtv9RqgePh
hxfQijOXtrpHY8ffd6wfXqmMX1d9sZr73+p5fiRDHbminZ5Xj+PolnePtBDAOpuuK4VOu4EBgjFn
aEamPAZ1L8WJmuY5dFCSYEE5+zI5f7NzCxHXSb7ThjNs4AuoF0/ClKVq4dPl8SjZTIxQKyym/mjl
ikzkIs2+rN6zXxG7miMn+XfR0eTs5z8uEr2Lin67izsAuJGLXlghku+TCxmNOUL4NhWRpjWO6YoF
BNqgxFkjEsa/J7UXvU1z+aUrjovywrXEScSgKKoM+h2eNaNMGd+9Bv+Onw1+k505gCvMc3Xz73ry
HcWW4KjJsAURfr1Him8lHJUP9vpTeBHdM40fmCEazCTvsPLEZZyItQy+8nk/Irf/Y94xVlgGyVWx
wjFRdKm8JBUP5cpVPDU9xnQX2BGeiFyiDv0s/mqoAZqnzvr5hLeejPa2lt7Jz7jQXo7TpTcZlUk5
NXJ0Gx6LbN0F2Hobe62RppEduoplcrhb2RhtHQit2l7KVrn1nUjTyvOckLDCyjbYbpCVh1+08RCs
NUP/xZ/5E+7x1M1FH54UlVUs/g1+HnL354SgSg4vpUmEBfYmqXuZsWvd4rjVGDWGPiEKZqjG/ENZ
2Q9njXwvDHAjKu+qeBSqc0Mnh/y9FaAXKKekZ7kpLt9DwFxIzwEeFKkUtXzwxxTHHqg3/NCpRcve
e3ahYnxQ7OjW7tyvppNv/RYw0gXqgRSZ7qN5o8GUY2k3Fo/ghciq/N1KyUBHkpI3b/PM+6xp2HHo
2U4FimCa/ia24LCykjdVmbw25sjRBOUwwOpriatH+lyEtYtPKoODQPZhsVBqM3l45qdXR6k+75Uf
A79+zMtkPigGD7IsMmBbWe7Z68bvu9hbClOY7VrqSf9xQLb4sT9H4HLrnOij3GTMDPdqneeijkPd
VFTNp2hSiYUKJQGZRHFYlN815PtAa0vET1k4HwOC3iPUB1f3CJCRyPcVuK+TjFsWO6kNr2s58xaH
JRnIlxO1roQfSYQsGGyE/Z3KVoQ3Bi2MSzMnGuBjANhE6MYwR/sxve3Yqr1h9Hp6eVC5i1kzyn6G
+UKajVLvypPsL7xSQLQ/VIIrLm0CBu+2wFCWs4VQmJEeHVvKw3Hvr8d0PS+BL2RPsoxegcz1eJ0M
VAatasJFHIPpxFaTs5dc5XvMnFAKh30wnD3eDMvR81ngcKoZ0H4nqfQ18hhMkZl5sJ7iRb1QQqdY
2oNyyuzFBVIWNEseDuDwNaBU0ah0OJw1SSzZQl5qlikjHLV5kYq/jaqdFjEZZ+k7FNp8tX9lIsJW
PgjQRcvx06bT8b8X7GbY+zYx9tMxAmXCLRKkuUOzi1w2YDQI16WJYKAne/9g6cFlj1xTmY5wKz/Q
ik1N9iEVvEf3yzeB0P3xNEKf0t8XljtGxsfbzmvMV3yGVmEA4HXWOpKs9QLvEx1Hl4FvPuPqXHzo
b5SpQaWcKn9F+a2yNWRF2zzKNn3BHvzHVza+EC4IK8Z+rRe27t3pzNQL/DZDHvAMMZj/DtHo8Ct1
oIXz7tKjHjZwPqkJJGMYMGYkli/vJD3vrFL6RqmbsTLw6/I95i+Plx0HKYTtn0La5vYEXnvRgNtg
mI6PD8DWCWSuGmhw8Mv+z1de4TUrPbgxd+TYlFEnH6J21aLEyX5A4ByRKA2ri4wDbKnQRo4WBDCW
wye6Lh0KHm+XR7SJ6ipRd1ILdCZcHnNjkqJVYKOuRMV8rMrhFwi8TNOwwLUhdqXYsHO37IbzqZ/H
FgN2m3ajI23CntGY3QBvqRRPaVUbeg4iwVlIoRzcX2QWCJMSXOHZp5X5UdknHWCSAkilvr87lxwm
JgY5+/kJjvMEQ0OPd0ZulZzCccJF++edrOLHLw2xMEwS/8mVkvxDEj3HsRet+PXOEwgQS6MQlSif
vHcgwEEpLj9XVEg9nLYaVPaKIB1M1utl1O2WIE+1OD3yNNVN04nVGzsRcBJ3iaAqa2BVMFuNpehY
1LaYnHmilm1sPGKBlDyAXaFFLwvgvOB7DEpGy19n4MWF8gIG24jcZeCJJiyTbFEGcDGFHRngPbf8
zxoz0+9F7p1tg+AvqBxnLlOVMa8N1JW0U5UWKH/N3nN3fdrOOGPo6rNBdIcm/IsAGbQv6LRqMW13
Jt4AQxpeksP7qs+tk4i+PUFV1gZ9L34ewhsB2avk6bTPAtmC1RA3pUn7+Bm2XGvbNdqM8wiKUNC9
i0jsBQL8gqlqqejFv2t69Qt4tf1VfRaHRLHOkufEbAB0rTanlOmjzkat04E90LXCvm7zFXP0vWgZ
//tCmKyk9ta/A4JGP81XrsWoGMrJOhMY2o3SoFpsVYcL6FbdB9+as/s/EO/UzWeG5jbRtoBZ2vl/
Q6QKT5caiDayZnm6O+cf3OsZgZk7yJAfjUr8efEX+Etz/M1n8oHC2wq3Woq/GID5ADeGjEwazQf9
kbOn7EcWMj/4e3gW53Z/82hL/wmJUHmYnRnztotsCbnXmJ/vaplV1MVs+wlu5xaq1DBjNWRHWiU8
74YWD1KVnhFlqFFV5tHvQxKqRSO4Sj/6SONlfnakkTOTH+5Jue1uREj3oT9dfjLKSMV0CTGF318i
hwa8IyNAHJc+8WnmTbAFDQ9gjZ+WlxVbww1cjvFLgioSccpHlbB4DrMpIISbfwpYZ6BayJpTFwhq
7Peo5GgYzaiWG33gWDUhQFZCvjD8mMaXagevoTLQcs37NWI5Z8YPtfefemh+TUsKj2ECD/CE0P3j
ZbYv73KQwGlk+YW0sX5MMZyw6SZsWa4N65W4r91990KNsjIVFJvaxDRq7o7bZcXWjw5QelqL7F3V
N5sMiqgc+G2lykwZAUDELjRfaI1Pqtyt9kpnMHICcO5pNs8HgoU/kDZ8zVP8hGZ+w3E+JVV4lC1S
DTzfyZwyYJRvV5mXUwD9HQrUCbLQIpLCCDbVuvjghLwQ5WGwkzVn80VttEO1oxNxqF1LSYZPdWXC
40+mqufTzX2O9fGkDSZ0NeE5jghpMq99uB7p1sM3PsG9D1wsO4wgRzpWsK04CIBZ+3ORU3PZvZIb
Nxeqq+FhqmTCnxoOkCB/YIdmZqA1Bcu75i1LhhTjZcGW8tPUNKpEHdIJob99hWb5tlV/xOlslBx5
efys6jdaneIsdmUts0BGXzFGiifX87fmzv/uc+fexBCl+DNL1d7qpEObT15fFesqPzx8KA/bVFYt
A9rBBdNyKV1v72rd163xmALF6PhRxp3KFqNWzwaiMJaGLJF2qI/E4Pkrtb6em7NscUj2WorKFdXO
PlBpZZvPSmjc2eaqzUkyKU7IWGzm2gIC5yyMUkkYRX4zcTu1fYziAt5Qpm2KlFeiBZjIdx8q21vT
xG9xoHweLw4gn8meR51tTr3HlaoejcSRMNzNodbYZ+887M6LmNl50kCQ9Gba5LyNBT0ZfkQ4LmKL
lQokJSuw7E20QZG46CcMKDeUYk64L3qrHkOva7l3Gga0/iiV310zV43FcXm2hlcYMetBPYUgBdyC
5LviPwtx/e/yM6xefFl5nzjnu6ioM0LxVXJ3vwhwaj86luaJRMdWR6AUtFp1rUWTZZtPqwFsqxn9
vMNLfPSLC1820lD1i+BUEL2CFvImmFS6SoEVS/LGSl6QerQ6zwT3O3vC6z811rO7L92D7V+uqYAN
exxJDQ9P36d02rGdM/pOBaWQOipOWxYl1IcLHdfOjQ7fdmDSCiAbc8Wu74b0AP0sM32zYsAcUkWr
Vw9QD9/bN6FgsUlE8o73U+Eor40WSrQK33NVIuRfMDhOfC8vpu3b3BmeV0+y+e+p7pjG5CVPkKc/
gQtkV6JuNYiMX1m21O435I/YygJbY9f5RFWoa39hZg1UygFMhBvyzlDRLbdW+aO/vImavPNJpd1I
n+ok+ArC4Div7LWUy2UxIdbOM08B8g6+rkHh7Sz5cAzZzyZsHccK1M3fdvaWHjD5suazQYrsoUhz
J4/iJXpRoesNft9XBx36Z/PcH7Dbz/awYY5DHalJi40LY7SzDRkzI+wVJBX2GJ0X4HppgajyIK2M
adtFPBk3hKQ4c+VUduK7jLuSurxmB9La6cvYSko7P0G6YcGf28HGxq+2CAkxVI6qRWcJ5Ar/Inb8
B6ZjU0Bwi4/gQh0lMfExJ+lEeXtP9aYRpjt5Uq9XqSEuNwkwf7okBI8MOaViwisY55N4gooPbTzK
7q+GNQquwtbxnIbYrtnDFmbhv98/0Bhtgwwj4mQYDtIVLVBTM8nUhYY1ic29xKMVtAOtyzX1IuDh
nGhkPaqTl2WRw54CPKwadA6LE/NW5czQBrWk1bMSBvF63R0dT3VzeCtCjHwOPbEZd98pjAbbVsgy
h2sr9bt3qhbFzlP66NOOmiJflKxINs3UxAACEf90bWXK4g91LQsabgEmReC7U6zEzyi8WStOjwsG
AfkQYF7nypcPNNKXgJyEe3l50qxkDRmsY2886YcXCyT2JcHTai4kNJtvKIDhfJw652gj6mIgd/yu
moCfGf61N59ksMleDsSTF1+VJ54iQA5PNkmm6UxTfZQ00bSNwPkxaXNtYQocbs3mCToWeuANHEUX
dpBH1j08eWYwI5E8f1jaFUuaxJXf3Gm8PoE7ZUXHe0Dkh++zHy0cvlxdrS6rNk9f0iAX1UfbQzC/
UHRWZgK+hp8T0HopvdxOnmqOGpcvvpXh9JAMzmMLkNvAh8bN4LOxwdiNj0z9u8oYxYZa7s5Qanoz
Zg4dDeKxrBKYPuZftOM0nqzTEJA88OugyjEBnJLDX8lTVDRGGHqzNhgmatD//U1BkfqNlT+LeC1m
GishPqOsYZXi5TaToch6NlFcTAMFedG+An8jJgImkc/mscRfAjgUImm/DiKxH5Rw4R2QlnM7vGv0
9wb1T8+fwSBOump0BVX2XwLJTK14lzSH1GQABhRNzhsyhKduu/SVwRIiuXaMSclwIbPEg/PZAtpT
ekEjcALMabfHWyK/EsdM/PlbTgpJEThUP/peAKyGOJq71sNnBLSvrvbWe1qn4yHROdOhuTlYBusP
6g77jd3I3LGlm4tsgI9J7ub9BvDJuhHUxjHmyqCBEb6xtajuDMu2LeLw+5vFgMnS7IO/1yyhM6+Z
I+UhX9If6MvThn+qfZ3IxEK7AFx1ij3HUzTQTX+F+Gwe1wNP0OuI11ptbCO8UvF1k+Lt28PYOXPG
obF2myrgdVqcwMNiJyM8M5d/VE5HuwDe19c1W4eGIJmCfF86QNyRGjDJDVNuSVLmES02mZKpAYf5
xv/jhyAaXPvaIWkByITvFYTZKWhzjf+k7wMxA094ZckWMBttNzBjPZ9913KMUjod3CWWzJ4P0JvZ
qsCt7Ovy/YnQeMxtry5jcx7IqPEcjCstxVanmWRVawU0pF9t5dI0fW82KW+qoPLCN8zTzLdgwCKc
mRHsr7D1TuRRkNWc+EjCc0WsDSAz3296Jqmu6TFTGzI9iNFDWV6XGseEDTjc1imVIpF7SQVgussw
vgJAhGjotLzok3qPehboYnVJjYlqRbKYN2yRCb5uX7rvznx+aR6UGTMQYmYXJCOjG3ns7SUFcVZo
Qxm9GEo9v3Hy/Rxa0J4EniCQ5TdBCLyGi5Xpb/Jm/MxysR8F/kWb4SrG/vS2FJYysC/NIjAMAETu
wHnFQjPAgJ8e3SpFksRCBsuQrUnP9BgRGP88OIgcBGCgABdhBUCU+nDfIiCUx+2v+ZcwnaCG6fXS
gjSXPzdRa/dk663ebGwnMoHc01yG656Y0j3c6K2YZhAZVyBYZ/mjSh0wfDTPlB3tgVPHc/aY+/SF
TTeJtD3YcS7MMn8xsrX9q4g1HWc/Ia82U88G/0A4qyIBx+b2LNYLVUNrDnMwKq44DMfiaCM/BiUm
FhTby6Pk1xwpJaXR8HDBfZ0O0ExMUou3doTzOEzq/vPjhGZ0mOkqwsjhxIqF1m/XUDvrJzbfnuVB
G37bqz6odSJyIyvS0LVOGq7EgAX0Ct7ZncxKHzza+huzEVbqzgAIa6L6sE2QxpbLO7Wyw/IgLR02
rAmoT6dV0MV5e42X62Wj+OwIVkCalOAQPgplPro3bufYEK8xCjAWX2LMGrkQiom58vGL7juYshOO
coeIlqlUloKMvbveUctFX0qG3vGkd+xKJr2lSYLPysXghcT0EqOKKFOxu4zBB8/aH9FYXyyUIOEq
HLZp7wpfROr+nY+9QaJLFy25/90kRlFMbChLv6ypPxWOqssAFp9FAwIoi32bGLYlsUCjFMaWNaa1
wd6UBkWERUD3VmMPd0N89j9zpqIojERzKazYbMDD58Qvz2CPZ/lWbqKMcFWEE7v0xpbPlSq0Z2UK
s7d8yIxpxgYa1/OmLPIhgU0hsu6uoi9hh/JTMtLuAC5jjNIVxRVqduaFzYFNEPtHZeKcyWoU2y6o
Dtfp4FeOFD2+l6KAwWgiKo30lHUs9fNeiIDn4FoQmrdAe04tc6vBx2Y1twvJ7o17km6tuM3VYiap
FPyMclk9pJf8Ou/gfmWoQY0HvRhKApxh4S1iOu4S3Si1AaHPgs0T8YqU7tcvx0zLPYpGDkaD/CFQ
eLnecaGX+EAekC3Kqzm5xWc8hFsHr9Jr3BJxpjEoexsc8LZrl9/NyVRgnokJgPgBGBLqGwfKV8LM
DD2m0sRxLD9ydNPfjJm20bXjSoEG24gPeBvCQ0hP6cv28RJqOXMy65NcIjFeS6YeXR/W4qSOx2gr
OPRUCoIyqRHnJV2fa/6StZvQvmk4JcjdjbPVhdAxeIQaP1FpZwLKE7s3R+wAJgz6meaBW0pF3N0x
mZoAocOBWJDmMGsWvHouR6jj1p21c04YjEfjxwc0QilEYcMYiQQdRd4abpdV1YWNbkzLmxe6j5DR
sK5Sbqon3rbFB0OM7bWJJDOSgCEyi28zXXVtvfHYsROPxD6tI/43mSB193pQvIdH1Lq18pFq2gfZ
2dnaYH0dWTZpLZPpa7cgv3IplXBJpv75KDDW+X923+JsmLuQUmVYByZPXMCEpPR7fBww6iz0wJsN
7iZ8iZym/X17UwZUFEEPup0n57A9TxtKzitGYlSjME1ggIu4sv9D7fXO8gBr0PhBfdNnPjV74a8r
Yl1LIfE8+FrUaaVTqbIzU3VI9CI0uEX8QWILrqcgQTqtBsLeDJfyKQ+1Zu2reZMbMn6o0dAdRVSC
tbPCVst5d5l9TvHttRiBIPENqzV1LNODEoLp0aaKIrIKSZ9aycPhAETIHCRLEjk8MAKsYsFDTx7A
3LRKgdU+htG3OucHGE8CgZKSGtfqZZoy0oMgb3n5BDjddAgzdh0Xu9fhaND5s0Uhrex4Z8+O2nJy
AYrpeE8FDQW+rHTs6HUDQi/iL8mSghNx6aKPp1IwybeA+Y0gLS9hWiUt9M9R+W3YzEiHt3gUGT92
zJKSnFRpL8dBt8L9xq4++69/lbHDdDjTTBRwN6fw8uwJARE4/K8vbUFCKsZ2Tses8Vr5f4+Sv7Ax
e6uVt5JoMaPi+97oXz0aWvocK8GvguYydCCbys81VagmvR2rfUICvOyN1C7oyD4lCnztxwWhMysc
13aM3eU8+Xg3e18l7KV+kl+FYJLAkdocJHAFJEhWhcLWXoG0qCL/APYFGMLM6id/mtsj5BGhjc/P
Mq1h5vVZttI5rToW0QY+eSVI+XPYt+jYYeSE5K3AAva9xBCC9+VPNdDC4vApGodZ1oCy2mFM6bAi
kwHmvToWCd6XiE+JTzs66nCA3r+fN7alNEb1ppv/hyvY6fvvfB12DQfhsodwYTNmyljuixtfO34D
TLtKn/dqk5e7aV/wsx8vedmbTqxD/6Qgtp5pWre4dDXMMdUTPD5j5gkwQZRBD0NhPY+gnPnLOTu+
Aeb6j5oXgZ7IBvP3ErYKuwmfO0l7HTeBBOzjn60rhpAzixQERqsszHsc/TJO8qpZoaPUH5uNpT3S
zdQaesaVdOzR2rLTtFjLLgpV8w5121eYt+gyEdhCLYt3wV0xHmPdxy+dTrQkqQbEZNwxot+08IED
ZT6I1FwV1Vq/GqQ2SRWIpe11ioL0rpzHlJuB8SZlb+dmFKUusQS2y8FT1ArHEvxp6dLhXcjFZS0E
NzQoJ2sGP2A6QIT7rkzegaVSP38q1/LJyfp4NkAbRdj5bzUSCvAeByokoGenm1YOeqt4clX/wQLs
PmY3dhj0fQC3TuUXUUhswmLa5E5IWXTWYJDYtT0+MO09O7S5fi12J4Y/PIGlhZYCkRxXCEEfWe89
6J76UOMZXRtWxVEn/lnPXhnJKNBCEBPZ/isUXYeK1R1Im1IJi8ZNrbFhe4NYAQKQ4XCLm7nmoX1s
uFBJY8sWRLkD/oGI2ocab7fBppedFgqnb7Qq+6AOF68B00iNyOV5j6oxoJ9tTQZ5w47x7AdetL7W
pkjVGsTdIi6psYAJXNu8HE0+Mz42dO7CIsSnnWstK4KbYubqMl0YMY2AXKoMeOl6g4iw45dpaZoX
t1kZBz3mYyt4DZwFsr9E6cEDU5RUJjzgqO3AbyumlB8kSZ9nQme1Zr3egb6Ypa0jfN4+UZ9W4DHJ
LMbv+BrC3OhjDQ4n/c5q8Y/yl/tsHmQHGFQ9PxbcADtfj+5qAoNGNK3CSXsqE7spWVpo2+oqkOvH
CQifcYxg9lm6Buxjna2nVBEFYQHeiSCA/0VT1soDW08i4K3mLD4zO57VJmCHhogLmz2cpMIq4Om+
CU80MvF4AVU6lrFURMsB1AmoBoQe+x7VtAlFKYo0akeS5Zbh6Y2o2P1GMOChH7FqtjfkqTI3rGg4
k3R4AACoq7nlqBlZnLkvYlkrBMt21WauJq+6Vw3tal6oF7Ca+Vb0GpSbziyGnJN3GDvPlNT48dQX
Oah7e9fNw0NhvvzPe+WB/NoiDyICTmVFi8T8yo9vFlLSxb2PYIKr0XAnJ3YUb/kagpgdpucMtWGL
2B08zqYyfuU6knJHJyDciyfXKb+b0QeoxhfHSMpvwU9415SdSpMQ3ZI3bin2uQ0VkBsij5CmL+Ru
+LtGGjn8cIrKlS8urAoN7CexkIO++Z9xMUCyj9Bp+vfSSoIk8tniJGOzGIXMmDaHMSL4UnYo6z+B
LnezSg6KOKM2nyqIIi8YcI6VmqU++9PELgP5rbnkDQyvrlRYWzSHEjSOeg3hraYPYBjnlcAISMjN
cIXKvZH7e0K1DCQbcxSiUp9kINIVu4RJT5iyfg65ME7LOa/AOWApzXHnoKf2ebnNKB/b4VAV20Nh
raB5VF5KdbVg5qlHiVAQf0I8MJCtr1Gm6svhnKwydeTkUaFtZ/cz1KWsLHpaGbH2PJEY3KBK7LCO
SDWbUcOQZeNWNupbeNf/C6fTWb79jKhI6W51Y5uR2pHvM702rB3v78BSKkwtRK9YiKjY6J/k3yg9
HPZkvcbcuBfMjVnYzCsq3vyC2FjKBBongFd+Ew65mC2pS2uLdDRqz5k2qkbMG98releiSBCIPENj
BdwvxLWEaIjld/VE8thblnfBv69wVVP/wZ4DiyXBS7WtvWea074Z4zwdSEWRlQRVtHIFHqvzecZa
N1Zay1au4kvnzm3UHJv/KP1gHiDuiJct2FdstjfvTKAn45Tm2k/sEaRVw1EMmORdCgoVM2Uc6U3g
GRbcVnSZ30WgrLQCWiz9DfSV1OmbteSNKoJOBKNRYDkIqVGg3q6sS1yRnDIWhV7XPql8DdESsJn0
jgQHKFNUVXPXhthRXuWpgsTxZEnBJXYSG/Q3dRg/hRz46UOSxSm9tRzySYyOfRfvfGlA3tbZ6CJS
M8bcq5fH0AKTO+UewHoLu80/7VyzvkPlemZDb44G63Lz8OW6FOsl7nmAv3HF5oj2b8qe+841YlHp
FcRi+pYmeDVFt9NAEDKaYrRIo4ClYuyUzA5rYdjKrK+pglcqbiC4VYEBjDSyGYkKDSbMS1DNv1uf
Fd06YHNMforHENdbgPwhITHxRYglMQW4ToWTrsKFyW1PcFSrDTpPCyLaHoHejeM6HATqRyrFyjdl
2HWaNtbwgdqqGJnCOhpQJdQ3MJJAaSRMyexyj+pdmvyrnKUNTkFcDQcZVZ8zcJ8tAMdYuOIFEJpP
5P0SAPUyGAisb1/N+CPRpb+xObd5A1hbu9ygLQbB5749juX34ESj10SB2cwZePuuJ+0VMDJP1i1H
RYVv409f3Ino8UfL4QwPxMpPjcqV/A/mZh9ZGLdUEkxcQfFDAYOPHNJxpO6NCR7B0+K09M5huWL3
SStMQsfCe+ngzSd5wTF+3xFQCNQx7z2CeH+Uq5paLuWsQoAJVrbzIs9U+oVoOwi1e/zRPm8qtj83
i2X9v7Wl85QrsUSEowbd2o6bqSyue+2ckHThlMyjQZcbozDEqRAh7xaVKFebLkIsrgli/Ny/lryP
ri7QRRMrQwvG9d66rxDgHQyXYwQlr61WA10NuB8xep7Z3SY90pkuyYIosBbTqXJFwXDBTWrLgy0s
y3c2PiOp39Gr9R5Bjx91PeeBGbLbSOG1wU/0niCF3sx1v1s+NMxEUWjtMVPIlO7FpiBkNEn0l/GD
IqsnR4+O53o2anD9GzDg017mjU7y2Jhg/mmHIIgWYuBV/5P474JDjVAcKz4UfQpTo16LzXSVCs9c
Ra0N8ue3wgGbO1aKr4dmvWYNTj5eSlFPLhGIwwhz6hIIdUZznyEmiWBZoi+nyL4UUq0l2cI/vrB5
lmQoZiraQtm2O+fdExMoQi2PeYF+9NfBiIN8WH7qnGB1c771ic+Jj8W3FAwnokCbSQpVDyPLkrkZ
ak/iSP0cJY9w102TtVSpD7d4xJ7fw6BZaUFkHB5u3yOTNEQAMujHcR3GFLoauseJRDHL2AdAea1S
MEXZMdc9X38AlYMtkop9GRKGpUS0fv2hA6+X+mG1pT0hEvvjfVA7D0wMTELDBJWHj8lfuCegcBga
VTmYlVMSKe/uM161n9HxdYoXtfG9e4ELkoeYReeZK/kIrQvWy0Qv5Hr6t1mWdwxASZIebLaDy/wo
GfBcus2j3pIe5Pc6j/SFEwWocO4CVAhi2+wa8zIOhbu7LmjZNIZMRjQD+vVRsVdTunzmd6YZKwHp
NXgjm83iEtKhByeUWPpJfZ+LzJaSpskM/bcWHHP98szQIRr1YzFztO182bMDyhZ7YH2BNK7I/+Bn
DyL0k1y7uQMXf2q+/c2RyaRPlIkr8pdPKGkr+ijEseavxQ1l0Q3WIkSvgzniJYn4j1sTh7fpgtiP
qygr7kRgZGPn59bcVxpSQJrIKf6qN5O12Ew+D4+j310WvoOeL2ln09JZzdZ1WqSUUDY1zQ1TTS7N
pNsm8ZE4Silx5Hewy7VFGaLgaJAJXSggKoLtTkO2RB2PbJW9lvmMt1yWwl2ESNd1wFJwEXXXX6hr
P60qh+wPeGuy964yrpyv8ahYYKK4UBJ2Z2oDksKO1jz2vZxlfTx3WU4LFTYmM3BYQguanjq0SR1F
vwN2Xx77cPtVI0t9UNBGSfOOuZgp1DYaaPiLbmbwltpj44WfgxiDno/PhQyxkLklN/RyH8tN03nz
kF9iOHKWkhXXTuyMiI37TppKbQrWjxlyTu7IC7OOJDPq4fHiaBJvef7K7TrpehOVO8Kg3OC7ad2S
aA7u/vtByvKDTMBOFrOsOwa1X1DWSlX6Ss1No4y8x2tnlBQr4qisHFz/DCpof0YbxFxtjS6v8KV9
vwbnI0C5geqgqEE3K9SYlUzQATTbh558JGHo5IFM/g7/2JHRPsyqI5UY9KDErjLHTXtTuTmJPk3K
Bb51keWte/SXkgnwoEAPnAjDCiqp9uRH5VCYQFvBqc3DtGBPr/3/4PL49Es5sgNRLhAgILkWRYnF
opASVTOgLps7I8hezuwpkIE07Pay5Uw7RwQlA2ZC2U09pqG6qGmVT25ZeplEHqYFpnplAbpXqPin
CmWduxR6hWLa+3z+4IKc6F3GFluvx/GhFZYER4KbdEQktgQfatUv2BEd4dkVwBg9i+uOqroshklI
sh0vWLsw89KIiV8vb624121AYN+PagQejwRbXps6OSFH4GIAkGqyP3rMJYzBVETM7liQQUorD1+C
CQefT/2x3TuQCD3Zrt2BJr33GE92ItoJhqfUIIDxqKag9p2JoI80YdDPV0ucvQtRZ16u5pZf7agj
2oUZkwzPHwp3e1dXDDMQ3j+61Q+rPdVv3/A3PGesRUzd/C7zFREsRvwumTIx6IRphXgECC7tD/pj
aTKeW25p20pTNdv4KtR2oMUEHz4x/eEyHW6Ga6pNm+ap74vdKi4NL/Xdp1CE5nUmgAvjyP3qZuLX
WXjpxEecsVEl4pvawuOvpariJNyEp2hTO88zNGgReUITOmn9jUaTu3a5IzNH7vJ4NmbBz0A8CFWW
vRAgCCoAyaFZ3RYu+TCcVEcifciebvorX4maluOKgK1adc6lzgtl6Ic6U/HxY1O7qP2al0UBfGPS
0PSSQZVGeq5Lde6sUFxK9Vo16zI0eu1mvuW42SHdnSLCNaONdSa+FHd5ja4UWwLUParitWX7s/OH
pd60izxqguniU8+o1MNwifqmv7tTuSLagrRBYw6bglzK/qTir1myUzIkIIHdbicw86GmmCl7G57B
TiJnZqpRq//eeG+yJe+dX08Oyrdo5bMt6kVuqjWGlSUvReGBb7oJCTwo5XHgl+k3gdLrwuVzXIcC
9okUXXk6D4nLm9KPSPpqciPwSXRVGWbVsXJMK4xt5xMyiesvlycV9lKvB1UQ/O3RZNm6xOa3XI2r
+Bu9hQAx/DqJWPIpCjPpv0fe8LxUJThKJ2EVOwVaMDC5iludlHFIxPrEl4nUa0BEwHCsdtjyFI7+
kEtz4Y1RBKddGppKKd3nLrksvuPHj07qlchKCNhI8iJGcDnCE7pj1YvSafa3/JB3gcuPp9vlcfqM
dPBIB4wyxKPyRe75YriUbI9ZMAfST5HjujZKpuHvzxG8eNXtS1fbTKROD/Cws92CLo48k0agBDyp
J6Z/uHoLsDoNGT8m/4fmV75MVyObInDYDXlNOG8znClMXECrlOp05KjTdewmxEo6QzuyAKFkehEu
qxeEQDB4NbZXc/rL7CanrzmrkBF0wFLsp9i/7FapiNpO1ortHjJU3zz09t6VbDNLGjR2zERFWToV
ZdqsegdIFFdD/NwzUAv+1kuduDQng0MvAxqUX+C8JQ/D43RqjkwUbonmD97F8We1bjgbq19tDv0f
ux6fwvU6GJ56QMXAbViLdDtbkEU6RnO4IS+MggMC8Lk5dYIoAQRNLKmjlRk+kVgBLpfS0KaGsoa+
4HKf1QwJx9Jx6H0UA58ZEqFg45e36HKltEaMnVbKWJZm5s+bSUNwkFfynV+ZOw2mxhK/Q6foufdB
wwea9hKmcG0Aau/GSYre/n7hwlOlHvy4vLMS8drkn+hef+p0vOCtTba+7PxhrFtG6olaraHXOXMq
/AB0MfrIXkrqP/4W8Tv+03VVYaii0riR8LNvDZuwBkrc4oFRaj+8UuNzuhfTy7pPnFsh64jV1YK+
/+RsToAQxbOJ5cqmegXUI+eZ+x3Mc5Gwa82nCSmhTH/FUR6fGt2zjDIgw8R9xONxOdV9zmKw7Q5t
CDcnodwg/gWoaX2z7Av3wGgdtZ9prgsK7FE17eaiw9tQAVJkICDxlzZTgKWUBjELtpEmqzx/98tV
8Dxobd/gt8iGQKU8jltRGYSKcjmuWTWYAs1ZoJxKR8m3ZM79jWpz573wfzhMrleiuk/BUiDZWupS
z/8aHKo0EfXMkDeP1bjyE+qryj610V2SyMKh0Fei710cvGacKcyDeOR1UVQdpZOm2WlRK3QJB1nW
wCwPv7/x5yehZVXNBHpW16Qw8JuxiFffB0tM9oHDY2hS/yDMjah2ATqBo175Q8nMjoizjZ67Vc/Y
cvW+sKKgBoaLiVV5lioc5nPfDlq1pnZ6X6ph5Qkf67qiLQZ1fhoMeW5CrknYBaMSQIcdqEagKCJE
T6XLg8VvgZnyktns+U/n4DZalEANEBYMTr1ZLznHYfbYxpIm1BppyscdjIYI/7HX9X3zpADEPp2Y
fLj/URY1av8HvJqRpm2ZHXRYwJJP78ecWtHEN5LPa7xtuij6MFNa/fNB7M6UjJBpCE8bPpHMzSsA
YO9LMbobC50xqzl41bHh1/kq4nbOw1hMl7YAH5h5PzxWYVmjcZS3zNC4dsCPVWe7FxsTqi7CR3mk
fwhrsl2llIJOyP53iBPNpYKJNIZg+NvwIt9SQAJSsbvhRCEjwNTRtUZq0m1CzB05G/+avoZErQ+r
zy4ZPB5NRqSI+qR0fQZbaxJrM73MPGJvGuBQB0TmoCiKJnDJ2ivQ4c/GuHj5y+Gm/h1lK2LCU8xh
USEtJaUduE3OgPDA86yQ6RD90OBVJlCSe8NIbvf51M3rlfIQQQUxNroZg9WbGSByIjOmN0GH2+R3
QdzHesVxPTJviGm/yeIazPB+7bAmFuDFdKiwun9HK7XHEBXKypfWr1JaLWDh7rT2Ob2UC/nMX8NB
ZdFwJXP7YrORjK1OGBnUoJWsQzf0VtmYrlaIAA5nNwe3Xm1ZK/iWDsJihXehto0UZKIXd9NQdhLy
9Po8Govfboe5RPL4tHuEkKMuc0i7x09BS+YXSwAG+OGKOpJVn0eYGdBcUe/YOKMj7TfxeJ5CAu3D
RjPGRgY6lgTZkaDOJH8UNYuXuA2Kk4z1ZQHXgAOJyZOAdPqAfqBA63oDCPwK1nvl3mpxiE7p6ZXz
Cwbqq6mI0acU0wq1p+sNfJLkli7Fh2z1wbERC5ZFq6rF+I0GzPEATepgCoLY2VL6uddYmAG5XDPA
3UWiAXE9oZkN2giFCDFnVLpClpsNor/ZJbiP8fGvhkmlRNWg9lIynQ+0FCJW9Ur6f8oRhjUNxQ/O
YmpV4JEpgcMVq8aOb7bY2ts/AOuZisuncqB4jc5bhAKPyPsHp/0JLOMzc+zoxxRJ4Vk5/6jnL3qk
6X9sBiSKoSy4YXnYNAQHk/tPtXq/U/DU3xfbYkxB6Ou3xUX3Z2NJ8COuazRtB1vtHLUU4ENoPKQf
Mm8DMcizreK2QiXi2aX6LTmOuMAGqUKKHNFhcZBj20nCQcpo95nHduXuNCA4g1Yg5O2BzVSkNTn4
2I6E8w4P15qF/zYXuVO2VZy5pFz6A2YMUf2rOA2if9Yqq6BRA2DwFpFL5yNF6TDQjeJUWgtBB502
ieKlj0ymS6uXCOF++Mf8tGpts6wAyPbcI74czNospBe5ib7+7XQ9TbOA7NdwVi9Ie8nQzpnD3lR2
fa7KMoVoEChB9w5GdRywZYDwgNG6gx7j4SRtTtVJaSYcEUjsNukgmF+lkz3aqReA+2oMi7Tg38hL
oeDy6h3n3YK8ebsF3urIXXUc4EhwklNzTtHh3WCSr5RbvvNPDJUV4qHqZlAzBm58h+zNqDIpEZxg
nD1vmfOagpB6fpehcAgWILDkpHyY2h2A9/1XcO89o6snPYsGlrxnV8EMj4dryy78mOiEBE1bKVzt
qAZ1Exg0Wpny7BxeIhxtkH6OQU+f4i/PLaru9+Hi1duBW0i4AfS/viBDYzLX+LzHZnAdBYdIuwlE
PEMm6PoP43U3FtD01ybDa6iYADi8vJzmRvwePLZGQk+A2FmA2xhlvVb0IWrsF+vFO9qJdcJu1lrE
Ln+k3UiylhKAuWWencIiCTSBJuDu5vkldv9n6iV7RL09vdTzAtnizfRpcMxeSvemMG/Zf3/WIx3H
INGJUxCF86V8Iz8J1j1ZDXh58nIiubIe0ZwK+7fk1504YYfDKWFLG0zBEdnAw7YudoK4YsZd9SWm
XLTB5VH3h7UmleaJBaAjVSEMZYt5p19gHKFU98+XBNeFeoag49L2PjetsKhRj/1UhfIqTXU3ecCr
zqgp6b8dqZu9NlUrJrOrnuTUYkyqNPPuOkJbdp2x/o0Yj3aV2zv57DMb48qCGzTYkJcneu2V+lnF
d0sdhjjcChCGVtyk55p+oq52utN64Isd/99XjtV2xE8GR3Qr6QpnDQaJG0RczsWV2xAFPBZCptHN
d70BkU2h/3pSF2qkO8mrzsAGaUfciTUYLl42XOIoJgYwblApT4o91jShPpBl7A71bGM5hexLRuyV
33JTIGl85yFZfURgygriMhETo+UAgI3r3Pex7GFOqnc1LCX7WRlqNnNr0wzKQS+WN61MgivhhDFI
IMH/lYW4zZ6m3LzuioiedU4Q7MudmHoe3sM2in7aoQ63tShnk72rHbdqdGQpmk4QT1SPzUryaAgs
uNtJ2Xac3rridR0DV3qQqPsXNGM/sSdcqyaAwZR65U9WjfhmDlE7+gaD8TGdLG7d28suAO2Ew3+p
DomKSXJ3OQSKDfHTyIc9AqYk6QXIKMpWofOBBKN2hOoP+dQUoK6RQqJAXQ/Sf5dZ9QD4+wSVGjmA
uGfLZv9Bve11FvJBhfWdfh8FrnmeQEav6qe/QufontDjHv6Pn/nOelYR6F1hkcfsY7/rJ1Xfp/DM
ZfZyJ7uOsBDYEgbpU1BUbvdLZKZ6uVCsc5HF78ukmUdTlxjtwAyjC9he8zdFbRw4qQ2UlLjuP3Dq
mRVGoAhd60Za4iE92g4Iud9CdncnIwSlVYPE20fLIKtnHCia0C8kAA0k5PzbcA8U5CiRKCGtZRYt
UDPFSUcRIaaH+Sa50nrpvgT1b9qcUpcNk4azuEc3EoQC6aR+5fUBMoTXz0TGfzlY05StVmMTlhXo
SaThHXn3gyuKGXhzVLqMSmcl/Va9fDoge4UtTDOzcI23ko4HoMK1XNLwHU/yNWILNJeGaRxip+3L
3FB9CFHhaX30rdbgJqJlXRrIz607lC21RVLKXw6FdsCyl3cnNh3DCBuwOcxO5KQVrU7t0W65/0GP
qRa0WavvxEe20WbQ8mCNFpgRLXwQnXDgGlHBmIlYdMCvviJZuNhjRGImtuYxJvr+L4OIoC2TI9Ie
Hwa7AvIEEdIbGBi6w4hWSAFzvl97vwYj8UTD9gWka7Qx6RHJXrgSoLnaXYxiP097uN9DuNuDLUNg
u2tZx+5NdXmNwyL7/lymK8LqhXFkKs3q+YNnBpdbpAd+7V+ZPcgqDWGXVHH6YFj+h+bwiJHBQYlh
CK1qaayBxLMNVo2Qbc0f/lM3qkG8URv0tOpnLGrVPoJGJntS+77ra2yZqNj4WZafZgx5nn7X88vL
6hAiRhdd2dYCYldtH70gHu1KFNM0UKlH9hPFKSBLc3P/gQXH/I8g3XeJTXnvuiq/UhQ9kCVTybBu
YuoIwrdnEHsUuC9dHwJw+MmlvInVloXtRpcNcn5ldiJwjnAGP0DHqeh6f71EUEMxwvxMwdk2qzcW
KTddPCTvz9hfSpmUiLCdbUhZiuDLasVtg2DpQJIEwlRFvVd2tlhSVmvMFzV6RZI8ZJnXsUE76xtz
n6LTUkVXqo2x6CLMCITEeJxr8WeoGBmNgCyu7air49pkohWc8ZQRAqafTutoyjGUQahW5/5uqhZR
i4rR3DiHwmMt238gJIESlt166gC6+yrZbq9x4CHjo3kGvspSZKpFenXw0Y7kl1wd4nTE5pgNkeHj
t4XZvPm+aPGd9b7XKCycMUXKCzKZX12pE5HWRNrblf7HG+YGq2c8Yi6FckymHVDHAV09OtwzVuPz
oeFaBYZ5Tq3dxd7bLICjmdAvqcLm1nuxAQ3GFs/YfOghPybDWTisuCaKRcKtVSIPV+XhFrfYb3y6
OMjeecB5EgAkmVwC/t2pvFIBqzajuDZnPw6HKaFlQQjhkahzC0VQPFhKqXE9NmPGr9LXAN7kuixn
fFKVh1wdQIjFqFfQuCNZv8cKAS15N0Pu766ZfHPxK6w5bSPkWWhCrfAd0aI8zE+8Txq9fVdDlMgn
x7E1seMkM+3ILmckDLC47lHl1BcIQkamXrlzNZM4sVpztpghaT7hrMXga1rqk2bhK7en/qcKv3ux
kKQUhg3yKMN1vV/g7rRdAyQfUxMmu0IikQIcAKXKxN1oKrwwDZj/oRlUAlyQGnYoWNezLuBdjukp
AyqKNBe89oWHZhhuEunaATgVct450K6ZcwWXv1TpOMGl+hw+2U8Dmi4MVd8S7Ibl8WRteom+8jHm
1MGqXRCjqBgwPgsfnn0cQMdUKYjZ4uQq56J7ZXrn454Q93B+oxvsguWLn3BM7XFyOg1rX6xFYDfQ
DMSMpJvrsmLao2qbAvmt+VLXWMuCKt90alIjHbZD3okKtF5bsE1cdyO4q0WGk0zveXd89DHmfSkS
IK/YLaceNpKpThmVBFdDxoc+J6ma5hgdULRGXzCr4uJfLcmSBwrVJUNxKD3za15eaHnqCbU8BmKG
+/jFPus39oyG9AUa3uLdaicpWQzhCLKl1cNSs82zGa6TJikde7/VN2pGXNtKj33PXPw1i92n023T
/JeEzbX9RfOEP/0Y5lM7aa4S4QtmjyjB771wsDSIT4Td3kcnxaZa3zD1PhDYBW+LPwp02BVrnL3W
DY6PUg1P5K6hGIbrWqAKlE/675ltthsyHLIsTX74WYiAuTgNIGgvGdlMn+iZbHbXAfvzhBOTGQab
APwB/5qdoOA9bbhB4zTUMfl8CYrmftA/N0PRfunz6sLwEEByHurM1V0QHEY9EWVD9al8NUkfrdmt
2fp5rt7PkklKVHFgLOcTBQX0nSuLrycJYbQxgf6ElI0WHTiehQg4TAsG+HO9SKceofmZqV970fHz
KHnGoRaEuKIs5WyO8vLWNK1OAsL+JCR69WpHdaiXUUlgPTcHrWKWcvOTNOXKns4j9KzWhmvgNJe6
IA0PrP0L6RDTsq3AEy0XIow7mVjjDeShcpGjYB0DSqErAw1kk0faZ03XEXq73UqNqlbQQZ/40mG/
Rn0SY7uRg7l6XUNKi3TOSNqz5U3nHaCT3DW2W+NlF18+eiRCCZSbieQWusB5Ii0JIGceTTTuXh2F
vjz9C8YaxNh8nbuB9Vp/84cj6VPhYir78oFO7jWEucggDxc1/pqe+kTzoA9bKgkLs6n0HtlhYmD7
mv31PJ4SQgfPvfrVGww/g4LX0qzx1MCoGiaC1qrBJn8vBSZK59AuieLJdfFh74zNwUtC/VB0T3Gs
tPngH0E+UsS4OaBsNjqOBu1Ty7CLoBhMi/jYQkaT//rccded1/byaRjyH3kXQVF07wRAcu0btvow
9yDpYeGpjS5eUOhmZttIhqApOx3XU/WFb/vb/OUakVuuwmu66gmYRVEdNCK/5fzE1KlUb1LpRk/Q
6NbFEMyWALg9cM11/FEHwgsvj6YxHYv98iNGYaylTVmrhyddoEvBHKM9yybcUPxFKkdQmDz7C6Oa
XzDYocJd828ROAubWnXIQsT05Fi2snTSo0N27itm6uhJsiXUcpzk+nRHhQU75dzzYOlOSr5xp2k/
f6Gpf68NpT+lnWP8SA+htk6hf4lNDASOMTx/GrpxJ3iDYY+LCwAfNFv/lp0gYYM2WTdCP0s8ooUq
8d4bWCBg8qyNl34K7eZAreDDKjUhtNepUW+UaCDcdLUaBprU1i/F+A0ackA7SaD4dUZ/c7Pyh3ah
ZXU982VMckgF9Pos84swy+Xh2Eg7JRpvmk/8KLrVImNxLokA3zB7ZUUT8IeD5M3PGJ++rzQpW75Z
2Beairt4MdF8BoyIWy1k64Zk+BNjrdxPDWBpvEdzxa1iN+AQsSZZrBAAJC82yYJM+VdrXAx5yp7J
nghJtnc2hpcq+mAkqlsbWFfwrglNx6/C27p01LTNFgY+azsMzrgnSF44UzfWLBVpPqmGybcE/s6p
vjAMCSXxNo6Rbq+AS+WE4jefv4YZ4e19M0q2OpkLcj/SrXp+AXUVIwUMWYhUz1BekxnaYMGlgyZl
31QLcsoajcU/lfBB5OdxE5xGk8p/1gNJB8CVE/OZIGlzM6bFdJSCVNZJrXMktQwlpJRN43wqAEDd
FVD+guMevl4hjYM1sveP5WSKAunBx0M9K91usgEmNyN0Ngeza38NJClQ6j1agD3+jOCzMruSj9AH
wGZ3dpOGZd/Ojveou5mrb/BEz7jh7P1l7pPHdU7i4w29pH/qHBZEwcuvtqa+RFlFU3GmDEYJAYCc
anvzlG0gQP9XrJGmWCULT1Z7Nx1BpRlxhdKpzpAIeJFSTECPWGLlOsgYhGqbzO63xLeUtZGoAz8s
Es75cZ3og5FS8A8rXwgigCPxh5vOJrTzFCOdeI9O5rNAM/N62Bvsqt/RZtq9C7XfY5DqyGfSEXgS
UQepVuMjEnTW7EIZQfhQc8LezriSX4kLtHfqsHKjeli8uGvE9y2HU0KqoNd4zSrb9JMMiYdOIhlT
vP5rFfNHZmDjIp4+7GkH30hS1hTHIUJKLbfd/CYWh4BxtU/6V+3ZrFNUo2V3lfYmAiMJuvWwHQeT
dXMC6XKSolm29QUEQsbxERH7VinYTDWiLW/xyWr69/CsK8RKfh+YX/rRs8Pe1HepiC7AgCz+wdf6
8Mn+euVTv+1EJ+h4hJPZM3PgjVnQ1jvwrMvvYTFQruDk61LAOqHD8GwBkstgTGM4YAKXRxg/sYvN
H1eW25rIQhC641dDIAeS35c3z4T3d8IWbTmjdhCaIy9eeO+WSSlD9i2TeIIa/QuqMJDPXaSRXbDm
kaQlF631X1H2ntLqOzMUMUtcP6v2Gpqk78WG+710zLpiQiriF21sLbQk34tv/ozTuNP02hUBYVvm
qHJWbcxCYt31XrMLi+zJtdWSMiKAtUl0TxfJfgFSQLWDIaR6anOah2pfuxkCrQPp8WZ6/h/IPy0L
2amTC/vkqJVD9Jak28PA1fy37xQfkMMkzafgPJ62xkcTdHa9w+Vtcp7pgkDSF9uTdHPIF5Amjqt6
jPSBmoA5SA7v370gQ6uNehReoWy73TD4UJ3Hvue4CI8BjN6bqxm2HyWPxcJtL0FlnBtG1ZMD0EVJ
eAwlsXAglbVFbClcqR4ne+AKjVOiICzUqpXzw+n+JHGtenyyLf8s3l13KkHA390dDJNSwuC4hS1+
vY4PPy+W3Wpdrk+0X+jikMGqYNTu6R+dXUw1lYE88mWVhBuioNZmiX0CGa/zHE6Xm1DP6I7x9hhY
3dcrIp7++jGRO9RGqmELi3O/Sci0qxSRrWRe7VmjDdI5neANGI/hpOJZMYsoyn2iAZUk84iToZHS
yhlnCOJvBrJHI29pn5RaZRt01RNfMxi4Ff0ob8j5dotmhXZz0VqZROT96hdsCg01uFEWEfqUFww9
hSgoT/ivY/hCMFICbvKzRAwbmbs8oTMMC5393HiOqJuYKmVz8OICrrXQR2fV7y35yYYa0zfZOeOG
dSSjspzpP41+mwCpGCebOwDb0sVjP8MmaVXjjdmKbEIvLU2hbitoC2xyUJPxLCTLE7a1tIFfDNaM
t31JEK1Ww8F++JUw4kZMYLxaxzHKBFMN5Twb6hkXomNJ85uSeaX7nW3gGsReRllaC4rtuOMBMqtY
3Him78lJlJrusetteaNO9k0L+h2DzFwqRYKQ0h8slXQG+HKcrd4CQ6KUoTKdIeU4YvapnhhcHssT
i2wl+VYSOF+Sfmmr5m6sFawqFoOBKnC0O21bP1WcIVPWA3KYljlpkQGZZyuq6dmia3aood9P+sve
gj8MeEAUdLzWIjkKj7kNpgUGynnllK4/tdw8xfqdztO+Dp2tQUToJsFFjy5MXGKDl5avHD1akjYQ
u84AOxYfC1Fc0eTANtpkV9RJQuqDD0UPqLCVBvOAmHCygKSD9EduVDNuIwGZYM/LzEuELOox4HDv
9PFHcvnMimNNlZUONOPS2OmlzBw7C7PGqbVNyMqsbMrAoWWy+2ZeGod9JQSfq0y8d27dP0Lolndt
mXgWjFz6F4Rtm6g0OCcq6HCG/4ml1t1zhzGkaJiPuBKTeMGZ2dYmnpsnHiwUVejzRmdShWqmWTnb
1VVVWX5vbspDU64awizmE98iAsKXwkUUOZ7Ed9+U7/crO+kMot+EDIEIY6MrsE7GyIDGyaUIwa4J
kdTLIAYQ1ZYLSDxkh83kLKQNXlw20PwR7aFvlk2279+lSoNYyNNgT2c67k6FukEHm4dByDvOm2Tk
NbKAGUdII5kmN1QLpxHfrI3QY78gxnR/dlThenkuKZdFWmKoaD+QTUzM3DQDe5ct4JdXcIRmUtu8
Q9LocUJoau5zszvOGdfDOek+OmQNL00GMHkxXS7AM/GyPbRppXQLGEJ7tizXcPKnzcTQvbPoXfkf
+cnwzAIlbdUilFGbfAEn5T06BNoaBt8d5zIJPzXLCz5LwhsJOtwdT34rDRLo4mZ9Tx+Zld3vwkun
rDVMACQ2Rh52SaSrrtiBxYIZOq7A8VD+3MqGW3Qc5H1dlpL4gmYUet2Ow5zNp2lv5OGRnTjCn7mE
xRjMj+8VhH+MW3YqkXmnyDaJQAkqyoNNm2pe2dnvtsFVb50QaUaH9rjLRAeLgftyR8V1oOA9xLHG
a+63ttDjIBQamaPR5L6LJvakuNLPspGbd6z/DU0iIQMAIZVZFx8z99KvSvoeB0rNDYU3hYUpJN/P
mDoYu0uW5OQ/xrSyBJQSrO0fGwBc6xFQpqt3pZzFNnHqmhrp6nMycWyOuEc6cX8+tzRz2UbWELoM
M1kwBRQ5UiniapImrGmBgz2sgAPhqrvJGVB5DStCdwd8ZI6Ye4laW8LAzncy27LqMJFMhSSEcJap
OUT4mFo35/HIE2vdBDTgEmCQY58bb2LcN6ppIAO1V3tPHLGeC4MiTRam88e/V1aXs5yKDe5MNhyY
6J5DQ16y5BB2ckmWn/SmTAOcNRmTTeQqbpuGmnlY534XfoE8MQcH4HC1uOVJPqszqq16PxzZ3ZmQ
IWa3zor1GI5MVNQIa31QSiHZCy83GaGB+IgAgMBaI7IFFX9D98P5kj/4Q+0qlH0z+IM8xhKDv2cn
n/26xvA64DkgawA0YBIlaxQAt4oiV16kk+wLCqkoGCVQ4ku2CvJvnJEtS1dVGmbOm6W6yWBvm+dQ
Nwv/Tj2vQ5GumPyWjSPWI2Zfk7bvheAKji3VJz6piCv0b05sVyOCaAzdP+q639ZV8eaRWL3p5+9x
Ddk8qcB0jqVA1yq/63FDYyZ5m5vMnxTnQlTLesXxWCsPW2hXc/WhptfjPfzRR/FYWy1LLpJ3CuGI
e1up8xM/r1LIwBwC6bkoI975HaVIgGe8px8wA3m/YjVHytE4mTjMzcXrGXNmh6OFrwPVV69Hg+PX
fGK08GubwF7UsgLW5+2UQ5AdG8t7UEAT8KJpTAQAVmOrVT+5QeiSPRnVRsrnvPC/ndu5Pz7sTR4B
N9z6daIirQoQlotlT709KgstnG2yzB8FGmyChGN9aUNGlbOriADPl3Lfnc5H8H/VVpkCvHEzZXCx
8C5lpUkhUQZQu63pulb/rKOvCuQihsdcFGlJ0PPur1ZtDWWMUVl79EB1m0wyAF7sckTWCrJjKVvy
nfH4AA/PgDnxLwsho9emK2RiarevrVYCHrSNCwMsCHW/SfHWcv2rZ1ttrAhAd5c43mfWWgs0tx9O
UDMg5q36RqxLxQW0043gDLnCFpMa8jkm0CBFjzOoPZQFaRCgI6lY6ruVvo66zFkzwkbt/+SJFuzB
i4FStMo6dwbBDpxB4RgzWgiMKK9F3Hrv4+H8/1uHgjOIYBKQeUiubChWInEy7N8PlVu8ffOvzNuC
4PVBj3IPdk64xlKSYL/2pXmSngZr0o3ILH1XMmjB+QU5c7TRhSYBPak+ZF/TiiOcgAF84ff+f11x
tmD4Z4cf0lM1e1P6TYynLh+rwItBRfJ0IMW+uH/ka3a4AU6IPU2S6xgdJynnGLrOIhBrQugISQss
EeQ7M2hUfd34gcOf9znsJ+v5i0fZCHz3dpIyfIiFXvOPUiZxicQ3DECzPBY2j56FnmPyfFZWdByg
XzcFe5MzNlalT9sq5s1v0EPG3wKNPU52VS2Kj5Hdxx0YBKnckNewbnCk4q26TIeS8PVnEcUrGOsv
rPi+9d9mDKX5hyAqaiokTHQ5cPb0miqt9BiHbriEgcDTp1h0ppe9SOnKw/TCYFSezNaIAyuAPIQh
2yVYSaXs1N6Ih+KQPlni5wMe5MfHzDMhLY8OdaWeE1DImCMiM2W6M2RjNbQxIQzBIz8Tm0bMMRdO
MmqO/T+Tcmwlmtj56LetHJmxjGnzD7lbUbKczS2FxiNCp+L30knsayvEuSngjJsFBfanUR9nuLPI
+WzbLfRTl3LW6r3t/lFrlW4jT66CfPo7+951Fi9d/2RgEOfG6PWYeljReDwq3qe7h+OCsh8F+j0C
H9WjAEuO1d5RJ0y2nzpnjvwkwTfN09M4RnP2+pgYR1L2WtpmFA57rFfZAAaLqjzCCsh1zzKDkMG5
HODfcWrVhYDWm9HE7Kt/g9iOpWhCs0PU4CjqEbSCVpK0pmGMZmbQRjXDanOtRcARlnyWqYk79mI8
nGeJ06sg2PlAJwRcQsbH+bUZL/3XQk4LqLzXM0HUIQHf4YlBry42KFUqP9dTP5Rz27R13nnowz0z
FEE/W6+rNLb09J40nGm8Bdie9hzTQdhAlY8UFl6bpI6/V8QKfLsPNKNAqBs1eUBt472zErCyfBAN
qbeTUXjQrk0pwxXsXb3jZ7n9apCldKrBvanQ5XrgylJ3orBgbgBkUQpbwCoDPKVaocNlhGUfQXdn
OwFfYrxTgB7MFxOmNa9CDZO3rva2FIUrGeqhrZEYD8IfuczfaFlOlcQmwp1Ve7HHUc65oJyV1FJY
pzWFGaoD4hZWASEIV/209Ze73cf4/hJnIO4vNLPHusssNtrVatWHiECmjCy10nXfmSfHDqXJAIvA
l5Z8khQ2AOZtKJDy9+OcsnGEz+2eyCzDL2HR/zZ/mApG5zvN88Y89MJVXDQc4SaG0y9dxhXM5H0t
8ght9uvHPeEtcjT/YPZzvj8YF/d5HNN3ARdbyjqfAANRmDqo47mkPHgofjBCUNYdNEJOlae2b2kZ
aR145VPu2jw0/17vs6AZzmlXom5Vp4PbA718kTiVWmePrIgmhhaOjvra918CssMhKmZnskZNpHTW
ebJMrHWcGqPt+PUokH1WQhMvR10/kVOAzTykkr0r2rIf8hgVsJ/Or649bwpbyoV4oBaQ/gMQVCz7
P5KxIb3xpGy++u32BAhc/SVjdMbro5C+ey0YeTjMK9O3Yu6JiSTlL9w1apon7xyfosp1iYbbrOkF
Bp6VHocElGeksNHsvyMxvQAy3QZoHKL5GoRhtY6tMIwLEx/f22ll046eTwGwakl2MGW/wgyJ3ROf
ypXIlmfsf3zCZJCxDw8bSBhmBdz0GlmA58IRYw7pZkNc9YqL862YQkC3hMFTgSlcx8uDfueP2BMt
zx5eBTW5y7VIewELAKfMe1E3H3lcHL8YyGWuPVFd+bOcnXlxRwlZ1oOnW94gVlOEK89r71PDeECE
JrqaCkltj+mYbyXBNShBQkUaDxupGfRL/hHYsgeV+wFAlSNkLsLD5+fY5eWl2QrY6Hhr3sKPxIHU
tMLNJe0yNiK2J4lPEV8xdrN8ntwazxKStGA9G7WHD6k+i3qAyqerIVl8olWU0kDmpf0cYPtKPW9G
mC4GAWxFClKpE2Rc5OCdXVRfoS+iwZYTytyHGyNf6AutQWbtig8uyjehNB8gsWgVooyXhLCeDGfs
JFid9fvZaWm8dBieFJFu8qlIcm/cXzBQfSBvDY8rg5x9r7HJvW3C0TmYZyoMzvpCE8WWmvIsW/3W
effpOV2h0y/pqz1voqygkElQMCChRO8raJVR0qvH7naM2YBv+lZZTnLpRpxKu/2cYzWDpjRGBw/j
jNXDP4duFEmnJY0DA2+P0HXaY+YcBs8pSkGZ7+ePfPAH+w4AYplzVmurhmFLve1dOyuAYp34ckKz
H8YD2EMhZCFQbqySP2QvAO/E1S7vRWPDvPxYJgQ5mYewlgVJ+Wvdk+uz+99g8gZwgBndn63zSxEl
IXCzTw1yXwGO1IXJBbqfbgkai2N9d4j+j4GreGlVczzB7QFl114PiKMb+9VfD8+rihdXpBNLIxMb
xcym458rLvb8X5LMsFXV9arH44PV3TMm9NrmZi5EYqaA0oC+NnFfFi4f/++/lgBmNCLjgNLIeDJ2
SBEBxRPHoqMnilhbDEbNILGAVwXqbfYTsN8Aerrzl4M7//z2sIg6sYWdT7+4db4DkT5RbznEGn6C
zkwqPyk80hQp5eR7eWKoMeqRjF/Fopep1hXJc1L4tAlSZp8BPQjcBMOnwqpcHxkt/ub4Jc8H8KBa
WOk3Tc90mV4y6+gkI/vYiLjcHi/j5LZJKbdUngqD3xOV4Hv8R9e/f2hOQMgwqj+UV54TjUoWf10l
djndXMBZVh1JpRlQN5DnAkQHMz9/EzInkyeakqGGVQi9y4uuL3nLyQ5WH7LSPHkVYwFsQODMZN97
cuCmvI0wuo4+rQDWBubP0/XstZl+1pBv1/9k6QBAnDCMgrimNC9wyoY6+oTQdicxIY4CfeS+MJYK
B/GGll1UKpPQOkdvelW9hu2DBreMNMcIIqN9fFTDIP5ih52iXcrkOrg7IAoey/nkdXgpPJBr/G2D
Wef35n6MKrneE7aduDLS2E0U5lDSVecL15wOCf9g9YDTt5sVFR31cUKilFixmRgTBIHRpzUdi1+V
H3ZHXvN2PNT/RsumW79C9ltEIZXM10oxcYigy/DNBZACr0tFltd5xjrbyy7cgFYCHc9WmgNpV9GA
pg4g7CEJa8OtpmgpxRFg98Kq+ha3yjvnddwSN8yKnGAgrb3pQnJDo+8dUrAGqf2wffaTa0y3fy9E
/tVb3vyZ+6Z7P3vr9D2FwhWiekI2JKMwSrlsIFjWGc+eidjyQXSoPXB5nw5KAdQA3xHuCcCjvCBN
iC6ySPuQ6z8JnFrQnMec47sB1yhLjeQo97MsAPCbfz+DrucqD+dsKeklt/YJWX7o/xBadGznhfhL
roROlQPla88DdRHV43x2Ug5QvEWAc+TORF9HmaIh+Wnhv8xoEcG1HM/Ta4YFtTFBXRJiKofrvKhl
tMACxxGBKkModOpdSL4UavSy8BzticbRoFIzU2w2g1O1g8k8Obo1rvEJxlZb7v+hlEanV/z6n5i5
61zLakGrkiRgtRP2QuahO4BYU6njmMUiOKIJvMLK1TLin5v2UFFGcdGmdmoCMXV0j21pj21Qseee
qsQ34fSm1bO1leOwLu9L7LLzhi8HewSz/YBn5jvtcDMfkyx5nkUJdt54zjB9/Zc75l1O69wPGQYD
5mJUu49PUKNaHZPN1/g4chBm3l6I1nS+DuUA0RKw6ZToRz7xZUUkI5Snm4uucFo4AUt/smwZAzF2
ia5bi1gCKtUuxbabpdVuqxpcvHDgP8Wywv0EOnejxmXN5NAfEvuLo/0ieRzBTG+UceUo5BjI5FUd
FiFlONT5KQic45Mz3cFwNX4ipbKNULlGJUh1YkgKdt7N+dntZfBBEOD4Q76+KKVp0kHDbiVP8S8S
rpmUXYHdzrFpGSR04VxYrEVmJvE3Vr63+nz/8gYIPdpPK8vf4fjhu7Yu2Yyo0lvgJMcXC8qPlINK
vjWDiRh+PtOpSnLwHtmtvlgOvimsrGuYOSwQo/A9lorWas7fvLg39w6dX8v/edoROW5cLZ95+Ap+
VuHQzr7jTIvSI3tSboqZtcV8CiItZbYJpD2GN/ATdibfBTBryspVy1STCFwgQBFG/t6PWmGETE7h
oCqt/XZm3IsBTNwVIrLN7k90buzJSUOsTqNrbEjyQd0W6jQw9G5DkVFgitAsPuxFfCtUVRcXrx/u
Tyr2NNEQxFUd5zh7uAbM1gBYDHyLZFaQojrDwgPS1IzDHOtXp7jekC+tuaArvwyzPOlhy1r6QHSS
siUIa3xcnFgoxWSWHW46dnsh6KMaVcpOqLcy6xasMAW3O6VNzATGV1FszsZ14kWiPoyXA2+sFuOL
A7NqKKgR/4G4FAmmSJ3qdy6HCqDzrX1LvHBLZaHezFh7FAo6ruB7LiCTFXwfO7ANX6M7yQyquARh
6TzLDvITQW0aWxpog8sxHoEQaSHvhXqe5TWUe9JMuMUa/UJQjZWi6FtVeZN7kaVSLXCerD+Vq2E4
Ww7m0NisoxDNaCftVgPGPQVlvw1tvssHkO26OMFjEzGntTJWbVAgAu10PFDaZPFJpkSNfIQeaSEu
2tgf2YMfaY1PsrqxTpHoUqYmjoX1N6/MFCbU6jG5X4q9jR1O2TX1jpwW8KCubonaprDQ+5pnRLYB
/T/DlIdN6TqEnJMA7S9VvecYJYVs0ik2rAlT+pfY9SDECvs0mt+5f+jYt3dMS0KFj+mGK4XXj397
+SvtTfBXqGcQUX03HsdMZJN6WtUyjzAna47gq2cXq8+j77oen73H1pZETMzwWTL6tsglJw0DCQQ8
Mbj9433kfbFTCPKfuwdpSYzxBoXHvI/s9HkM+sc7HAp/jBuYtTAoRv01pb52w9rt0q6zN1jeQcaE
CVone9h1a3UnMv0XtSMI/CvgPfC0MrmZq3DAJH7nNeNftp9RIiEU8+cC8rT3ld7pn5AzB9Dnn6Cb
2Z44kRTqCy7WGi4HPRYT3UGg6r7xTCAPNf88zFqzdj2wTOhMs2qWqThPoqWcKSY8GB9nB6eoX4xh
Wtp0SyhAWncMF4Oar04TmorifHFVVX8HdLVuR9D5zzKvELq3M3tgP9goI3gq4AqwowNQE8JMBjAn
bhe5VO8DJQbCj3+TbGOEb/Ytm18yHwyTLzQg5WbqQdw0bIPTWCD58VOk7ZI9KQ6rJr11AQv6MuEl
DyqWwuBOasuALspsjQOCaAfgfvTH1DBsYi5ZnaWNQj+2Pl/P+VQllChftAlPxNPsIxLm4zJ+MGKE
g8Q5jSQofyqYCQyLsqlWrIXHs900TOtDnAy48Mv0EkTIeE/aUNDrskK6H7D5AJZ4IS/WPb/CLtB7
R7p5std3CUfGLjzhwXFcyaqldQEOZc33saB8OFz620/acbsVR9t6FSjJYiA2cqCBSa50FzalBQ6y
n4KYdlWVu5D2LXz02DOLWsjLj1PzkDc/WPVjWrScEBcNkx+M234I3KSDofXItEJMjOFRhjxhUJPc
7dhJfnX14l1N/9RHd5GDA6FGXZPg9OdeSVYb4BWAL56iTIv32xbYH+PQtELbN+hmjgNaj/stZ9xK
2InePeYheud43AspF9v/LZwNuR5Zhpq6roYl/vxfJszQT1kSNGWkzLjDzF1bMBOVsNJ/WToTJ2No
dBkSOZ1kw+l0XfLABWCjpCoYh1SPXsQ1diQN2Or2ef0k/xYc+jyW4KQshICjn39BfjySNBtLtRp7
CSTJjpa4OAw9W9OZjsQRsNpvoYLKZpDEFYnKH1SnnBEjHprGF0Ve86uFtwybM/oASCjA3T1hZMiv
d0tvD6VDhJ7SDUnA0GYFLY+e2OdKd8/ymP7Lr5dqFvmJkOEV30Bc+PRG1VdjSGyK+yOSX2LEb5v3
BMJ4pMVNrlRUe1dKRYt/JFfNp3z17kHOKEmZ6i1etSY/MWsr1d3U4m8OXvmTtSYVF7D/RLMmIGQg
VQMKEP3D5UVf7GVc1xBZY1coWgrRVdjJ/82oD/hunH/RNNdkRzf6qxa0DgbchEkLNpIEFya8pLEL
6KeguivPVRrPeI5VIqYcTiYCb9UVUcNvkGA/Q0AWlKvj3A+u9o5nNuDA3eBOOrYuFqz2mjYfwaCL
ZayY6MoOwjYsfEWFYi3sJuIu0D323FlZt8xhQ2XSiEoMcsR9FyA27T3NOg5M5v3c+J4EK2kYoj8q
b/ve3lvsxSRS61iMV9QFYXEDtLki72Fym4TP+dx+32F7eQNDZP5qIHN4yN3/+shqUAmEmvaHaRfb
Z/K7phtwoPMwkbDivgoBuye+q7cn8uqUpSfUAMxsrxaIRm4SoshcyERVy2Q3M0iIwMv0LxMnXpY1
SfICRUXTAZDgp7GwCsKA3T8tTGl59QOMDUKcaoS1+sxGW2FcAfznzUN3recz29BaQz8INkdAUqht
hGjE0RvLbKxcfMohl+7kftBUpnighAFfBqIgc80soBeAvq1c1aszzNFXN3CzE9o8iMMIlla6oFWm
3m+ZVdXN0MtPPJvZdsaDprE1SrVxs7hSX9qSU1ZluyQA8e2od+3C/dYU/86/kap/nLjKecD8axtt
12sgLE5JJvHTBUYyR8lObX5UtaEdBAeuRTSxNhVFiDqb5aJmHWUmNPbt2SjuMW4ryv2QOT9A5uNu
QNEw4qQ7g8kYd1QOYwa1V7jDbXKfi3bj5hbATJFmN1azKWE90CXAkACBLYQIYGy0D3ok8FNzazGB
/319FZEah16T+MqWF83usbTw9enihtxPJJ9Flqb7iR1E8lg9NDC01MMK+E8PxNh4Fi5cY6dTOgqK
EjrSa4OLvq0vn3TRAtuByJ/mS043lezBZ0AS6/vaeloEnNhf0DSyt3B7GnyydyDp+msZBEXbTsJ3
hXOHmRt29mTmgRPXMez6lrKoYBfujW/Nh3GVEtAM6RuR9JBG+hLGUX81r7UFHUW9YD5VJoP1bdBW
JKtuS3Lo6J0X2ySKFt/Zm7D5LCAhJZoVe7pTFgF34gncOS+6awWyRlbHEu8895V4QoezFCoBNi9J
nJMbeUtVDrm3wLiaXHJz/nHMAwmmnUq7x0IhqQflV+/Hfv6/Jx65N0FGNPV/UQEL7rs1fCt/IceO
+c58sp3zcM0GYAaPMSOlmGaQhPdV2mpKG7SFGq2IcV6zEP5KFO4Ei0XdbFHzXPgOWmTyNjQUrTm0
qLxydK6GEfRbO1JV+YLSCSI6QD41GsOoR6uEeeDrp8vVy4YwalBEXrocNONQpvGCCLvXZgn4ph93
GOl4Aa7syU43qpWLdPpaHXxvCgqkA+FJ2O/H2fJikxP++Dikzrj48W/zewMLB3B46zpkzfyuOiHi
X7trrNnSZDpnZAddesss1bBEculx42AOwM0smUV+SZGpVqTYMF/mW0h9GgnVSyMtLv/aQZN7pKKj
o9jhqEuk/5zOrOA6knCAEKKvTZcqymCOv7iSGAcguT3+a4ZyenPZERz65fYk2RCTKBD90vuwwNYv
HvcpY7eL94wyVzWsmAGe4NaUxXWJddRsP/p8Yz1aZVz+f5Tlpod/JXkNjk2ev+vdr8fqXf6ZkSBS
CKvGvPbVofq1xqFO91GRu12zl4kXZVB0K872rUHq9xRlt/RiWEXdGZq5byuTH2Y+/EsirsB/y11x
cTCB9QGBmuE2EIYuBXxmolMSuEUTNfPPfjScKuLdIapo7PpTUhgKHNP7UD+z258OKWch7WkoAFhk
fDHe7D5ys8lGUhCmRK0mfYm10oKwyyJ2UwYx9Rv8nbB07lVfbdaCU/IahltM4tQD5xXc5ysXMgyZ
VW6SgDy2p0VD8x6Y6jTyM/FjrsOImm1fxBaR7rOXJPPWCaF0D46uN19mYGFCz7e9kTUK+b8lBIDy
4oQACNazff9htvz7a+RBiqNPaGfilfxIhT7TUOpaIpQL+pdrsI/Df8VoCmO5CR1lMHhk+uFOFVr8
RxQ+a293BNbuUK1WaxHra8GZ51PerTG3praXnfnW9GbQxdNrM/kx4lsdOJSXHx29znpd3Jf7k2TX
678sx17kEeCeLnIZUpbbPyYLfJJdGBhs6+Y4JFxsrxNHd3FkBR8z6oiWCvsiXIlDH2ZJgCgzk+yy
oVdfz1ITfT4tJ80QySEVCB0Q2Mwv5BIP+qHwaSq7pNf+pHB5OewaIYO9taxWeqWxYOBCRxD7SHQL
v44z4UfWt28k5nfKdtfs5aXo+iEAwCFM2Jlc7eriumOArmeiAEYzL4SSOEzlYBprxv9L1U3FoO0t
NIyf0Gkp8eTc+D5xlLg0ZdNn1fgmCkZ6bbfY3EN225oRgj4rzcZVHYhIDF93HpE2jWQHlGGOzLAL
+vySCX805FUvsK7xlP0oIIKXq6bFPZlbpea2JwfDP1TqEVjxG7c8lp6JFSdYBpyk9rIq4buvSS3G
2HKCHLIbMSGiPOiP/1AVFx2GtgBpbD3DYePJVvl5EHEdImGOt20ANpBrTdqcos8Zu6Wb8lWn5Wtq
bguNuBwE16y36JKKk4rSIfK/C/47lyKoxjxnC7yQcjg0O6I+5oyG94/wkY1ng7DEbFQXgdLljJIk
iwG+TRGyHUWF1K/QrBYuFqLSf7S/gRmw2iT6JY8dQXF2MIQupcp4OtARHnDWBoSSEpQPQ2BdPXuY
vPnrlKTFqMjplwhHt6OzvGvPsteYvDkCT4c0DNUnlU2cYhEgee8sln7MusEmZaUHQAd6N6oLS3sX
yluGtv7rI6Iv9t6B5JywHHUU3f6z92JXUeCoCpj8PJZCQKMcYGW+dYY++RXTl3Bc8P1IFctj5jTd
zTnUpM0TdScFNnqPTefz5n5MCwKTyTaf7sfCjJG3CMqvJtRKJtgorcWHcKcaU/o9nR7a+ari3hEE
Jg/z5v5B71cqN71D0EYgrm93E9qIUgsjSHi5U+DmWz83AD9hVI9rP+PUrrPhGIn+B7JRFdX3oJY/
10hkHhuvDjAGNjz/GDodQcZ98mPb4CAsedhHyr3EVaUSxOwVC8mtcyrJ6ck3EGGkvOhEo5JcKOSR
4vtS+K+kHDba+P+B0pen4kOHcBh71AsRE/C+10B0ghxK835VgG+f0GjO0rts7TMYokbuqakLC9OI
s/pa18rXhfPwc9gIm/4RFohz3999LJzMK0lsVZ45UEP+pe7/LGAfjzEXQXZlm+nOT5v9f/GS8jQh
2/tzOEietxNBKye3ExdY26Z1W7xlTUXQtskybPdTbwMFXdsIBTLwO+Mtupy14dp0/3VFsGsUmFHD
vJpgyd7dM4IpvDe4svpLaF+GaUbWf5yV0OymlZOPU7pKo0S3CazvQJmBOjwfeUA9C6mopvKERrHQ
FzloGlDkux9IhjjPfCZ+Sg97iJ+x86/kv+NoJg1dbeZgnUr3WJv/rfL2VyZEpWOLOK9X6pogcR90
E7ubd/THOrVor9nqhPS/IojpY9gGRauRVv4tysWKsf0wodxheSpUZaqwGz9Mepk4uAGyE7dETWik
SshcmvHsPQV0cf02Mk+6k5mQg2lY3xsN2a504qax2RC7uC1wHObPR5XZ1y6aUaObMeOLS099sAQM
QbZ7rHOUstpOjDUdyBIl62qsDPNTI9zqpT+R+COXpisFdYXxv9nyhR4hASMBK0WVJOXg0FR7iQh0
DFpgz/IE7CTlN7k2XKOCX19N0jxZXI8GZ3SwFnmRk6VVSm1Kzs02lQ10sAi4/LdCNFyhPjii5IPz
cEfwQajamKRT86e8DVv6jw8myfRjgRcRSoRORnmp7ckP3lKLp7WpEP5PQapfXlgnOTolxtaNL0j+
7OXEZkOuPEounblx5X+OUborCsT3lbyMFcXhdfe2ej9zg1l45ajDe4YLkMD04w8QbOWN+A3PLqWY
m2ddfUrrMfR/egcWFjee0awR6oiFoOWAoCl2jUNxLpmNDetSwVmmbcswicscMmXnQ8fNWjAdJqWO
tgTVL+gUsvPIcQ7PLbkjr8/d/zd9ud6EbuKGbQ5NW0tMiFI/hc2Q9gLA4pV3OJj9vAZTmvhicMhn
ole+/lTjQ0eVCCBmcT81BqZW8x4l8vHMYZvxvNRkAObvWJE81df1S/f33doIMG3z3eypwhtLcuEI
Hd/TLMONNUgYfPpl+LSgQNX8+C2vkjzjariGK/pJAldFBxP0bHD23bPaSMOS1VofeQVfx098ZvfG
uWvPR/hSYODqFNYxXyishf3qPz9JCgLltk6wHGVZ2LH8fFH9CLkxxKtNhtFHxTCORN8P7bLdvYGG
Ct2cj1px1flCHOLDO1p4k/Jb2CUYRukjsR1Ql/MeJOKFInf7moZwPGaR4jznkXYIJcycHe+OwfBF
U7rZqIUeUwq4da4h/xbd7KePAM+5FejWZsohFnwsW56uKBAYjN/IwJT0kjJ078G6v1JkRiRubhWV
hk/rwf9F+8X7HxYaQLK6NEUYY7fP6Dh3xwXEOJyINeby8us5KtE8RQpgnxIZFS1NSuMOeyq3vt6L
4NkDa9coUNb9SvZDphwRHEYOTsjoytAqkYx8GDeusFqU1ka3M/yj5reKCjRxupgnAg9yYiI3cC3t
V6XAsORfSh/9vKTWQxYfjqBSEdLZQp3m2osOr+njjHeFrLrnN3uS54ul2e/mAuv0vamKfiecFQlu
3AKb+nBNP8ouxwIV52UGw1k6U0ojk5SOadsC1JdF+2XCErsxATTMErNnvZUCcINtqi9M0WAA4qzL
Puhd8uioQXiAfYoR0UN68DWzKyDH0H0EfmJrAko9HksVkVWH+idST1JcPq5o7rbzFAA46EUiI8dJ
B+6RfRyTjGW0sFQMyaFUOlYSZ5frLm00bWUMsuM1hK6c0Y2HPJs3Fy0TzJb0f/UbLJ6O4Gn8s4tX
MVITNBENW4mEzSVA/u8MlW+NHfEPUn/ZucFT+OJotWtSX1hCmzS1U5Tbw0hUZB2RbapHWnDtCyQj
4MrtS5SS9eVM2tagOHEpCuuZLC+eafoDIDREbEJiHzFjyK2O1Y96bDzvpol+QzHGk4HRORd9G9c7
DrmLRlgqkxtgtZOEGCGDpsLOUDeFgC6I0SsmkB9l1i1Y3hI5W3MqjkiZuQrnItNl5rrp0EM3u0J8
UmSVL63e2X2CBDEPuorEFy8gaRHTyconc5vDdzka0/e2SRX7jgp8nJ7227LWpvF940MMxQCUgVhD
fFAM0MupH2ca7qSPj4Ne8o5ILyzIR+AKMjijlKg0VeXgJOizykuIfi+niu0lxoq5ETFBkG0lhQZJ
jniGCT/XYnhYCDqagYRQmzeYmpZkEhkNXgAYVCHtiqOOgek2Wr/INXHaT5xVwZJDejcHbXoNbklL
JItXDBP7UcWHZQFEgWWzvP2f522QpDOQb6WN7H13FCZvHyOItpF4IXXiGJUHP3KJwiw/v9ARIRwh
h2n83fyJ18feX31fmAaYQc0NoGrWca8wPq0iICO3qxS/K54E3HfxST8+EKdzp8/XbARXGJTbvm3u
y5+naCqhBNRgrvOCFsU4gAmtff6rFYt4KTSJMI3I5aC7qgw6xq5CU7J63pziE1WbSV2PzkuY3aCj
99eXbfqmT46C9NE7CHsa16p5uIsgqh9lhMkMKfa6ccbPIk5EeKKF27JvXvfvdcjIsEjQApjBfeyy
YBezMnZxlpamRWziN1lg3hVoRRccp3/UfBKrWsJT9udymZyBAPl76z/fwMWHJoEnEDX5TakXBbIP
Sx3SSIQt/uh+V+GhMtroNTJi6Px0ZaJmpV0gezKCLoRbRTlpiizdIMx/qBnKJxySjyMA11TSvqcQ
SMFiNc9BGurtHz3/rtv/2a5IbxT5gAzoTeL3hsQOWNdVaP65e3O+Yqo0udZ6d357nR2mSTr0XPRd
sX8DHsBEh3oc5Ft+TJb1SHgIB0gNhpmz7p5LydPGROTABWYoB6RU0OSh40P6sULyggUJxuufZQxP
TPZISuefqD+GWRZTLq6GKmq8AIXQ3ELnGBFRczs07DwguHcjEdT8oQoMvQ8DuuPmyTpDIqyYYk6t
kg2RvBO8mpHf5SuH5En8k/WDAJjaly3YB340F+w4YfAuQjlohKU0F0nzhnwd1o86eJRpqzM/Owi1
uWtXFFMXolFNSjEbMdtCua5/xgB+Okm4AjO9AThOUCXdOk6EDE2UBW3VdFrMlzl3/Bsv8UH35pH6
zOvNUpXs06VFGZSMVediwdPQv0kRkxdw3gBd6OSJabWvv4rR/rtMQXiq+c/GOdAA6fKObKDzkj3B
5A4qJ8AeR6BG4Ba16chnahyIh4JC4ZFbusP/DldtyZptpI5B1doCAXeAWOnWt5oHXuYWQnRKQrl2
ktoPSqypshVd5gTUKQvvwtDuo1JELKEkpwex1MwTMX8VrEabeFxVsyZqxxMdp+xy9gCZoqUW65jV
70kT1dgGVrnUt5ce6ovnydChPNuHq/oCoTE3NLB/NOb9zh33+hd3nXZWwgZBx4zKnRdI659bPYlm
4RY+D77jV9i1RLni0vG0DlGwRBp7EpMoS75Bo16D+QV1Z3+zdVn/fMm3MBIrzr66w6a8r70um287
HAMdE+Um4u3nagODDDbaauffhDcaq/o4KOpAHjaG1/BFHkeRR7Ze0ZzBS+F7OsGRSo7DCZxbSKXY
mwgxGuFoauTHkVqJD9q8zJfmCxPi1sppyAz3wQds21sP2mbOJrZ8LTofaG3ky+pBb2tgXHSaQLJW
QPJXMKpEAl2Dgxne1wbp/WRO6dkLMVgTOw0IxqoXjFTCF5zab6fKRkhFuVSCHvp8xkjtSCXkHhe6
1d1XylAo65hgqCPDg56jmOHpBP3WJMxzfoD8fHVY+SM7FPHSDM28i8em+AvXChFA3K8UtVRI4DLq
EoJ+mAemhhWXqjbvvhdNLQ37XviyRtgdLa5Vyk1nHC4VkEbjxeWoNvrpPO1hO4fbHbn2vqNjE2HK
bZvI9Lna/H9bqNvkZuWc3tQBxelW7OEYPygIvIlW2rTtO4CP5HuVXlI17KKWINkbmR7Bd5lyDs8a
eFR/gCtkxLbxnyFqGmjZgiTgDfm9XEjUOebU8cI1OPyQgqEdofbrANKyF7iE0MEXCjWCKhHHKryq
yZZNhX4bh3hZYSuEZnl0FhDQmXRpe3uRVi496WvvNtOLuD9GHAE+JeroPGm7wvQLPLoiAX823Y+/
94oA/Yxs2uZvO+WZN0Zj691DRYYjxtVr1v/NW6qBRnIZ0J2PFYajZ6KTz4RRdQ9wvmfj4AIAIGwF
KCFAH5hOFgn7Ohqg3e+J+TDP8B4vH0tUshj6U4urOakLSsSmYS+L9ngVTVCKsQZRSfLhv/ld+h2Y
JzRWOYNUEyZyr0ChHRJocX/q5UWouwmGDUCuX9RspqjCRRk5ZLPJ7Otl4V5M3t5js4JKRx859u8H
D1dDNot0VDuJERYzar+vZsXQtXXheX4RAZaWPSCvP1J1oH8nXSMUp/vaikORhhe9Hp6h0cX7cRpR
m/vbaHS10uU1WOLZaA6VP6eqSPjpohQFPnxJzCoxJAYKG5xmdQwB8Qfmac7Wm0Q2m3UGyJlWzDPX
wwM2PaxEM/ewql0TiG7xT8b0zFKa7JCiBiHMEmGVaJ+a091TiLGHKMY6tJpzNV8tGQJYdDvJd165
CytVkwqbGrSZJwAzdLCrLyWa5xDSeWRFzkQRCOR9n9I9H9Drw4Wmw0ym5CL/PuQCEvENLV29fiPZ
/tK78erBHClaPWwgGGLvxue9DJFuf9LXGfptKXTOQm7oyB7XksZrqBn+W3+Lg0FTls8Xpqeur3jQ
jxyizjT5tUGdESs7jqVsrUBCIIufvKn9w8YarHfQMCIKuAjQqF0WQtfOgcA0DsTYh4m3qJq6K9mz
df9T3hgJpim8qkNNNBmckwi5KUHx8IS841+OSAUycXBGNMmMLQ31MduucxzhWie5hIo3pcCy96Sp
iKCjU9bhjkQQwZ67APQbBj0rUCxCaCm1Wz5sTR7SujQlHUoJNXeQ/cGVng0IuT2fUS2A1H03SnsU
qWjfOrVAJIBsCoQSds6Yk39GCliW/RTx0JsyuOtIdKN/IUNoufMxdM0CFRbSGmo1FDEl7I/UU3r/
52V+jqdR1pw1kH/HqRutdJBuv0jAf2uS42Zrz20cWi5+m7iha91ZXAR0DsnU6TPvImeavIZPLKvg
8gRNTdqM2KnlGSkI3mPffG2cZDysdc+YMbAhwam2TOI2Yy2KGTlBH4/qurROL1zwbyVZqNCFsUr3
pxIj2V+dWNLlHSOzmNjOWkTERYY6rbkZJURe8QfUzyOBf3QXxHKQL13PkVjwWzqAjXF1408LrQCr
uqkrnSuUjOweBDvb9TN2O81DCZSj2mSFSDWLrBGRejuZKhkQiguXyxbpVl3p0LYBIcR/XfoLd50L
uQhnawmSjJGF+5tC3VbG5ozfHq10x3iEM2G56yma525qtAf5Vl6FNU1vzQiJXkDrO2zD8ce24c8X
Xyd/vQRpzjD3E5ujSCsX4LEAqr0pH/KO11bgKyB4cKUZk0qzWFT5g1WPB5wp1/qmPLaXLroZyhei
DCbInlE19qxOKLF4eziYCHBQyDIOcFZzmRsiSUYEMHLBzZa+/uowp/uDndUjLbbLDXljwQA8gR+D
iIyIxnvluVa8Pq3OkL2kYbcfE3jJAVa755PSGdYQQ7XDHXp2XWvg+RRRvCNgwOqvZmQZzqtIJ9dW
U+WZHoA73oOgC6bYKF0N6jycGKV5rN4js6gf2YUAHszz90PQvRUq9AK2L1/PulkpvPKukUVSXv7d
2XDcSl8fAYP/6+VwQ8Gu0UCkensmk1IknZ2mTLV9SaSbwdYC1YcF15OBl8oekW5chxwMHQrztdX/
aK8GO/Wtl5Mi9HnbtT/LllVX1fphoIT1wIdFViwnR/WrtdTlYr82WrHcI18Xc7Z8Hu0Eqmz8NkSl
hhYbIqU87gLmQJrmnqIVHNOQz4/llFvv4hr7l1oYjdx8LyiKLxmqFTmvjewjKJVG/6+4vDoHCmjF
m9X0a5U1hX8TqJNc8wvQpw2PR+QVdq3iCDR0F4Fj91UuNODh5EwsysrVLGKtrSNHOhE+Yw+BIyPf
98GVcze4lCjhx4DQTSjEN2TJABzMDd7o0N9v0pAaxRvewIUvTi1cBWGJRqOcJL25EgKj67u2DVEk
jnkcuyEX3BioYLsh5aAqX0KxIOc1DXzCiyhiEDNI1w39B+wBAB1F6CSZARRXAsG1XVb/LmNkCz+N
NJOxnQLGnnZpAtjQjWGcwifzt9tMsivl0bryyknACAR1eMXGQll2mNEp/xFecO2r1xTUxoFoPwkj
YBwwDz638Ev6QtLWAG7LoPagyZmciYxaelKpgTyVpjgya3wjOYiQXJzJnw33eXYllDmOH+z7YbwJ
AJdLJoGDJcBn6/fcGmzAByHnOfU4lIxg9ywhlzhdFL3dO33zmPvqbduwxh4mv7eIQB+yVFiwWmEP
7NxmQ6LmRJb/p0ICUEbm0CT9Mb4/R7C+9ShPWwZgpfZ4vyv4b2qdUwJvlYEq/EdsQNAR22oQRVlS
Slr4Cx+Nu2SnQ1ERsHmyhegLJyzzT45Kg2NEh9EAoByRtA4njezG5au9oMd5XtMNJ2NsQAue8TLH
E9gctbtkEe7mFQe56I1RbwXBAjaXW6lhyvthk+4EVTzK6MZpYTVmLGOmAQs1L/1649rzNP7wZ8ZG
WNYohj8RNGycbkZkFoGOLyJEjHTmoAnc9jQO3E1PreeKklRKCTsXcCVjMa0CYdvcsg9XKRwUtTCc
JCYKJjOdhF9fR+Ahrnsl+rqlOu3CydjmYpOwThEYny1pK3Hl/oy54zdBnImXB68bmqDMfTW1dwwQ
0O7isM56WWymhwXbWgL8hH1gHMeCtTEAwY3wi5F0ORsXWLb7N3GUND/u5Lw9ZaEZvv61eLDYnmII
K62I9eZpSdNvKlZkRPsOhX6Zyg0oceAw4rio/XEwoIbUKssXwz3q4cXZOOPq99cV8POFgXNuRaZH
kPPl/dCGvXAJ9vdjetWUTy0EgKpkV2vY3zcKCTHszR1whO0tQOUegUaX9Ay2yw9qgNqBKJWdyHd1
51OT8OtSAlQhCT3L2SHM7ycxwkvr/MmqnNmSzH4jQ+59O06BW/ywfP2DDbVPDgghPr+gPT2Xr4T0
LJn4seoCb2w6pGiMVPxld7Y+/iVNTh5bnjhREhwcTGlBzsdAZxcKpTYV9bfEH/2gvIv5cnZxTkFp
Z2Jvy0dfqA+HG2QrW1fGV8xWK+st51wMX1HDL3rpr5ezyuYiCV3fDK7VKanbEIJTj9IerIqiWsxS
p1LvYpxbNTrV9Pd77pLjO/EgLh+n21eZ8iurIotl/hgcnFUuJDOqf77l77n8r/txGZqj9vgm9kFs
pzyfosiq5zoHjF7wNjpbvWzfamFHwTCGbPOaxyAEwWiSwDgy28VL8TwJkvEBmaSg0wFCIQXzPMS7
0Q3st/GLSQeWfvZXgXIsSpcDW6sY96GAFITO0Jf9UnOY9lev5uO3Lc6ZUuoUkm6nfppb7WnrwC0b
gkxu72mPP/Tb4vPLWtmYm4+c+nT2N0uJi8PO0i9qItW8Dvl/nSBoBx+LM7DXm9zJTs6R6rN81E8l
hfUha4GxQweqIQ7sDHnPsDSWY8V0P/DRr9NOVWlfUz5MQTu9PJIwY6BacMUhKekurkFmzm/6ci8D
J6c4VdloZIqWTAMQDhKRHMO1kP2m71qJqDCQNKX4Kqu7+5WPYITLjOHENqKT58z2j8LSgQgXwLjs
THADh0zsRPq2W96cbQVX5mOPPMlb17yJhbFjU2BIBL/n9wuBWYfxN0Tvjv5T/VSvU1AuhxmxfuIn
KwhatJkS0AZnooutw40YbNmn1FBUShOWJTPXMU/CvxvIajQwLDatM+nuO4BIU8ZW621MSFLc0VEn
PCnlrbBJYDhySR6nZLqwZQLnzgGZ5UQLNvIBvJpqZ6asWvtnxs5C8Z8G96YqOUIJrtEIGJUdHdZD
Ss6HeNoSuqD5t/BwYvzlj1cUc0WAXBMDT6g6MApZ2Fp+2TAL2NUTs7iu8+Bk4Fu5olEGJaYjc/rG
EGYiuIQFV/spoxiUTrT6vWK62L1V5cfuOSQXCQiGN5P/VHNAL5A1j1xn8avFng6hGNgx19ngpFqS
hOFisQr4mL9CtyG5QTtp1cfxwQ/me12EyDj42kgSrWARwVeC4s7zajAZ4J1Y4hlkhbS2+bxe7LyZ
jG21QqPv6J76YEYz3ppc0Y3tytpZUel7J+Q3TzqBsyymVm7x4ee66yhmbrQ83EgWaVjr6QDkBmrE
4/60+R+qYWMGMF/QXkj0/0/cW4hAPmfm8n5EqXjn5qoiiK9btv9/f2qV+QgW1xJb3WgcciRU7YeA
oE0CDmFNQS4fNcXzRvAl8s+gzkogyI4DTTKEFNIbkB6NButjI+Wdyi7iFP1aX+AyjbUM6u/RQ1+f
pEW/hfxsV4Ybz1G+aT9wsXhevxlOi0SkhlCSGOJEx9oObC3RTNH/1y7DQhPg8QhrZKD3eZEBAWhG
uuadOyGvyJJCVa1fZ0WnTACSKu/5UyeQmrICGf+d/FrMnMd0+2bdhNzcpv5nkbQxlKNIchb2p1fd
oStIds7eDvMH0Cg8EFZjz9DvLhJE7tmv/Y8+MhKlq0iYAxFOR+IFlsYEJSPDjINm42/ZvW3WRxkt
9oUjWmtrdtj9UvUzhT697loIHgSao1wIDijrs1zV/0oMsVCVbXzXdpxxpmhKM4hLSan2amqLFMSV
AeCPuPEV/Edp0X9k6yEzcBHA6iVsGnBzG8w40kkEOkOvj02J4YbLp9uq1PeznhCFpcCrPCSmjGgV
YqVZxUlTNUKk+6HtvbmO/gWzqHMilzS0I2IyXX+I4PTyv0HpPyal3yHYla16PvAdGb9uFaXAeg7L
Jruuae6IrBR7EsGwJ5pTkjhWh57g/UPCQZds9vslHYLUAhmMuPu6XS0NQh8RCYc/tLyxjCYA6dGy
TqPHJ9Vb7Ey4W+jjLMuT3+FnUSHVRHuFDXE3aU7CECW0xPJx5qGlox+O/zPHbBOgDmlX503rGAy4
weJC+fkb2o08tHf9AObxso9utpv+Cu37xqPifA4HziG0Ieinug5KDKaBmsr0nHHmi+m1o2ZYGROx
eLl/yO+fRzZSxC1XHN3bQbi6Rr4Z8JVu5Y9dQfc9T86+4pi58hKGgPIVmVHbmBMR8NmBPvsImfjN
MBDhf30gYKb2kSTIZNpOFCBoXENVi9CJuzKKjFipEtLEhsk8Hy+MnXf06LTaGN3PlpjzxeHL7EU1
wNhm9IOkxI4XyJ3uJRZraiWEap4Ia2O2bIHhFpKWJYcB4TzdT3ebMG7917cN1RVW/cxG72cXrvO0
0aPV/srAId3RNPgnfeqENrTo9AQiy85GtaSX1Mv4gsIz4/oG+Yl5BHSQ4ccUiHafqJs46z3j8QFM
28K4Wu8XUtCK9eAFp4kMG2Tpz27aNjft8jZutLcw1iOAzrPo8Yeun1AkQhSi66++6RajJjtHIuue
R68fxUvHsn7YvZrfpY6OIJMJCfX+4nRobh4K7UxXREqXyHw4qcUvqzsjm8HgrBBNpqsaljRe1+KN
ld2dAYDPqy5golU60rAWlNmcgVbGf2kG7iAnBu2oaRU8XOpCOGtWiXypBrrjn1Vb+eR+fcUxqh0W
4JO4rFe2NgrvxGQgIzMHgq15R/ulivZhH/F0UMBXPhOpzV1w6lNkb1zuXsY6++jc7dLnpIR1tokm
YTSLw3SmudtaQz2MCLkcsQcpYN4feE9luA/F0Gas//UPeSYZdNp0HLIjn4rNhuTJzg0CnbJAYZY6
+wwrZfzmk4jMCGwIDpZldLNOjPZMdxKp/GMIXVHmgLJWEqGoRS3Jn31hthqTe1FtNWaRFQjYCILv
/NjrbEbYfOrxcXkz0mKyDCyr3wxFNALqHy9JmHrqbt67pdzUQLyTba5kB1tm88wtrBg88S0ucPAX
mebOjoSinq21ZDNTQdRIgVumHP5EmJ2YANYbdZWzninFN3mhw6KbVMWwj0rtxhjlf8UzyuorxUpp
IpKYH2HSxW5w4nJ+5nxXgBLIbnYr9+fyvCb4YqFcCexbnO26jEh3DwlcaMMvnQsMmBty4HIysQKt
+Sm9vAYkAtNJsjzCdgK5QTcMVFc0RNojbHcIwZqiVraJSHhzG0zk3cO6040Ikfu4WX3CpMAlIcie
eHdv8kcfgNUErSRpTBun3bPG3EXuNFQgYExGWO/YH4qBKP+9qRz4bz/Ch3qeuYQhHUwXfiPop/ah
Pf3bWJJzVEutJCHyK7yIKHyN7HMQxEhRp6aftXKEoDUSa6OU71BalnaXQO1CT6OlrKktUlkKNuxL
EzsctDwGh+v/0vhRVbYCZp+PqaSs3A3BjpwA0unTTPpaGvItd9sXWXg/ViHYv7PuQu4/uvYFBgXm
pim4Q4gFU+rUQIbWvsJOx5H3xR9QSex1B4mR0tqrkBCppqR4TCGGC+iOnOv8vSaUbl20s0zdDeAp
FJfksyNP2GDvQe49LQ6K6QNKg360Uga04eVTS/bb3z4MABxhug5yB6JSunHhw7YIIk4n0jEWiteU
uZbpudEPv8crIq+ogmx8Xj6PtiNl8KXcG+NwkytHxjki0ziUavAbpB+43naGW0ZnVsoKNoDtuqxi
B7oQM87yMCS8wWQ3eqK2AsCI1fWXHQ5AD83xxaJgXGZ7IA+rjQj5O+yMkuhxQ6d7GcoTFkVF3QAd
PyvJta+/960i3IkKEWFjhpTHslap/m5cP0d9eHwj7D/KZb/ZV/xDEsbQnK9Wj53MD62Z6MwJPcBh
kzVuk/sXJDhNRwYxR5jKrRcCXkIMFUoMHF4W3V4vwY9uZUycR2NUX7yUr15hZ9RNBpotzl92xva4
P8pjwvrneTnPZWX9lKSlx+S5GsLS7JtLDq/TAdIbYFuHmHHY6lUFPBh9+u4rr5dEoZwRI/6iVG/z
iet+0TVtjLa+UH+n35/HfvmLuCLydDcYLoC0gVOK2yF3gNy4Vl6drVd9cbswslmxzYmAAqsmMH1B
dKlQh4JeAr6M/1jJtoAw+vVhAcHSt77YeYsX/fFN17YpHA+DZdL8gAYt5tlWaodHQwdbXFRiXia/
bdez/FtLDvNhqRipRu6YtAVOZFd4Ope9+LIxPcs/qKfrJfDg7UtCJvr2SZp47VHXDLR2VhYP4+d1
iJnFhBsISTnQTfR3UF0yIrQVtLaMEVsCp+d3SsDl0VjMmkox+GyfUdP7UeDzKXhzPbGlHcJO2wAY
oZ8rNWBALEi63CB8XJeMms0Y1JXsJzbALc27DhoKo1Q6rvdkRHERDyqQ9tUrCUCfcWNB4Lk08+Gw
GiPS5hhFt8nj4quzYEs20AyBXCJ5Cw6TUDQ7E6bpr1ult89iHTMFFDwkMYCIoepSvi9ppJPCEw20
cDpdywnH6ISsDxrL692jROm1Xwc+E6cqLM6iQXE2VpkWAMWm6wBCzUc5jSvluLExveqes+TGmO9x
EN0QpkiY2ARdmi5fdSNqJnEPgz1Ytjbk5tBcRDaLMiG2XKISuiyRCFUlT6M/AAQPR7kHSQB2RS4x
7G8uo3b+GcDJS2uRo72qrQwK3Ll1HSQXPuzL/e2iA9+lM6jf+E6YCna1Xm7XpBgQOn+SnZGs+o0r
x0bC8H2uNdYRLvYoqsKU8VHKjHJeqo6JC/sSaGCyU96QhbEMB9KVoqu7spFvEn8un9WIsI5Om0FQ
pfT/Gw/ceN/gqorsxU6JQYkbO4wvmy33x8qZt9KTslgfPXT7OzuvF1XAhzReYLG/i0QwbIo8oq1Z
OmPr0YiZUXS1hwZNZ227CzDHQdSQUnFfAt+o3SmWqrXkIqyi+4UsAknSR8OAJCjuez3HLQktz8wh
zI7IErNFXKZRIXud41FizVLd3F51p9eK6ZJ5ygUKdZEwUfY7YYVVHBxnkzFra41zdqhslU6toMrI
1vn04Z3UjF4FXYG6J8GPGblmoYg3RhqdCwLydGIkOsrA/Zkoi9Zh7IKgGgsK0tJVIZu1GkhXv1qO
UxS8OfEyh8/hQ4Z4BWODd9fMSVcuym7kkBJw1zq2Ta6akZrn8fUnFcKYZJBVP5zThRk2ImYhsmHZ
eLynJhgctsBRBa7+545eMhHg/Y/9xwJwPVNa3hOmsDfyaWFid6upPjR2RfSZAmNSaH/EYtCzOiDu
V8oDlWtbedZo4JbfWIN8C7WUnHjOSyGIhrAuTuLRXwgQt97UQbJFAxyiUE2ljvFY+NerCU2r1Sj2
czIvMX2YC+SLHbBn+1cBBrPoKip19ekxrmdwoNiuE9ZHn/pk2wfLjMrhXacIR3e7W090S6S9C2cu
Sep7sn+b12/0bfCbfx4mhIp9Y8Qs5p/pqbHT+2NnFr54rdiX9d6sVDFaHXekqeOIWpRnWrKRINNf
zQ4iTgfChnWBsiQK0+IolqeiHymi9JiK2WdNLOjtCquepOf7V2PGh7fWiEjpHrzUmZ1h3m9R63bA
i69VvNU9qy1qAQzabHghMkIOxLACjp+glessik+dKrizDBbgsr6Otwczels17Gqtjf0Zs2KeoTtf
BwH1za5I8f7HvUSjP2UprLjO0ccbWmSjuXlcX36gUChUl7ebd5S/Okqt6CNMWLfpTQrHL/Ivnl/b
8GS9qRcU5jHikBHbiRGCw04i5eQolA/j5lNzqp3U/5AOGtX73gny9gVt0QKnT9IjA+1GDz8VuVCw
k8Vnt/Z78NEExtN/Fhs5UsHyCCyJhpTDPSBY5H3QkVR+QsMf4QOst+x01c4oEBYtq8UdSk6R/Skd
9gRTIHo6H2cZ+aFUsKY0EkBzKR3anL49aVH1dtn1lQMnbYrz+dhr2OXJD0WmuhV0ifa8eV7RNJiv
2Mvlycu8toGk+WJvQZHtTrGDsdpEUvGkV9HuSpaY0Fq4eNNmomVYJ+iL7HmGp8DsXWoaoVsW/u7u
QLhstSXWgWlB+iTJV78tD1cEA9NOKlFD3uf6r6TwJ9/LDtA2ZcvpHAwX+zPHgAbWb8lHND9W7JlD
XMIKRrca8MwuqHwSjrWv+beuKMaTWM3AmE8cDlF0PAqXio+zQvP/NbGBOVJm/lpj+gSs8g1a9mS5
PKaTnDMtVPIVLZLQIgnYNM1p6pmNdcy4d6xqdjBW2Jl485KT8MtZQK20rbVm+ul3ZlkHEiX7vbMs
onoi+qN7DdjUL2G+Z4FLiFDKNa04vfGJZTzLCBQc0KTFuiWBtMc4d5+PhozMgyLQQY8S2swI2uf6
w69V1vv4uNaLrfxkUFBUuZldh+AqFiu4PO9XFlRxILvwvyBNPCxuc38H883VHk/W1ug4MSdGwbZq
Tzyuv52yoTFqWDl//piWBDcbSitFRndAs/xYg6k+fqDR+dUmFw34RQpXzWj19WaLTuwBhPDzLt3N
oWg9JScV65OX3G5KMtd8J8rCtYhqJSgrgjIu8HNCwLpj3Tic3zGryt0aq2kzdN5hCVEtras8+S+l
pm3VeWJVbgpHy3wJGcc2JVhva/UDHURHII4XqVDW6g/YvvwakgGJONA5oOR9Z1ym4cMK3WbbV0/R
ZlJU5/PjOIM9C0T5QRGPkZPJMcT1jy3c82DFLyZjpCnIAWNWknZ88moVFdMTepNFypot1XwBZv80
dVjYTorOrjGLb3citEn/rl5l5kgWgUeNt0XxiLO2qyO//MuqO3UWlQPb4po7AcycKRjxfFtVNDwa
GvYGBbT0q9so6u00EpP9/P1kxZoz01v04kJAxpAZXnxJJmZyiJa978OVyyMHf157MBJIBwrCTXnh
Cpj93Dzs07X5Amv0/GGpjplXgM/Ili21J7ax/SUvyEmos4NGk+Rj/pvH95gBYb1CoFskMW5BzOsq
wtTCgwz524SXtlN7kfs5TiaxS21OeQjOxhmsZBcUGmfh6LK4+kQ6Xz6DBc3XCyncSsi0JukAxH5Y
xMMfapWPHhlGAZdQIaGSI3dWfldTmz0Fqs0VOA+CheVvB7MN8z40HdNY9XEXJmtv2xqECSRUBfSd
wrkwuXuIzJeMVElbNnQMASJDQ/ST/7UlqI1DWiukS2Se6/P45OfRdFBn74F3CTXucTX9mMK4hRgo
4sR/kWkCQ97/ml7XZy3MWBaTRnssR5C9wx0WHo0p95BUqQwOgBOnOCAdJHtNYW4sKc7KGRoUbCN9
UFvpYbunwFCMypOS2tCYV/AegikfXSQIlx6MWaRTfgl1On8bdB8ITSEgtLlmgiYSTgHJU51ro5oB
N23hYLAvx640y3pCF2BblpEaHzenJZyhQ2rRlstSrL4U796jP8BHC9eRVKTwYFcHo8Rc52/yeZoM
bGQGnCrUSsdy4KQCqdoM7KwzSNR5KrtLQheXwJhxakugAbJmpDWLCOEEz1UZgrLnq3Me/BLvyfUw
W85pLQQrocO8XIsxAYOyPjAlnd1KO2UI8GiL9zaecN7HtewfTpFHTmsp7PdiuiuUVdQFYPbaprW5
cRlN2w66IKHJai6QWgnOnyleB/jxr0hNjFZzz4Kc44SmaMs0O7k19++ctnDYtA2HflPCxkRkSFLt
HDKcnhjAAReuceLdu9wfmsNzvRB0jhNM4u0zei7+O+vYhugqcm+6dA7FCyUPaQ7uPqFcZmq0XXAm
me7T/9OcQLbqVxtZ1JZbGwnQtXVElmY9u4KQMm8XBeFxZWwTfefocBet7sLmtCgRuR01VqZAfoJ+
MJLh1zS5UtlN23Qaum1wdNFMh+R/aIpKELuNuZYZI/W5F/zalncLMfJ1rYR8EpR7f8XDEoCj6Te0
rDMBMe0zX/rjJlZvtKJZ+RuyLi+eLBHSQOLF0Uuw7rq8BvKawQDpz+Ddt+pMsv5KrBUM7lhPkFE+
B916AJ0NATjR1r34u7N4ZKIt/pOPPZGSVzIlliXx60lGMpoBfGY2DpNZboFu5ds83+huvDBn2hLq
h0gKkIMrwBwYLBNkUoDrutotl+Mw+81Qq1b3SyOqR0ygF2PubCL1bW4W0Q57VUxWSujjX+vENMBy
+0kA3qt1ing/jo4umDz7gzLfCe9rmEKBTDEXzLhNDVti5aj1JbwGlqKtf8U3zJxZp9CD4zkcqqq7
nIegMkxtNYJnNa7ujxuM0oSyw3dWYet9/ifpOJ7C1humcjE8KU+2WY0xre/i9p/JfPTk/DQ0S086
uMXTU645v17gv9pde+VvHbaPY2c0PCghxRh01q6aww1oENWDZYS6yS9Cart0wEdyTwLAEL1vnhcs
gjv8HHBBPuWhF7f2O58ngfQBrzZSl3CKbpjiTR0w3J4AwuYP1EUAmLj/z50Xd0Hy0DBH1DbMxJQk
mZhRlIR8LDkeKx/ym3xn2S7r/gLtP2SMWUUq65xVOu1MpY/jd38YA9rDE+ia4x3IT+f6puUYsadt
IUZa1g3PxSBmShCiGoSvaQDDo3Uli2XT9k95H5l/2jTzwVD+cbucIihlrl5XQnNWLC96Qs7e9s+o
oJEKjBW7uRM5oUi9kaQqGAn/lOXvz37Ta118WzZDf/tXuDE0qL2m2B+qUVrJe11ukmU0LWwsOj6r
BQYYYEdi/0lgL8ZTbar8/W1DFrVtCmtXchIUMOd5CMuLP2aDA2Tm1uN2LMl31PnumI+K3I4juN+T
jT4qck8TdFu7+9KQ0OEhNCbGa6GrCXfb2vWZFBiX4r7IgFi2X0umHqdvsPFnna+U5XxL1qxjA+Hr
pDWN3AlMdv+2DpP1YCQk26DC/Qn8PeCcBfBOGTWk2HbCmlGmBK8lf/AxzO+Nud9Efa06kiGK0ExI
oPoEt8328KBl5KGxGU6V/AXRvIsq4yPlrVu/MBDx1jlxbhPcITa08fDDXQURiePS/gM/IksEC+Sg
Kexury+hDiv38kQ935/TMp5UmwSkqdZ8qLBZsLCAach+tw6vZIaTZKKfgsGz3n0tdGLLDa0UeAft
24HmYzkz+0Pn/MIZWe32D7YoXgEeXbmp0ORI4VmYQ6hzweUBGfYwZ6FBTgzb/JSRowArAZDAutjx
//Wuf9/ixaGUGQ935q1qO3ih8mzszaR4r1KT+1j84b7Fp2R3Gt0LeOy+iVb3kIYFOL6YF2M6V5iq
33ZclVUt2Yij3NpCyGKT8qC+z+0x6+kanQQM5Fws58lEIX5mnpLlqbmGiRymdNdK5qAKNTodsWro
N/Dy9o+qahKrcMUJ4FdDXtRsVcYXjGxHiSDmD2rHAMEt4j1bCPg42MKG8EXFSAgysNEXDIiq6J0b
LwL8Yee06SkEXCvX4XJD+MxiwmM57yR7SUWZldq7ZcLr8xOAnGzDzJ0TXJiblXBz9h8gexVm+QJp
8mbOHRwj+fIbgnKJW/YilNnYxSSe4TeySEqggpJc3FFhrSAaCivMwgIQc6uIsT2kCt18yPmM8vJg
I63SZNsYa7zpU1rvXfJTXWqbq4suV/3exve4P+kS30uc7XczQCk1puuQWb2inFMJ5HEA+nn9ogA0
HxXJScwFe3mVgdZIp8YPD8LLIrpFvrjgFHxclTdKbrE2YqOMQKD4cyAmMmogCaIu5aEzvPFlZizI
F/sMhQcvfB3J2GNhVWTcf6+xAZ/jbmQvV0ny/Eyu/2ZTH7d2R7egwYeUn51BGgKg/gTAA5epl+k8
F5oBoG8L3LuZu5oLDQ4cgXWafNE0x/ujUrTdRSX9Npbv9QrryCUyh3eCb4Bl/UTdiTa9hsG0OP6h
pif/kBtCF+WGgXfZV/TvPGM0TDtUovniT1vOLOj8QlL9TbU2bWE9zTTI10v7TQ7ShQrJsi3wvI0p
8FyQFYuw718i729gB31TE3Aypa3gmpiIJ2RNbjTZhTyIesXNjzAhS4pN2BF4LqNtQp5ftlmWFXtV
9bfElnM/EII1ySjkIrrHc5Un/YSPWmTuuiWEKMcePcm6/2JWmgD2SYo+hDwOVyn+v7OOsOTzp8Zr
dYDXBwKqNFYr9KbxCvXakoCE1vBcH1j4ukigL/jSjm/f0GP3skXmIqqj7Te1Por/tMs7Wd1Tp9DE
zLiK3IcEUMahEGZYFmqf85vPJo28C6DCV6Oj8CBxj9kmnHjRFnUw9Lj4Uz1IkAqtHwjqU2jqe7Mg
nWEdwTmdIBMEvcrfwHl6/z5htmhLyks/x7zCS6jbiu83rCYhBD7mIH66/zWX2C1OYwwO1bouP+kO
qEYB75uHjADrUx8oRL7DYND/7WNRFnef+Yq9Vxj0USh+Lmlp7+xh7My1ELmTmECKhJJgczhzy8Q8
fZJWgS47JQzxGVcpxpCNkkQc/IgYmLFgJyMR3G7kWJXdtFkSoFIi74y8UYGsyqh4IWngGg0S7JZT
DGVXrmZ5UhVmrUij4l2/teOOcdrB227jLTCri2/tZ7QFeOIataa9zRa+b0w6t/w/ttTD+MlfMTsf
7ViCMBpesXjZNzDj2LG1Np1r6KhJZAhj5yqhKVJ62bNqmr5FM5Ezj33ZCHXU6OfIerA1sZsXZwZW
bR7Rgcpw6KlmggUDifq7kG+Ys3mxnS4ZgNwcfgkXcm7zAmwrkc8OvDZvVrRl9gFVt8AAeEUnNj6b
eUbXisYSVWwicTG0FvSBJOh91yHYScloX9lU0fjrXxp9qDHc2M7duMnIOiPmlF9C2AYJsQDH3Cix
p7rIJnJusfNjQdokmSFCbwI8XK0/ZZUPBtluDDJLljP2upXPa+QL+Pcice6TAXUywSvdbdppPKNp
TiCBlKL+lbqaEAitNWhzRiyz1G3Y8L2anBJ5WmA+uNfVd3X/yZmC9Sh6GlkVE6CXVSedlAs+jPy7
mdxH6oyELpUuLwKOWCFxO2bUA3ReSO8JDGkZGTf7JqYdcYaKiTenPbE89n/XhTKCM+XSm5LQDL6W
mCt13UoE4uA9xS0l6ARlGRzGul1RKZP3+tuG/OTyjwqZWVg2Fgb1DMp9nPiZdMxJ8uLQPv+U9kO7
hQSBwLcBWXibDHcBGSlLuMsVx/zhYnuj2K6OZVu0tc2Pc02HwAHz4rQ4vDd4W1ZyLSS0dRtk8TES
FUrVKDkbcfIxKrIN+CxuUSm2fkM/eLYmaS29tgzPHhGqwxPMtAXseGWYwLViIttmD3p8aQhVYAyh
o9nAUqujT+UYkGee3n8BmRsWJ6FpVq3nKnGAWlcoN4POQ5SDras7qPhKvC+EYsGtY+pF4qBWk1sn
oZUY0dGzP2PGcJRVDdr95EzRsWcYMMIFge6astDH7ekbAITvV/VKOUOxIS7koNHEE00WMxDPYRZ6
kus/EIP2mIQjdze/ApsmFmeirPFyWEkNJMUhBJjZtalotbSz15mvgbh41C4Oo8FIQjr3c4DcHE7S
98RljgbgtCKfz/dQNbWZaqmxTYoHyBytmjzoFnH532kQkHZKrOLCY5reAX+aZhwwZjuyqCxpOopG
wjK+q9ENpM1W2rPGgXXDYjdg9bZi5bJydPvzk50VNny0kdI0IxawhIlIg1Q9gLbcwj0q4gSguLAd
bfBiaJRLADtSUOazVttZ2A7VwVxRIB6cmCcFhoSMk3+DbH1BSdolHvChrZX2+Q2UFJxJftXiSLa8
GJ1zzPxbT6X65S/xHjHSUHwQULZOECitUTM9eqmUBElFVHaJu821ByfhnptZorZrSzoyf68tcCgI
2gSkiHeTZ3S6mU5Ytl8+nGUWSQK3dU9AAKoGJWwkySV/PINREhTKQvWPGd4Kk8fhIQ0PO7+7pPzt
zKVp1Q2EKF3Jqm9S2n93VND1yRTYcvaGiEjiQd7tYPVTcBoTKX6/NvTPNo/+UwHXk1Soz5fBtIxy
jv7P1r78rywH5w0x3qkU3WXjZJCXxIHX/XwxRLYjvttN2ZQKQDqP1G0Q5IORd9Nvn0/CEmn4dIQd
Sml7f4pFSEOzblyjysMUKr4Ln652OTqpz6KDY6LWXge1njokZeS3xUqQHLnVzBTFFKeIAghEPRM/
UzXGvEcoyZwYuTHLKHoB7KEZOTJ9R+axgfx8IVYeEXIuMUVxmEIhVWuTV+6TqN0yaXdN5zxms913
vH3P1GG3nM9JO5wEFG4rJ7LsGVxSHX1Ons9AsxJlwO2ThYDXSLFtpNZtbFHxeslIIJXPzI0UDqv3
H7PX6jRsVOdmHsvZdcZFCrysNdxgyN4dDWv50W80/YIoUWU9rROHcx5+tGIy5DLotZZ0ijoxlffI
fha3ziYH2/OQGgiJrNM9ftBK67SFLeRmmI+SjQs29+STRqY608dR8LqV8grKI+FlTJjP5TDf6cSk
CxAo1BIBfcs/uFtxXGzks3wTphINTTGsg1V/tQD16pNrQoYoEIgKrkDe78nGdSzcr8V0qZmS14T1
Pet3iUF37Vsmy5oFgbLJtbhiUaI8g2Ca8Uc93/CnqPN2uq89PrV9JmCeKiQkVV1yn50Q8gAB98gw
wKwdiFPQIzAvji/QjCdmDKUOUFAR+Iw0kDI1Bptv1fYfNaWIlhtlW6aQKPspJYyAtu5IoajJxn2h
8umcORVqfDeZ81BTcwPCQiyk/A1geLpyYR1TUnKLnZPQcpZ1QcqGqre7gvwIzR1ci9VkA9rmeCUG
SPLyeMmvEDuIA4CcjOSZI7XYshzw7WfYvsd7H2rBA02LTEEJgHq7kwOqsXneip7gs+NRJ4KBUctB
z0N78AiCeRhOjy7/LcTIZuquBlTnvGaMn3EvVyF/f1oL+yMiDJeF2ZKYscY4IXKdnmdl/WHLjS/+
QIyhDTrLu1sawMWXfgmSriVC9tJ6b2D163bKemCNlUsvJh+U/GZE/DiYzqG9lIJ5t7U2dJuEi1ux
gecTx2jubW/T683lshNYwEa1h+kPNI7F4320knPy2evYYz96uBgl59xdAOcI4A1IjCw9UdHFhvta
O9nMI5qRXTS2YR4DGv1e89t872hyRWF+QCu7CsTrQnXAu28dpEuOYv2wWsMn/upDvIWiCBWHzj01
zc7OoKbPVDfY73F1RoyxjzyDxP4OEDPAM/hLEhf05zUIvjbIICjqibyvvXnJU9MkACRtQMKe6OCT
BZzqLFOvj/Ny0Rk5cy6tUrZNUcUNfJ3cjECGt+7mR7eqfjpDlz0qiOpaFUsHjDXPk42RudjdAUdl
xjRto64/XSDSak5wK+fxAuSSOLASubn+32T970kti1j6taYgY2VVI6UBG96jFhMqde5fsxEREX6N
8pGkASVHuaRtlZg7XsycL2U1OPaS3cO8IiBaKFPMCV8YeNUzm1uvDRhtn3qlz6MextL88Nu7tMFF
5EhrE1Bpx4M5hJDDfoYyIkA8QmaQbLDl/HQHN1hcab5ZaTIY46id+HTuKLNifHrMxYRJLf8s6M10
nBgFoz8buTogYFuP1DrAB5D//d6LDln5uh1rdaU+wz9nJeDyuLegRhU55Edx5Y0ssB4SQ+GUbpQj
tyBtsD94TaWAI+pE22qW4oDdK5OeEvwV7REIFCXMj0mxonGylPqKmlTd47tLt6NYdneTJa/p2+vs
DjDGMay/FK2MmCceIPUR5GE0VGlwlVVg53S7jzj23WjGx76u5nPDJI/4y45bHSX2Lt+XeJRWN4wb
4kP08HJCPqgNnHbL96b7Q+NS9fBuIODPD3in6i83vPBU2a/77lT8rSacZocvjHHhDvzEAtpVQvEP
g1jHbhdEDT8RPWoIfzGD6a9W4Uh5NCnsEV88LAUCJ3G9RCupyEY6vzNbZ/hBbT5HoJDjXzli/BGi
1+soLu+IEgLfcA3xpuGsVuQiL8tJ0jr+w/kPVce/SwKNrLxiXSEZRxZununYXVWkl0Gte8hHa1Qe
amclkuN3f77XV6eNmvQd3Xmen0qkJt0GTDpbpb17nCvpL8Rss0Bbvy92HTUaBJ/8/QEfrmJCgv+3
Zf2HvM6MriInbRC7swwBmL0qkTCYY7c6SbWz3vtkGGVaQXvSODjjdkLbOV7MX2P+trLEDrUXikuh
ZB6018zYQ9JWTDh89XFe8lQ8WbtTeTsA4sMaE+x/9w8b6wNB61gxy+u34NGrmp715sGXj7Z/koxp
aMK7cI12R6qTOU+7nu7ltX/hvy8ohTLro5FwegKMqa3QOLrhx7A2n3DvQjto2TlklXdmlDTrZqaP
BjASUbGFGblyAtRUcW3Ac7nI8G3U2IAe9ATAenZyVuO+nbKCWGbsZlw0SJoF4couwd8zsoSOccV+
O1IOVEf3r5KWUmFNScaeVtDqz31WUmXal6Eo3oECOGhqwSzxfCM7iND3/SJuAHWnUdusKxvVkxg0
KCdxkYmd2NUuXoPH35PzEvxgj6h8yDsA/azCnk0qY8RezRCPB3KwVlREVBltuLj8Wg/TQA1fcdAT
722zg/kAMZEL0Ag1UDKAzHfRFwJUNjRYH5cmhSUWW1hwjVJbVQthQQ8KfKxSne8VsGJ3NgpDsfRI
DUZlx4We9MnsoFStj2IdUyX8D2i69h9AJ7V6fmI6036FiXUG+jbw5PTGEA4BAdIew6Y4PdMmePST
sxhND33+ZlhC2vKmTDWDHZQDdDaSLx00c4IkAUaw72kX0HEX26IlMTkASOJjNz4h3nnxW7fnoZsv
16GUzfMTvrxrN1CE87LXG2ep1qoCHBbwLRTAEECXdeIeKN8MjwbFkb9POwZQzew7xo8KKsIJ09tI
Oj04mw2QB0pHWfxQIc9ziqhl0H8Af/11ZR7G9+ecszfzCzv+s42NwVbR4ZN3AHsvys7pt06hzYSj
5k5KpyY6OhHF7XcKozBs11EkWCvQoks4h6QFCQYrRhOWhNoIFImEK+jmXcBiKcssc/OhzTZnzBES
tkOXPCj3Tri1Xt9w6FhIAES6YvPsniryvCP438sbpQT1dLRVj069vKHm10fr7KpoVY+Jxku88RWb
wOO7h3oh8ZObhRo8030dmrYomc6YG3GetwiDutFwSq2XkPjeIHk8D+ImY6+r4LjN3nlGl1qOxjX4
iV95sVbu4x2RG+fd1kdvsOOIFrd7gNZ1waOOSxTNckCI7Wdy1fIMo5xP299hRbQ9gu+7G8au2Cac
38+eg6BS6IBbkMviAVlovXuhRCHUqnr9WEa1pwRklroePX5drcPGN9sjbAgtFVuOdzvLKurYeul9
aokosUi2uv+vMakUJGRRDlniMjp8ZukBpPq7Ix5U+90zjum7WBzKOeRQgIZNnCN+Zi3gpavdhyXJ
EPoB6rA0HFcAR4whFLqIiAbwVpZN1ByCLUEkSFO+8ghvs++bU3Ftw1WIpTQSkXCsuNziGQrxpGUI
/LOUGdOXvqe4BKg/i2h3W4C3NQaCd9Y1EaLm7hV27vScKwUUSmryO4zRlDt6yFKMl6Bjz/h1+99M
4MBqlS1awPxlCmdwunz5DHJU3Q6TDP97Oadx2cfZ0fxvHsR7cPi5uoHFo/3m6T4uirldgKxqgFEu
YlPdP4N1J3R5sX/oerSCa8lECeoBsXruMRgvf2l0tJUxJ3YFGWOI+mw+/cAzo3z7Ywpe9uqja3XH
S4ExHTXkN9Or6nwL8TyfPg9sZ5aTi1S4EcIhVhBDsxv3CCekNcEmi0z8sv0zr56x4tvEEq5ocAB1
JKQ7ze9T1Ef11gKrOK9ad0eW0jQvdsdxDu1LMYFnD9pXbHd9lQFPbawVMoU3gvFLZzH3tWBYeo80
ZSIzqhzDM2C351KKgqwhTVWyPUtpITsnz8KKt4sEWof0r8uTZXZmAKZtRVTqeolLdmfEmBe0EAG8
ui5p1MUr9dovaunc9Y48PbWaF8O7ymcOb+ps85cYwk8Av1KMwtHrF5hkZL7/VRhEPdJA89dhdI7l
MMSpUORtNU7El3sWOSJg63h6smmTCCWBgQSUtf6ahshNRajuHXJ3yUvCFjXj3YXFnfALHhBCifF5
Twcpoo5wUoo5hn7ImJpVMW7Kp7p7TcrUpsXe+EKcJKZ+FFkmtOYLA2DY2JJDPtl3N4J/Bp6O7n7U
O6/BB7w09Jhmn4PCUjXrEk3OGLTy7W5TQMo9lC2hmwWzodx2cJwqxZHxt859BTJ3OWcBD1ozBmBk
eaGCokgBhWtub+iSPogBZtxDzqJPCruraD5w0/RHh0/06NUm6Vr4HhpT+GRwvTBV+sjN4fHL7la7
nufPoQ/UxVeTAZ2oz/hd56A1N63vM1YXwiC4352fJnSLUra75FWm+nOgk+IJGf2rZHHeuvdq/WBq
ZEsE9XJjN48McQiaMJ4WCCjIbABOYOAvkO5SEqUZqgrffqnCs5RUNrJtKtuxlABMo8VZZ+g0/p4K
yvjyALrLvUGjnkqmbGJwghiO687R7csj9NY26z1mIskmJJ3PQhLDbPz/8XwpusKxsykvx7tws/95
mxxY04l6abt8yNe9mhlSfGpYSYREhdvibXvMH10VMOffZaUHcY7NmyoPaBXL367JkF0E7U5XOFpa
tJN4HJMAchf7LXFBsHs5HtqkZ6G87DFkg1Xcb3dM+rDb9nWaTRZ0i1jCoCLVlNTLcg0ydxR9gn6U
9KN+7DP1HHkVwZ4ekLYQAi+TWkdQKNPxgiN7CDxcRIw4UpDjqtKd5m87lUSVgLyx3OF/0NopDQj/
bqlPJQ+UkfcGzTeodif7RTyHrezyJaF1Sn5TQQB4VbZ7ydX4ZhzuYBa2kEqGljI+eYCIlI9myCHa
PxHa6VHnYZolerPSoKNWuOHJI569f8Q+okvSb+q6SD3UZI1hO2sJqHlbkZvku96pmQq1E/iqVVjB
0lAay5ZsoosqOufEa3XcKYrllZw87RSotFfuHjaT0MNHxyKp05T2zvAN0telRVsuuoGl5nKvraXp
+rwXaDTXzKbT5JcpdVRun1BK/EqRL4bipyVbI9ej7GAFZG4INILuTmfxxCK3Q7pgAVMO9ZX7tHu8
GY84uLgXbTi8u7iJVl7bfHC7ZsREJlPwyV23NN3bkTktN4EdVS22AvbTBF6AvbBv+C2YIwk1uGgR
oY8K7ygOcP35GegsG7e4B3yKAd1C8KnzbFE6H9ZE+LIcj14/SdAURE04b8gquAhmBH7Q01wk+P1r
yZzmyO+t7Z0ouZL5dSBOenCiXozXLIhVD91LZm1yq1cB35C4slf//aE+y0BKK493xpMozMpUzQCP
VciyeLZdoW5iQv4+6Y6QdL855BjlCU+7KmZJhRroP+UJam6uXVSgltM29uUwMrTq2g8MczVFCEBs
sPy881S8ReR9WZL4qkGBq0al3IyHw/VBPWvdBm4QEcRLTyjuYSstP36ROhXS6tqYBebbJoR9wPTe
eqTeYxIqZu1m8OkfeyQ0LokW47NX0nwGAM1kin14qmcqgrqOi8dU/Vun8OnCYSTiMrmFVy1/DfAd
UTLZD4cKRhdlgxEdFihT4ZdDEW5OdL04xzVzUCyURhmg1eBEVW5G+w3zY9E41cjzK+OPwNbxbFgd
QiOMnk2feQsdhuR2Jnjn5dBeE2HrNlrF53xNXQ+CucDvGe0OcrHdCKhrm2phQ/3AxBRlyJbQom9/
f0ZGKTCWpm8PaYH+itVVRfX5TFM/0SZ9R43H1lqYUQOSYCkqqwcG+fc8RLGYzz2gwIADjj9y4HsW
ffviUS4UP0BkEMJPOTqhjhnLDLzOC+dENtnBlE6t/HZaCJNdou8iobeXdI1gSnn6XrZFdnq0+MIQ
zdnU9593VRS2NyANeXcBPVJFUX8KUhW8qqeOSY5hh5OXnu75w2c9S6n723YG7QaQCkc5Q8a4c3zU
54QqndEjElTdJdKprJGdY3OWJrcfWLr0cdNSrMcXg+tzV7f4ZXfYZwFbaAaLao1+ESSvBDoCdknC
56ZSkWUc5PcCZU1CNzbPnjjK1gwVdvxg7XAx571HUNWB9RP+2X/5ikfRZLNXif/mDjs5DgX0FFp0
whJWtZPbgBn+QBkKoLl+GthQowTZTubu/MqZucyMrI4d4yFNi29Fz2+lTY/cijR2D9xVf4VtZrd/
6GXtoCOz51z9yA3IaCKWhPPpZB77hCPHSHidKS9eY4GB5AnOhczXmYH1HpAK2fJ9/xTgf+qrMGZT
5zzNjXWo2wfWqhXEFR9qLveKLXeim53SfE3k6ExYYbzc0XS83g0rUg1+5FzqSxrtLGtAbtr23Jw0
k5k5li+IQk9gUnUb/MBX2o2v1hFHRESanbhseBOm7zPqCiURRMKSrbOd1Rwl7vV/dH3SUwtThZ4f
e6NHMG7kP86bzrgmg738C0PfkUBb4poTMTy8gSrCuj6f0zFr7BhLyHMyxjVRSfHYR8PclChltW1H
Ba/kmUpcIjUfsGUujx5ty20ghn/UPTtjvVq6hLnyZiWwJX1eHlethCY3sEOeJ5uTpAwY5BIoyQhH
DP9KwZNMUHZUAuhZOiMEgtcR5NIOx5MraIdf6j2Xhcd8avJMW6Erj9WN67tKEI2FUUPm14bfauaM
Vw85FhwFqPOBdvm3AoUNAw3IPl02MVyaOR0bg/KiY0erEFk4we5xIl3Q4R6ZIezMdk/Lc1mpsgJe
EXwWo7nTIAmOb9E4I8vVw2OlHOYhUuG3v3vI1hnUTbDycBO/zVusn4Gswe43uGgHh/fyyC4XbfUW
HTXMNLHDTj6QskdRoo1bCXugwukNgynvJYdMoyHcO2j0ivlZXF6fyGHkc7vTVI0cSUh29V/B5E3R
d2FccJuzbEvVmynkXWLIiKxv4M5f0nKChoCRsjN8IIVokxvhCZeZwllXjwjfTg1o2q24TYS57hWD
gnIdKiUC84wwUN6bmbnttfLUiQfpqou/Jcv9vdR1+jY3UJ6qomY4kDeQZnZJgyq4D3OOPr4+A9W1
SQFd6TNXyspXraOjd/ZwXrDqnOxaHb8NevIKOquNIk43nEiPRQWNFnu48lqHed7Q/88ga9IZKBEu
LZwhJV1BBeER5Sd7rfH+C9V7sjmubYLCWZ1dkPPdsjlsKNyF0lRWQMCbBDAnihPnTJ5skqZ81ii5
PVrnUgpoNDbTQ2efAzBJ3YNAe4zheEaYwd5pBSjcTs5T4NlHi61iXSjH1IXKfQ41lOOWaFQ57Yk7
7+DruU8Q+uchlq2H3gLfVWWKwqxpxnQ/VsuP5RTwJvxnyXSi/U9n00UrhWBOij8byf35+zRUNnGc
P40rdGeq/12YI8Z7IfgfS0n7BsplcJFMBkU7fxLM3Sq6JaTbcvSkSAa1tIbRXN/Gakxy4JGZTxvj
DEqeuvmYpY+hFO6TvLGqu4AZnKVi5etx/4NS6LDUBf4y7lOjDDSN4M7ULUc6w/MO+/HwAyiDpilP
6l/mo4HgWPczTfQGRWunNpGSWYhKWdzmA+XgEZESK5QVG6wqhJ9qpClEbvfV52KzyeBo/jq6CLfE
KEqwFPuRXz/3PeutA3QW1BeAiAddqGKX6TfQwL7iL+Z4HM2/Rn/Tp+2H8QtzHxHKJVsA7vF++iAj
OGz+Fex27r3B7dGcaKSWgsBTeexXivaTl3/b+uOhwbJBFZFidUUux4G1ITNIox9BN1teQwJge4QD
yUQuSDElA4UCU/76b13K6aB7kAAKNMYRbmh6CsPvGMcy3BAvnem8Ro9B3VjYEmV7Ui6vkMhdw1Ts
bIpmIwc8LBk4lg0FPwubol57nN2YaDnXLfeYpRWloXhehbW6LqnenIDj0DkOLTiziW/mIUzgFnZi
lnteNcAnjYLizFKWbUDJlMS5hmkUiopH4F74JPteSIjYqXvULtGKXzAa9oFIhi739+6USpy2EYn/
dXLI41Ebhh34aiX/EwqYWL1QO8Msi1IZ/66V75w9FH5cMrxzXjD+W0wN8HVc+BRTWcH+/zdRK3Rh
9N/e4EhkSeKjak2OENB5OYm7RiV8tI8fIvGyEdwRcjP8whaMTJ941AY5nsUhe86Wtx4h65aEtSxh
zQQtdn8od8bTEpmjsZ3ySEo0ogaK3jEaMDxHHBmqEJn+rr6vmCAwJR4a6iY/X5MHaFPfrNkFnCDG
NiQt7DZ/dmshCb0jfvW+VT//8MqQwfBDcmhsWlOpRFahKoVFtwASbqOL0U/3wa1s9ZJjzVKTC5Fq
ZzXjmzoSDC1/9QJzh8lwjyOq+OFapDt+zi4Gm8WhLiMuTDx6LjrNH5V5EV1opau0bFwLcBOIFzV2
ql8Sn0/YF6QzfvfDJQHJeV/izB86NvJxqmen3vBHfeD1dVZoZq9qITVkAajdtDb9BWXhf///OBuX
UUgw893HaED2nZStCxuQLtF1By6XRz+GHpTslIihkEPDMEHb7QpKYmdlgcqbanOJ3FUmRKLU+4aJ
eQvRwKWEm6/Eh+emnen/lWspDaI+c1gMTIcdJ/R1vlLdR6wI38P25PYU3VBaaQOk0CGKQI8/ZNzY
w1VOxisCU+mybseAlipPy55JP++/PBiSq9b9+VKjJbXHp/89Ny1FZjK1oEFQbA9t011bznSe2Py7
RZ3hj2LnxDuAgC3FNEV/y2W1jeAYi5B3Na5AHKAHY+bCd2LmUqVOy3/pYSfrpmy74gO3N+g9VKW+
X6+Mo+Bt82CTglaNxZd4ro8KiQRLnbWgsXAZXDqGqZZMwfIjxEjTIC2jXN5hndsc6FFscEGqeQM1
flxu8+iX5IUFcXQ2oQrKerDl14eSGgMC5tKhTF5/8HuAdZf+i46pDaOmhHvtAvu5qkkrUhE7FjXh
a19umDd5ueLqV3RP3qLRXnc8j2WFkBvya7AzIyVwju3XmD4y5cS73fz+XzgfTNQl/kbF+Lm3WsFl
oAFkqOInxJNJvCv6+CfJu/gNJJYqYmfEHTLWKgw8Moj5DWpoV4DgRx8w8roqiY3Z/c46PeuWflDC
G+4dwIjtpdmnqGGLChf45vUetw+K8xzN21N/Knu6R9fRrQZm/p2sUe+4z5tVnzihJFWIjvAxdXET
EgDqt2AkOF09KOuKUQbBek7erw13qtft2Rt3Zm81yf7ccrIjuGDbf9CeXIYX2UxjpUjLHVOvXueY
dFqCjBHoXkT0UcCeu6MenmzPpxZ54ijoUi2qcLJ2sXq3VWSIOuuNpN0ilVUUhtqOHHZ5JumTQvZd
okQDJ3UH2N6yPx8qWOH/cjvor5rYdeUpA3gsnugcBV+Td32sG8vBRFgTYgm8iJHKNiLh7rSNwew1
BmWsGpmpW8Zt7QjDqDHl5PIX9Mbtx4W+JBP/QnJzKVobRFtESfaRxuhuUjGqKxd59KMkTPNSDCfY
l0OtPaf40Ke/uBNJKyu/iBJL1shsYw0JBlXXILGTmiYf7nLVQ2M6xIKssYJVzgx4Gb/4jBsN67T8
oX/LT8azX5spJYDDCVrBjAb4QRBhQ/U712JDttbYr5RXzZ2nrFt+DB8Gc4Alcuz/34zn53YI9hRZ
sgYIQtiKMlx1Q0VLR0A4YEh/TU38UzUnRJ/D443fjShLCLqFlo4ycm0L05OwFCacwML5vLA9Ne30
FvOdyuxrABFrmFT6Pw7bVQVNB/VR3bLFni5pOpZqxYQXo+fzmGiMv3nFd1IExv2ufEyV5/B9kfaW
wmSFnJghGCkpOW6Oh5KPju39iVHDLzJiyEL5vPioZ/WqVwVAKUMEeBd06/dVSRZizn+Q8p9pwbu/
LZSUiB+R9dRlOZkVdICt+MSm7mwqvJr3f2tlaXWW0dd7WhqhAlPddmWPc2t5zZGmmBjcs8628SBz
hcfI4IDlByj7+DpuEcLPmxH5FpR+v9ppAOsAdaWdbWy136YISDgJEBbD8aSH8oOrNHAG7FjeyNdG
zstiYFAwZmt8GXmjc93z+C0HUkMRCI2NUvCJ4iVRbQc/pB+m6TINsb4oQ6MTwTbkrYfOmHQX8xPa
xx1ApWDk5Qfmt58hCKAqEo7PCbVIizsaswU83m+e7Mndyx7p2T6oBmTfUSTBrZ/djXvZWJc1LCnk
eGp5yt4FH4KhvSf7xzoe+vZlfeXqBYhhvKcfaBnBDx+g/46tAUU0EOG2IuHI/cuLRiWh65OqwPWL
bDfuAY7wo2XwTmoKw/p64tesB31DdGFZPFubMRbLKrWF54OvI1EbWJggHaRc57O5iD0EvUW8Hlkx
K4JF11iGMr+0gMWxCvtfK8n1paObxB5zvnP30N4VW/EDXsPNFrbeZUWvrlOjYcORMoclfp5jaf10
XsJxtLo0SYvu+LcW5KOwblFvFZmNIWj/Tb2bDWVD0UYhgsAIDXRS5EQkbZSkXkUodZgGsC8Detwl
rPdCm/g69mZpUWPrC92dY+h+/ko6z+DTG9fkUVhMsGLB+B7CJl+YhqhaXMbLOM9/dTj3ZlDdlmH5
qKj/NUVfaaO56jwA80t0APADj02bOy/Ml9yg8pwaCKc/i+r5l3J7LmKJGhp564QInW+PD3J4N6Hn
rqvCeYgaJcMAN7jDUxdpehj+wCFHbf4pzdZCh+2OnXQT+NrD4HEsVmJJATycyl2Re6a+8g5h+YWy
NQUuH04oBlHJMQyMuWKWLOGE3tGpdHHM8mrYxSxH5q0EkdDiVlkAbWg7iRb7GKUTzM0kTlWkOjqu
+vWsUUqDLJNFoolJkv5qn4S+oknjLmDx8m6g+8Agb3470Fn/j8QXnqwofoC4G+8nViV1Ug6S9f0L
rKhjz/f28wxYN0zEx00ShxcaBL7LpGbonr0N97qIIDWkeX7uv0LNt0hnmHxEtM0JQwJbhwzwpVXo
aBqze9fPCyEtAv7MWy1bJrA5vJIXu+KMCYTEiF6q5xL4mvM+iUbOLlInpdYRO5YSFMaJpnj6EPej
LFHFjOfDt4+nIaqO73737ULzIbwsZMmDZwJMl/w33dmEMtxiq3iyvkTIKHKfPE3gI1BCEYw1rni+
rT2eerU/IqCkXV7l/zRN0mIJkoirHQHq+4qLuTUJKkefejqnQhsmHHE2FDIcqaHpnsvN6P8Vy7Bb
gfZ1CUv7iEMyWXncPBlwN3eFUFPexTSSin/IxG71b6NyfEwwB9sE7HA1yFAYi+Tey+VVvhY0PwVk
TuUAZiZ/qXAmwWuWLlocI71N8WoIe983iQinvolIcPTgj6Np+RlWyUKHdEf+cPHR8KnHda0F/dCu
8Hs/SyCKkAjc9ruAz+YdbyMvdFwBkE+/TUooW5QkQEAgkZuTWGf3IyGgK85puw/fFXIMv2QcIDlK
dC231hjr1a++xYZkJ8eo3m1iNI5BEKyILyPpahH4bBcHdXEeylGlPOy4n5zAYrrA5MPUZqrQgxPb
n2JzRyTgw0YsyjY97P9M1Z5EjSoYf49zkfUYdvhatmnXkrX/04NIIo5ki7A/4rdDGKoyOEFcUjS7
Go/AfWM2LVu2N580mNRHyhYPSMeSehWK0A2TPw/p9Kqw4dsHRoN9EAxt9QBHsflub2j6Xi5FcbBa
j4aBzS83WVqlFgfvq1alo4sZqCWcHbDWMWdutfqgL2+rBMmkl73KbBTj0oHWCfmdohpv+TAd/XEY
2q58aJDK2Yy57j4PQZfphQIIubNWThBfUiMKOQZmXYw1Zi/yjP3wrtfQG5q5W3930giwoDvPlTSQ
RmBIaEQx2Gp/fedE0X6wICyfYIgoxXG9kHrvjQ1py1luKdPGTau2kQgsO0OYU14NFNaZCz8sGlKz
PnpVX66PQXgIFb16YWG8mofsO3ZPAhJH6GkMtex8s5VGx+oyYLY9H+q+YAI31FTR87jaepsr23v9
o8yckAcgN5ZJrn6yi3YXUUIUU/RaoOjX+JLrDhmOS1y/yzzBuyO8jZNlQjLVTq2RmoZcXvjgtl75
H1/owxI95FiNxpg+OqM6WbOoN7f1Eaa8hvHL2tIJoZVseiowGV6qA7SqqaEZNhjTo5yHir7WmSlK
U2gryW94WdqspaHbWtigOb5lUSIo+Rqctdcom6TTMj5VKpi0B3WTcNWTEWfnwYmEZnowCrn0Rfq2
QJSoimu/CB1uixTu3rru7Ufg8k4N+xaL+7htW6SJdKg9AJs9DFgWse/KJQzbfD5TUKM39GEB+PPx
8Ev/2yK7QJitBKom8wCA2gXzpsOTV2LXuWasm7C02uey6X+JjFWJZqC3c8OeR4e4jkoL2OyqRtzj
Yv3DHw8Bm3fhELjIk0gVkf5S0cbWPGWVT/MoWvI+rn+PpZd7lRVEgNywgCfZCW03ASeH9Y3ACyQB
dcJLsZ6rsIvdcZibPSeWiTiuHtxupO7mtYnLw77MRjtrA/oIO/I6KXg4MB/OQlthznRsx3htjUTT
puhrwjkxSyMWhLp/mVwvkoePzhnauGRms7OGRUun5Wvi9hS4oGY6mmbpbILXu7kWuGi/2L2Uni2C
eWFj1JeZvC6IZz+y3HANmzt1ICcqRKV78wV0JgFuSlj/dMRACGEY7hllA8mOkjk5q+StWEbCZ9Um
lILyVBqBGPqQjv28k/CJ88RTXTwHFn8ojR+IGFSQxwIJ5+I4rsfdDzLPzWmKo4OxsbxZw7YisNBR
padxnYDsGZLiw87Igz0E/iyFR7+giv4XOeiScUe284D/zxSUXFbddT1zTBErBhJOM89CEGkuIMmh
X2H3xioau1l7WFtE33ku/CiXwo+JXNmeamUlvJhaF4O13m2jtFSlkvu9u0ci6A3FgGqYDPSV1d5U
DdepfQGL2IQqFnFmJOeXjxbWG3uYN7qX24Ck3L4Df6B6h83LkCUokvPPalYEdYsYrPbbQzwFX4CR
r9oAUao8d2n3JgIAzfd69XPHfcjyydc6ARCTRZRMDeIaruXwIgtC3IYNiXK15QUMm7diMo8A/hFC
G7rN9sUolvwBLfdJMPCrOturRbJhQXzyZ8HYsplCuI/MS5vo5mkkE9gtG4VyozoKraDn4b/0EbsY
bf2DGLITYCFwE9LeitY1YluMABs6spErljX/bWQ0nRj9F8YcF1tIedG9oRWprIOX5I1dvYle6iTl
s4BQ+wNLa2QlWD+ExQNsMeRjNgt1a6hpw5QjZuc7mwiIEtBaJcS4F4Us6YG8HhqS7T1alO3+FYat
H+lmlVu0/Qjt78lopZ1Vzc6GiIiFc7z73mRxEYaqLhomUTM6ciUDMZeukrhkSP0LKKYkV/jfCCOW
7VGBVxzi9w9mIgtQ85h8WHGCkEc82JOeh8WkQd/2YF0rQChvImU46NQvN2j7fc0jPGh9LXLuoAqi
UbqUFnmQ787FGzXlhNW8KKAvI6qXfWCwgKX3Jw6TY5M5FRWYaCcQPV6yQN0mSJsbkvzFSvsKIxCD
B7k5LXlT3F8EHb4AcGGHKPkpcDY4Qs8h5IuZx9fymKmMxxwmbHtklNO7h65Lul9iUKppSpYWRNDI
VExQM0ObA5IfAimRtQKlwX7k8l1LVoS2NgxfXjUrRN56xaqhtiECJjkYAlUYSQNtseJBrotRQuWq
MyXWscBAV+PKhps2Rinb8VLSQHoZtEqhlFGwKjEJ9YRCMUkCsYhz2Bf6GWawjlM6PVsw4s+qxuK+
28kTampB6G5Er58a68v2ZsBesE2cRUEERVdQa/WMI1/QCz0tqqUdjpdKVy5h5xGfRJocM1YoFtsl
1U1VeSix/xnVqGkkAXkE2HS8DtmsRcgIVBqpf5Xx0QBbtXnPjvbmuUWSVvP4J+mwIUBvSoC2Pbtt
DF96K8dCTP6v5C4nq/ZEkgT3FeqzMQ670l4HJ3vJoyxm6sTsI97wsBKJrOWD3cZM4RPhFtZTFJhz
d3TotvHuIUs68tt8NeEuuB+XgSNdAgJ6HoGjJVpuplD7FRrD+klH+mKl0in4Q9/N605YfB0JUXsQ
rWH1PcalfyPiooQrn5VnB5baH+BFaf3tuG0IbzPFC2Ez3LRMJ052WPCM/FBo8hq+Rd1yIBtBmaql
T8TJlUxqaEM1mQqnQowXum//QNIaG1UhcgjOKM9iMvMlegqMU9YWdWeu+ZoRQmF3xXM+mNFj9/Sb
NoM83P9Mfo+TLysxc/ilWoY2oAT5YtWi6eQbuv4CEvjxw0VaIdgAWAt9toe2aVKKLmXbdod9wb+N
9682ledCFqHgYOjyKG5vmzlRr1LbZ+Is+jJu9bP8cJANvfeJCkcOTV5iVDb94KSkJEtvQnN8UeUq
l/3c7RuUFN0RnR0kYwpAWMfn04Ng34scjLYLEf2nnRdF+TMVaHkOBHw/0u5YBtV888KClO//p3ZH
O0DsDNH8RTiFVWaLc9143ii3vb/w9wntYH8RhGArIXs0g3eDuPFXCl8+qVFdQWcohgUxafbZaa1+
b+6mCvIkqDBWiZNecBkY1EUvonkXXIkjeGWTEVrDWKYLe9j0GFcaUmvv/y1QS8U3CQSRCtLAM8/N
RZX6Lbvbx+5IikYpFT++Z8vSx3V7nv661pnsy3yVwmO8/naRdqVThSYhpgeUGH0KyaQsAoMU3JvZ
AxdYiIwi/0cjSO2lulHpdmjkvDQtBLjoPQCvfTbIWUEHERBfXvv8S/vIoWdiVSY5JZEDZ15aKBpw
eyn4e1wZThAoQcBeDkDEoGKfseMJ6kFXp7N5b2cidTVOnE2X/C0ucBhqFXHmSAoiVXLH1nrXdlNL
0KH2bM8wcNChWivy2lKTyPjiwLx+LrrEaYZ6EBwtjQZDyWCpzlH0f1csYxwAmzxVsgQIP4XnF4I2
uuF7fquLzQ/Ugh8L9WlcX+JgICaHWIEmg6EHfGZIG2OrMx9WQnldJDTWnDLEVw5jhQt5NurU6oAx
E4GOkaXI7fBb04thBsZsrpAG1u3xu49HMCXbxJ6D/gLA/W5u2IAYe4aii4JcXQofh70p8oM0adSx
dHKEN6YB54N4Ocn92bz7V2ubxWYpTTTJ4/SMY0Hx+9hASuwScxGDbjP1rVqotgBL8VjCbSjeT+rZ
eQ0cbF0BmFfQtl6etHT1tT7jEztPWRLogRhYQpsq6PC/mxFtzkvf3PUXvg/sT+G2u1tRQ0ZVsJ3J
2nBoXEA4AtcSzhEgiSMteJmRw81QsiqwYRpKm3mjpw1nh6g+weTQQDnFQ6XVIUhUYFN1bJiWSH4s
+0LirhH13tJ9M8WqKPO94yJr3KSv1w/ah8BHGNXTkKKVkqdex5jrTDJo6NiR/V/DX9bsG1hRJung
3anWPpH8arnS+c/oKSNffDZoLqUs/NbiZQvr6GIe8EeGZSevqmdJzcHsgZmNebJOZQp6LDw2SLfF
v0uhsV8lwQOxQ+/6HYqK7rMy/TnRclhfyZynycMDLwvMDK9qZ5PCumraXy23RtXTxjGgtogHJY9c
GZo+GyM0kMQOinMH70RXgZe8icUfzCx64H1PyrO7l1f+8shIMNUZLbvuTWbqSv6BzWXSSG6Ddv6m
sGzS08F8Vz7ewSY0l/KT7+SY/Vu7AKGWVGSE4JP330mREtmFG57DoHyJzeRqM2ZPpB7P3rPeO5sP
TPuF7QQ6XYg0Pk1a55ht9hqpb1nxQDjLSR7+HIlbfLF/rHD5eKufOdr1gVk6urrm7fufst4CNe72
hs04P60zypSK5PXEURDmHxlpliO6xQG/qImWEQ9RS0pZeTa9tcbJs+xeryohDITUrqmClgzEyA/J
Ha/8n+ubCbtx6XP/b411+63nd2Tn37Y60LHK62zUtmno7MSmq/A1PLlq2XQSwEj53MGHUfS4jDSM
PK+lkjcsqPuzfOJkMJZIbotzDt/dNKpJESQMutdSI/vAJ3x77XFkvhWykN2bJ6rEf7wSA1sTS4iI
LWXWMka+u1SZDlXcMXewUGxmueB8DOdx+sdBgZvJ5XhsqHbpqKn803KCpVoq3Rbw16lyuh6yPMdg
dbJFujQaqdA8zyjpYipcJYcE2wvGdGynAyWnWXglBxzHB+GB4zAkidZjOgvSw9GCXiVe27g3bFVi
D3pRLLgUoM86lymYtrIwLWuSU/ymCYgz/rGSNFZq6zWkclCcN4xu/v7SdUjdy6mhQw9DWaFgUFue
0FY4U0mRwS6FS9zLm/bfTlURrAbqR7HzxQtBZSEedPHyuWLKIVCLoYv6FL9WgSCTbDtgKip59Hnl
iDambXywsD7Pp7JWHOO9D8uheX/cqAwE4H81XlUkUJguX+mUDo6N+EVgUmsYFyc1qT3R2CdF6EG2
5PBO7BG2XKFhkEVvXTlZAXQWsLTM0FHf3DQgGhsB3J2EVQzt8v/gkaW1skwq+u4+oOfJUVEOu4Zi
Pr3LLtfvykwKim/zzUePJ2h92v/uvqGC5hD/nILuGF8vz9f2nr6gL0Uifw7wgXSpUG0QndKNkCHn
UaurziFdF8BIl8nARhtKA3hp5RQdQjCK0xg/q6KAfwdHsqjkb4/PUMYZ4ofFPhTRbzz6Tzosx/uH
R7bwPgXFYq4+b69Ft7HoqICeVyAaocmuXqD3OKaiSg28qSPHt/4b6g0i0a495Bk6WEOcwseuT5GF
d83iebb6al5PIjODJJ4mDwu1u/iq6XjUWYhs/VO6TUlmt/8rtwDZTBXB/kkFGNBcDTLbSR2qN8dz
lvxqeRBNDLtUvltPtJ/z8A8kngfmGz4DcQNeVHL1hVjX+7HbLyRw/m8Ptyzk7cCKEkaeGLjfkYRV
MdNr/DBMqcCVV/AOGhLyfnp5j7JpVz2uftfvcWtA02qzEmpc4PaZfrwg3U3lwLti5aeMzDdIACIJ
XLUCA4lsQQ9Ci2ek7FnzWvcjTuau8mTzFa7RuCWsF+mnD4THJcjaZdlQ2eo+STTFPGJrvYli9Gpq
Zavr/V4fs8GaT3WsZ87W83KP2+wsd5HOQtgyNZ8o3nmAXi5q0vnsRBv42fucO9BzHae0V3Dvl27f
r17mkX/IM3Vd6nm1ROS19lRzZt+w8mOzh1tynpdbhjDUAcT9UztaDMSYl2fcgfmzPBi+JaNofyFm
qBIb/ODfxBiqRyhLVkbiljL/mc24BeT3fpr9UM7OWkmOAsHE5ThxmwpNMCNkUl86iqh+gMfGVcMG
ktSa21F8tgjPX7fa/82ScgBGHxxXlS2EGxZ0+3RUk2N5OIgyvUBFKknee00iCeQaUsDQkqJXfTuC
xlXtnez2JfL52vk+iRePLXKUWJnjxFRfvAPP2sPUEW0XzMpjBrnIGfAxCfj8IWgxzAtayaeDO7o0
IyEj6aOaBc9UHQN2mkc/UZ+wjLbRRi7Pxr04ftxjKAOYmCi8OI1YcTw0LeuEGiNOg1tvWhlxEU7k
xV7KeHlv8XFuWlPkMXjj7tn43WnxGq5YPyKKJunCL4WhVbIffb7W1s9CsiMxcDhm5yA8H8vNqP0q
dy2vVTrvNUQBTIVc+oFnqvndgS15inf2NGuSazYWLZj257RS4EB0o0HlNzJKl8a1FVvrIjuEKsFX
xQpyTbD8Tpe7BkRf5RQoC6+CHYUU2VCT0kABS2R72QhtkdvmzFGiuJP6cRaasm0M/0RBsjdTWr3B
z3RBgnCWJiGIanaHqVLTXpHk1orL0svitY8JK8Bm2xnmG6qlGPTsaMi7yGoD3tO7t2f+FXO8svt8
QAGT0MtgpHCRoUxweo40ZrrfKaPvJO8Bhi06nlMLAitMhPAT2BJqyI3OsJoNA73E2M4hytRQsY5b
j5qaVkLnpRW7e4Vqfvrc/f1BBLAmsowM3tJRTGllswIIXCs6T49+cMPTC/gXdXReNNgPQYiaDsTP
VGF96tx2/e7bbQR2Ohwvz2jR9Qr8ru3yv2vwk5xpO5q+EoyBo+j+sKA+YmiiwMbzYMl3bPjryvAe
hKaYwI5fGdvxRp5AVZyIbzh70R+wEqc4BINJzXdN7pE/1C99OtSTOZwArRlTYDhco9tGeZ8RUhBU
gwuWKTztoHruSJDSW9J9OxLZgGvhJnRn/OJZJoIvckhjhv/0KXtNe19wJJr/LVPALNOmXcYC56n8
BeTus6I9zb2hDoOtkWS4wD0v83v39sFi73pHiS8XUHr6AbhdBX8Fe0RUOToDL2s4lbAGR9SmC7Vh
oq418+IpR5K/QSB1m0g6DqY0B4M5YNj1OoIAELBOLBntwT/prOBV/QL76EUgW10CbK/wwVzcU/f9
22837HnsR87s3smZ3pYHBIXDblGMCVjfHAd2JYAxEmD8sH+V6SJab9zbsMuHNNY7EXKxf1dVVB5H
JI5BzdBG0jxOFSzXhOvnqDOb1beG9VtOhnn7OOKTRVvPVzKDp2hbE5ppku3Ke3ON5wPg/hnpb9rV
XMpznikJg2j/Ywys52hvTLZRhEBbYCohS4H8F08C4YGNAj3XTyN/ZH2GwhMGPee4wu6nmH6ZrmHR
Dn7TjxCJdQ+CwV79iSVduUtKNEDbbO26ATEcs7xGumJXUm31X+GoojRXGJBiVGrYx7t0BJb/cAKk
KmezkSz6jpyF3HxIREOe0aZucbrB5Yd9q142QlnCaeAARZ0Gyb+YWdU+XtWIOKwoLySYatZdcLEm
bzeiIf6T13P26U6xJVqiJf7h06PK3fR6KrcaE/ML50zSry68L9xFK4uqyfoB4IPTXrkkYPPfxTSF
EqX0nhbR5qf/5BoMCDC5JfKb4J5hbTwtPOFM80lCBf3C1hlOCDcHhFCBIpaLBgHUNtRDHessk6AX
rZvZKGxubqEV7b2d7gqNM/Nx9GAdoq7O8FKZXItxQejEl17g6u5EAu5/IHYSVkjHr2agPx3WpptQ
2U15GOM1M8xNdsYV1EiT1wh4hMEPurpYMiQI574EEdEEMp5xZnY0s35QhLtQzwI/mdwjfSxO0iCA
6A63kHW7BwrqNQGeRfQj6BJFkQUxEEDsBj4o1202ixEianvO5ehQlNddqwTI+4gs80YnIOXHDsXD
AB8Qj/dFzxopMlf+veBYI/mUe1QuvZNL9cY8m1n8qWJGTIcnQNqAHzyRJpA2W0BPTwOY51xRVp8f
f0BM1BZH97bPa+35eu3F7E+Ns528VxQzDDS5ClEArItj7AGlQRH4XBySURIHOmxUO/zbc9pvR2LZ
/QGQgCGkE/b3QsW1r08OT9Fi8bdLXrOaP3QBSfT4R2cbM75DNIBo3X1BNqG9OfP4KqdwlVTjd9KF
21Z6eGPt3/pVLyp7CAqj/jWRQPJD2withMjPS0ySuz82zDAOHsIppJw8LROym073xYCI21+6Dk5s
rP2k1bujilABns7DWaIbTYnZsBZnKJDu7H/7BKVOORAxJLnglCOs1kDCR8CFVELz9PxkH/X+iot1
teVMBQhN0a4hbl61TVHUg9tGgrMr4LvthNLuC1qwogEsz+DWKEFKmkxCzXHN2ITvg+8/U08HAXy9
e/C4xCYYwr/3MVkixju69QX3o3pbsybCGWFewEl8iPMcvjpnvm067K9RSxQ4dth+K2lNaK9Hewgd
V720oqT46lkFNuWiRKGxKGSWTGoRlrOnnSB1fcKu6szz9MOpNiINZPxSZlA0kqpcJ1wLdzC+SRzy
NnWdkrIlazqpNA8P2DCWZXxqp3+v+2R64DbJWr4dBjjXiv758+OrYxoqBTDeQpUO+lMy3jkViBmD
/faxiG1HoRJx6mtgh/JGpynewxs1vOHTfutEV2J+VlN2klRMOfn9tXrrhowMUxn5oiiyXv28j5Kk
V5P6kBCBTRxZurXYFL2EhpV48OyTlWJ2mFbBbxuKTewvbSQ2lBMsLVX4YNaNFRGe1WGpK+ln5cGf
6+h0+/zq57ygX+j7Qu5BuIJU2246RjouHOPeSZsA6u8LxuJl6nGQRvZDDCduAaJnQsvF+yHqr6oo
rZq8p7VEFt8mU8OEMWYILinIUAra97kwjyoQINaJmgBkzgtK7cRroBt2fA63jO0CCly0eJgkVlhw
hwEiVkcPwafzzMSp4FDZM4FuproeTBQKDc+OAGyaB+a3x91RcFPRkVqGlsxiu/BmjNDDZNCyWxPK
j+ioCyX5SX0Wx8DuZRIJ2MNJXuLX6FpymQO7/x81wtGYhIPAdS8JnQrwYISlCi4BSEuqywfjAWdn
i/YM/8QoC0ZtQpFedmHV/vKPDYDbDs1OsdM9wAGyCTE10QyVvCCISa0gmw9WJpqaZ2JcS3pGcbzl
6CC9OOhuwnb6FtbFPPTbx969TbQfGJGSpsrUT9FEE8aGE8Gq6FbHkUJIrx0MJah3uB6v2mgcqpAb
LUFDOSBnlsmAaIF6dsSw1+e+jg/AUxliLTf3Fw+HP/O4vhfgfxZzkyqJBhYN8vj310wClc5S7nrJ
jwabC41HUzxLWBOT4PnKBMSHlcHhbKed1+YCgvtHYJadwV096kO2mzH7KpFPNzjiUFjQbVWOILZq
hk/Hea4aS0ZDDEagtMsLOCpH1qQ0DbAIIiWvUb22SBD/Zp188qvuNG3F2ghNTb4FCXRNAZmlfLGI
tSIhnIFsvfvNRc1M378O1Cf99jCj/v1lbT1fGE1AGQVxGd9M6C3yHfa0VXC6unrbLhhv/2gVCOQ2
Q6l1BSa+BAzZkjYtc/I5uqGNxHeTgOJnrRkz0twD3jnB/VFQqkfVk6gFEKXI5+BbXYxV8mIyRHIP
cN9GsjHdWvlurIFqdQjFFNYbWUN5o7cH1n/k90YYgf3me4aGynJ7CT8z51rwWuapFN8eZDY0mPGa
nFY+6OUkN6cEDOzbIB6uaz0dKKj6ejJ1jzroN1rcKEBUK8+5kGXJWKGFkZFMCl4Sc4FYbz/PQuST
2clWLmYbIFV46If106CwZK2ARWxvqJYJHeBomVyDCa74/YbF4UjmSlgLodlFxlEm9jJp+LSjKwsk
e+JJKlo8VHon7HDgNp5VRfFl7LFqrxI7slWETGSNKEOVWMlbYDndiGHvtHQAKW/YRoJVf5qW1ozr
rQuq2TG6n6Pf6hZ1o+muMZG+M0KArHzBNv9jCm5i6TumAHo5Kbv4wyUle+UiiF2lusF7zXe2Y+VG
TDHz0dtMXGIh3QsTNcwiMqxHntZW8rfhvSS164EDFIPakfciCkhwgZ9IyrWTqvIa8wJUmi2f+dti
FDQuYvKMzda5MQbYqJn+DBGrvlLyeYcf6uz10KO4Yq/TG4nx5KLPzQiz9zUebVmjWuT8gwsZnZIs
4lt7WoolDbxf/2UqhgZSI+nem74F+6uYuWiaHSkZma56+gSGkWSfCLy74uRPS3CUOsmRaJj35Mf2
KjJWMZ05u3c70KRHYUEwR35Vg1mm5kiG/WlIWICOShQAKrqN1ovCQ7NiHApVRF9SlyCKVISJSFRC
ItUmqI4K3H6xFf3L5dEgjPRN8cxHh6YiUy0inX2T8RDZCIjPI86rhsmayvUaal91VsNauIcbKQ34
Jo0fjBjiwFqzsBooa+FtaqkoAv4wvK+NWFwdr2PnQ/odF9tnTFLNpPqMKZSFupYOeEGw+q7TDjAB
FRxyFHTvQKf8yu84iIMVDEpurartolFma6KnLRjUbH3xcs+0PjRWsm77Stt0e20Cs6EaLVF3BDsu
lLDQmXMkCC7ZV6aJFyDEPPXvHVg3883v07/gcTeSQaXhpuzirqwFZXKTThA5CamsK8UQiyDDJIp4
BuHFB86x9XrPxouq+ejkzpwRr1iVEpgPKAbU/4QQmQ4EqEPi94ZXDIORiMur3ubRkgP+RwWIwDEZ
BKRhCO9Xl9oXqy/25FhDDqGntOThkVGBxh3nv7K2PLXdBodF/Iu/rJYmomRz5AEQaGhlAuYyiPau
yLlht8YV5qfem8kbw3TWy7OoAIBTUB+f53jIamduTkrer8Tu4FIdFgjdJzJiZuiI+wjlgEP76Zm7
Qf/XcxGGhJXz7q12Ue3jjhLfYEhBt97Zxt6vvMTboq9ryWB339wKpt+2sZa/gV1xEm8qyxcV72QA
W0yGn/Ob5pXwdnkoIlgdxjNr2JKZRqDPEKgRY/cwoQ3IHSlVySmuS8mNJNpsfPr+UgkqyUeulB6+
NItGnpMesydcNi7ko2mMdfTZcgWC4QnKEa56ZsEiT1kyGP6hhgDsIofhWs8r/DEBa0HhOHwj5Z+m
SykGbFpUH5Ry9Y5haINOSlCDEdyvGVrrrfz0COW9zvuX30sfKC+ly+L+wRxcizm6KEH0j6BaP7+q
7sIIlAMlu/HZiO7LYiDWJcp7w/CIN5tHH/ryzJhFkeN1I40aVfRQTUo1oZHOvaSVGZHfDrvReFnT
EE/NM5S6T3eblIG+MzrV1hP2IH1dOp/pluqPyTaAfTdn37Uh1JhMPzgjBET8pWKR93bDytMcDEVH
Rok8VxtPovopfYIO22Od1/bN7++vRzT0nHaKNo9o5qTll/taotRCeT0I/EkKMtAIKeUZS9lMSdjx
ZLq08r/+CKp9VKuse3mGYljYKbJyCcRJJb06pmhTQqRe4OX5qi2cDZaOBgc57nYfEpxUnKNXMiuw
aQdDlQBZopQwyrzQzAm0EawbCRhR7l6MPlkfCZWcvFjtdfgdLD4foAPmTi9+9gW5+A3HY3iFdrTy
VMXP5gn4rXjqn/P037WkLrQ7K8E33d496o5VJvj44qk9uNUdcCSs7uWkYXryKdH58yYmyRnRedE7
kkFUfzztv2+W7HrE2PXnQWaSc3BGtKh28DrTgCXWnXLYuqO38uraUHkcQbVdXUcrPk9NNCPPGiPz
E+gqGunmJhn3j4ufaSdke7scT6qeRBad4a6x3ZzraDDPkrsjcYiLUfScVWQxxTnq9n5Xscgwl6U3
049fT12EgUOOAXVQwihX6pl0rgJAEBJfqRquKnQqzD/20m72Fe+fPvAXgur1fae3hZgaxoVpCcns
qI0wLlmn5lpTSI/UTWx/P+hPZRgA9hqzCNLYRM+IghPiREI+jLxga/ydHgtnndGNddRYC3bbtc0u
i+3oig8AY798XGXttnM4tVDhqERbaz58iQynMEAlzTOp5613pR5ou7epD/Edp4jg2XYoK6wo+bDB
6lymAYJ6rYgUMT2OsQO83lHmSZNORAck9o1fXDcl9wCCkNiFQBjrHo3k2CurTJK24WZSVyvZcsLm
Zvwz9rFpKQUL/BKAHSr7LNdRZDGV9K0Jx2YQFW3wpr56yARP4iRJxrg2gIkOQtN88RiZ2di0vHoL
4b+Fi5QQqQ/18lcFpsEbikqikkcXfV17QU4/M3PDmcaCkRBao+5jC8ed9O8LxjEvi9YLC1tM2Y8T
xQ32BN0qq39yfR5Xu7b1RFoCj1SM2euyE53A3BBAPmRGYauN5xZMn8XTOND+DDRjqLT2d0lSeDcc
MjFsULLXzEFFjOJNemapcH1YAA8OJgC8gR+8rsYrJd8h1w3lGXoGgJX17VJwAFoD034vc7BqilT7
FlwYaADx9Tr+vj60iKR4epjJI9dqpkmXemNEC4i9elqfUnlZWZYdG9hbjdIxVuV+cCOXXdsJCRtf
nuYC2rI7ow5rfSURl9S0hpn2E+bHCB5/R5Km98LjdTRWJxy13+wgfdfXTh72HMgC6v2dEEr1PBpU
hyZyyKURyftTzUd7m0wiLD3040JaxgBaW2A4SfRovqOgEkwR+xN8k3FMPPWRz60hPMkYGUzLwblV
nN1Gfi5G6wlXcmj7cgsfvFg4MowjhBEhVFz4WWtBHklTKK0G+09Iepw8nYk+T3KLPN3UW9gfFHoP
0Ny01hhbZcgQznvPaGq49OpT4uMUvC/9tKymCHuHiUFR4yXGGUCq/BbJzPJbh1q/QLXQmIUSHQcP
cv5rI66mbeSGvL443D1VFO5Q41F9ZxtOipEs3m1NDTgcS79uYr4cUNI6/ta7SpUuCO4YUBcuSxwA
07Ld1hSL26v6+xLyBhoJ2PfZm6nD6W9xUjocApn0sjSC5ZuLc7nZ75M5klO1QJBx43+mVIjhHoi5
5o3+7tJpbpdDfRBmm9kJFmgbFHGh6aup0enldCArh23jwzroqOmEaHlODJHwYDWWmOM4LDZ4pvKj
tgcoEFAiAFNrcPbAfoLy/BCiVu/1Bxizvei3u17WjvWeZDWxjLeqEBnDrWWQVr1X9Mz+nyPwPBhu
9k/y29m/+BNSrNFth1lMEhNft+n7nHuXUyiehFO+snEuSSvkWEZ0MrWArPOxjpic8PYWobUU7ULH
yDAWhtvjIbFUiCZNfinU1adpvgWHPoDfjFsMF8Os8TXWQQrUEhZmPyMZW0KE29PISJLWVQwPL0WV
Nleu0wDXL3hy5zk3q07AJ9bFtjz3jRZnOAcrbuLU0S0sB2Yk1R53yGtBFJydf8tyHfuiyt01wjfP
oZmrHkzJ1MUcunTNewH2qcSeHk+1JX1r885aeQ+mc1Eq8S0fet0ZzklMwp3WBKpe8s7Cm6B+IDDP
om5DXNdMuumdRiBSLbK8qBgq4qS6CJMoShb5WeFhACBZo5oP/IakaornDf+W63Ecd9tSxKhSpqZd
ieaBTWRClxILkI1DIsfanudIiWyqN/FZUAyFJJUIDNfH8JShcTWchMhYgFKYiwI9EA76qdc32UOi
OR9rGtMeUU+QceFIVkOWI6/bzju4G/sV0ltSv+6HybIGA61zwQyddBu3ZVCvm7Oku7SAEiGlKcGD
nxFku4fdwFpU2HuA3g6IQ5i+wIWMx/yOu8EXf429jnt75LthXztLJvU1bTmhsFm9nBpGiwxf6ao9
s7EQseaA1q4//KCPkB6YSEJ4flbBm2TAfs0c1RIGF7g2nHfN8DT6upTYZGeNobyOJI7v8hzdQ33j
apGupeTJhv8pEBuT1hxJ1tgnGNpvuDycC2ZjUWyZvR7apMKMMRqE5ZxfE1GzoLvqfsU+sGdbz+6q
F+wWwESM2gdyypfpGJMCnmA86ljhXNnUx4ECvwenlaiYGZZN7TRiIwxV5aUgQ9Kw3gcmnWje3ydU
/yei3roEZ+JKTJcQ/lrbXKtTx9OVtY8tyKArdOziPMqnr2NvhGszzofuDPJRCfiaVqq1+uEtCXx1
IqXe/zEJZvxIYY+g12Pdt0YFQ/ZLQyOnJpGRxyozA809Sgt+LMxpO1HNSG9onTbpD4TnTriIWctv
cOdw+S6bTNQgnT2s/tvTXJqneUQYis2deAov2SBQQuzjGIsDEI7YV2Ewx5xc44V4i6KsVsT0a+fj
V8MTK46YN0YzdOsFfD7ts/K/kirhffcPRwvD0vUN+REwgZMpFcDQM+p7ab0E/6uMWewb6bzgQZ0b
1jnk7GLqpbHiiUqSX78T4XdYPTPhA3n+7329CcFLC82472o2SuoRCicmzWEwcmpue7p3VeCgk1T/
zzEL6qZ0kJyIV41Ff0a5vIgyq04WpKx3tQ4DzH5tdM4ezLvsHcn5YfYBzD6aevGZpDEC8wxKDvG4
PsSlJX5pkH3HA80ojDwVD6oZip0c0dSGlT6uAu6CeFfYMIv2vTDGhR7U5In12QcfgWP1tlb8dD1e
FpsIh0BH2oyz9BkuAPukKtNVBb8/VLGdquzYaq7xsNWGq31SBRc1yw9fNUeuKCC4vX4+skb69a+0
NZ+pSeEsF2dWfPMDhIJue+Brv0ZhlmNQERFR+LsiYge510/8QjP1EENwqTslFGoykV98+L4OhILz
7I78ngZ9VBFGUlfzP3Gyy0FYULxRBjrlKW6TaBIEa4CxOxdE7SywUhkwB4K8M1iFFVxCfD0oPw6V
ksjyg3JkmvQPByghZByy+nZocfGgYx7YOJSVKmb40iJdqTio4tWzpCgOjs4mFnEY1upMbHkF4OX8
EQDbk4mA4yVdypml4falqxJBR9ohWDaaH68PqaB4NO3dqRbr+a3QPXjiMIDd8HpuTjQxgQNjWt32
rZU86d22pOxm8iUI6kp5do6pV9U+uKREVDKnYSSEWRZYQHv1foZfcctbGq/Zt4yuMTV8ln3tnW1W
Ls6szvaxNtvPoJ1fmVKQeMorrBcaxP7p2Mcc20O2vPl+iImi2cW36ignx9tbhHCg0MjltWzDT4Xa
AVuBB1vkqrAfxVEjAjy8b/zjMXWX3Kqr+KkAvW7UoADhc1tVr9m+0cdJs+L2J5EW3BLXzTBrBNvF
DYu+vUP++gHp8F0ZQK1IOcDYYhUq0J6eXYRht5UYzen+JIeJkm8h2wyabOXELIQvO2GhGaJxQuCV
D8ISLqWLGtM0VaL4i20Jw+YaECOjjgtJ7pE+D8j52DXQmTIWDuLhfeowsDA69GWEfWjv+sypZtnv
lJbkaChC2bR4wddy97mPeTRadpkrnBxasDIoOYR1HJcNCLo6XbgYI9ZfBSJQDQ77JCqmy+7O5HKn
z+/G+EbVG4RD8ZMl7k7ZDY+VWWOybkTvF71PQOqvEUXl7bluTARHTx/FwMGZwu/BoogL9Cuuj0Hk
Er83XoRhvhpA5f1o++NzodoFM90lsp7Tl+cFsy6tFeMD0ejytLL3rc2HQ+hpDdxWhLoLsehBNouP
YTK9EQL55QMYzWua2iz0OTY7lZoKlz0Ld2OwerortYJfrLV9XqNCMrOauDp2/vX6s5Q/5SpSJ7mr
SN11DwDWTRkEkUSw+R7ogzU6tosqoP8Wwsr7A9QV7kZYpqAzf9rc2+Eft/avN2BE8EKgtdEWu1id
WadIFee2tCVO+5muhLOSlrBQMxjecJ8t6+6qtGl4r+oy1OeCw+ocn3M8B/fbER91ANm+5QwcmqJG
INCP8vuQPyhzaJ4FxtiVom5yr87mlakeaegKfE4MerFbvwfRQGP/Kpe705FoXWE+2SnPsr36hs5C
tzZp04SX59Y3Qa5HaC2qmv3x+5wh99cSxeeo68uIRBRSB3XTfLO5xmwUGDPmdNXw0DcGP7G3HhTZ
/iLK561HUZ2nS/s9I5xo1lFnyhrX0D2IqTQ8RcZto2ZHLP3jKbpbesC9gWwqcbqH0WkNcpO5FbLc
3qXIohZud/bR3BOZlR1310hAKZu2NzVF/j85tjzXaf3rrCCHcGb3opg0ulH9PtYPnQVsaj6nB+6/
e/1ky/yL5lzbWStTsflX/pQbs/CVzyGmmCiYHxxVXWhxNVt/EsdaIEP+G5xzj4oVBBpNP+pgvPMR
1MI+CU2lreuEDcMH1aehv2yhOFzJ5SNJCsAr/rxi18B3AkSwXLMpzJMEt/dll7msNHh/3WXQMRO3
Fl1u9hqKybrDnjYOnrjNuVrFmnzAUzJlWyD7guznXgpoBFdvAPCjTULeac+Wf4r02hAsD6q10Gf0
WVYGCKlnYNwG9KeuDwb+Fc9l7JbHBLGgpYrqLx03BUVVqvOYOQ33wanTdQ+PD4Dczqw9qAmWu7dB
gamhpG+E6EcVdZo5cpup9WLierKThXHCZPwLOuNSrIAY04owh8Na5J6BE/lOEKQTwIBb8JMuIxoH
FTN74zFkZuMcbh/SdLFc5NjL0AcdPw7fm0iYvKioPvvJT+VgG8MAwnTcLkH1tXJkKg7KZ9AcyBds
SXz2XlxAmice5FS4ogJ7QuZK7Hyrvu4aQd2NhCGDDLbwxrm68mALgvImDwUrSCVGHKyd02wbObJS
W6G2l4Y0IPH/7/wN/xlAYt5bJHBNMDA1Pvl9mrNmsaAK6BdmTuQ2gl7BrnyVzToTdyD72BQXM0ud
yQCFpidhCD2Xmlk7QFakQN6IIjJfaY1s4Eg6aeSZ7H1ivAFFIaHvA6TY9RJppavxKJ4TjuCaw1Fm
7tJyyQwpBKyUHofZnGKaRO+o/fnm0eFCZQt1S3JAa4rIQz6WgtzrxWomLSOCHUtKlI7O2TY5hVZx
c4hfGNxAyZQkmnV/XsMcRZaZEdmWl5TGJR9CxGUlhJZ2El2F2RiQ4oSRGUA/flvuh4IS5eNc9NQD
dlcVXgOjhzWgtYBHsruyyX737k35
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
