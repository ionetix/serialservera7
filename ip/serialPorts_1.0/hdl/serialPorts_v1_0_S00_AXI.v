
`timescale 1 ns / 1 ps

	module serialPorts_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 8
	)
	(
		// Users to add ports here
		input wire RX_PLC,
        input wire [15:0] RX,
        output wire [15:0] TX,
        output wire LEDS_CLK,
        output wire LEDS_DATA,
        output wire LEDS_LATCH,
		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 5;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 64
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation and axi_awaddr latching
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if (S_AXI_ARESETN && ~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	    begin
	      axi_awready <= 1'b1;
	      axi_awaddr <= S_AXI_AWADDR;
	      axi_wready <= 1'b1;
	    end
	  else
	    begin
	      axi_awready <= 1'b0;
	      axi_awaddr <= 0;
	      axi_wready <= 1'b0;
	    end
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= reg_data_out;     // register read data
	        end   
	    end
	end    

	// Add user logic here

  reg [16:0] rx_sync = 17'h1FFFF;            // synchronous version of RX input
  reg [15:0] rd = 16'h0;                    // RX FIFO read signal
  reg [15:0] preRd = 16'h0;                 // RX FIFO read signal to prevent duplicate reads from a single command
  reg [15:0] wr = 16'h0;                    // RX FIFO write signal
  reg [15:0] clkDivider[15:0];              // clock divider defaults to 115.2kbaud with a 100MHz clock
  reg [1:0] parityCfg[15:0];                // default to no parity
  reg [15:0] sevenBits = 0;                 // set to 1 for 7 data bits instead of 8
  reg [15:0] reset = 16'hFFFF;              // start in reset after power-up
  reg [16:0] okLed = 0;                     // OK LED status
  reg [16:0] failLed = 0;                   // Fail LED status
  wire [16:0] actyLed;                      // Activity LED status
  wire [7:0] rxData[15:0];                  // data in RX FIFO
  wire [15:0] rxFifoEmpty;                  // RX FIFO empty status
  wire [15:0] rxFifoFull;                   // RX FIFO full status
  wire [15:0] txFifoEmpty;                  // TX FIFO empty status
  wire [15:0] txFifoFull;                   // TX FIFO full status
  wire [16:0] t1RX;                         // Min delay between transmissions (3.5 chars for 19.2kb and slower, 1.75ms otherwise)
  wire [16:0] t2RX;                         // Timeout bit if there is no response after 1s
  wire [15:0] t1TX;                         // Min delay between transmissions (3.5 chars for 19.2kb and slower, 1.75ms otherwise)
  wire [15:0] t2TX;                         // Timeout bit if there is no response after 1s

  initial
  begin
    clkDivider[0] = 868;
    clkDivider[1] = 868;
    clkDivider[2] = 868;
    clkDivider[3] = 868;
    clkDivider[4] = 868;
    clkDivider[5] = 868;
    clkDivider[6] = 868;
    clkDivider[7] = 868;
    clkDivider[8] = 868;
    clkDivider[9] = 868;
    clkDivider[10] = 868;
    clkDivider[11] = 868;
    clkDivider[12] = 868;
    clkDivider[13] = 868;
    clkDivider[14] = 868;
    clkDivider[15] = 868;
    parityCfg[0] = 0;
    parityCfg[1] = 0;
    parityCfg[2] = 0;
    parityCfg[3] = 0;
    parityCfg[4] = 0;
    parityCfg[5] = 0;
    parityCfg[6] = 0;
    parityCfg[7] = 0;
    parityCfg[8] = 0;
    parityCfg[9] = 0;
    parityCfg[10] = 0;
    parityCfg[11] = 0;
    parityCfg[12] = 0;
    parityCfg[13] = 0;
    parityCfg[14] = 0;
    parityCfg[15] = 0;
  end

	// AXI Bus memory map
	// 0: configuration register (r/w)
	// 1: status register (r/w - LED bits, read only - others)
	// 2: RX FIFO read register - read only (returns RX FIFO data and removes from FIFO)
	// 3: TX FIFO write register - write only (read returns RX FIFO data)

    always @*
    begin
        case (axi_araddr[3:2])
          2'h0: reg_data_out <= {reset[axi_araddr[8:4]], 12'h0, sevenBits[axi_araddr[8:4]],
            parityCfg[axi_araddr[8:4]], clkDivider[axi_araddr[8:4]]};
          2'h1: reg_data_out <= {22'h0, t2TX[axi_araddr[8:4]], t1TX[axi_araddr[8:4]], t2RX[axi_araddr[8:4]], t1RX[axi_araddr[8:4]],
            failLed[axi_araddr[8:4]], okLed[axi_araddr[8:4]],
            txFifoFull[axi_araddr[8:4]], txFifoEmpty[axi_araddr[8:4]],
            rxFifoFull[axi_araddr[8:4]], rxFifoEmpty[axi_araddr[8:4]]};
          default: reg_data_out <= {23'h0, rxData[axi_araddr[8:4]]};
        endcase
    end

	always @ (posedge S_AXI_ACLK)
	begin
		rx_sync <= {RX_PLC, RX};

    if (slv_reg_wren)
    begin
      case (axi_awaddr[8:2])
        7'h00: {reset[0], sevenBits[0], parityCfg[0], clkDivider[0]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h01: {failLed[0], okLed[0]} <= S_AXI_WDATA[5:4];
        7'h04: {reset[1], sevenBits[1], parityCfg[1], clkDivider[1]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h05: {failLed[1], okLed[1]} <= S_AXI_WDATA[5:4];
        7'h08: {reset[2], sevenBits[2], parityCfg[2], clkDivider[2]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h09: {failLed[2], okLed[2]} <= S_AXI_WDATA[5:4];
        7'h0C: {reset[3], sevenBits[3], parityCfg[3], clkDivider[3]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h0D: {failLed[3], okLed[3]} <= S_AXI_WDATA[5:4];
        7'h10: {reset[4], sevenBits[4], parityCfg[4], clkDivider[4]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h11: {failLed[4], okLed[4]} <= S_AXI_WDATA[5:4];
        7'h14: {reset[5], sevenBits[5], parityCfg[5], clkDivider[5]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h15: {failLed[5], okLed[5]} <= S_AXI_WDATA[5:4];
        7'h18: {reset[6], sevenBits[6], parityCfg[6], clkDivider[6]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h19: {failLed[6], okLed[6]} <= S_AXI_WDATA[5:4];
        7'h1C: {reset[7], sevenBits[7], parityCfg[7], clkDivider[7]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h1D: {failLed[7], okLed[7]} <= S_AXI_WDATA[5:4];
        7'h20: {reset[8], sevenBits[8], parityCfg[8], clkDivider[8]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h21: {failLed[8], okLed[8]} <= S_AXI_WDATA[5:4];
        7'h24: {reset[9], sevenBits[9], parityCfg[9], clkDivider[9]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h25: {failLed[9], okLed[9]} <= S_AXI_WDATA[5:4];
        7'h28: {reset[10], sevenBits[10], parityCfg[10], clkDivider[10]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h29: {failLed[10], okLed[10]} <= S_AXI_WDATA[5:4];
        7'h2C: {reset[11], sevenBits[11], parityCfg[11], clkDivider[11]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h2D: {failLed[11], okLed[11]} <= S_AXI_WDATA[5:4];
        7'h30: {reset[12], sevenBits[12], parityCfg[12], clkDivider[12]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h31: {failLed[12], okLed[12]} <= S_AXI_WDATA[5:4];
        7'h34: {reset[13], sevenBits[13], parityCfg[13], clkDivider[13]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h35: {failLed[13], okLed[13]} <= S_AXI_WDATA[5:4];
        7'h38: {reset[14], sevenBits[14], parityCfg[14], clkDivider[14]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h39: {failLed[14], okLed[14]} <= S_AXI_WDATA[5:4];
        7'h3C: {reset[15], sevenBits[15], parityCfg[15], clkDivider[15]} <= {S_AXI_WDATA[31], S_AXI_WDATA[18:0]};
        7'h3D: {failLed[15], okLed[15]} <= S_AXI_WDATA[5:4];
		7'h41: {failLed[16], okLed[16]} <= S_AXI_WDATA[5:4];
      endcase
    end
    wr[0] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h03);
    wr[1] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h07);
    wr[2] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h0B);
    wr[3] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h0F);
    wr[4] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h13);
    wr[5] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h17);
    wr[6] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h1B);
    wr[7] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h1F);
    wr[8] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h23);
    wr[9] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h27);
    wr[10] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h2B);
    wr[11] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h2F);
    wr[12] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h33);
    wr[13] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h37);
    wr[14] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h3B);
    wr[15] <= slv_reg_wren & (axi_awaddr[8:2] == 7'h3F);
    preRd[0] <= slv_reg_rden & {axi_araddr[8:2] == 7'h02};
    preRd[1] <= slv_reg_rden & {axi_araddr[8:2] == 7'h06};
    preRd[2] <= slv_reg_rden & {axi_araddr[8:2] == 7'h0A};
    preRd[3] <= slv_reg_rden & {axi_araddr[8:2] == 7'h0E};
    preRd[4] <= slv_reg_rden & {axi_araddr[8:2] == 7'h12};
    preRd[5] <= slv_reg_rden & {axi_araddr[8:2] == 7'h16};
    preRd[6] <= slv_reg_rden & {axi_araddr[8:2] == 7'h1A};
    preRd[7] <= slv_reg_rden & {axi_araddr[8:2] == 7'h1E};
    preRd[8] <= slv_reg_rden & {axi_araddr[8:2] == 7'h22};
    preRd[9] <= slv_reg_rden & {axi_araddr[8:2] == 7'h26};
    preRd[10] <= slv_reg_rden & {axi_araddr[8:2] == 7'h2A};
    preRd[11] <= slv_reg_rden & {axi_araddr[8:2] == 7'h2E};
    preRd[12] <= slv_reg_rden & {axi_araddr[8:2] == 7'h32};
    preRd[13] <= slv_reg_rden & {axi_araddr[8:2] == 7'h36};
    preRd[14] <= slv_reg_rden & {axi_araddr[8:2] == 7'h3A};
    preRd[15] <= slv_reg_rden & {axi_araddr[8:2] == 7'h3E};
    rd[0] <= preRd[0] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h02));
    rd[1] <= preRd[1] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h06));
    rd[2] <= preRd[2] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h0A));
    rd[3] <= preRd[3] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h0E));
    rd[4] <= preRd[4] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h12));
    rd[5] <= preRd[5] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h16));
    rd[6] <= preRd[6] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h1A));
    rd[7] <= preRd[7] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h1E));
    rd[8] <= preRd[8] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h22));
    rd[9] <= preRd[9] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h26));
    rd[10] <= preRd[10] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h2A));
    rd[11] <= preRd[11] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h2E));
    rd[12] <= preRd[12] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h32));
    rd[13] <= preRd[13] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h36));
    rd[14] <= preRd[14] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h3A));
    rd[15] <= preRd[15] & (~slv_reg_rden | ~(axi_araddr[8:2] == 7'h3E));
	end

  ledDriver ledDriver(.clk(S_AXI_ACLK), .acty(actyLed), .ok(okLed),
    .fail(failLed), .ledClk(LEDS_CLK), .ledLatch(LEDS_LATCH), .ledData(LEDS_DATA));
//  ila_0 ila_0_inst (.clk(S_AXI_ACLK), .probe0(actyLed[16:8]), .probe1(okLed[16:8]), .probe2(failLed[16:8]));

  genvar x;
  generate
    for (x=0; x < 16; x=x+1)
    begin
//      if (x!=8)
//      begin
      rs232rx rs232rx(.clk(S_AXI_ACLK), .clkDiv(clkDivider[x]), .sevenBits(sevenBits[x]),
        .parity(parityCfg[x]), .reset(reset[x]), .rd(rd[x]), .rx(rx_sync[x]), .data(rxData[x]),
        .fifoEmpty(rxFifoEmpty[x]), .fifoFull(rxFifoFull[x]));
//      end
      rs232tx rs232tx(.clk(S_AXI_ACLK), .clkDiv(clkDivider[x]), .sevenBits(sevenBits[x]),
        .parity(parityCfg[x]), .reset(reset[x]), .data(S_AXI_WDATA[7:0]), .wr(wr[x]), .tx(TX[x]),
        .fifoEmpty(txFifoEmpty[x]), .fifoFull(txFifoFull[x]));
      actyCtr actyCtr(.clk(S_AXI_ACLK), .rx(rx_sync[x]), .acty(actyLed[x]));
      timeout timeoutRX(.clk(S_AXI_ACLK), .clkDiv(clkDivider[x]), .reset(~rx_sync[x]), .t1(t1RX[x]), .t2(t2RX[x]));
      timeout timeoutTX(.clk(S_AXI_ACLK), .clkDiv(clkDivider[x]), .reset(~TX[x]), .t1(t1TX[x]), .t2(t2TX[x]));
    end
  endgenerate
//  rs232rxILA rs232rxILA_inst(.clk(S_AXI_ACLK), .clkDiv(clkDivider[8]), .sevenBits(sevenBits[8]),
//        .parity(parityCfg[8]), .reset(reset[8]), .rd(rd[8]), .rx(rx_sync[8]), .data(rxData[8]),
//        .fifoEmpty(rxFifoEmpty[8]), .fifoFull(rxFifoFull[8]));
  actyCtr actyCtrPLC(.clk(S_AXI_ACLK), .rx(rx_sync[16]), .acty(actyLed[16]));
  timeout timeoutPLC(.clk(S_AXI_ACLK), .clkDiv(868), .reset(~rx_sync[16]), .t1(t1RX[16]), .t2(t2RX[16]));

	// User logic ends

	endmodule
